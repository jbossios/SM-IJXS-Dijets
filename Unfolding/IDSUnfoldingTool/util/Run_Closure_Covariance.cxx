#include "IDSUnfoldingTool/IDSUnfolding.h"

#include <math.h>
#include "TCanvas.h"
#include "TFile.h"
#include "TF1.h"
#include "TH1D.h"
#include "TH2D.h"
#include "TLegend.h"
#include "TLine.h"
#include "TList.h"
#include "TRandom3.h"
#include "TSVDUnfold.h"
#include "TMatrixD.h"
#include "TVectorD.h"
#include "TEnv.h"
#include "TObjArray.h"
#include "TObjString.h"
#include <stdlib.h>
#include "../../AtlasStyle/AtlasStyle.C"

#include "BootstrapGenerator/TH2DBootstrap.h"
#include "BootstrapGenerator/TH1DBootstrap.h"

using namespace std;

typedef std::vector<double> VecD;
typedef std::vector<TString> StrV;
typedef TString Str;
typedef unsigned int uint;

// Functions
void FillSpectra();
void NominalUnfold();
void ClosureTest();
void CovarianceMatrix();
void RelativeUncertainty();
void PurityEff();
void CheckNIterations();

Int_t gNIterations = 1;
Int_t gNBins;

std::vector<double> gBinLimits;

bool Unfolding, Bootstrap, Closure, m_Debug, JES, Covariance, purityEff, runDijet; 

int ybin, jsyst;

TString PATH_local, PATH_1, PATH_2, histTMname, histTMBname, histReconame, histRecoBname, histTruthname, histTruthBname, histDataname, histDataBname;
TString local_folder, input_mc, input_data, dijetN;

Double_t gMassMin;
Double_t gMassMax;
Double_t gMinMatrixBin = 6.;

//----------------------------
// Histograms for Closure Test
//----------------------------
// Inputs
TH2D* gMigMatrix = NULL; // x-axis --> reco, y-axis --> true
TH1D* gReco = NULL;
TH1D* gTrue = NULL;
TH1D* gData = NULL;
// Outputs
TH1D* gIDSUData = NULL; // Unfolded Data (IDS)
TH1D* gSVDUData = NULL; // Unfolded Data (SVD)
TH1D* gRWTrue = NULL; // re-weighted Truth
TH1D* gRWReco = NULL; // re-weighted Reco
TH1D* gToyReco = NULL; // re-weighted Reco
TRandom3 gRand;

//--------------------------
// Bootstraps - Nominal case
//--------------------------
TH2DBootstrap* gBMigMatrix = NULL;
TH1DBootstrap* gBReco = NULL;
TH1DBootstrap* gBTrue = NULL;
TH1DBootstrap* gBData = NULL;

//--------------------------
// Bootstraps - JES case
//--------------------------
TH2DBootstrap* gBJESMigMatrix = NULL;
TH1DBootstrap* gBJESReco = NULL;
TH1DBootstrap* gBJESTrue = NULL;
TH1DBootstrap* gBJESData = NULL;

static void Abort(Str msg) { printf("\nERROR:\n\n   %s\n\n",msg.Data()); }

StrV vectorize(Str str){
  StrV result; TObjArray *strings = str.Tokenize(" ");
  if (strings->GetEntries()==0) return result;
  TIter istr(strings);
  while (TObjString* os=(TObjString*)istr())
    result.push_back(os->GetString());
  return result;
}

VecD vectorizeD(Str str) {
  VecD result; StrV vecS = vectorize(str);
  for (uint i=0;i<vecS.size();++i)
    result.push_back(atof(vecS[i]));
  return result;
}

void ReadInSettings(TEnv *settings) {

  Unfolding = settings->GetValue("Unfolding",true);
  Bootstrap = settings->GetValue("Bootstrap",false);
  Closure = settings->GetValue("Closure",false);
  m_Debug = settings->GetValue("m_Debug",false);
  JES = settings->GetValue("JES",false);
  Covariance = settings->GetValue("Covariance",false);
  purityEff = settings->GetValue("purityEff",false);

  //  runDijet = settings->GetValue("runDijet",false);

  //  ybin = settings->GetValue("ybin",0);

  if (!runDijet) {
    histTMname  = "h_pt_TM_";
    histTMname += ybin;

    histTMBname  = "boot_inc_";
    histTMBname += ybin;

    histRecoBname  = "boot_inc_reco_";
    histRecoBname += ybin;

    histReconame  = "h_pt_with_wgt_";
    histReconame += ybin;

    histTruthname  = "h_pt_truth_";
    histTruthname += ybin;

    histTruthBname  = "boot_inc_truth_";
    histTruthBname += ybin;

    histDataname  = "h_pt_with_wgt_";
    histDataname += ybin;

    histDataBname  = "boot_inc_reco_";
    histDataBname += ybin;

  }
  else {
    histTMname  = "h_m12_TM_";
    histTMname += ybin;

    histTMBname  = "boot_m12_";
    histTMBname += ybin;

    histRecoBname  = "boot_m12_reco_";
    histRecoBname += ybin;

    histReconame  = "h_m12_reco_";
    histReconame += ybin;

    histTruthname  = "h_m12_truth_";
    histTruthname += ybin;

    histTruthBname  = "boot_m12_truth_";
    histTruthBname += ybin;

    histDataname  = "h_m12_reco_";
    histDataname += ybin;

    histDataBname  = "boot_m12_reco_";
    histDataBname += ybin;
  }

  PATH_local = settings->GetValue("PATH_local","");
  PATH_1 = settings->GetValue("PATH_1","");
  PATH_2 = settings->GetValue("PATH_2","");

  if (!runDijet) {
    gBinLimits = vectorizeD(settings->GetValue("gBinLimits_inclusive",""));
  }
  else {
    gBinLimits = vectorizeD(settings->GetValue("gBinLimits_dijet",""));
  }
}

//-----
// Main
//-----
int main(int argc, char **argv)
{
  TString config("");

  for (int i=1;i<argc;++i) {
    if (TString(argv[i]).Contains(".config")){
      config=argv[i];
    }
    //if (TString(argv[i]).Contains("nominal")){
    if (TString(argv[i]).Contains("MC")){
      input_mc = argv[i];
      cout<<"inputMC: "<<input_mc<<endl;
    }

    if (TString(argv[i]).Contains("Data")){ //FIXME
      input_data = argv[i];
      cout<<"inputData: "<<input_data<<endl;
    }

    jsyst = atoi(argv[4]);

    if (TString(argv[5]).Contains("mjj")) {
      runDijet = true;
    }
    else if (TString(argv[5]).Contains("incl")) {
      runDijet = false;
    }

    dijetN = argv[5];
    ybin = atoi(argv[6]);
  }

  std::cout<<"jsyst: "<<jsyst<<std::endl;
  std::cout<<"runDijet: "<<runDijet<<std::endl;
  std::cout<<"ybin: "<<ybin<<std::endl;

  TEnv *settings = new TEnv();
  int status = settings->ReadFile(config.Data(),EEnvLevel(0));
  if ( status != 0 ) Abort(Form("Cannot read file %s",config.Data()));

  ReadInSettings(settings);

  local_folder = PATH_local;

  // Read in histograms from file
  FillSpectra();

  //   CheckNIterations();

  // Do unfolding and closure tests
  if (Unfolding) NominalUnfold();
  if(Closure) ClosureTest();

  //
  //   // Produce the covariance and correlation matrix
  if(Covariance) CovarianceMatrix();
  //
  //   // Relative uncertainty
  if(JES) RelativeUncertainty();
  //
  if(purityEff) PurityEff();

  return 0;
}

// --------------------------------
// Helper functions
// --------------------------------

void FillSpectra()
{
  //------------------------
  // Nominal - Without shift
  //------------------------
  TString filename = PATH_1;
  filename += input_mc;

  cout<<"filename: "<<filename<<endl;

  TFile* file = new TFile(filename,"READ"); // Nominal

  // x-axis --> reco  y-axis --> true

  //-------------------------------------------------------------
  // Nominal Migration Matrix and Nominal inclusive distributions
  //-------------------------------------------------------------

  TString data_filename = PATH_2;
  data_filename += input_data;
  //data_filename += input_mc;

  cout<<"data_filename: "<<data_filename<<endl;

  TFile* data_file = new TFile(data_filename,"READ"); // Data

  gData = (TH1D*) data_file->Get(histDataname.Data());

  // gData = gMigMatrix->ProjectionX();
  gData->SetNameTitle("data","data");

  /* 
  // Make Data distribution from Reco distribution
  // Define a re-weighted shape
  Int_t nbins = gData->GetNbinsX();
  for (Int_t i = 1; i <= nbins; ++i) {
  Double_t xpos = (gReco->GetBinCenter(i) - gReco->GetXaxis()->GetBinLowEdge(1)) / (gReco->GetXaxis()->GetBinLowEdge(nbins+1) - gReco->GetXaxis()->GetBinLowEdge(1));
  gData->SetBinContent(i, gReco->GetBinContent(i)*(0.95 + 2.00*xpos - 1.5*xpos*xpos));
  gData->SetBinError(i, gData->GetBinContent(i)*0.01*i);
  }

  // Creating output file with data spectra
  TString outputfile("data_spectra.root");
  TFile* tout = new TFile(outputfile,"recreate");
  gData->Write(); //Writing the histograms into output file
  tout->Close();

  gNBins = gData->GetNbinsX();
  */
  // ---------------
  // TH1/2DBootstrap 
  // ---------------

  cout<<"histTMname: "<<histTMname.Data()<<endl;
  cout<<"histReconame: "<<histReconame.Data()<<endl;
  cout<<"histTruthname: "<<histTruthname.Data()<<endl;
  gMigMatrix = (TH2D*)file->Get(histTMname.Data());
  gReco = (TH1D*)file->Get(histReconame.Data());
  gTrue = (TH1D*)file->Get(histTruthname.Data());

  if(Bootstrap){
    cout<<"histTMBname: "<<histTMBname.Data()<<endl;
    cout<<"histRecoBname: "<<histRecoBname.Data()<<endl;
    cout<<"histTruthBname: "<<histTruthBname.Data()<<endl;
    cout<<"histDataBname: "<<histDataBname.Data()<<endl;
    gBMigMatrix = (TH2DBootstrap*)file->Get(histTMBname.Data());
    gBReco = (TH1DBootstrap*)file->Get(histRecoBname.Data());
    gBTrue = (TH1DBootstrap*)file->Get(histTruthBname.Data());
    gBData = (TH1DBootstrap*)data_file->Get(histDataBname.Data());



    TH1D* gBReco_Nominal = (TH1D*)gBReco->GetNominal(); 
    gNBins = ((TH1D*)gBReco->GetNominal())->GetNbinsX();

    gMassMin = gBReco_Nominal->GetXaxis()->GetBinLowEdge(1)+1; //FIXME
    gMassMax = gBReco_Nominal->GetXaxis()->GetBinLowEdge(gNBins+1);
  }
  /*
  // Make Data distribution from Reco distribution (New)
  // Define a re-weighted shape
  //     TF1 pol2("mypol2", "0.95 + 2.0*(x-gMassMin)/(gMassMax-gMassMin) - 1.5*(x-gMassMin)/(gMassMax-gMassMin)*(x-gMassMin)/(gMassMax-gMassMin)", gMassMin, gMassMax); //FIXME hardcoded
  TF1 pol2("mypol2", "0.95 + (2.0*(x/14000)) - (1.5*(x/14000)*(x/14000))", 0, 14000); //FIXME hardcoded
  gBData->Multiply(&pol2);

  if(gNBins != 37+7 ){ //Temporary FIXME Hardcoded
  std::cout << "ERROR gNBins != 37+7" << std::endl;
  std::cout << "gNBins = " << gNBins << std::endl;
  }

  // Setting errors for data
  for (int bx = 1; bx <= gNBins; ++bx)
  {
  gBData->GetNominal()->SetBinError( bx, (gBData->GetNominal())->GetBinContent(bx)*0.01*bx );
  for (int i = 0; i < gBData->GetNReplica(); ++i) gBData->GetReplica(i)->SetBinError( bx, (gBData->GetReplica(i))->GetBinContent(bx)*0.01*bx );
  }
  */

  /*  
      if(JES){ // Only for shift distributions
      TString filenameJES = PATH;
      filenameJES += "/JES/AllJZs_Bootstrap.root";
      TFile* fileJES = new TFile(filenameJES,"READ"); // for JES Uncertainty

      gBJESMigMatrix = (TH2DBootstrap*)fileJES->Get("MigBootstrap");
      gBJESReco = (TH1DBootstrap*)fileJES->Get("RecoBootstrap"); // Inclusive
      gBJESTrue = (TH1DBootstrap*)fileJES->Get("TruthBootstrap");// Inclusive
      gBJESData = new TH1DBootstrap(*gBJESReco);

      // Make Data distribution from Reco distribution (New)
      // Define a re-weighted shape
      //     TF1 pol2("mypol2", "0.95 + 2.0*(x-gMassMin)/(gMassMax-gMassMin) - 1.5*(x-gMassMin)/(gMassMax-gMassMin)*(x-gMassMin)/(gMassMax-gMassMin)", gMassMin, gMassMax); //FIXME hardcoded
      TF1 pol2("mypol2", "0.95 + (2.0*(x/14000)) - (1.5*(x/14000)*(x/14000))", 0, 14000); //FIXME hardcoded
      gBJESData->Multiply(&pol2);

      // Setting errors for data
      for (int bx = 1; bx <= gNBins; ++bx)
      {
      gBJESData->GetNominal()->SetBinError( bx, (gBJESData->GetNominal())->GetBinContent(bx)*0.01*bx );
      for (int i = 0; i < gBJESData->GetNReplica(); ++i) gBJESData->GetReplica(i)->SetBinError( bx, (gBJESData->GetReplica(i))->GetBinContent(bx)*0.01*bx );
      }

      }//END: Only for JES
  */
  gRand.SetSeed(2015);
}

TH1D* CorrectEfficiency(const TH2D &mc_migmatrix, const TH1D &mc_reco, const TH1D &data)
{
  Int_t nbins = mc_migmatrix.GetNbinsX();

  Double_t *recomatch = new Double_t[nbins];

  for (Int_t i = 0; i < nbins; ++i) {
    recomatch[i] = 0.0;
    for (Int_t j = 0; j < nbins; ++j) {
      recomatch[i] += mc_migmatrix.GetBinContent(i+1, j+1);
    }
  }

  TH1D *data_matched = new TH1D(data);
  for (Int_t i = 0; i < nbins; ++i) {
    if (mc_reco.GetBinContent(i+1) > 0.0) {
      data_matched->SetBinContent(i+1, data.GetBinContent(i+1)*recomatch[i]/mc_reco.GetBinContent(i+1));
      data_matched->SetBinError(i+1, data.GetBinError(i+1)*recomatch[i]/mc_reco.GetBinContent(i+1));
    } else {
      data_matched->SetBinContent(i+1, 0.0);
      data_matched->SetBinError(i+1, 0.0);
    }
  }
  delete [] recomatch;
  return data_matched;
}

void DrawTH1D(const TH1D* histo, std::string name, Int_t Toy = -1)
{
  // Setting ATLAS Style
  SetAtlasStyle();
  TCanvas canvas("result_toys", "result_toys", 400, 30, 600, 600);

  TH1D Histo(*histo);

  gNBins = Histo.GetNbinsX();

  TString Histo_Name = name;
  if(Toy != -1){
    Histo_Name += "_";
    Histo_Name += Toy;
  }
  Histo.SetName(Histo_Name.Data());
  Histo.SetTitle(Histo_Name.Data());
  /*
    for(int i=0;i<gNBins;i++)
    {
    Histo.SetBinContent(i+1,histo);
    }
  */

  Histo.Draw();

  if (!runDijet) {
    Histo.GetXaxis()->SetTitle("p_{T} [GeV]"); //FIXME
    Histo.GetXaxis()->SetRangeUser(100,3137); //FIXME
  }
  else {
    Histo.GetXaxis()->SetTitle("m_{12} [GeV]"); //FIXME
    Histo.GetXaxis()->SetRangeUser(310,9066); //FIXME
  }

  // Histo.GetYaxis()->SetRangeUser(-0.4,0.4); //FIXME

  canvas.SetLogy();
  canvas.SetLogx();

  TString out = local_folder +  "Results_IDS/systematic_";
  out += jsyst;
  out += "/";
  out += dijetN;
  out += "/";
  out += "ybin_";
  out += ybin;
  out += "/";
  out += name;

  if(Toy != -1){
    out += "_";
    out += Toy;
  }
  out += ".eps";
  canvas.Print(out.Data(), "eps");

}

void DrawTH2D(const TH2D* histo, std::string name, Int_t Toy = -1)
{
  // Setting ATLAS Style
  SetAtlasStyle();
  gStyle->SetPalette(1); //Color Palette for colz

  TCanvas canvas("","",770,700);
  canvas.SetRightMargin(0.15);

  TH2D Histo(*histo);
  TString Histo_Name = name;
  if(Toy != -1){
    Histo_Name += "_";
    Histo_Name += Toy;
  }
  Histo.SetName(Histo_Name.Data());
  Histo.SetTitle(Histo_Name.Data());
  Histo.Draw("colz");

  if (!runDijet) {
    Histo.GetXaxis()->SetTitle("p_{T}^{reco} GeV");
    Histo.GetYaxis()->SetTitle("p_{T}^{true} GeV");
  }

  else {
    Histo.GetXaxis()->SetTitle("m_{jj}^{reco} GeV");
    Histo.GetYaxis()->SetTitle("m_{jj}^{true} GeV");
  }

  //FIXME
  //  Histo.GetXaxis()->SetRangeUser(318,2500);
  //  Histo.GetYaxis()->SetRangeUser(318,2500);

  canvas.SetLogy();
  canvas.SetLogx();
  canvas.SetLogz();

  TLegend *leg0 = new TLegend(0.2,0.8,0.4,0.9);
  leg0->SetBorderSize(0);
  leg0->SetTextFont(62);
  leg0->SetLineColor(1);
  leg0->SetLineStyle(1);
  leg0->SetLineWidth(2);
  leg0->SetFillStyle(0);

  //   TLegendEntry *entry=leg0->AddEntry("","0<|y|<0.5","");

  //   leg0->Draw();

  TString out = local_folder +  "Results_IDS/systematic_";
  out += jsyst;
  out += "/";
  out += dijetN;
  out += "/";
  out += "ybin_";
  out += ybin;
  out += "/";
  out += name;

  if(Toy != -1){
    out += "_";
    out += Toy;
  }
  out += ".eps";
  canvas.Print(out.Data(), "eps");

  // out += ".png";
  //canvas.Print(out.Data(), "png");

}

TH2D* ModifyMatrix(const TH2D &migmatrix, const TH1D &bias)
{
  TH1D *proj_true = migmatrix.ProjectionY("proj_true");
  TH2D *bias_migmatrix = new TH2D(migmatrix);

  Int_t nbins = migmatrix.GetNbinsX();
  for (Int_t by = 1; by <= nbins; ++by) {
    for (Int_t bx = 1; bx <= nbins; ++bx) {
      if (proj_true->GetBinContent(by) > 0)
	bias_migmatrix->SetBinContent(bx, by, migmatrix.GetBinContent(bx, by)*bias.GetBinContent(by)/proj_true->GetBinContent(by));
      else
	bias_migmatrix->SetBinContent(bx, by, 0);
    }
  }
  return bias_migmatrix;
}

TH2D* ModifyMatrix_reco(const TH2D &migmatrix, const TH1D &bias)
{
  TH1D *proj_reco = migmatrix.ProjectionX("proj_reco");
  TH2D *bias_migmatrix = new TH2D(migmatrix);

  Int_t nbins = migmatrix.GetNbinsX();
  for (Int_t by = 1; by <= nbins; ++by) {
    for (Int_t bx = 1; bx <= nbins; ++bx) {
      if (proj_reco->GetBinContent(by) > 0)
	bias_migmatrix->SetBinContent(bx, by, migmatrix.GetBinContent(bx, by)*bias.GetBinContent(by)/proj_reco->GetBinContent(by));
      else
	bias_migmatrix->SetBinContent(bx, by, 0);
    }
  }
  return bias_migmatrix;
}

TH2D* FluctuateMatrix(const TH2D &migmatrix)
{
  TH2D *toy_migmatrix = new TH2D(migmatrix);

  Int_t nbins = migmatrix.GetNbinsX();
  for (Int_t bx = 1; bx <= nbins; ++bx) {
    for (Int_t by = 1; by <= nbins; ++by) {
      Double_t mean  = migmatrix.GetBinContent(bx, by);
      Double_t sigma = migmatrix.GetBinError(bx, by);

      // Assume gaussian for weighted events, although Poisson is correct for non-weighted events
      // Need covariance matrix for inclusive jets, or better yet TH1DBootstraps (avoids throwing random numbers)
      Double_t toy = -1;
      while(toy<0) toy = gRand.Gaus(mean, sigma);

      toy_migmatrix->SetBinContent(bx, by, toy);
    }
  }
  //   std::cout<<"GetSeed: "<<gRand.GetSeed()<<std::endl;

  return toy_migmatrix;
}

// --------------------------------
// Plotting functions
// --------------------------------

void DrawSpectraRatioPlots(const TH1D &matchData, const TH1D &matchRecoBias,
			   const TH1D &nomatchReco, const TH1D &nomatchTrue, const TH1D &matchReco)
{
  // Temporary
  if(false) std::cout << "Inside DrawSpectraRatioPlots" << std::endl;

  float binNorm=10;

  // Make histograms from vectors
  TH1D matchData_H(matchData);
  TH1D matchRecoBias_H(matchRecoBias);
  //TH1D matchData_H2(matchData);

  TH1D nomatchReco_H(nomatchReco);
  TH1D nomatchTrue_H(nomatchTrue);
  TH1D matchReco_H(matchReco);

  //   int nbins = matchReco_H.GetNbinsX();
  //   int index = 0;

  //TF1 *f1 = new TF1("f1","[0] + [1]*(log(x)-log(92)) + [2]*(log(x)-log(92))^2 + [3]*(log(x)-log(92))^3 + [4]*(log(x)-log(92))^4+ [5]*(log(x)-log(92))^5 + [6]*(log(x)-log(92))^6 + [7]*(log(x)-log(92))^7 + [8]*(log(x)-log(92))^8", 100,894); //incl
  //TF1 *f1 = new TF1("f1","[0] + [1]*(log(x)-log(116)) + [2]*(log(x)-log(116))^2 + [3]*(log(x)-log(116))^3 + [4]*(log(x)-log(116))^4", 100,894); //incl

  TF1 *f1;

  //     if (ybin==0) {

  //      f1 = new TF1("f1","[0] + [1]*(log(x)-log(100)) + [2]*(log(x)-log(100))^2 + [3]*(log(x)-log(100))^3 + [4]*(log(x)-log(100))^4 + [5]*(log(x)-log(100))^5", 100,3937);
  //      f1->SetParameters(1.00e+00,2.03600e-01,-5.71656e-02,-5.06366e-02,3.21403e-02, -5.07156e-03); //y0 Pythia, x0 = 100, x^5, xMax=3937, binNorm=8
  //      binNorm=8;
  //  }
  //  else if (ybin==1) {
  //      
  //      f1 = new TF1("f1","[0] + [1]*(log(x)-log(100)) + [2]*(log(x)-log(100))^2 + [3]*(log(x)-log(100))^3 + [4]*(log(x)-log(100))^4 + [5]*(log(x)-log(100))^5", 100,3937);
  //      f1->SetParameters(1.00e+00,1.12144e-01,1.29691e-01,-1.80572e-01,6.82430e-02,-7.96162e-03); //y1 Pythia, x0 = 100, x^5, xMax=3937, binNorm=8
  //      binNorm=8;
  //  }
  //  else if (ybin==2) {
  //       f1 = new TF1("f1","[0] + [1]*(log(x)-log(100)) + [2]*(log(x)-log(100))^2 + [3]*(log(x)-log(100))^3 + [4]*(log(x)-log(100))^4 + [5]*(log(x)-log(100))^5", 100,2500);
  //       f1->SetParameters(1.00e+00,1.12752e-01,1.96142e-01,-3.42088e-01,1.72448e-01,-2.83322e-02); //y2 Pythia, x0 = 100, x^5, xMax=2500, binNorm=8
  //       binNorm=8;
  //  }
  //  else if (ybin==3) {

  //       f1 = new TF1("f1","[0] + [1]*(log(x)-log(92)) + [2]*(log(x)-log(92))^2 + [3]*(log(x)-log(92))^3 + [4]*(log(x)-log(92))^4 + [5]*(log(x)-log(92))^5 + [6]*(log(x)-log(92))^6", 100,1992);
  //       f1->SetParameters(1.00e+00,-3.34537e-01,2.05897e+00,-3.16620e+00,2.15278e+00,-6.68386e-01,7.66681e-02); //y3 Pythia, x0=92, x^6, xMax=1992, binNorm=9
  //       binNorm=9;
  //  }
  //  else if (ybin==4) {
  //       f1 = new TF1("f1","[0] + [1]*(log(x)-log(116)) + [2]*(log(x)-log(116))^2 + [3]*(log(x)-log(116))^3 + [4]*(log(x)-log(116))^4", 100,1162);
  //       f1->SetParameters(1.00e+00,3.58666e-02,8.16764e-02,4.85436e-02,-4.80573e-02); //y4 Pythia, x0 = 116, x^4, binNorm=8, xMax = 1162
  //       binNorm=8;
  //  }
  //  else if (ybin==5) {
  //       f1 = new TF1("f1","[0] + [1]*(log(x)-log(116)) + [2]*(log(x)-log(116))^2 + [3]*(log(x)-log(116))^3 + [4]*(log(x)-log(116))^4", 100,894);
  //       f1->SetParameters(1.00e+00,-1.14849e-01,4.90660e-01,-4.06802e-01,5.29130e-02); //y5 Pythia, x0 = 116, x^4, binNorm=10, xMax=894
  //       binNorm=10;
  //  }

  //*********************mjj********************************

  //   if (ybin==0) {
  //
  //          f1 = new TF1("f1","[0] + [1]*(log(x)-log(210)) + [2]*(log(x)-log(210))^2 + [3]*(log(x)-log(210))^3 + [4]*(log(x)-log(210))^4", 210,9066);
  //          f1->SetParameters(9.16408e-01,2.11654e-01,-1.22988e-01,4.11294e-02,-6.33751e-03); //y0 Pythia, x0 = 210, x^4, xMax=9066, binNorm=8
  //        binNorm=8;
  //   }


  //    if (ybin==1) {

  //     f1 = new TF1("f1","[0] + [1]*(log(x)-log(210)) + [2]*(log(x)-log(210))^2 + [3]*(log(x)-log(210))^3 + [4]*(log(x)-log(210))^4", 210,9066);
  //  f1->SetParameters(9.61974e-01,8.85077e-02,3.85540e-02,-4.46868e-02,7.90482e-03); //y0 Pythia, x0 = 210, x^4, xMax=9066, binNorm=8 
  //   binNorm=8;
  // }


  //    if (ybin==1) {
  //      f1 = new TF1("f1","[0] + [1]*(log(x)-log(210)) + [2]*(log(x)-log(210))^2 + [3]*(log(x)-log(210))^3 + [4]*(log(x)-log(210))^4", 210,9066);
  //  f1->SetParameters(9.61974e-01,8.85077e-02,3.85540e-02,-4.46868e-02,7.90482e-03); //y0 Pythia, x0 = 210, x^4, xMax=9066, binNorm=8 
  //   binNorm=8;
  // }

  //    if (ybin==2) {
  //   f1 = new TF1("f1","[0] + [1]*(log(x)-log(210)) + [2]*(log(x)-log(210))^2 + [3]*(log(x)-log(210))^3 + [4]*(log(x)-log(210))^4", 210,9066);
  // f1->SetParameters(1.02338e+00,-1.19295e-01,2.07378e-01,-1.05188e-01,1.48284e-02); //y0 Pythia, x0 = 210, x^4, xMax=9066, binNorm=8
  //   binNorm=8;
  // }

  //    if (ybin==3) {
  //        f1 = new TF1("f1","[0] + [1]*(log(x)-log(510)) + [2]*(log(x)-log(510))^2 + [3]*(log(x)-log(510))^3 + [4]*(log(x)-log(510))^4", 510,9066);
  // f1->SetParameters(1.02338e+00,-1.19295e-01,2.07378e-01,-1.05188e-01,1.48284e-02); //y0 Pythia, x0 = 210, x^4, xMax=9066, binNorm=8
  //      binNorm=11;
  // }

  //    if (ybin==4) {
  //        f1 = new TF1("f1","[0] + [1]*(log(x)-log(850)) + [2]*(log(x)-log(850))^2 + [3]*(log(x)-log(850))^3 + [4]*(log(x)-log(850))^4", 850,9066);
  //        f1->SetParameters(1.01729e+00,-6.33953e-01,1.12359e+00,-7.83149e-01,1.65080e-01); //y0 Pythia, x0 = 210, x^4, xMax=9066, binNorm=15

  //        binNorm=15;
  // }

  //    if (ybin==5) {
  //f1 = new TF1("f1","[0] + [1]*(log(x)-log(1310)) + [2]*(log(x)-log(1310))^2 + [3]*(log(x)-log(1310))^3 + [4]*(log(x)-log(1310))^4", 1310,9066);
  //f1 = new TF1("f1","[0] + [1]*(log(x)-log(1310)) + [2]*(log(x)-log(1310))^2 + [3]*(log(x)-log(1310))^3 + [4]*(log(x)-log(1310))^4 + [5]*(log(x)-log(1310))^5", 1310,9066);
  //f1->SetParameters(1.13253e+00,-8.28753e-01,-2.35473e+00,8.11782e+00,-6.78044e+00,1.69239e+00); //y0 Pythia, x0 = 1310, x^4, xMax=9066, binNorm=19 
  //binNorm=19;
  // }

  if (ybin==0) {

    f1 = new TF1("f1","[0] + [1]*(log(x)-log(950)) + [2]*(log(x)-log(950))^2 + [3]*(log(x)-log(950))^3", 950,7630);            
    f1->SetParameters( 9.86029e-01, 1.49238e-02, 2.20959e-02, 2.47343e-02); //y0 Pythia, x0 = 210, x^4, xMax=9066, binNorm=8
    binNorm =20;

  }
  else if (ybin==1) {
    
  }
  else if (ybin==2) {
    
  }
  else if (ybin==3) {
    
    f1 = new TF1("f1","[0] + [1]*(log(x)-log(2550)) + [2]*(log(x)-log(2550))^2 + [3]*(log(x)-log(2550))^3 + [4]*(log(x)-log(2550))^4 + [5]*(log(x)-log(2550))^5", 2550,8317);
    f1->SetParameters(1.00e+00,1.12144e-01,1.29691e-01,-1.80572e-01,6.82430e-02,-7.96162e-03); //y1 Pythia, x0 = 100, x^5, xMax=3937, binNorm=8
    binNorm=26;
  }
  else if (ybin==4) {
      
  }
  else if (ybin==5) {
      
  }

  if (!runDijet) {

    if (ybin==0) {
      f1 = new TF1("f1","[0] + [1]*(log(x)-log(516)) + [2]*(log(x)-log(516))^2 + [3]*(log(x)-log(516))^3 + [4]*(log(x)-log(516))^4 + [5]*(log(x)-log(516))^5", 516,3937);
      binNorm =25;
    }
    else if (ybin==1) {
      
    }
    else if (ybin==2) {
      
    }
    else if (ybin==3) {
      
    }
    else if (ybin==4) {
      
    }
    else if (ybin==5) {
      
    }
  }

  // Normalize the data to the MC
  //   matchData_H2.Scale(matchReco_H.GetBinContent(15)/matchData_H2.GetBinContent(15));//  dijets
  matchData_H.Scale(matchReco_H.GetBinContent(binNorm)/matchData_H.GetBinContent(binNorm));//  inclusive
  matchRecoBias_H.Scale(matchReco_H.GetBinContent(binNorm)/matchRecoBias_H.GetBinContent(binNorm)); //inclusive

  DrawTH1D(&matchReco_H,"matchReco_H");

  matchRecoBias_H.Divide(&matchReco_H);

  matchData_H.Divide(&matchReco_H);

  TH1D *matchData_H2 = (TH1D*)matchData_H.Clone("matchData_H2");
  TH1D *matchRecoBias_H2 = (TH1D*)matchRecoBias_H.Clone("matchRecoBias_H2");

  matchData_H2->Divide(matchRecoBias_H2);

  int nbins = matchData_H.GetNbinsX(); //FIXME

  //   for (Int_t i = 1; i <= nbins; ++i) {
  //
  //
  //       std::cout<<"i: "<<i<<"\t lowPtEdge(i)"<<matchReco_H.GetBinLowEdge(i)<<"\t Content(i): "<<matchReco_H.GetBinContent(i)<<std::endl;
  //   }

  // f1->SetParameters(matchData_H.GetBinContent(20),0,0);
  //   f1->SetParLimits(0, 200,100);

  //f1->FixParameter(0,1.02533); //incl
  //f1->FixParameter(0,1.00); //incl
  //f1->FixParameter(0,0.947579);

  matchData_H.Fit("f1","RB");

  //   std::cout<<"Data/MC fit parameters: "<<"[0]: "<<f1->GetParameter(0)<<"\t"<<"[1]: "<<f1->GetParameter(1)<<"\t"<<"[2]: "<<f1->GetParameter(2)<<"[3]: "<<f1->GetParameter(3)<<"[4]: "<<f1->GetParameter(4)<<std::endl;
  // std::cout<<"Data/MC fit parameters: "<<"[0]: "<<f1->GetParameter(0)<<"\t"<<"[1]: "<<f1->GetParameter(1)<<"\t"<<"[2]: "<<f1->GetParameter(2)<<std::endl;

  // Draw
  // Setting ATLAS Style
  SetAtlasStyle();
  //   TCanvas can_spectraRatio("spectraRatio", "spectraRatio", 400, 300, 600, 600);
  //   can_spectraRatio.cd();
  //   can_spectraRatio.SetLogx();

  TCanvas *_canvas = new TCanvas("spectraRatio", "spectraRatio", 400, 300, 600, 600);
  _canvas->Clear();
  _canvas->cd();
  _canvas->SetLogx();

  //   TH2D axis("axis", "axis", 100, 10, matchReco_H.GetBinLowEdge(matchReco_H.GetNbinsX()+1), 1, 0, 1.5);
  //   axis.GetXaxis()->SetMoreLogLabels();

  if (!runDijet) {
    matchData_H.GetXaxis()->SetTitle("#font[52]{p}_{T} [GeV]");
  }
  else {
    matchData_H.GetXaxis()->SetTitle("#font[52]{m}_{jj} [GeV]");
  }

  //   axis.GetYaxis()->SetTitle("Ratio wrt det-level MC");
  //   axis.Draw("axis");

  matchData_H.GetYaxis()->SetTitle("Ratio wrt det-level MC");
  matchData_H.GetYaxis()->SetTitleOffset(1.1);
  matchData_H.GetYaxis()->SetRangeUser(0.0,2.0);

  if (!runDijet) {
    if (ybin==0) {
      matchRecoBias_H.GetXaxis()->SetRangeUser(100,3937);
      matchData_H.GetXaxis()->SetRangeUser(100,3937);
      matchData_H2->GetXaxis()->SetRangeUser(100,3937);
    }
    else if (ybin==1) {
      matchRecoBias_H.GetXaxis()->SetRangeUser(100,3937);
      matchData_H.GetXaxis()->SetRangeUser(100,3937);
      matchData_H2->GetXaxis()->SetRangeUser(100,3937);
    }
    else if (ybin==2) {
      matchRecoBias_H.GetXaxis()->SetRangeUser(100,3137);
      matchData_H.GetXaxis()->SetRangeUser(100,3137);
      matchData_H2->GetXaxis()->SetRangeUser(100,3137);
    }     
    else if (ybin==3) {
      matchRecoBias_H.GetXaxis()->SetRangeUser(100,1992);
      matchData_H.GetXaxis()->SetRangeUser(100,1992);
      matchData_H2->GetXaxis()->SetRangeUser(100,1992);
    }     
    else if (ybin==4) {
      matchRecoBias_H.GetXaxis()->SetRangeUser(100,1530);
      matchData_H.GetXaxis()->SetRangeUser(100,1530);
      matchData_H2->GetXaxis()->SetRangeUser(100,1530);
    }
    else if (ybin==5) {
      matchRecoBias_H.GetXaxis()->SetRangeUser(100,786);
      matchData_H.GetXaxis()->SetRangeUser(100,786);
      matchData_H2->GetXaxis()->SetRangeUser(100,786);
    }
  }
  else {
    if (ybin==0) {
      matchRecoBias_H.GetXaxis()->SetRangeUser(210,6440);
      matchData_H.GetXaxis()->SetRangeUser(210,6440);
      matchData_H2->GetXaxis()->SetRangeUser(210,6440);
    }
    else if (ybin==1) {
      matchRecoBias_H.GetXaxis()->SetRangeUser(210,8317);
      matchData_H.GetXaxis()->SetRangeUser(210,8317);
      matchData_H2->GetXaxis()->SetRangeUser(210,8317);
    }
    else if (ybin==2) {
      matchRecoBias_H.GetXaxis()->SetRangeUser(370,7630);
      matchData_H.GetXaxis()->SetRangeUser(370,7630);
      matchData_H2->GetXaxis()->SetRangeUser(370,7630);
    }     
    else if (ybin==3) {
      matchRecoBias_H.GetXaxis()->SetRangeUser(440,7000);
      matchData_H.GetXaxis()->SetRangeUser(440,7000);
      matchData_H2->GetXaxis()->SetRangeUser(440,7000);
    }     
    else if (ybin==4) {
      matchRecoBias_H.GetXaxis()->SetRangeUser(950,8317);
      matchData_H.GetXaxis()->SetRangeUser(950,8317);
      matchData_H2->GetXaxis()->SetRangeUser(950,8317);
    }
    else if (ybin==5) {
      matchRecoBias_H.GetYaxis()->SetRangeUser(-1,4.0);
      matchRecoBias_H.GetXaxis()->SetRangeUser(1310,8317);
      matchData_H.GetXaxis()->SetRangeUser(1310,8317);
      matchData_H2->GetXaxis()->SetRangeUser(1310,8317);
    }
  }

  //   matchRecoBias_H.SetMarkerStyle(1);
  //   matchRecoBias_H.Draw();

  matchData_H.SetLineColor(kRed+1);
  matchData_H.SetLineStyle(1);
  matchData_H.Draw("hist");
  matchData_H.GetXaxis()->SetMoreLogLabels(kTRUE);

  f1->SetLineWidth(0.03);
  f1->SetFillStyle(0);
  f1->SetFillColorAlpha(0,1);
  f1->SetMarkerSize(0.03);
  f1->SetLineStyle(2);
  f1->SetLineColor(4);

  f1->Draw("same pe");

  matchData_H2->SetLineColor(kGreen+2);
  matchData_H2->Draw("same pe");

  //   nomatchTrue_H.SetLineColor(kRed+1);
  //   nomatchTrue_H.SetLineStyle(1);
  //   nomatchTrue_H.Draw("same pe");

  TLine* line;
  line = new TLine(100,1,3937,1);
  line->SetLineStyle(7);
  line->Draw();
  //TLegend legend_SpRatio(0.22, 0.68, 0.90, 0.93);
  TLegend legend_SpRatio(0.22, 0.80, 0.90, 0.93);
  legend_SpRatio.SetMargin( 0.08 );
  legend_SpRatio.SetTextSize( 0.04 );
  legend_SpRatio.SetBorderSize(0);
  legend_SpRatio.SetFillColor( 0 );
  //legend_SpRatio.AddEntry(&matchData_H,"Pseudo-Data with acceptance correction","PLE");//Temporary FIXME
  // legend_SpRatio.AddEntry(&matchData_H,"Data wrt Reweighted Reco MC","PLE");//Temporary FIXME
  //legend_SpRatio.AddEntry(&matchRecoBias_H,"Modified matched det-level MC","L");
  //   legend_SpRatio.AddEntry(&matchRecoBias_H,"Reweighted Reco wrt Reco MC","L");
  legend_SpRatio.AddEntry(&matchData_H,"Data wrt Reco MC","L");
  //legend_SpRatio.AddEntry(&matchRecoBias_H,"Reweighted Reco wrt Reco MC","L");
  legend_SpRatio.AddEntry(f1,"Fit Function","L");
  legend_SpRatio.AddEntry(matchData_H2,"Data wrt Reweighted Reco MC","L");
  //   legend_SpRatio.AddEntry(&matchData_H2,"MC-True wrt MC-Reco non-matched","L");
  // legend_SpRatio.AddEntry(&nomatchReco_H,"Non-matched reco MC","L");
  // legend_SpRatio.AddEntry(&nomatchTrue_H,"Non-matched true MC","L");
  legend_SpRatio.Draw("same");

  gPad->RedrawAxis();

  TString out = local_folder +  "Results_IDS/systematic_";
  out += jsyst;
  out += "/";
  out += dijetN;
  out += "/";
  out += "ybin_";
  out += ybin;
  out += "/";
  out += "spectraRatios_R04_Eta.eps";
  _canvas->Print(out.Data(), "eps");

  //  out = PATH;
  //  out += "/Plots/spectraRatios_R04_Eta.png";
  //  _canvas->Print(out.Data(), "png");

  // Temporary
  if(false) std::cout << "Quitting DrawSpectraRatioPlots" << std::endl;

}

void Draw2TH1D(const TH1D *Histo1, const TH1D *Histo2, std::string legend1, std::string legend2, std::string output, Int_t Toy = -1, bool logx = true, bool logy = false)
{
  TH1D h_1(*Histo1);
  TH1D h_2(*Histo2);

  TString Legend1 = legend1;
  TString Legend2 = legend2;
  TString nameout = output;

  bool Logx = logx;
  bool Logy = logy;

  Double_t deltaY = 2*0.05;

  Double_t max1 = h_1.GetBinContent(1);
  Double_t max2 = h_2.GetBinContent(1);
  Double_t max;
  if(max1>max2) max = max1;
  else {max = max2;}

  if(Logy){
    max = 10^9;
  }

  //TH2D axis("axis", "axis", 1, h_1.GetBinLowEdge(1), h_1.GetBinLowEdge(h_1.GetNbinsX()+1), 1, 0, 1.2*max);
  //axis.GetXaxis()->SetMoreLogLabels();
  //axis.GetXaxis()->SetTitle("#font[52]{m}_{12} [GeV]");

  SetAtlasStyle(); // Setting ATLAS Style
  TCanvas can_com("comparison", "comparison", 400, 30, 600, 600);
  can_com.cd();
  if(Logx) can_com.SetLogx();
  if(Logy) can_com.SetLogy();

  //axis.Draw("axis");

  h_1.SetLineColor(kBlue+1);

  if (!runDijet) {
    h_1.SetXTitle("#font[52]{p}_{T} [GeV]");
  }
  else {
    h_1.SetXTitle("#font[52]{m}_{jj} [GeV]");
  }

  h_1.SetLineWidth(4);
  h_1.SetLineStyle(2);
  h_1.Draw("same hist ][");
  // h_1.DrawNormalized("same hist ]["); //FIXME

  h_2.SetLineColor(kRed);
  h_2.SetLineWidth(4);
  h_2.SetLineStyle(2);
  h_2.Draw("same hist ][");
  // h_2.DrawNormalized("same hist ]["); //FIXME

  double legendaux = 0;
  if(Logy) legendaux = 0.65;

  TLegend legend_BIASrel_toys(1 - can_com.GetRightMargin() - 0.55 - 0.2, 1 - can_com.GetTopMargin() - deltaY - legendaux,
			      1 - can_com.GetRightMargin() - 0.55 - 0.025, 1 - can_com.GetTopMargin() - 0.015 - legendaux);
  legend_BIASrel_toys.SetMargin( 0.4 );
  legend_BIASrel_toys.SetTextSize( 0.05 );
  legend_BIASrel_toys.SetBorderSize(0);
  legend_BIASrel_toys.SetFillColor( 0 );
  legend_BIASrel_toys.AddEntry(&h_1,Legend1,"L");
  legend_BIASrel_toys.AddEntry(&h_2,Legend2,"L");
  legend_BIASrel_toys.Draw("same");

  gPad->RedrawAxis();

  TString out = local_folder +  "Results_IDS/systematic_";
  out += jsyst;
  out += "/";
  out += dijetN;
  out += "/";
  out += "ybin_";
  out += ybin;
  out += "/";
  out += nameout;

  if(Toy != -1){
    out += "_";
    out += Toy;
  }
  out += ".eps";
  can_com.Print(out.Data(), "eps");

  //out += ".png";
  //can_com.Print(out.Data(), "png");
}

void DrawComparisonPlots(const TH1D &IDSUData, const TH1D &SVDUData, const TH1D &Data, const TH1D &RWTrue, const TH1D &RWReco, const TH1D &MData)
{
  TH1D h_IDSUData(IDSUData);
  TH1D h_SVDUData(SVDUData);
  TH1D h_Data(Data);
  TH1D h_RWTrue(RWTrue);
  TH1D h_RWReco(RWReco);
  TH1D h_MData(MData); //Matched data

  Double_t deltaY = 2*0.05;

  //---------
  // IDS case
  //---------

  // UData - Data

  Double_t max1 = h_Data.GetBinContent(1);
  Double_t max2 = h_IDSUData.GetBinContent(1);
  Double_t max;
  if(max1>max2) max = max1;
  else {max = max2;}

  //TH2D axis("axis", "axis", 1, h_Data.GetBinLowEdge(1), h_Data.GetBinLowEdge(h_Data.GetNbinsX()+1), 1, 0, 1.2*max);
  //axis.GetXaxis()->SetMoreLogLabels();
  //axis.GetXaxis()->SetTitle("#font[52]{m}_{12} [GeV]");

  SetAtlasStyle(); // Setting ATLAS Style
  TCanvas can_com("comparison", "comparison", 400, 30, 600, 600);
  can_com.cd();
  can_com.SetLogx();
  //can_com.SetLogy();

  //axis.Draw("axis");

  h_Data.SetLineColor(kBlue+1);
  h_Data.SetLineWidth(4);
  h_Data.SetLineStyle(2);

  if (!runDijet) {
    h_Data.SetXTitle("#font[52]{p}_{T} [GeV]");
  }
  else {
    h_Data.SetXTitle("#font[52]{m}_{jj} [GeV]");
  }

  h_Data.Draw("same hist ][");

  h_IDSUData.SetLineColor(kRed);
  h_IDSUData.SetLineWidth(4);
  h_IDSUData.SetLineStyle(2);
  h_IDSUData.SetXTitle("#font[52]{p}_{T} [GeV]");
  h_IDSUData.Draw("same hist ][");

  TLegend legend_BIASrel_toys(1 - can_com.GetRightMargin() - 0.55 - 0.2, 1 - can_com.GetTopMargin() - deltaY,
			      1 - can_com.GetRightMargin() - 0.55 - 0.025, 1 - can_com.GetTopMargin() - 0.015 );
  legend_BIASrel_toys.SetMargin( 0.4 );
  legend_BIASrel_toys.SetTextSize( 0.05 );
  legend_BIASrel_toys.SetBorderSize(0);
  legend_BIASrel_toys.SetFillColor( 0 );
  legend_BIASrel_toys.AddEntry(&h_Data,"Data","L");
  legend_BIASrel_toys.AddEntry(&h_IDSUData,"Unfolded Reweighted Reco MC","L");
  legend_BIASrel_toys.Draw("same");

  //gPad->RedrawAxis();

  TString out = local_folder +  "Results_IDS/systematic_";
  out += jsyst;
  out += "/";
  out += dijetN;
  out += "/";
  out += "ybin_";
  out += ybin;
  out += "/";
  out += "IDS_UData_Data_Plot.eps";

  can_com.Print(out.Data(), "eps");

  // out += "/Plots/IDS_UData_Data_Plot.png";
  // can_com.Print(out.Data(), "png");

  // UData - Truth re-weighted

  Double_t T_max1 = h_IDSUData.GetBinContent(1);
  Double_t T_max2 = h_RWTrue.GetBinContent(1);
  Double_t T_max;
  if(T_max1>T_max2) T_max = T_max1;
  else {T_max = T_max2;}

  //TH2D T_axis("axis", "axis", 1, h_Data.GetBinLowEdge(1), h_Data.GetBinLowEdge(h_Data.GetNbinsX()+1), 1, 0, 1.2*T_max);
  //T_axis.GetXaxis()->SetMoreLogLabels();
  //T_axis.GetXaxis()->SetTitle("#font[52]{m}_{12} [GeV]");

  SetAtlasStyle(); // Setting ATLAS Style
  TCanvas can_com_Truth("comparison", "comparison", 400, 30, 600, 600);
  can_com_Truth.cd();
  can_com_Truth.SetLogx();
  can_com_Truth.SetLogy();

  //T_axis.Draw("axis");

  h_IDSUData.SetLineColor(kRed);
  h_IDSUData.SetLineWidth(4);
  h_IDSUData.SetLineStyle(2);

  if (!runDijet) {
    h_IDSUData.SetXTitle("#font[52]{p}_{T} [GeV]");
  }
  else {
    h_IDSUData.SetXTitle("#font[52]{m}_{jj} [GeV]");
  }

  h_IDSUData.Draw("same hist ][");

  h_RWTrue.SetLineColor(kBlue+1);
  h_RWTrue.SetLineWidth(4);
  h_RWTrue.SetLineStyle(2);

  if (!runDijet) {
    h_RWTrue.SetXTitle("#font[52]{p}_{T} [GeV]");
  }
  else {
    h_RWTrue.SetXTitle("#font[52]{m}_{jj} [GeV]");
  }

  h_RWTrue.Draw("same hist ][");

  TLegend legend_Truth(1 - can_com_Truth.GetRightMargin() - 0.55 - 0.2, 1 - can_com_Truth.GetTopMargin() - deltaY,
		       1 - can_com_Truth.GetRightMargin() - 0.55 - 0.025, 1 - can_com_Truth.GetTopMargin() - 0.015 );
  legend_Truth.SetMargin( 0.4 );
  legend_Truth.SetTextSize( 0.05 );
  legend_Truth.SetBorderSize(0);
  legend_Truth.SetFillColor( 0 );
  legend_Truth.AddEntry(&h_RWTrue,"Re-weighted Truth","L");
  legend_Truth.AddEntry(&h_IDSUData,"Unfolded Reweighted Reco MC","L");
  legend_Truth.Draw("same");

  //gPad->RedrawAxis();

  TString out_Truth = local_folder +  "Results_IDS/systematic_";
  out_Truth += jsyst;
  out_Truth += "/";
  out_Truth += dijetN;
  out_Truth += "/";
  out_Truth += "ybin_";
  out_Truth += ybin;
  out_Truth += "/";
  out_Truth += "IDS_UData_RWTrue_Plot.eps";

  can_com_Truth.Print(out_Truth.Data(), "eps");

  // out_Truth += "/Plots/IDS_UData_RWTrue_Plot.png";
  // can_com_Truth.Print(out_Truth.Data(), "png");

  // ---------------------------------------------------
  // Comparison between Matched Data and Reweighted Reco 
  // ---------------------------------------------------

  Double_t com_max1 = h_MData.GetBinContent(1);
  Double_t com_max2 = h_RWReco.GetBinContent(1);
  Double_t com_max;
  if(com_max1>com_max2) com_max = com_max1;
  else {com_max = com_max2;}

  //TH2D com_axis("axis", "axis", 1, h_Data.GetBinLowEdge(1), h_Data.GetBinLowEdge(h_Data.GetNbinsX()+1), 1, 0, 1.2*com_max);
  //com_axis.GetXaxis()->SetMoreLogLabels();
  //com_axis.GetXaxis()->SetTitle("#font[52]{m}_{12} [GeV]");

  SetAtlasStyle(); // Setting ATLAS Style
  TCanvas can_com_new("comparison", "comparison", 400, 30, 600, 600);
  can_com_new.cd();
  can_com_new.SetLogx();
  //can_com_new.SetLogy();

  //com_axis.Draw("axis");

  h_MData.SetLineColor(kBlue+1);
  h_MData.SetLineWidth(4);
  h_MData.SetLineStyle(2);

  if (!runDijet) {
    h_MData.SetXTitle("#font[52]{p}_{T} [GeV]");
  }
  else {
    h_MData.SetXTitle("#font[52]{m}_{jj} [GeV]");
  }

  h_MData.Draw("same hist ][");

  h_RWReco.SetLineColor(kRed);
  h_RWReco.SetLineWidth(4);
  //h_RWReco.SetLineStyle(2);

  if (!runDijet) {
    h_RWReco.SetXTitle("#font[52]{p}_{T} [GeV]");
  }
  else {
    h_RWReco.SetXTitle("#font[52]{m}_{jj} [GeV]");
  }

  h_RWReco.Draw("same hist ][");

  TLegend legend_new(1 - can_com_new.GetRightMargin() - 0.55 - 0.2, 1 - can_com_new.GetTopMargin() - deltaY,
		     1 - can_com_new.GetRightMargin() - 0.55 - 0.025, 1 - can_com_new.GetTopMargin() - 0.015 );
  legend_new.SetMargin( 0.4 );
  legend_new.SetTextSize( 0.05 );
  legend_new.SetBorderSize(0);
  legend_new.SetFillColor( 0 );
  legend_new.AddEntry(&h_MData,"Matched Pseudo-Data","L");
  legend_new.AddEntry(&h_RWReco,"Reweighted Reco MC","L");
  legend_new.Draw("same");

  //gPad->RedrawAxis();

  TString out_new = local_folder +  "Results_IDS/systematic_";
  out_new += jsyst;
  out_new += "/";
  out_new += dijetN;
  out_new += "/";
  out_new += "ybin_";
  out_new += ybin;
  out_new += "/";
  out_new += "MData_RWReco_Plot.eps";

  can_com_new.Print(out_new.Data(), "eps");

  // out_new += "/Plots/MData_RWReco_Plot.png";
  // can_com_new.Print(out_new.Data(), "png");
}

void DrawTVectorD(const TVectorD &result, std::string name, Int_t Toy = -1)
{
  // Setting ATLAS Style
  SetAtlasStyle();
  TCanvas canvas("result_toys", "result_toys", 400, 30, 600, 600);

  TH1D Histo;
  Histo.GetXaxis()->Set(gNBins, &gBinLimits[0]);
  TString Histo_Name = name;
  if(Toy != -1){
    Histo_Name += "_";
    Histo_Name += Toy;
  }
  Histo.SetName(Histo_Name.Data());
  Histo.SetTitle(Histo_Name.Data());

  Int_t nbinsx = gNBins;
  Int_t nbinsy = 1; 

  Int_t i = 0;
  for (Int_t by = 1; by <= nbinsy; ++by) {
    for (Int_t bx = 1; bx <= nbinsx; ++bx) {
      Histo.SetBinContent(bx, by, result[i++]);
    }
  }

  /*
    for(int i=0;i<gNBins;i++)
    {
    Histo.SetBinContent(i+1,result[i]);
    }
  */

  Histo.Draw();

  TString out = local_folder +  "Results_IDS/";
  out += name;
  if(Toy != -1){
    out += "_";
    out += Toy;
  }
  out += ".eps";
  canvas.Print(out.Data(), "eps");
}

void DrawRelBiasPlots(const TH1D &biasIDStestSB_VB_toys, const TH1D &biasSVDtestSB_VB_toys)
{
  TH1D biasIDStestSB_VB_H_toys(biasIDStestSB_VB_toys);
  biasIDStestSB_VB_H_toys.Scale(100.0);

  TH1D biasSVDtestSB_VB_H_toys(biasSVDtestSB_VB_toys);
  biasSVDtestSB_VB_H_toys.Scale(100.0);

  // Setting ATLAS Style
  SetAtlasStyle();
  TCanvas can_bBbBIASrel_toys("bBbBIASrel_toys", "bBbBIASrel_toys", 400, 30, 600, 600);
  can_bBbBIASrel_toys.cd();
  can_bBbBIASrel_toys.SetLogx();

  Double_t maxIDS = biasIDStestSB_VB_H_toys.GetBinContent(biasIDStestSB_VB_H_toys.GetMaximumBin());
  std::cout << "max IDS = " << maxIDS << std::endl;
  Double_t maxSVD = biasSVDtestSB_VB_H_toys.GetBinContent(biasSVDtestSB_VB_H_toys.GetMaximumBin());
  std::cout << "max SVD = " << maxSVD << std::endl;
  Double_t max;
  if(maxIDS > maxSVD) max = maxIDS;
  if(maxIDS <= maxSVD) max = maxSVD;
  max = maxIDS; // Temporary FIXME
  max = 10;// Temporary FIXME
  //if(max < 1.0) max = 0.1;

  std::cout << "biasIDStestSB_VB_H_toys.GetBinLowEdge(1) = " << biasIDStestSB_VB_H_toys.GetBinLowEdge(1) << std::endl;
  std::cout << "biasIDStestSB_VB_H_toys.GetNbinsX()+1 = " << biasIDStestSB_VB_H_toys.GetNbinsX()+1 << std::endl;
  std::cout << "biasIDStestSB_VB_H_toys.GetBinLowEdge(biasIDStestSB_VB_H_toys.GetNbinsX()+1) = " << biasIDStestSB_VB_H_toys.GetBinLowEdge(biasIDStestSB_VB_H_toys.GetNbinsX()+1) << std::endl;

  /*   TH2D axis("axis", "axis", 1, biasIDStestSB_VB_H_toys.GetBinLowEdge(1), biasIDStestSB_VB_H_toys.GetBinLowEdge(biasIDStestSB_VB_H_toys.GetNbinsX()+1), 1, -1.2*max, 1.2*max);
  //TH2D axis("axis", "axis", 1, biasIDStestSB_VB_H_toys.GetBinLowEdge(1), biasIDStestSB_VB_H_toys.GetBinLowEdge(biasIDStestSB_VB_H_toys.GetNbinsX()+1-3), 1, -1.2*max, 1.2*max); // Temporary FIXME Removing the two last mass bins
  //TH2D axis("axis", "axis", 1, biasIDStestSB_VB_H_toys.GetBinLowEdge(1), biasIDStestSB_VB_H_toys.GetBinLowEdge(biasIDStestSB_VB_H_toys.GetNbinsX()+1-30), 1, -1.2*max, 1.2*max); // Temporary FIXME Removing the two last mass bins
  //axis.GetXaxis()->SetMoreLogLabels();
  axis.GetXaxis()->SetTitle("#font[52]{m}_{12} [GeV]");
  axis.GetYaxis()->SetTitle("Relative bias [%]");
  axis.Draw("axis");
  */

  //TLine one(biasIDStestSB_VB_H_toys.GetBinLowEdge(1), 0, biasIDStestSB_VB_H_toys.GetBinLowEdge(biasIDStestSB_VB_H_toys.GetNbinsX()+1), 0);
  //TLine one(biasIDStestSB_VB_H_toys.GetBinLowEdge(1), 0, biasIDStestSB_VB_H_toys.GetBinLowEdge(biasIDStestSB_VB_H_toys.GetNbinsX()+1-3), 0); // Temporary FIXME Removing the two last mass bins
  //TLine one(biasIDStestSB_VB_H_toys.GetBinLowEdge(1), 0, biasIDStestSB_VB_H_toys.GetBinLowEdge(biasIDStestSB_VB_H_toys.GetNbinsX()+1-30), 0); // Temporary FIXME Removing the two last mass bins
  //one.Draw("same");

  biasIDStestSB_VB_H_toys.SetLineColor(kBlue+1);
  biasIDStestSB_VB_H_toys.SetLineStyle(1);
  biasIDStestSB_VB_H_toys.SetXTitle("#font[52]{p}_{T} [GeV]");
  biasIDStestSB_VB_H_toys.SetYTitle("Relative bias [%]");
  //biasIDStestSB_VB_H_toys.Draw("same hist ][");
  biasIDStestSB_VB_H_toys.Draw();

  /* //Temporary FIXME
     biasSVDtestSB_VB_H_toys.SetLineColor(kRed);
     biasSVDtestSB_VB_H_toys.SetLineStyle(2);
     biasSVDtestSB_VB_H_toys.Draw("same hist ][");
  */

  Double_t deltaY = 2*0.05;
  TLegend legend_BIASrel_toys(1 - can_bBbBIASrel_toys.GetRightMargin() - 0.55 - 0.2, 1 - can_bBbBIASrel_toys.GetTopMargin() - deltaY,
			      1 - can_bBbBIASrel_toys.GetRightMargin() - 0.55 - 0.025, 1 - can_bBbBIASrel_toys.GetTopMargin() - 0.015 );
  legend_BIASrel_toys.SetMargin( 0.4 );
  legend_BIASrel_toys.SetTextSize( 0.05 );
  legend_BIASrel_toys.SetBorderSize(0);
  legend_BIASrel_toys.SetFillColor( 0 );
  legend_BIASrel_toys.AddEntry(&biasIDStestSB_VB_H_toys,"IDS","L");
  //legend_BIASrel_toys.AddEntry(&biasSVDtestSB_VB_H_toys,"SVD","L"); //Temporary FIXME
  legend_BIASrel_toys.Draw("same");

  gPad->RedrawAxis();

  TString out = local_folder +  "Results_IDS/relBiasToys_R04_Eta.eps";
  can_bBbBIASrel_toys.Print(out.Data(), "eps");

  out = PATH_2;
  // out += "/Plots/relBiasToys_R04_Eta.png";
  // can_bBbBIASrel_toys.Print(out.Data(), "png");
}

void DrawRelErrPlots(const TH1D &data, const TMatrixD &covSVD, const TVectorD &avgSVD, const TMatrixD &covIDS1, const TVectorD &avgIDS1, const TMatrixD &covIDS2, const TVectorD &avgIDS2)
{
  TH1D relErrData_H(data);
  relErrData_H.GetXaxis()->Set(gNBins, &gBinLimits[0]);

  TH1D relErrSVD_H(data);
  relErrSVD_H.GetXaxis()->Set(gNBins, &gBinLimits[0]);

  TH1D relErrIDS1_H(data);
  relErrIDS1_H.GetXaxis()->Set(gNBins, &gBinLimits[0]);

  TH1D relErrIDS2_H(data);
  relErrIDS2_H.GetXaxis()->Set(gNBins, &gBinLimits[0]);

  // Determine relative error of methods
  for (Int_t i = 0; i < gNBins; i++) {
    if (data.GetBinContent(i+1) != 0.0)
      relErrData_H.SetBinContent(i+1, data.GetBinError(i+1)/data.GetBinContent(i+1));
    if(avgSVD[i] != 0.0)
      relErrSVD_H.SetBinContent(i+1, sqrt(covSVD[i][i])/avgSVD[i]);
    if(avgIDS1[i] != 0.0)
      relErrIDS1_H.SetBinContent(i+1, sqrt(covIDS1[i][i])/fabs(avgIDS1[i]));
    if(avgIDS2[i] != 0.0)
      relErrIDS2_H.SetBinContent(i+1, sqrt(covIDS2[i][i])/fabs(avgIDS2[i]) );
  }

  relErrData_H.Scale(100.0);
  relErrSVD_H.Scale(100.0);
  relErrIDS1_H.Scale(100.0);
  relErrIDS2_H.Scale(100.0);

  // Plot relative error
  TCanvas canErrToySVD("ErrToySVD", "ErrToySVD", 400, 30, 600, 600);
  canErrToySVD.cd();
  //  canErrToySVD.SetLogx();
  canErrToySVD.SetLogy();

  relErrData_H.GetXaxis()->SetRange(gMinMatrixBin, gNBins-1);
  relErrSVD_H.GetXaxis()->SetRange(gMinMatrixBin, gNBins-1);
  Double_t max = relErrIDS2_H.GetBinContent(relErrIDS2_H.GetMaximumBin());
  Double_t min = relErrData_H.GetBinContent(relErrData_H.GetMinimumBin());
  min = TMath::Min(min, relErrSVD_H.GetBinContent(relErrSVD_H.GetMinimumBin()));

  TH2D axis("axis", "axis", 1, gMassMin, gMassMax, 1, 0.8*min, 1.2*max);
  //  axis.GetXaxis()->SetMoreLogLabels();
  axis.GetXaxis()->SetTitle("#font[52]{m}_{12} [GeV]");
  axis.GetYaxis()->SetTitle("Relative error [%]");
  axis.Draw("axis");

  relErrData_H.SetLineColor(kBlack);
  relErrData_H.Draw("same hist ][");

  relErrSVD_H.SetLineColor(kGreen+1);
  relErrSVD_H.Draw("same hist ][");

  relErrIDS1_H.SetLineStyle(2);
  relErrIDS1_H.SetLineColor(kBlue+1);
  relErrIDS1_H.Draw("same hist ][");

  relErrIDS2_H.SetLineColor(kBlue+1);
  relErrIDS2_H.Draw("same hist ][");

  Double_t deltaY = 4*0.05 + 0.015;
  TLegend legend_ErrToySVD(1 - canErrToySVD.GetRightMargin() - 0.55 - 0.2, 1 - canErrToySVD.GetTopMargin() - deltaY,
			   1 - canErrToySVD.GetRightMargin() - 0.55 - 0.025, 1 - canErrToySVD.GetTopMargin() - 0.015 );
  legend_ErrToySVD.SetMargin( 0.4 );
  legend_ErrToySVD.SetTextSize( 0.05 );
  legend_ErrToySVD.SetBorderSize(0);
  legend_ErrToySVD.SetFillColor( 0 );
  legend_ErrToySVD.AddEntry(&relErrData_H,"Raw data","L");
  legend_ErrToySVD.AddEntry(&relErrSVD_H,"MC portion","L");
  legend_ErrToySVD.AddEntry(&relErrIDS1_H,"Data portion","L");
  legend_ErrToySVD.AddEntry(&relErrIDS2_H,"Full error","L");
  legend_ErrToySVD.Draw("same");

  gPad->RedrawAxis();

  TString out = local_folder +  "Results_IDS/relErrorsToys_R04_Eta.eps";
  canErrToySVD.Print(out.Data(), "eps");

  out = PATH_2;
  //  out += "/Plots/relErrorsToys_R04_Eta.png";
  // canErrToySVD.Print(out.Data(), "png");
}

void DrawRelErrPlots2(const TH1D &data, const TMatrixD &covMC, const TVectorD &avgMC, const TMatrixD &covData, const TVectorD &avgData, const TMatrixD &cov, const TVectorD &avg)
{
  TH1D relErrgData_H(data);
  relErrgData_H.GetXaxis()->Set(gNBins, &gBinLimits[0]);

  TH1D relErrMC_H(data);
  relErrMC_H.GetXaxis()->Set(gNBins, &gBinLimits[0]);

  TH1D relErrData_H(data);
  relErrData_H.GetXaxis()->Set(gNBins, &gBinLimits[0]);

  TH1D relErr_H(data);
  relErr_H.GetXaxis()->Set(gNBins, &gBinLimits[0]);

  // Determine relative error of methods
  for (Int_t i = 0; i < gNBins; i++) {
    if (data.GetBinContent(i+1) != 0.0) {
      relErrgData_H.SetBinContent(i+1, data.GetBinError(i+1)/data.GetBinContent(i+1));
      // std::cout<<relErrgData_H.GetBinLowEdge(i+1)<<"\t"<<data.GetBinError(i+1)*100.0/data.GetBinContent(i+1)<<std::endl;
    }
    if(avgMC[i] != 0.0) {
      relErrMC_H.SetBinContent(i+1, sqrt(covMC[i][i])/avgMC[i]);
      // std::cout<<relErrMC_H.GetBinLowEdge(i+1)<<"\t"<<sqrt(covMC[i][i])*100.0/fabs(avgMC[i])<<std::endl;
    }
    if(avgData[i] != 0.0)
      relErrData_H.SetBinContent(i+1, sqrt(covData[i][i])/fabs(avgData[i]));
    if(avg[i] != 0.0){
      relErr_H.SetBinContent(i+1, sqrt(cov[i][i])/fabs(avg[i]) );
    }
  }

  relErrgData_H.Scale(100.0);
  relErrMC_H.Scale(100.0);
  relErrData_H.Scale(100.0);
  relErr_H.Scale(100.0);

  //   TFile* ferr2 = new TFile("stat_error_cov.root","recreate"); // Data
  //   relErr_H.SetName("error");
  //   relErr_H.Write();
  //   ferr2->Close();

  // Plot relative error
  TCanvas canErrToySVD("ErrToySVD", "ErrToySVD", 400, 30, 600, 600);
  canErrToySVD.cd();
  canErrToySVD.SetLogx();
  canErrToySVD.SetLogy();

  //   relErrgData_H.GetXaxis()->SetRange(gMinMatrixBin, gNBins-1);
  //   relErrMC_H.GetXaxis()->SetRange(gMinMatrixBin, gNBins-1);
  //   Double_t max = relErrgData_H.GetBinContent(relErrgData_H.GetMaximumBin());
  //   Double_t min = relErrgData_H.GetBinContent(relErrgData_H.GetMinimumBin());
  // min = TMath::Min(min, relErrMC_H.GetBinContent(relErrMC_H.GetMinimumBin()));

  // TH2D axis("axis", "axis", 1, gMassMin, gMassMax, 1, 0.8*min, 1.2*max);
  TH2D axis("axis", "axis", 1, 210, 7000, 1, 0.0001, 100);

  if (!runDijet) {
    axis.GetXaxis()->SetRangeUser(100,3937);
    axis.GetXaxis()->SetTitle("#font[52]{p}_{T} [GeV]");

    relErrgData_H.GetXaxis()->SetRangeUser(100,3937);
    relErrMC_H.GetXaxis()->SetRangeUser(100,3937);
    relErrData_H.GetXaxis()->SetRangeUser(100,3937);
    relErr_H.GetXaxis()->SetRangeUser(100,3937);
  }
  else {
    axis.GetXaxis()->SetRangeUser(210,7000);
    axis.GetXaxis()->SetTitle("#font[52]{m}_{jj} [GeV]");

    relErrgData_H.GetXaxis()->SetRangeUser(210,7000);
    relErrMC_H.GetXaxis()->SetRangeUser(210,7000);
    relErrData_H.GetXaxis()->SetRangeUser(210,7000);
    relErr_H.GetXaxis()->SetRangeUser(210,7000);
  }

  axis.GetXaxis()->SetMoreLogLabels();
  axis.GetYaxis()->SetTitle("Relative error [%]");
  axis.Draw("axis");

  relErrgData_H.SetLineColor(kBlack);
  relErrgData_H.Draw("same hist ][");

  relErrMC_H.SetLineColor(kGreen+1);
  relErrMC_H.Draw("same hist ][");

  relErrData_H.SetLineStyle(2);
  relErrData_H.SetLineColor(kBlue+1);
  relErrData_H.Draw("same hist ][");

  relErr_H.SetLineColor(kBlue+1);
  relErr_H.Draw("same hist ][");

  //Double_t deltaY = 4*0.05 + 0.015;
  //TLegend legend_ErrToySVD(1 - canErrToySVD.GetRightMargin() - 0.55 - 0.2, 1 - canErrToySVD.GetTopMargin() - deltaY,
  TLegend legend_ErrToySVD(0.2, 0.2,
			   0.4, 0.4);
  legend_ErrToySVD.SetMargin( 0.4 );
  legend_ErrToySVD.SetTextSize( 0.05 );
  legend_ErrToySVD.SetBorderSize(0);
  legend_ErrToySVD.SetFillColor( 0 );
  legend_ErrToySVD.AddEntry(&relErrgData_H,"Raw data","L");
  legend_ErrToySVD.AddEntry(&relErrMC_H,"MC portion","L");
  legend_ErrToySVD.AddEntry(&relErrData_H,"Data portion","L");
  legend_ErrToySVD.AddEntry(&relErr_H,"Full error","L");
  legend_ErrToySVD.Draw("same");

  //   gPad->RedrawAxis();

  TString out = local_folder +  "Results_IDS/systematic_";
  out += jsyst;
  out += "/";
  out += dijetN;
  out += "/";
  out += "ybin_";
  out += ybin;
  out += "/";
  out += "relErrorsToys_R04_Eta.eps";

  canErrToySVD.Print(out.Data(), "eps");

  //  out += "/Plots/relErrorsToys_R04_Eta.png";
  // canErrToySVD.Print(out.Data(), "png");
}

void my_DrawRelErrPlots(const TH1D &data, const TMatrixD &covIDS1, const TVectorD &avgIDS1)
{
  //   TH1D relErrData_H(data);
  //   relErrData_H.GetXaxis()->Set(gNBins, gBinLimits);


  TH1D relErrIDS1_H("rel", "rel", gNBins, &gBinLimits[0]);
  //  relErrIDS1_H.GetXaxis()->Set(gNBins, gBinLimits);


  // Determine relative error of methods
  for (Int_t i = 0; i < gNBins; i++) {
    //      if (data.GetBinContent(i+1) != 0.0)
    //       relErrData_H.SetBinContent(i+1, data.GetBinError(i+1)/data.GetBinContent(i+1));
    if(avgIDS1[i] != 0.0)
      relErrIDS1_H.SetBinContent(i+1, sqrt(covIDS1[i][i])/fabs(avgIDS1[i]));
  }

  //  relErrData_H.Scale(100.0);
  relErrIDS1_H.Scale(100.0);

  // Plot relative error
  TCanvas canErrToySVD("ErrToySVD", "ErrToySVD", 400, 30, 600, 600);
  canErrToySVD.cd();
  canErrToySVD.SetLogx();
  //   canErrToySVD.SetLogy();
  /*
    relErrData_H.GetXaxis()->SetRange(gMinMatrixBin, gNBins-1);
    Double_t max = relErrData_H.GetBinContent(relErrData_H.GetMaximumBin());
    Double_t min = relErrData_H.GetBinContent(relErrData_H.GetMinimumBin());
  */

  relErrIDS1_H.GetXaxis()->SetRange(gMinMatrixBin, gNBins-1);
  //   Double_t max = relErrIDS1_H.GetBinContent(relErrIDS1_H.GetMaximumBin());
  //   Double_t min = relErrIDS1_H.GetBinContent(relErrIDS1_H.GetMinimumBin());

  //TH2D axis("axis", "axis", 1, gMassMin, gMassMax, 1, 0.8*min, 1.2*max);
  TH2D axis("axis", "axis", 1, gMassMin, gMassMax, 1, 0.01, 2.0);
  //   axis.GetXaxis()->SetMoreLogLabels();
  axis.GetXaxis()->SetTitle("#font[52]{p}_{T} [GeV]");
  axis.GetYaxis()->SetTitle("Relative error [%]");
  axis.Draw("axis");

  //  relErrData_H.SetLineColor(kBlack);
  //  relErrData_H.Draw("same hist ][");

  relErrIDS1_H.SetLineStyle(2);
  relErrIDS1_H.SetLineColor(kBlue+1);
  relErrIDS1_H.Draw("psame");

  //
  //   Double_t deltaY = 4*0.05 + 0.015;
  //   TLegend legend_ErrToySVD(1 - canErrToySVD.GetRightMargin() - 0.55 - 0.2, 1 - canErrToySVD.GetTopMargin() - deltaY,
  //                            1 - canErrToySVD.GetRightMargin() - 0.55 - 0.025, 1 - canErrToySVD.GetTopMargin() - 0.015 );
  //   legend_ErrToySVD.SetMargin( 0.4 );
  //   legend_ErrToySVD.SetTextSize( 0.05 );
  //   legend_ErrToySVD.SetBorderSize(0);
  //   legend_ErrToySVD.SetFillColor( 0 );
  //   legend_ErrToySVD.AddEntry(&relErrData_H,"Raw data","L");
  //   legend_ErrToySVD.AddEntry(&relErrIDS1_H,"Data portion","L");
  //   legend_ErrToySVD.Draw("same");

  //   gPad->RedrawAxis();

  TString out = local_folder +  "Results_IDS/relErrorsToys_R04_Eta.eps";
  canErrToySVD.Print(out.Data(), "eps");

  out = PATH_2;
  //  out += "/Plots/relErrorsToys_R04_Eta.png";
  // canErrToySVD.Print(out.Data(), "png");
}

void DrawLabel(TString txt, double x, double y, double texsize) {

  TLatex tex;
  tex.SetNDC();
  tex.SetTextFont(42);
  tex.SetTextColor(kBlack);
  tex.SetTextSize(texsize);
  tex.SetTextAlign(12);

  tex.DrawLatex(x,y,txt);
  tex.SetTextColor(kBlack);

  return;
}


// --------------------------------
// Steering functions
// --------------------------------

//------------------
// Nominal Unfolding
//------------------
void NominalUnfold()
{
  std::cout << "Entering NominalUnfold()" << std::endl;

  // This is the basic unfolding
  TH1D* result = (TH1D*)GetIDSUnfoldedSpectrum(gReco, gTrue, gMigMatrix, gData, gNIterations);
  //   TH1D* result = (TH1D*)GetIDSUnfoldedSpectrum(gReco,gTrue, gMigMatrix, gData, gNIterations);

  TH1D *matched_reco = gMigMatrix->ProjectionX("matched_reco");
  TH1D *matched_true = gMigMatrix->ProjectionY("matched_true");

  TString outputDir = "unfoldedFiles/systematic_";
  outputDir += jsyst;
  outputDir += "/";
  outputDir += dijetN;
  outputDir += "/";
  outputDir += "ybin_";
  outputDir += ybin;
  outputDir += "/";
  outputDir += "Udata2015_PythiaMC_newbin_withDS_v8.root";
  // outputDir += input_mc;

  TFile* UData = new TFile(outputDir,"recreate"); // Data
  result->SetName("UData");
  result->Write();
  gData->SetName("Data");
  gData->Write();
  gReco->SetName("MC_reco_all");
  gReco->Write();
  gTrue->SetName("MC_true_all");
  gTrue->Write();
  matched_reco->SetName("MC_reco_matched");
  matched_reco->Write();
  matched_true->SetName("MC_true_matched");
  matched_true->Write();
  UData->Close();

  Int_t toy = -1;

  Draw2TH1D(gTrue, result, "true", "Unfolded Data", "UReco_True_Nominal_Unfolding_dijets",toy,true,true);
  Draw2TH1D(gData, result, "Data", "Unfolded Data", "UReco_Reco_Nominal_Unfolding_dijets",toy,true,true);
  DrawTH2D(gMigMatrix,"NominalMigrationMatrix");

  std::cout << "Quitting NominalUnfold()" << std::endl;
}

TH2D* FillMigMatrix(const TH2DBootstrap *boot, const Int_t rep = -1)
{
  TH2D* Mig = NULL;

  if (rep >= 0) Mig = (TH2D*)boot->GetReplica(rep);
  else          Mig = (TH2D*)boot->GetNominal();

  return Mig;
}

TH1D* FillBSpectra(const TH1DBootstrap *boot, const Int_t rep = -1)
{
  TH1D* Histo = NULL;

  if (rep >= 0) Histo = (TH1D*)boot->GetReplica(rep);
  else          Histo = (TH1D*)boot->GetNominal();

  return Histo;
}

std::vector<double> ComputeMatchingEfficiency(const TH1D* Reco_, const TH1D* matchReco_)
{
  std::vector<double> Eff;

  for (Int_t i = 1; i <= gNBins; i++) {
    if( Reco_->GetBinContent(i)>0 ){
      Eff.push_back( matchReco_->GetBinContent(i)/Reco_->GetBinContent(i) );
    } else {
      Eff.push_back( 0 );
    }
  }

  return Eff;
}

TH1D* ApplyInefficiency(std::vector<double> Eff, const TH1D* Data)
{
  TH1D* MatchData = new TH1D(*Data);
  for (Int_t i = 1; i <= gNBins; i++) {
    MatchData->SetBinContent(i, MatchData->GetBinContent(i)*Eff.at(i-1));
    MatchData->SetBinError(i, MatchData->GetBinError(i)*Eff.at(i-1));
  }

  return MatchData;
}

void DoIDSUnfold(const TH1D* mdata, const TH2D* Mig, TVectorD *result1, TVectorD *result2)
{
  Int_t nbinsx = mdata->GetNbinsX();
  Int_t nbinsy = mdata->GetNbinsY();
  Int_t nbins  = nbinsx*nbinsy;

  /*
    std::cout << "gNBins = " << gNBins << std::endl;
    std::cout << "nbinsx = " << nbinsx << std::endl;
    std::cout << "nbinsy = " << nbinsy << std::endl;
    std::cout << "nbins = " << nbins << std::endl;
  */

  // Check
  if(nbins != gNBins) std::cout << "nbins != gNBins" << std::endl;

  //DrawTH1D(mdata,"mdata");
  //DrawTH2D(Mig,"Mig");

  // Put inputs into vectors
  TVectorD bdata(nbins), bdataErr(nbins);
  Int_t i = 0;
  for (Int_t by = 1; by <= nbinsy; ++by) { // loop over pt
    for (Int_t bx = 1; bx <= nbinsx; ++bx) { // loop over gap_size, for each pt value
      bdata[i]  = mdata->GetBinContent(bx, by);
      if (bdata[i] > 0.0) {
	bdataErr[i] = mdata->GetBinError(bx, by);
      } else {
	bdataErr[i] = 1.0;
      }
      i++;
    }
  }
  /*
    TH1D* prueba = new TH1D("prueba","",nbins,500,3000);
    for(Int_t i=0;i<nbins;++i){
    prueba->SetBinContent(i+1,bdata[i]);
    prueba->SetBinError(i+1,bdataErr[i]);
    }
    DrawTH1D(prueba,"prueba");
  */

  // Make transfer matrix
  TMatrixD Adet(nbins, nbins);
  for (Int_t i = 0; i < nbins; ++i) {
    for (Int_t j = 0; j < nbins; ++j) {
      Adet[i][j] = Mig->GetBinContent(i+1, j+1);
    }
  }

  Int_t NstepsOptMin = 1;
  Double_t lambdaL = 0.;
  Double_t lambdaUmin = 0.5;
  Double_t lambdaMmin = 0.0;
  Double_t lambdaS = 0.;

  PerformIterations(bdata, bdataErr, Adet, gNBins, lambdaL, NstepsOptMin, lambdaUmin, lambdaMmin, lambdaS, result1, result2);
  DrawTVectorD((*result2),"DoIDSResult");
}

void NewCorrectEfficiency(const std::vector<double> &eff, const TVectorD &unfoldedMatched, TVectorD *unfoldedAll)
{
  for (Int_t i = 0; i < gNBins; i++) {
    if (eff.at(i) > 0.0) {
      (*unfoldedAll)[i] = unfoldedMatched[i]/eff.at(i);
    } else {
      (*unfoldedAll)[i] = 0.0;
    }
  }
}

void DoBbBCorrection(const TH1D* bdata, const TH2D* Adet, TVectorD* result)
{
  // performs the bin to bin correction (for comparison only!)
  // compute the mc true and reco spectra and normalize them
  TH1D *recoMC = Adet->ProjectionX("recoMC");
  TH1D *trueMC = Adet->ProjectionY("recoMC");

  for (Int_t i = 0; i < gNBins; i++) {
    if (recoMC->GetBinContent(i+1) != 0.0) {
      (*result)[i] = bdata->GetBinContent(i+1)*trueMC->GetBinContent(i+1)/recoMC->GetBinContent(i+1);
    } else {
      (*result)[i] = 0.0;
    }
  }
}

void DoSVDUnfold(const TH1D* bdata_H, const TH1D* bini_H, const TH1D* xini_H, const TH2D* Adet_H, TVectorD *result, Int_t kreg = 6)
{
  TSVDUnfold *unfold = new TSVDUnfold(bdata_H, bini_H, xini_H, Adet_H);
  unfold->SetNormalize(kTRUE);
  TH1D *result_H = unfold->Unfold(kreg);

  result_H->Scale(bdata_H->GetSumOfWeights()/result_H->GetSumOfWeights());

  for (Int_t i = 0; i < gNBins; ++i) {
    (*result)[i] = result_H->GetBinContent(i+1);
  }

  SafeDelete(result_H);
  SafeDelete(unfold);
}

/*
  TH1DBootstrap* RemoveExtraBins(TH1DBootstrap &orig)
  {
  //TH1DBootstrap *rebin = orig.Rebin(gNBins - gMinMatrixBin - 1, orig.GetName(), gBinLimits + gMinMatrixBin); // original
  TH1DBootstrap *rebin = orig.Rebin(gNBins - gMinMatrixBin - 1, orig.GetName(), gBinLimits);  //Temporary FIXME
  return rebin;
  }
*/

//-------------
// Closure Test
//-------------
void ClosureTest()
{
  std::cout << "Inside ClosureTest()" << std::endl;

  // Get only the "matched" data (efficiency corrected)
  TH1D *matched_data = CorrectEfficiency(*gMigMatrix, *gReco, *gData);

  // Project the matched reco and true from migration matrix
  TH1D *matched_reco = gMigMatrix->ProjectionX("matched_reco");
  TH1D *matched_true = gMigMatrix->ProjectionY("matched_true");

  gNBins = gData->GetNbinsX(); //FIXME

  //  DrawTH1D(gData, "m12_data_inclusive"); //FIXME

  // Define a re-weighted shape (in theory to improve MC/data agreement)
  Int_t nbins = gMigMatrix->GetNbinsX();
  TH1D bias_shape(*matched_true);

  TH1DBootstrap TrueResult("TrueResult", "TrueResult", gNBins, gMassMin, gMassMax, gBData->GetNReplica());
  TH1DBootstrap TrueBias("TrueBias", "TrueBias", gNBins, gMassMin, gMassMax, gBData->GetNReplica());
  //TH1DBootstrap corrFactor("corr", "corr", gNBins, gMassMin, gMassMax, gBData->GetNReplica());
  TH1DBootstrap corrFactor("corr", "corr", gNBins, gMassMin, gMassMax, 1000);
  TH1F NominalVal("nom", "nomVal", gNBins, gMassMin, gMassMax);

  //Reference Function
  //   TF1 *f1 = new TF1("f1","[0] + [1]*(log(x)-log(x0)) + [2]*(log(x)-log(x0))^2 + [3]*(log(x)-log(x0))^3 + [4]*(log(x)-log(x0))^4 + ...", 100,xMax);

  TF1 *f1;

  //   if (ybin==0) {
  //
  //
  //     f1 = new TF1("f1","[0] + [1]*(log(x)-log(100)) + [2]*(log(x)-log(100))^2 + [3]*(log(x)-log(100))^3 + [4]*(log(x)-log(100))^4 + [5]*(log(x)-log(100))^5", 100,3937);
  //     f1->SetParameters(1.00e+00,2.03600e-01,-5.71656e-02,-5.06366e-02,3.21403e-02, -5.07156e-03); //y0 Pythia, x0 = 100, x^5, xMax=3937, binNorm=8
  // }
  // else if (ybin==1) {
  //     
  //     f1 = new TF1("f1","[0] + [1]*(log(x)-log(100)) + [2]*(log(x)-log(100))^2 + [3]*(log(x)-log(100))^3 + [4]*(log(x)-log(100))^4 + [5]*(log(x)-log(100))^5", 100,3937);
  //     f1->SetParameters(1.00e+00,1.12144e-01,1.29691e-01,-1.80572e-01,6.82430e-02,-7.96162e-03); //y1 Pythia, x0 = 100, x^5, xMax=3937, binNorm=8
  // }
  // else if (ybin==2) {
  //      f1 = new TF1("f1","[0] + [1]*(log(x)-log(100)) + [2]*(log(x)-log(100))^2 + [3]*(log(x)-log(100))^3 + [4]*(log(x)-log(100))^4 + [5]*(log(x)-log(100))^5", 100,2500);
  //      f1->SetParameters(1.00e+00,1.12752e-01,1.96142e-01,-3.42088e-01,1.72448e-01,-2.83322e-02); //y2 Pythia, x0 = 100, x^5, xMax=2500, binNorm=8
  // }
  // else if (ybin==3) {
  //
  //      f1 = new TF1("f1","[0] + [1]*(log(x)-log(92)) + [2]*(log(x)-log(92))^2 + [3]*(log(x)-log(92))^3 + [4]*(log(x)-log(92))^4 + [5]*(log(x)-log(92))^5 + [6]*(log(x)-log(92))^6", 100,1992);
  //        f1->SetParameters(1.00e+00,-3.34537e-01,2.05897e+00,-3.16620e+00,2.15278e+00,-6.68386e-01,7.66681e-02); //y3 Pythia, x0=92, x^6, xMax=1992, binNorm=9
  //   }
  //   else if (ybin==4) {
  //        f1 = new TF1("f1","[0] + [1]*(log(x)-log(116)) + [2]*(log(x)-log(116))^2 + [3]*(log(x)-log(116))^3 + [4]*(log(x)-log(116))^4", 100,1162);
  //        f1->SetParameters(1.00e+00,3.58666e-02,8.16764e-02,4.85436e-02,-4.80573e-02); //y4 Pythia, x0 = 116, x^4, binNorm=8, xMax = 1162
  //   }
  //   else if (ybin==5) {
  //        f1 = new TF1("f1","[0] + [1]*(log(x)-log(116)) + [2]*(log(x)-log(116))^2 + [3]*(log(x)-log(116))^3 + [4]*(log(x)-log(116))^4", 100,894);
  //        f1->SetParameters(1.00e+00,-1.14849e-01,4.90660e-01,-4.06802e-01,5.29130e-02); //y5 Pythia, x0 = 116, x^4, binNorm=10, xMax=894
  //   }

  if (ybin==0) {
    f1 = new TF1("f1","[0] + [1]*(log(x)-log(210)) + [2]*(log(x)-log(210))^2 + [3]*(log(x)-log(210))^3 + [4]*(log(x)-log(210))^4", 210,9066);
    f1->SetParameters(9.16408e-01,2.11654e-01,-1.22988e-01,4.11294e-02,-6.33751e-03); //y0 Pythia, x0 = 210, x^4, xMax=9066, binNorm=8
  }
  if (ybin==1) {
    f1 = new TF1("f1","[0] + [1]*(log(x)-log(210)) + [2]*(log(x)-log(210))^2 + [3]*(log(x)-log(210))^3 + [4]*(log(x)-log(210))^4", 210,9066);
    f1->SetParameters(9.61974e-01,8.85077e-02,3.85540e-02,-4.46868e-02,7.90482e-03); //y0 Pythia, x0 = 210, x^4, xMax=9066, binNorm=8
  }
  if (ybin==2) {
    f1 = new TF1("f1","[0] + [1]*(log(x)-log(210)) + [2]*(log(x)-log(210))^2 + [3]*(log(x)-log(210))^3 + [4]*(log(x)-log(210))^4", 210,9066);
    f1->SetParameters(1.02338e+00,-1.19295e-01,2.07378e-01,-1.05188e-01,1.48284e-02); //y0 Pythia, x0 = 210, x^4, xMax=9066, binNorm=8
  }
  if (ybin==3) {
    f1 = new TF1("f1","[0] + [1]*(log(x)-log(510)) + [2]*(log(x)-log(510))^2 + [3]*(log(x)-log(510))^3 + [4]*(log(x)-log(510))^4", 510,9066);
    f1->SetParameters(9.85694e-01,-2.10041e-01,4.13221e-01,-3.05351e-01,6.39286e-02); //y0 Pythia, x0 = 510, x^4, xMax=9066, binNorm=11
  }
  if (ybin==4) {
    f1 = new TF1("f1","[0] + [1]*(log(x)-log(850)) + [2]*(log(x)-log(850))^2 + [3]*(log(x)-log(850))^3 + [4]*(log(x)-log(850))^4", 850,9066);
    f1->SetParameters(1.01729e+00,-6.33953e-01,1.12359e+00,-7.83149e-01,1.65080e-01); //y0 Pythia, x0 = 850, x^4, xMax=9066, binNorm=15
  }
  if (ybin==5) {
    //f1 = new TF1("f1","[0] + [1]*(log(x)-log(1310)) + [2]*(log(x)-log(1310))^2 + [3]*(log(x)-log(1310))^3 + [4]*(log(x)-log(1310))^4", 1310,9066);
    f1 = new TF1("f1","[0] + [1]*(log(x)-log(1310)) + [2]*(log(x)-log(1310))^2+ [3]*(log(x)-log(1310))^3+ [4]*(log(x)-log(1310))^4+ [5]*(log(x)-log(1310))^5", 1310,9066);
    f1->SetParameters(1.13253e+00,-8.28753e-01,-2.35473e+00,8.11782e+00,-6.78044e+00,1.69239e+00); //y0 Pythia, x0 = 1310, x^4, xMax=9066, binNorm=19
  }

  for (Int_t i = 1; i <= nbins; ++i) {
    //    Double_t xpos = (matched_true->GetBinCenter(i) - matched_true->GetXaxis()->GetBinLowEdge(1)) / (matched_true->GetXaxis()->GetBinLowEdge(gNBins+1) - matched_true->GetXaxis()->GetBinLowEdge(1));
    //      cout<<"matched_trueBinLowEdge(i): "<<matched_true->GetBinLowEdge(i)<<"\t weight: "<<f1->Eval(bias_shape.GetXaxis()->GetBinCenter(i))<<endl;
      
    bias_shape.SetBinContent(i, matched_true->GetBinContent(i)*f1->Eval(bias_shape.GetXaxis()->GetBinCenter(i))); //inclusive

    //bias_shape.SetBinContent(i, matched_true->GetBinContent(i)*(0.95 + 2.00*xpos - 10.0*xpos*xpos)); //dijets
  }

  if(false){
    DrawTH1D(&bias_shape,"bias_shape");
    //  Draw2TH1D(matched_true,&bias_shape,"matched_true", "bias_shape","Matched_true_vs_bias_shape");
  }

  // Modify matrix to look like bias shape (data)
  //TH2D *bias_migmatrix = ModifyMatrix(*gMigMatrix, bias_shape);
  TH2D *bias_migmatrix = ModifyMatrix(*gMigMatrix, bias_shape);

  if(false) DrawTH2D(bias_migmatrix,"bias_migmatrix");

  // Project out modified reco spectra
  TH1D *bias_reco = bias_migmatrix->ProjectionX("bias_reco");
  TH1D *bias_true = bias_migmatrix->ProjectionY("bias_true");

  // Scale biased toy so that total events matches that seen in data
  double     factor2 = matched_data->GetSumOfWeights()/bias_reco->GetSumOfWeights();

  //    std::cout<<"toy: "<<toy<<std::endl;

  bias_reco->Scale(factor2);
  bias_true->Scale(factor2);

  // Give modified MC same errors as data
  for (Int_t bx = 1; bx <= nbins; bx++) {
    if (matched_data->GetBinContent(bx) > 0) {
      bias_reco->SetBinError(bx, bias_reco->GetBinContent(bx)*matched_data->GetBinError(bx)/matched_data->GetBinContent(bx));
    } else {
      bias_reco->SetBinContent(bx, 0.0);
      bias_reco->SetBinError(bx, 1.0);
    }
  }

  TH1D* result_nom = (TH1D*)GetIDSUnfoldedSpectrum(matched_reco, matched_true, gMigMatrix, bias_reco, gNIterations); //(unfolded data)

  for (Int_t i = 1; i <= nbins; ++i) {
    if (bias_true->GetBinContent(i) !=0)
      corrFactor.GetNominal()->SetBinContent(i, (result_nom->GetBinContent(i) - bias_true->GetBinContent(i))/bias_true->GetBinContent(i));
    //cout<<corrFactor.GetNominal()->GetBinContent(i)<<endl;
  }
  //    for (Int_t i = 1; i <= nbins; ++i) {

  //    cout<<"pTbin: "<< matched_data->GetBinLowEdge(i)<<"\t dataError: "<<matched_data->GetBinError(i)<<"\t recoError: "<<matched_reco->GetBinError(i)<<"\t biasRecoErr: "<<bias_reco->GetBinError(i)<<endl;

  //   bias_reco->SetBinError(i, matched_data->GetBinError(i)); //aprox
  //     }

  // Draw spectra ratio plots, useful for tuning bias function above
  // DrawSpectraRatioPlots(*matched_data, *bias_reco, *gReco, *gTrue, *matched_reco); //FIXME

  // Loop over toys for relative bias
  //Int_t ntoy = 1000; //FIXME Temporary
  Int_t ntoy = 100; 
  //Int_t ntoy = -1; 
  Printf("Looping over %d toys", ntoy);
  Double_t factor = 1.0;
  TH1D biasIDS(*gData);
  biasIDS.Reset();
  TH1D biasSVD(*gData);
  biasSVD.Reset();
  gIDSUData = new TH1D(*gData); 
  gSVDUData = new TH1D(*gData); 
  gRWTrue = new TH1D(*gData);
  gRWReco = new TH1D(*gData);
  gToyReco = new TH1D(*gData);
  gIDSUData->Reset();
  gSVDUData->Reset();
  gRWReco->Reset();
  gRWTrue->Reset();
  gToyReco->Reset();

  //for (Int_t toy = 0; toy < ntoy; ++toy) {
  for (Int_t toy = -1; toy < ntoy; ++toy) {
    //Printf("  Toy series 2, toy %d", toy);

    // Get fluctuated mig matrix
    //      TH2D *toy_migmatrix = FluctuateMatrix(*gMigMatrix);
    //      TH1D *toy_reco = toy_migmatrix->ProjectionX("toy_reco");
    //      TH1D *toy_true = toy_migmatrix->ProjectionY("toy_true");

    //	   cout<<"before"<<endl;
    TH2D *toy_migmatrix = FillMigMatrix(gBMigMatrix, toy);
    //     cout<<"after"<<endl;

    TH1D *toy_reco = toy_migmatrix->ProjectionX("toy_reco");
    TH1D *toy_true = toy_migmatrix->ProjectionY("toy_true");

    // Modify matrix to look like bias shape (data)
    //TH2D *toybias_migmatrix = ModifyMatrix(*toy_migmatrix, bias_shape);
    TH2D *toybias_migmatrix = ModifyMatrix(*toy_migmatrix, bias_shape);//FIXME
    TH1D *toybias_reco = toybias_migmatrix->ProjectionX("toybias_reco");
    TH1D *toybias_true = toybias_migmatrix->ProjectionY("toybias_true");

    if(false){
      DrawTH2D(toybias_migmatrix,"Modified_Fluctuated_migmatrix",toy);
      DrawTH1D(toybias_reco,"toybias_reco_before_Eff",toy);
      DrawTH1D(toybias_true,"toybias_true_before_Eff",toy);
    }

    // Scale biased toy so that total events matches that seen in data
    factor = matched_data->GetSumOfWeights()/toybias_reco->GetSumOfWeights();

    //    std::cout<<"toy: "<<toy<<std::endl;

    toybias_reco->Scale(factor);
    toybias_true->Scale(factor);

    // Give modified MC same errors as data
    for (Int_t bx = 1; bx <= nbins; bx++) {
      if (matched_data->GetBinContent(bx) > 0) {
	toybias_reco->SetBinError(bx, toybias_reco->GetBinContent(bx)*matched_data->GetBinError(bx)/matched_data->GetBinContent(bx));
      } else {
	toybias_reco->SetBinContent(bx, 0.0);
	toybias_reco->SetBinError(bx, 1.0);
      }
    }

    // Debbuging
    if(false){
      std::cout << "Before Unfolding data toy" << std::endl;
      DrawTH1D(toy_reco,"toy_reco",toy);
      DrawTH1D(toy_true,"toy_true",toy);
      DrawTH2D(toy_migmatrix,"toy_migmatrix",toy);
      DrawTH1D(toybias_reco,"toybias_reco",toy);
    }

    if (false && toy==0) {

      //Sanity check before doing the unfolding
      for (Int_t bx = 1; bx <= nbins; bx++) {

	std::cout<<"toy_reco: "<<toy_reco->GetBinLowEdge(bx)<<"\t"<<toy_reco->GetBinContent(bx)<<std::endl;
	std::cout<<"toy_biasreco: "<<toybias_reco->GetBinLowEdge(bx)<<"\t"<<toybias_reco->GetBinContent(bx)<<std::endl;
	std::cout<<"toy_true: "<<toy_true->GetBinLowEdge(bx)<<"\t"<<toy_true->GetBinContent(bx)<<std::endl;
      }
    }

    // Perform IDS, SVD, BbB unfolding 
    //TH1D* toy_result = (TH1D*)GetIDSUnfoldedSpectrum(toy_reco, toy_true, toy_migmatrix, toybias_reco, gNIterations); //(unfolded data)
    // Now using the nominal migration matrix instead of the modified fluctuated migration matrix
    //cout<<"before unfolding"<<endl;
    TH1D* toy_result = (TH1D*)GetIDSUnfoldedSpectrum(toy_reco, toy_true, gMigMatrix, toybias_reco, gNIterations); //(unfolded data)
    //cout<<"after unfolding"<<endl;

    for (Int_t i = 1; i <= gNBins; ++i) {
      //corrFactor.GetReplica(toy)->SetBinContent(i+1, resultIDS2Toy[i]);
      if (toybias_true->GetBinContent(i) != 0.0 && toy !=-1) {
	corrFactor.GetReplica(toy)->SetBinContent(i, (toy_result->GetBinContent(i) - toybias_true->GetBinContent(i))/toybias_true->GetBinContent(i));
	TrueResult.GetReplica(toy)->SetBinContent(i, (toy_result->GetBinContent(i)));
	TrueBias.GetReplica(toy)->SetBinContent(i, (toybias_true->GetBinContent(i)));
      }
      if (toybias_true->GetBinContent(i) != 0.0 && toy==-1) {
	corrFactor.GetNominal()->SetBinContent(i, (toy_result->GetBinContent(i) - toybias_true->GetBinContent(i))/toybias_true->GetBinContent(i));
	//   cout<<NominalVal.GetBinContent(i)<<endl;
      }

      // if (i==30){
      // cout<<corrFactor.GetReplica(toy)->GetBinContent(i)<<endl;
      // }
    }

    // std::cout << "================After IDS Unfolding Closure Test===============" << std::endl;
    //TSVDUnfold *tsvdunf = new TSVDUnfold(toybias_reco, toy_reco, toy_true, toy_migmatrix);
    // Now using the nominal migration matrix instead of the modified fluctuated migration matrix
    //      TSVDUnfold *tsvdunf = new TSVDUnfold(toybias_reco, toy_reco, toy_true, gMigMatrix); 
    //      TH1D* result = tsvdunf->Unfold(6.0); // use kreg = 6.0  (unfolded data)
    //      for (Int_t bx = 1; bx <= nbins; bx++) {
    //         if (toybias_true->GetBinContent(bx) != 0.0) {
    //	    biasIDS.SetBinContent(bx, biasIDS.GetBinContent(bx) + (toy_result->GetBinContent(bx) - toybias_true->GetBinContent(bx))/toybias_true->GetBinContent(bx)/ntoy);
    ////	    biasSVD.SetBinContent(bx, biasSVD.GetBinContent(bx) + (result->GetBinContent(bx) - toybias_true->GetBinContent(bx))/toybias_true->GetBinContent(bx)/ntoy);
    //         }
    //      }

    //if(true) std::cout << "After Unfolding data toy and after filling bias histograms" << std::endl;

    //      Int_t N_UData_IDS = toy_result->GetNbinsX(); 
    ////      Int_t N_UData_SVD = result->GetNbinsX(); 
    //      Int_t N_toybias_true = toybias_true->GetNbinsX(); 
    //      Int_t N_toybias_reco = toybias_reco->GetNbinsX(); 
    //
    //    //  if(m_Debug) Draw2TH1D(toybias_reco,result,"Reweighted Reco MC", "Unfolded Reweighted Reco MC","UReweighted_Reco_vs_Reweighted_Reco", toy);
    //
    //      if (N_UData_IDS != nbins) std::cout << "Error N_UData_IDS != nbins" << std::endl;
    ////      if (N_UData_SVD != nbins) std::cout << "Error N_UData_SVD != nbins" << std::endl;
    //      if (N_toybias_true != nbins) std::cout << "Error N_toybias_true != nbins" << std::endl;
    //      if (N_toybias_reco != nbins) std::cout << "Error N_toybias_reco != nbins" << std::endl;
    //
    //      for (Int_t bx = 1; bx<= nbins; bx++)
    //      {
    //      	gIDSUData->SetBinContent(bx, gIDSUData->GetBinContent(bx) + toy_result->GetBinContent(bx)/ntoy);	
    // //     	gSVDUData->SetBinContent(bx, gSVDUData->GetBinContent(bx) + result->GetBinContent(bx)/ntoy);	
    //      	gRWTrue->SetBinContent(bx, gRWTrue->GetBinContent(bx) + toybias_true->GetBinContent(bx)/ntoy);	
    //      	gRWReco->SetBinContent(bx, gRWReco->GetBinContent(bx) + toybias_reco->GetBinContent(bx)/ntoy);	
    //      	gToyReco->SetBinContent(bx, gToyReco->GetBinContent(bx) + toy_reco->GetBinContent(bx)/ntoy);	

    // }

    //     delete tsvdunf;

  }//End: toy loop

  //corrFactor.SetValBootstrapMean();
  //corrFactor.SetErrBootstrapRMS();

  TH1D *correction = (TH1D*)corrFactor.GetNominal();
  correction->GetXaxis()->Set(gNBins, &gBinLimits[0]);

  TH1D *true_res = (TH1D*)TrueResult.GetReplica(1);
  true_res->GetXaxis()->Set(gNBins, &gBinLimits[0]);

  TH1D *true_bias = (TH1D*)TrueBias.GetReplica(1);
  true_bias->GetXaxis()->Set(gNBins, &gBinLimits[0]);

  for (Int_t i = 1; i <= gNBins; ++i) {

    //correction->SetBinContent(i,NominalVal.GetBinContent(i));
    //   cout<<NominalVal.GetBinContent(i)<<endl;
    correction->SetBinContent(i,corrFactor.GetNominal()->GetBinContent(i));
    correction->SetBinError(i,corrFactor.GetNominal()->GetBinError(i));
  }

  // Save plots for relative bias toys
  //   DrawTH1D(&biasIDS,"biasIDS");

  TString outputDir = "biasIDSFiles/systematic_";
  outputDir += jsyst;
  outputDir += "/";
  outputDir += dijetN;
  outputDir += "/";
  outputDir += "ybin_";
  outputDir += ybin;
  outputDir += "/";
  outputDir += "testbiasIDS_average_v2.root";

  TFile bias_f(outputDir,"recreate");
  correction->SetName("biasIDS");
  correction->Write();

  true_res->SetName("TrueResult");
  ///true_res->Write();

  true_bias->SetName("TrueBias");
  //true_bias->Write();

  //    for (int rep=0; rep<ntoy;rep++) {

  // TH1D *correction_rep = (TH1D*)corrFactor.GetReplica(rep);
  // correction_rep->GetXaxis()->Set(gNBins, &gBinLimits[0]);
  // correction_rep->GetYaxis()->Set(100, -0.05, 0.05);

  // TString hname = "rep_";
  // hname += rep;

  // correction_rep->SetName(hname);
  // correction_rep->SetTitle("replicas");
  // correction_rep->Write();

  // }

  //DrawTH1D(&biasSVD,"biasSVD");
  //DrawRelBiasPlots(biasIDS,biasSVD);

  // Save plots for data ulfolded-truth re-weighted and ulfolded data-data comparison plots
  //   DrawComparisonPlots(*gIDSUData, *gSVDUData, *gData, *gRWTrue, *gRWReco, *matched_data);
  //   Draw2TH1D(gRWReco,gToyReco,"Reweighted Reco MC", "Reco MC","Reweighted_Reco_vs_Reco");
  //   Draw2TH1D(gRWReco,gIDSUData,"Reweighted Reco MC", "Unfolded Reweighted Reco MC","UReweighted_Reco_vs_Reweighted_Reco");

  std::cout << "Quitting ClosureTest()" << std::endl;
}

//----------------------------------
// Check NIterations of the Unfolding 
//----------------------------------
void CheckNIterations()
{
  // Temporary
  std::cout << "Inside CheckNIterations()" << std::endl;

  // Nominal Mig Matrix and Nominal Distributions
  TH2D* MigMatrix = new TH2D(*FillMigMatrix(gBMigMatrix));
  TH1D* Reco = new TH1D(*FillBSpectra(gBReco));
  TH1D* True = new TH1D(*FillBSpectra(gBTrue));
  TH1D* Data = new TH1D(*FillBSpectra(gBData));

  if(false){
    DrawTH2D(MigMatrix,"MigMatrix");
    DrawTH1D(Reco,"Reco");
    DrawTH1D(True,"True");
    DrawTH1D(Data,"Data");
  }

  // Variables for toy series
  Int_t gNtoy = 1000; //Temporary FIXME
  //std::vector<double> matchEffRecoToy;
  //std::vector<double> matchEffTrueToy;
  TH2D* migMatrixToy = NULL;
  TH1D* RecoToy = NULL;
  TH1D* TrueToy = NULL;
  TH1D* matchRecoToy = NULL;
  TH1D* matchTrueToy = NULL;
  TH1D* dataToy = NULL;
  TH1D* matchDataToy = NULL;

  //TH1D* resultBbBToy = NULL;
  //TH1D* resultSVDToy = NULL;
  TH1D* resultIDS1Toy = NULL;
  TH1D* resultIDS2Toy = NULL;
  TH1D* resultIDS3Toy = NULL;

  // Initialize vectors, matrices for rel errors
  //TVectorD avgBbB(gNBins);
  //TVectorD avgSVD(gNBins);
  //TVectorD avgIDS1(gNBins);
  TVectorD avgIDS2(gNBins);
  TVectorD avgIDS2_mc(gNBins);
  TVectorD avgIDS2_data(gNBins);

  //TMatrixD mToysBbB(gNtoy, gNBins);
  //TMatrixD mToysSVD(gNtoy, gNBins);
  //TMatrixD mToysIDS1(gNtoy, gNBins);
  TMatrixD mToysIDS2(gNtoy, gNBins);
  TMatrixD mToysIDS2_mc(gNtoy, gNBins);
  TMatrixD mToysIDS2_data(gNtoy, gNBins);

  //TH1DBootstrap corrFactor("corr", "corr", gNBins, gMassMin, gMassMax, gBData->GetNReplica()); 
  TH1DBootstrap corrFactor("corr", "corr", gNBins, gMassMin, gMassMax, gNtoy); 
  TH1DBootstrap corrFactor2("corr2", "corr2", gNBins, gMassMin, gMassMax, gNtoy); 
  // Start toys for covariance matrices
  //Printf("Looping over %d toys", gNtoy);
  for( Int_t toy=-1; toy<gNtoy; toy++ )
    {

      // Printf("  Toy series 1, toy %d", toy);

      if(m_Debug) std::cout << "Inside the toy loop" << std::endl;

      // Get mig matrix, fluctuating
      migMatrixToy = FillMigMatrix(gBMigMatrix, toy);
      RecoToy      = FillBSpectra(gBReco, toy);
      TrueToy      = FillBSpectra(gBTrue, toy);
      dataToy 	   = FillBSpectra(gBData, toy);

      if(m_Debug){
	DrawTH2D(MigMatrix,"MigMatrix",toy);
	DrawTH1D(RecoToy,"RecoToy",toy);
	DrawTH1D(TrueToy,"TrueToy",toy);
	DrawTH1D(dataToy,"dataToy",toy);
      }

      /////////
      //New Only fluctuate MC
      //resultIDS1Toy = (TH1D*)GetIDSUnfoldedSpectrum(Reco, True, MigMatrix, dataToy, gNIterations);
      //resultIDS2Toy = (TH1D*)GetIDSUnfoldedSpectrum(RecoToy, TrueToy, migMatrixToy, Data, gNIterations);
      resultIDS2Toy = (TH1D*)GetIDSUnfoldedSpectrum(RecoToy, TrueToy, migMatrixToy, dataToy, 1);
      resultIDS3Toy = (TH1D*)GetIDSUnfoldedSpectrum(RecoToy, TrueToy, migMatrixToy, dataToy, 2);

      //      if(gNBins != resultIDS2Toy->GetEntries()) std::cout << "ERROR: gNBins != resultIDS2Toy->GetEntries()" << std::endl;

      //      for (Int_t i = 0; i < gNBins; i++) {
      //         mToysIDS2_data[toy][i] = resultIDS1Toy->GetBinContent(i+1);
      //         avgIDS2_data[i] += resultIDS1Toy->GetBinContent(i+1)/gNtoy;

      //         mToysIDS2_mc[toy][i] = resultIDS2Toy->GetBinContent(i+1);
      //         avgIDS2_mc[i] += resultIDS2Toy->GetBinContent(i+1)/gNtoy;

      //         mToysIDS2[toy][i] = resultIDS3Toy->GetBinContent(i+1);
      //         avgIDS2[i] += resultIDS3Toy->GetBinContent(i+1)/gNtoy;
      //      }
      /////////

      //  if(true) DrawTH1D(resultIDS2Toy,"resultIDS2ToyMC",toy);

      for (Int_t i = 1; i <= gNBins; ++i) {
	//corrFactor.GetReplica(toy)->SetBinContent(i+1, resultIDS2Toy[i]);
	if (toy>=0) {
	  corrFactor.GetReplica(toy)->SetBinContent(i, resultIDS3Toy->GetBinContent(i));
	  corrFactor2.GetReplica(toy)->SetBinContent(i, resultIDS2Toy->GetBinContent(i));
	}
	else if (toy==-1) {

	  corrFactor.GetNominal()->SetBinContent(i, resultIDS3Toy->GetBinContent(i));
	  corrFactor2.GetNominal()->SetBinContent(i, resultIDS2Toy->GetBinContent(i));

	  // cout<<"Cont: "<<resultIDS2Toy->GetBinContent(i)<<endl;
	}
      }
      //if(true) std::cout << "After all the unfolding methods" << std::endl;

    }// END: toy loop

  if(false) std::cout << "Just after the toy loop" << std::endl;

  corrFactor.Divide(&corrFactor2);

  //  corrFactor.SetValBootstrapMean();
  //corrFactor.SetErrBootstrapRMS();
  corrFactor.SetErrBootstrapRMS();

  TH1D *correction = (TH1D*)corrFactor.GetNominal();
  correction->GetXaxis()->Set(gNBins, &gBinLimits[0]);
  correction->GetYaxis()->Set(100, -0.05, 0.05);

  TString outputDir = "StatUncFiles/systematic_";
  outputDir += jsyst;
  outputDir += "/";
  outputDir += dijetN;
  outputDir += "/";
  outputDir += "ybin_";
  outputDir += ybin;
  outputDir += "/";
  outputDir += "Udata2015_PythiaMC_checkNIter_v1.root"; 

  TFile* ferr = new TFile(outputDir,"recreate"); // Data
  correction->SetName("Nominal_AvgRep");
  correction->SetTitle("nominal");
  correction->Write();

  //   for (int rep=0; rep<gNtoy;rep++) {
  //
  //   TH1D *correction_rep = (TH1D*)corrFactor.GetReplica(rep);
  //   correction_rep->GetXaxis()->Set(gNBins, &gBinLimits[0]);
  //   correction_rep->GetYaxis()->Set(100, -0.05, 0.05);
  //
  //
  //   TString hname = "rep_";
  //   hname += rep;
  //
  //   correction_rep->SetName(hname);
  //   correction_rep->SetTitle("replicas");
  //   correction_rep->Write();
  //
  //   }
  ferr->Close();

}

//----------------------------------
// Covariance and Correlation matrix
//----------------------------------
void CovarianceMatrix()
{
  // Temporary
  std::cout << "Inside CovarianceMatrix()" << std::endl;

  // Nominal Mig Matrix and Nominal Distributions
  TH2D* MigMatrix = new TH2D(*FillMigMatrix(gBMigMatrix));
  TH1D* Reco = new TH1D(*FillBSpectra(gBReco));
  TH1D* True = new TH1D(*FillBSpectra(gBTrue));
  TH1D* Data = new TH1D(*FillBSpectra(gBData));

  if(false){
    DrawTH2D(MigMatrix,"MigMatrix");
    DrawTH1D(Reco,"Reco");
    DrawTH1D(True,"True");
    DrawTH1D(Data,"Data");
  }

  ///////////
  // NEW
  // Nominal IDS unfolding performed here
  TH1D* matchResultIDS2 = (TH1D*)GetIDSUnfoldedSpectrum(Reco, True, MigMatrix, Data, gNIterations);
  if(m_Debug) DrawTH1D(matchResultIDS2,"matchResultIDS2");
  ///////////

  // Variables for toy series
  Int_t gNtoy = 1000; //Temporary FIXME
  //std::vector<double> matchEffRecoToy;
  //std::vector<double> matchEffTrueToy;
  TH2D* migMatrixToy = NULL;
  TH1D* RecoToy = NULL;
  TH1D* TrueToy = NULL;
  TH1D* matchRecoToy = NULL;
  TH1D* matchTrueToy = NULL;
  TH1D* dataToy = NULL;
  TH1D* matchDataToy = NULL;

  //TH1D* resultBbBToy = NULL;
  //TH1D* resultSVDToy = NULL;
  TH1D* resultIDS1Toy = NULL;
  TH1D* resultIDS2Toy = NULL;
  TH1D* resultIDS3Toy = NULL;

  // Initialize vectors, matrices for rel errors
  //TVectorD avgBbB(gNBins);
  //TVectorD avgSVD(gNBins);
  //TVectorD avgIDS1(gNBins);
  TVectorD avgIDS2(gNBins);
  TVectorD avgIDS2_mc(gNBins);
  TVectorD avgIDS2_data(gNBins);

  //TMatrixD mToysBbB(gNtoy, gNBins);
  //TMatrixD mToysSVD(gNtoy, gNBins);
  //TMatrixD mToysIDS1(gNtoy, gNBins);
  TMatrixD mToysIDS2(gNtoy, gNBins);
  TMatrixD mToysIDS2_mc(gNtoy, gNBins);
  TMatrixD mToysIDS2_data(gNtoy, gNBins);

  float nominal_integral=0;

  //TH1DBootstrap corrFactor("corr", "corr", gNBins, gMassMin, gMassMax, gBData->GetNReplica()); 
  TH1DBootstrap corrFactor("corr", "corr", gNBins, gMassMin, gMassMax, gNtoy); 
  TH1DBootstrap data_rep("data_rep", "data_rep", gNBins, gMassMin, gMassMax, gNtoy); 
  // Start toys for covariance matrices
  //Printf("Looping over %d toys", gNtoy);
  for( Int_t toy=-1; toy<gNtoy; toy++ )
    {
      // Printf("  Toy series 1, toy %d", toy);

      if(m_Debug) std::cout << "Inside the toy loop" << std::endl;

      // Get mig matrix, fluctuating
      migMatrixToy = FillMigMatrix(gBMigMatrix, toy);
      RecoToy      = FillBSpectra(gBReco, toy);
      TrueToy      = FillBSpectra(gBTrue, toy);
      dataToy 	   = FillBSpectra(gBData, toy);


      if(m_Debug){
	DrawTH2D(MigMatrix,"MigMatrix",toy);
	DrawTH1D(RecoToy,"RecoToy",toy);
	DrawTH1D(TrueToy,"TrueToy",toy);
	DrawTH1D(dataToy,"dataToy",toy);
      }

      /////////
      //New Only fluctuate MC
      resultIDS1Toy = (TH1D*)GetIDSUnfoldedSpectrum(Reco, True, MigMatrix, dataToy, gNIterations);
      resultIDS2Toy = (TH1D*)GetIDSUnfoldedSpectrum(RecoToy, TrueToy, migMatrixToy, Data, gNIterations);
      resultIDS3Toy = (TH1D*)GetIDSUnfoldedSpectrum(RecoToy, TrueToy, migMatrixToy, dataToy, gNIterations);

      if (toy==-1) nominal_integral = resultIDS1Toy->Integral();

      //      if(gNBins != resultIDS2Toy->GetEntries()) std::cout << "ERROR: gNBins != resultIDS2Toy->GetEntries()" << std::endl;

      float Norm = nominal_integral/resultIDS1Toy->Integral();

      for (Int_t i = 1; i <= gNBins; ++i) {
	if (toy>=0) {
	  corrFactor.GetReplica(toy)->SetBinContent(i, resultIDS3Toy->GetBinContent(i));
	  data_rep.GetReplica(toy)->SetBinContent(i, dataToy->GetBinContent(i));

	  mToysIDS2_data[toy][i-1] = resultIDS1Toy->GetBinContent(i)*Norm;
	  avgIDS2_data[i-1] += resultIDS1Toy->GetBinContent(i)*Norm/gNtoy;

	  //    mToysIDS2_mc[toy][i-1] = resultIDS2Toy->GetBinContent(i)*Norm;
	  //    avgIDS2_mc[i-1] += resultIDS2Toy->GetBinContent(i)*Norm/gNtoy;

	  //    mToysIDS2[toy][i-1] = resultIDS3Toy->GetBinContent(i)*Norm;
	  //   avgIDS2[i-1] += resultIDS3Toy->GetBinContent(i)*Norm/gNtoy;
	}
	else if (toy==-1) {
	  corrFactor.GetNominal()->SetBinContent(i, resultIDS3Toy->GetBinContent(i));
	  data_rep.GetNominal()->SetBinContent(i, dataToy->GetBinContent(i));
	}
      }
    }// END: toy loop

  if(false) std::cout << "Just after the toy loop" << std::endl;

  //  corrFactor.Divide(gBData);
  //corrFactor.SetValBootstrapMean();
  //data_rep.SetValBootstrapMean();
  //corrFactor.SetErrBootstrapRMS();
  corrFactor.SetErrBootstrapRMS();
  data_rep.SetErrBootstrapRMS();

  TH1D *correction = (TH1D*)corrFactor.GetNominal();
  TH1D *dataErr = (TH1D*)data_rep.GetNominal();
  correction->GetXaxis()->Set(gNBins, &gBinLimits[0]);
  dataErr->GetXaxis()->Set(gNBins, &gBinLimits[0]);
  correction->GetYaxis()->Set(100, -0.05, 0.05);

  TString outputDir = "StatUncFiles/systematic_";
  outputDir += jsyst;
  outputDir += "/";
  outputDir += dijetN;
  outputDir += "/";
  outputDir += "ybin_";
  outputDir += ybin;
  outputDir += "/";
  //outputDir += "Udata2015_PythiaMC_newbin_withDS_v8.root"; 
  outputDir += "Udata2015_ids.root"; 

  TFile* ferr = new TFile(outputDir,"recreate"); // Data
  correction->SetName("Nominal_AvgRep");
  correction->SetTitle("nominal");
  correction->Write();
  dataErr->SetName("dataErr");
  dataErr->SetTitle("nominal");
  dataErr->Write();

  /*
    for (int rep=0; rep<gNtoy;rep++) {

    TH1D *correction_rep = (TH1D*)corrFactor.GetReplica(rep);
    correction_rep->GetXaxis()->Set(gNBins, &gBinLimits[0]);
    correction_rep->GetYaxis()->Set(100, -0.05, 0.05);


    TString hname = "rep_";
    hname += rep;

    correction_rep->SetName(hname);
    correction_rep->SetTitle("replicas");
    correction_rep->Write();
    }
  */
  ferr->Close();

  //   TCanvas c("c", "c", 600, 600);
  //   c.SetLogx();
  //   TH2D axis("axis", "axis", 1, gMassMin, gMassMax, 1, 0.8, 1.2);
  //   axis.SetYTitle("Equivalent correction");
  //   axis.SetXTitle("#font[52]{p}_{T} [GeV]");
  //   axis.GetXaxis()->SetMoreLogLabels();
  //   axis.Draw("axis");
  //   correction->Draw("pe same");

  //DrawLabel("anti-#font[52]{k_{t}}  jets, #font[52]{R} = 0.6",0.50,0.90,0.040);
  //DrawLabel("#sqrt{#font[52]{s}} = 7 TeV,  #scale[0.75]{#int}#font[52]{L} dt = 4.5 fb^{-1}",0.50,0.83,0.040);

  //   TString out = PATH_2;
  //   out += "/Plots/correction_R04_Eta.eps";
  //   c.SaveAs(out.Data());

  //TMatrixD covBbB(gNBins, gNBins);
  //TMatrixD covSVD(gNBins, gNBins);
  //TMatrixD covIDS1(gNBins, gNBins);

  TMatrixD covIDS2(gNBins, gNBins);
  TMatrixD covIDS2_mc(gNBins, gNBins);
  TMatrixD covIDS2_data(gNBins, gNBins);

  //  TH2D *covMat2d = (TH2D*)MigMatrix->Clone("covMat2d"); 
  TH2D *covMat2d_data = (TH2D*)MigMatrix->Clone("covMat2d_data"); 
  //  TH2D *covMat2d_mc = (TH2D*)MigMatrix->Clone("covMat2d_mc"); 

  TH2D *MigMat2d = (TH2D*)MigMatrix->Clone("MigMat2d"); 

  TH1D *covMat = (TH1D*)corrFactor.GetNominal();
  covMat->GetXaxis()->Set(gNBins, &gBinLimits[0]);
  covMat->GetYaxis()->Set(100, -0.05, 0.05);

  TString outputDir2 = "StatUncFiles/systematic_";
  outputDir2 += jsyst;
  outputDir2 += "/";
  outputDir2 += dijetN;
  outputDir2 += "/";
  outputDir2 += "ybin_";
  outputDir2 += ybin;
  outputDir2 += "/";
  outputDir2 += "covMat_test_MultiNomial_cont.root";

  TFile* fcov = new TFile(outputDir2,"recreate");

  // SVD, IDS1, IDS2 covariance matrix
  for (Int_t i = 0; i < gNBins; i++) {
    for (Int_t j = 0; j < gNBins; j++) {
      //for (Int_t j = i; j < gNBins; j++) {
      //covBbB[i][j] = 0.;
      //covSVD[i][j] = 0.;
      //covIDS1[i][j] = 0.;
      covIDS2[i][j] = 0.;
      //      covIDS2_mc[i][j] = 0.;
      covIDS2_data[i][j] = 0.;
      for (Int_t toy = 0; toy < gNtoy; toy++) {
	//covBbB[i][j] += (mToysBbB[toy][i] - avgBbB[i])*(mToysBbB[toy][j] - avgBbB[j])/gNtoy;
	//covSVD[i][j] += (mToysSVD[toy][i] - avgSVD[i])*(mToysSVD[toy][j] - avgSVD[j])/gNtoy;
	//covIDS1[i][j] += (mToysIDS1[toy][i] - avgIDS1[i])*(mToysIDS1[toy][j] - avgIDS1[j])/gNtoy;
	//      covIDS2[i][j] += (mToysIDS2[toy][i] - avgIDS2[i])*(mToysIDS2[toy][j] - avgIDS2[j])/gNtoy;
	//     covIDS2_mc[i][j] += (mToysIDS2_mc[toy][i] - avgIDS2_mc[i])*(mToysIDS2_mc[toy][j] - avgIDS2_mc[j])/gNtoy;
	covIDS2_data[i][j] += (mToysIDS2_data[toy][i] - avgIDS2_data[i])*(mToysIDS2_data[toy][j] - avgIDS2_data[j])/gNtoy;
      }
      //covBbB[j][i] = covBbB[i][j];
      //covSVD[j][i] = covSVD[i][j];
      //covIDS1[j][i] = covIDS1[i][j];
      // covIDS2[j][i] = covIDS2[i][j];
      //covIDS2_mc[j][i] = covIDS2_mc[i][j];
      //covIDS2_data[j][i] = covIDS2_data[i][j];

      //cout<<i<<"\t"<<j<<"\t"<<"covIDS2[i][j]: "<<covIDS2[i][j]<<endl;

      //     covMat2d->SetBinContent(i+1,j+1,covIDS2[i][j]);
      covMat2d_data->SetBinContent(i+1,j+1,covIDS2_data[i][j]);
      //   covMat2d_mc->SetBinContent(i+1,j+1,covIDS2_mc[i][j]);
    }
    covMat->SetBinContent(i+1,covIDS2[i][i]);
  }

  //  TH2D corrIDS2_H(*covMat2d);
  //   TH2D corrIDS2_H_mc(*covMat2d_mc);
  TH2D corrIDS2_H_data(*covMat2d_data);
  // corrIDS2_H.GetXaxis()->Set(gNBins, &gBinLimits[0]);
  // corrIDS2_H.GetYaxis()->Set(gNBins, &gBinLimits[0]);
  //  corrIDS2_H_mc.GetXaxis()->Set(gNBins, &gBinLimits[0]);
  //  corrIDS2_H_mc.GetYaxis()->Set(gNBins, &gBinLimits[0]);
  corrIDS2_H_data.GetXaxis()->Set(gNBins, &gBinLimits[0]);
  corrIDS2_H_data.GetYaxis()->Set(gNBins, &gBinLimits[0]);
  for(int i=1;i<=gNBins;++i){
    for(int j=1;j<=gNBins;++j){
      //   if (sqrt(covMat2d->GetBinContent(i,i)*covMat2d->GetBinContent(j,j)) !=0)
      //   	    corrIDS2_H.SetBinContent( i, j, covMat2d->GetBinContent(i,j)/sqrt(covMat2d->GetBinContent(i,i)*covMat2d->GetBinContent(j,j)) );
      //   else
      //       corrIDS2_H.SetBinContent( i, j, 0 );

      //  if (sqrt(covMat2d_mc->GetBinContent(i,i)*covMat2d_mc->GetBinContent(j,j)) !=0)
      //  	    corrIDS2_H_mc.SetBinContent( i, j, covMat2d_mc->GetBinContent(i,j)/sqrt(covMat2d_mc->GetBinContent(i,i)*covMat2d_mc->GetBinContent(j,j)) );
      //  else
      //      corrIDS2_H_mc.SetBinContent( i, j, 0 );

      if (sqrt(covMat2d_data->GetBinContent(i,i)*covMat2d_data->GetBinContent(j,j)) !=0)
	corrIDS2_H_data.SetBinContent( i, j, covMat2d_data->GetBinContent(i,j)/sqrt(covMat2d_data->GetBinContent(i,i)*covMat2d_data->GetBinContent(j,j)) );
      else
	corrIDS2_H_data.SetBinContent( i, j, 0 );
    }
  }

  fcov->cd();
  //   covMat->SetName("covMat1d");
  //   covMat->Write();
  //  covMat2d->SetName("covMat2d");
  //  covMat2d->Write();
  // corrIDS2_H.SetName("corrMat");
  /// corrIDS2_H.Write();

  //  corrIDS2_H_mc.SetName("corrMat_mc");
  //  corrIDS2_H_mc.Write();

  corrIDS2_H_data.SetName("corrMat_data");
  corrIDS2_H_data.Write();

  MigMat2d->SetName("MigMat");
  MigMat2d->Write();

  fcov->Close();

  if(false) std::cout << "Just before DrawRelEffPlots" << std::endl;

  if(false){
    DrawTH1D(Data,"Data");
  }

  // Draw relative error plots for all methods
  //DrawRelErrPlots(*Data, covIDS2_mc, avgIDS2_mc, covIDS2_data, avgIDS2_data, covIDS2, avgIDS2); //FIXME Temporary
  //   DrawRelErrPlots2(*Data, covIDS2_mc, avgIDS2_mc, covIDS2_data, avgIDS2_data, covIDS2, avgIDS2);
  // DrawRelErrPlots(*Data, covIDS2_mc, avgIDS2_mc, covIDS2_mc, avgIDS2_mc, covIDS2_mc, avgIDS2_mc);
  //   my_DrawRelErrPlots(*Data, covIDS2_mc, avgIDS2_mc); //FIXME Temporary
  //DrawRelErrPlots(data, dataErr, covSVD, avgSVD, covIDS1, avgIDS1, covIDS2, avgIDS2);
  /*
    TString out = "/afs/cern.ch/user/g/gmarceca/WORK/Analysis_SM/Unfolding/Unfolding/dijets_inclusive_v7/Results_IDS/systematic_";
    out += jsyst;
    out += "/";
    out += dijetN;
    out += "/";
    out += "ybin_";
    out += ybin;
    out += "/";
    out += "cov_IDSresult_R04_Eta.root";

    TFile fRoot(out.Data(), "RECREATE");

    if(m_Debug) std::cout << "After open a TFile" << std::endl;

    out = "IDSresult_covMC_R04_Eta";
    TH2D covIDS2_mc_H(covIDS2_mc);
    covIDS2_mc_H.GetXaxis()->Set(gNBins, &gBinLimits[0]);
    covIDS2_mc_H.GetYaxis()->Set(gNBins, &gBinLimits[0]);
    covIDS2_mc_H.SetName(out.Data());
    covIDS2_mc_H.SetTitle(out.Data());
    covIDS2_mc_H.Write();

    out = "IDSresult_corrMC_R04_Eta";
    TH2D corrIDS2_mc_H(covIDS2_mc);
    corrIDS2_mc_H.GetXaxis()->Set(gNBins, &gBinLimits[0]);
    corrIDS2_mc_H.GetYaxis()->Set(gNBins, &gBinLimits[0]);
    for(int i=1;i<=gNBins;++i){
    for(int j=1;j<=gNBins;++j){
    corrIDS2_mc_H.SetBinContent( i, j, covIDS2_mc_H.GetBinContent(i,j)/sqrt(covIDS2_mc_H.GetBinContent(i,i)*covIDS2_mc_H.GetBinContent(j,j)) );
    }
    }
    corrIDS2_mc_H.SetName(out.Data());
    corrIDS2_mc_H.SetTitle(out.Data());
    corrIDS2_mc_H.Write();

    DrawTH2D(&corrIDS2_mc_H,"CorrelationMatrix");
    DrawTH2D(&covIDS2_mc_H,"CovarianceMatrix");
  */
  /*
    out = "IDSresult_covData_R04_Eta";
    TH2D covIDS2_data_H(covIDS2_data);
    covIDS2_data_H.GetXaxis()->Set(gNBins, gBinLimits);
    covIDS2_data_H.GetYaxis()->Set(gNBins, gBinLimits);
    covIDS2_data_H.SetName(out.Data());
    covIDS2_data_H.SetTitle(out.Data());
    covIDS2_data_H.Write();

    out = "IDSresult_cov_R04_Eta";
    TH2D covIDS2_H(covIDS2);
    covIDS2_H.GetXaxis()->Set(gNBins, gBinLimits);
    covIDS2_H.GetYaxis()->Set(gNBins, gBinLimits);
    covIDS2_H.SetName(out.Data());
    covIDS2_H.SetTitle(out.Data());
    covIDS2_H.Write();
  */
  if(m_Debug) std::cout << "Before make bootstrap of unfolded spectra" << std::endl;
  /*
  // Make bootstrap of unfolded spectra
  TH1D resultIDS2_H(resultIDS2);
  resultIDS2_H.GetXaxis()->Set(gNBins, gBinLimits);

  std::cout << "After make bootstrap of unfolded spectra" << std::endl;

  TVectorD **replicas_V = new TVectorD*[gNtoy];
  for (Int_t toy = 0; toy < gNtoy; ++toy) {
  replicas_V[toy] = new TVectorD(gNBins);
  for (Int_t i = 0; i < gNBins; ++i) {
  (*replicas_V[toy])[i] = mToysIDS2[toy][i];
  }
  }

  TH1D **replicas_H = new TH1D*[gNtoy];
  for (Int_t toy = 0; toy < gNtoy; ++toy) {
  replicas_H[toy] = new TH1D(*replicas_V[toy]);
  replicas_H[toy]->GetXaxis()->Set(gNBins, gBinLimits);
  }

  std::cout << "After create replicas" << std::endl;

  out = "IDSresult_bootstrap_R04_Eta";
  TH1DBootstrap result(out.Data(), out.Data(), &resultIDS2_H, replicas_H, gNtoy);

  //TH1DBootstrap *finalResult = RemoveExtraBins(result); //Temporary //FIXME
  //finalResult->Write();
  //result.Write();// Temporary FIXME

  */
  //   fRoot.Close();

  if(m_Debug) std::cout << "After close the TFile" << std::endl;

  // SafeDelete(finalResult); // Once written to file, closing the file deletes it already
  //for (Int_t i = 0; i < gNtoy; ++i) {
  //   SafeDelete(replicas_H[i]);
  //   SafeDelete(replicas_V[i]);
  //}

  //std::cout << "After SafeDelete of replicas" << std::endl;

  //delete [] replicas_H;
  //delete [] replicas_V;

  // Temporary
  std::cout << "Quitting CovarianceMatrix()" << std::endl;
}

//---------------------
// Relative Uncertainty
//---------------------
void RelativeUncertainty()
{
  std::cout << "Entering RelativeUncertainty()" << std::endl;

  //-------------------------------------------
  // Getting migration matrix and distributions
  //-------------------------------------------
  // Nominal histograms
  TH2D* MigMatrixNom = NULL;
  TH1D* RecoNom = NULL;
  TH1D* TrueNom = NULL;
  TH1D* DataNom = NULL;
  TH2D* MigMatrixJESNom = NULL;
  TH1D* RecoJESNom = NULL;
  TH1D* TrueJESNom = NULL;
  TH1D* DataJESNom = NULL;

  // Nominal case
  MigMatrixNom = FillMigMatrix(gBMigMatrix);
  RecoNom      = FillBSpectra(gBReco);
  TrueNom      = FillBSpectra(gBTrue);
  DataNom 	= FillBSpectra(gBData);

  // JES case
  MigMatrixJESNom = FillMigMatrix(gBJESMigMatrix);
  RecoJESNom      = FillBSpectra(gBJESReco);
  TrueJESNom      = FillBSpectra(gBJESTrue);
  DataJESNom      = FillBSpectra(gBJESData);

  //----------------------
  // Nominal IDS unfolding
  //----------------------
  // Nominal case
  TH1D* resultIDSNom = (TH1D*)GetIDSUnfoldedSpectrum(RecoNom, TrueNom, MigMatrixNom, DataNom, gNIterations);
  // JES case
  TH1D* resultIDSJES = (TH1D*)GetIDSUnfoldedSpectrum(RecoJESNom, TrueJESNom, MigMatrixJESNom, DataJESNom, gNIterations);

  // Variables for toy series
  Int_t gNtoy = 1000;
  TH2D* MigMatrixToy = NULL;
  TH1D* RecoToy = NULL;
  TH1D* TrueToy = NULL;
  TH1D* DataToy = NULL;
  TH2D* MigMatrixJESToy = NULL;
  TH1D* RecoJESToy = NULL;
  TH1D* TrueJESToy = NULL;
  TH1D* DataJESToy = NULL;

  TH1D* resultIDSNomToy = NULL;
  TH1D* resultIDSJESToy = NULL;

  // Initialize vectors, matrices
  TMatrixD mToysIDSNom(gNtoy, gNBins);
  TVectorD avgIDSNom(gNBins);
  TMatrixD mToysIDSJES(gNtoy, gNBins);
  TVectorD avgIDSJES(gNBins);

  //--------------------
  // Start replicas loop
  //--------------------
  Printf("Looping over %d toys", gNtoy);
  for( Int_t toy=0; toy<gNtoy; toy++ ){

    Printf("  Toy series, toy %d", toy);

    //-------------------------------------------
    // Getting migration matrix and distributions
    //-------------------------------------------
    // Nominal
    MigMatrixToy = FillMigMatrix(gBMigMatrix, toy);
    RecoToy      = FillBSpectra(gBReco, toy);
    TrueToy      = FillBSpectra(gBTrue, toy);
    DataToy 	   = FillBSpectra(gBData, toy);

    // JES
    MigMatrixJESToy = FillMigMatrix(gBJESMigMatrix, toy);
    RecoJESToy      = FillBSpectra(gBJESReco, toy);
    TrueJESToy      = FillBSpectra(gBJESTrue, toy);
    DataJESToy      = FillBSpectra(gBJESData, toy);

    //----------------------
    // Unfolding Pseudo-Data
    //----------------------
    // Nominal
    resultIDSNomToy = (TH1D*)GetIDSUnfoldedSpectrum(RecoToy, TrueToy, MigMatrixToy, DataToy, gNIterations);
    for (Int_t i = 0; i < gNBins; i++) {
      mToysIDSNom[toy][i] = resultIDSNomToy->GetBinContent(i+1);
      avgIDSNom[i] += resultIDSNomToy->GetBinContent(i+1)/gNtoy;
    }
    // Nominal
    resultIDSJESToy = (TH1D*)GetIDSUnfoldedSpectrum(RecoJESToy, TrueJESToy, MigMatrixJESToy, DataJESToy, gNIterations);
    for (Int_t i = 0; i < gNBins; i++) {
      mToysIDSJES[toy][i] = resultIDSJESToy->GetBinContent(i+1);
      avgIDSJES[i] += resultIDSJESToy->GetBinContent(i+1)/gNtoy;
    }
  }// END: toy loop

  if(m_Debug) std::cout << "Toy loop finished" << std::endl;

  //------------------------------------------
  // Making bootstrap of the unfolded spectrum
  //------------------------------------------
  if(m_Debug) std::cout << "Making bootstrap of unfolded spectrum" << std::endl;

  // Nominal histograms
  // Nominal case
  TH1D resultIDSNom_H(*resultIDSNom);
  resultIDSNom_H.GetXaxis()->Set(gNBins, &gBinLimits[0]);
  // JES case
  TH1D resultIDSJES_H(*resultIDSJES);
  resultIDSJES_H.GetXaxis()->Set(gNBins, &gBinLimits[0]);

  // Replicas
  // Nominal case
  TVectorD **replicas_Nom_V = new TVectorD*[gNtoy];
  for (Int_t toy = 0; toy < gNtoy; ++toy) {
    replicas_Nom_V[toy] = new TVectorD(gNBins);
    for (Int_t i = 0; i < gNBins; ++i) {
      (*replicas_Nom_V[toy])[i] = mToysIDSNom[toy][i];
    }
  }
  TH1D **replicas_Nom_H = new TH1D*[gNtoy];
  for (Int_t toy = 0; toy < gNtoy; ++toy) {
    replicas_Nom_H[toy] = new TH1D(*replicas_Nom_V[toy]);
    replicas_Nom_H[toy]->GetXaxis()->Set(gNBins, &gBinLimits[0]);
  }

  // JES case
  TVectorD **replicas_JES_V = new TVectorD*[gNtoy];
  for (Int_t toy = 0; toy < gNtoy; ++toy) {
    replicas_JES_V[toy] = new TVectorD(gNBins);
    for (Int_t i = 0; i < gNBins; ++i) {
      (*replicas_JES_V[toy])[i] = mToysIDSJES[toy][i];
    }
  }
  TH1D **replicas_JES_H = new TH1D*[gNtoy];
  for (Int_t toy = 0; toy < gNtoy; ++toy) {
    replicas_JES_H[toy] = new TH1D(*replicas_JES_V[toy]);
    replicas_JES_H[toy]->GetXaxis()->Set(gNBins, &gBinLimits[0]);
  }

  TString Diffname = "DiffUnfoldedspectrum";
  TString Nomname = "NomUnfoldedspectra";
  TH1DBootstrap* resultNom = new TH1DBootstrap(Nomname.Data(), Nomname.Data(), &resultIDSNom_H, replicas_Nom_H, gNtoy);
  TH1DBootstrap* resultDiff = new TH1DBootstrap(Diffname.Data(), Diffname.Data(), &resultIDSJES_H, replicas_JES_H, gNtoy);

  // Making a bootstrap for the difference
  resultDiff->Add(resultNom,-1);

  // Relative Uncertainty 
  /*
    TH1D* h_RelUnc = new TH1D("h_RelUnc","Relative uncertainty",gNBins,gBinLimits);
    for(int i=0;i<gNBins;++i){
    h_RelUnc->SetBinContent(i,resultDiff->GetBootstrapMean(i));
    }
    DrawTH1D(h_RelUnc,"Relative_Uncertainty");*/

  resultDiff->SetErrBootstrapRMS();
  resultDiff->Divide(resultNom);

  // Saving the histogram into a ROOT file
  TFile* tout = new TFile("RelativeUncertainty.root","recreate");
  resultDiff->GetNominal()->Write();
  tout->Close();

  std::cout << "Quitting RelativeUncertainty()" << std::endl;
}//END: RelativeUncertainty()

//---------------------
// Purity and Efficiency
//---------------------
void PurityEff()
{
  std::cout << "Entering PurityEff()" << std::endl;

  //-------------------------------------------
  // Getting migration matrix and distributions
  //-------------------------------------------
  // Nominal histograms
  TH2D* MigMatrixNom = NULL;
  TH1D* RecoNom = NULL;
  TH1D* TrueNom = NULL;
  TH1D* DataNom = NULL;

  // Nominal case
  MigMatrixNom = FillMigMatrix(gBMigMatrix);
  RecoNom      = FillBSpectra(gBReco);
  TrueNom      = FillBSpectra(gBTrue);
  DataNom 	= FillBSpectra(gBData);

  TH1D *h_purity = new TH1D(*RecoNom);
  TH1D *h_efficiency = new TH1D(*TrueNom);

  TH2D *MigMat2d = (TH2D*)MigMatrixNom->Clone("MigMat2d");

  h_purity->GetXaxis()->Set(gNBins, &gBinLimits[0]);
  h_purity->GetYaxis()->SetRangeUser(0, 100);
  h_efficiency->GetXaxis()->Set(gNBins, &gBinLimits[0]);
  h_efficiency->GetYaxis()->SetRangeUser(0, 100);

  std::cout << "MigMatrixNom->GetEntries() = " << MigMatrixNom->GetEntries() << std::endl;
  std::cout << "RecoNom->GetEntries() = " << RecoNom->GetEntries() << std::endl;
  std::cout << "TrueNom->GetEntries() = " << TrueNom->GetEntries() << std::endl;

  // DrawTH2D(MigMatrixNom,"MigrationMatrixTest");

  for(int j=0; j<gNBins; j++){	 
    double sum = 0;
    for(int i=0; i<gNBins; i++){
      sum += MigMatrixNom->GetBinContent(i,j);
    }
    //  std::cout << "PuritySasha = " << MigMatrixNom->GetBinContent(j,j) / sum << std::endl;
    // Purity I should use the proyection (reco)
    std::cout<< "bin: "<< RecoNom->GetBinLowEdge(j)<< " Purity = " << ( MigMatrixNom->GetBinContent(j,j) / RecoNom->GetBinContent(j) ) * 100 << std::endl;
    if (RecoNom->GetBinContent(j) != 0)
      h_purity->SetBinContent(j, ( MigMatrixNom->GetBinContent(j,j) / RecoNom->GetBinContent(j) ) * 100);
    // Eff (Stability) I should use the proyection (truth)
    std::cout <<"bin: "<<TrueNom->GetBinLowEdge(j)<< " Eff = " << (MigMatrixNom->GetBinContent(j,j) / TrueNom->GetBinContent(j)) *100 << std::endl;
    if (TrueNom->GetBinContent(j) !=0 )
      h_efficiency->SetBinContent(j, ( MigMatrixNom->GetBinContent(j,j) / TrueNom->GetBinContent(j) ) * 100);
  }

  DrawTH1D(h_purity,"Purity");
  DrawTH1D(h_efficiency,"Efficiency");

  //   TFile bias_f("purityEff.root","recreate");
  //   h_purity->SetName("purity");
  //   h_purity->Write();
  //   h_efficiency->SetName("efficiency");
  //   h_efficiency->Write();

  TH1DBootstrap purity_rep("purity_rep", "shifted", gNBins, gMassMin, gMassMax, gBData->GetNReplica()); 
  TH1DBootstrap efficiency_rep("efficiency_rep", "nominal", gNBins, gMassMin, gMassMax, gBData->GetNReplica());

  // Variables for toy series
  Int_t gNtoy = 1000;
  TH2D* MigMatrixToy = NULL;
  TH1D* RecoToy = NULL;
  TH1D* TrueToy = NULL;
  TH1D* DataToy = NULL;

  //--------------------
  // Start replicas loop
  //--------------------
  Printf("Looping over %d toys", gNtoy);
  for( Int_t toy=0; toy<gNtoy; toy++ ){

    Printf("  Toy series, toy %d", toy);

    //-------------------------------------------
    // Getting migration matrix and distributions
    //-------------------------------------------
    // Nominal
    MigMatrixToy = FillMigMatrix(gBMigMatrix, toy);
    RecoToy      = FillBSpectra(gBReco, toy);
    TrueToy      = FillBSpectra(gBTrue, toy);
    DataToy 	   = FillBSpectra(gBData, toy);

    TH1D *proj_trueToy = (TH1D*)MigMatrixToy->ProjectionY("proj_trueToy");
    TH1D *proj_recoToy = (TH1D*)MigMatrixToy->ProjectionX("proj_recoToy");

    //TH1D *h_purity_with_err = new TH1D(*RecoNom);
    //TH1D *h_efficiency_with_err = new TH1D(*TrueNom);

    //h_purity_with_err->GetXaxis()->Set(gNBins, &gBinLimits[0]);
    //h_purity_with_err->GetYaxis()->SetRangeUser(0, 100);
    //h_efficiency_with_err->GetXaxis()->Set(gNBins, &gBinLimits[0]);
    //h_efficiency_with_err->GetYaxis()->SetRangeUser(0, 100);

    for(int j=1; j<=gNBins; j++){	 
      double sum = 0;

      cout<<"jotas: "<<j<<endl;

      for(int i=1; i<=gNBins; i++){
	sum += MigMatrixNom->GetBinContent(i,j);
      }
      //  std::cout << "PuritySasha = " << MigMatrixNom->GetBinContent(j,j) / sum << std::endl;
      // Purity I should use the proyection (reco)
      //  std::cout<< "bin: "<< RecoToy->GetBinLowEdge(j)<< " Purity = " << ( MigMatrixToy->GetBinContent(j,j) / RecoToy->GetBinContent(j) ) * 100 << std::endl;
      if (RecoToy->GetBinContent(j) != 0) {
	purity_rep.GetReplica(toy)->SetBinContent(j, ( proj_recoToy->GetBinContent(j) / RecoToy->GetBinContent(j) ) * 100);
	//  cout<<"purity: "<<proj_recoToy->GetBinContent(j) / RecoToy->GetBinContent(j)<<endl;
      }
      //purity_rep.GetReplica(toy)->SetBinContent(j, ( MigMatrixToy->GetBinContent(j,j) / RecoToy->GetBinContent(j) ) * 100);
      //   h_purity->SetBinContent(j, ( MigMatrixToy->GetBinContent(j,j) / RecoToy->GetBinContent(j) ) * 100);
      // Eff (Stability) I should use the proyection (truth)
      //  std::cout <<"bin: "<<TrueToy->GetBinLowEdge(j)<< " Eff = " << (MigMatrixToy->GetBinContent(j,j) / TrueToy->GetBinContent(j)) *100 << std::endl;
      if (TrueToy->GetBinContent(j) !=0 ) {
	efficiency_rep.GetReplica(toy)->SetBinContent(j, ( proj_trueToy->GetBinContent(j) / TrueToy->GetBinContent(j) ) * 100);
      }
      //efficiency_rep.GetReplica(toy)->SetBinContent(j, ( MigMatrixToy->GetBinContent(j,j) / TrueToy->GetBinContent(j) ) * 100);
      //  h_efficiency->SetBinContent(j, ( MigMatrixNom->GetBinContent(j,j) / TrueNom->GetBinContent(j) ) * 100);
    }
  }

  purity_rep.SetValBootstrapMean();
  efficiency_rep.SetValBootstrapMean();
  purity_rep.SetErrBootstrapRMS();
  efficiency_rep.SetErrBootstrapRMS();

  TH1D *h_purity_with_err = (TH1D*)purity_rep.GetNominal();
  TH1D *h_efficiency_with_err = (TH1D*)efficiency_rep.GetNominal();

  //h_efficiency_with_err->SetBinError(gNBins,h_purity_with_err->GetBinError(gNBins));

  //cout<<"Error: "<<h_efficiency_with_err->GetBinError(gNBins)<<endl;

  h_purity_with_err->GetXaxis()->Set(gNBins, &gBinLimits[0]);
  h_purity_with_err->GetYaxis()->SetRangeUser(0, 100);
  h_efficiency_with_err->GetXaxis()->Set(gNBins, &gBinLimits[0]);
  h_efficiency_with_err->GetYaxis()->SetRangeUser(0, 100);

  TString outString = "purityEff_wErr_";

  outString += jsyst;
  outString += "_ybin_";
  outString += ybin;
  outString += ".root"; 

  TFile bias_f(outString,"recreate");

  h_purity->SetName("purity");
  h_purity->Write();
  h_efficiency->SetName("efficiency");
  h_efficiency->Write();
  h_purity_with_err->SetName("purity_wErr");
  h_efficiency_with_err->SetName("efficiency_wErr");
  h_purity_with_err->Write();
  h_efficiency_with_err->Write();
  MigMat2d->SetName("MigMat");
  MigMat2d->Write();

  /*
  // Variables for toy series
  Int_t gNtoy = 100;
  TH2D* MigMatrixToy = NULL;
  TH1D* RecoToy = NULL;
  TH1D* TrueToy = NULL;
  TH1D* DataToy = NULL;

  //--------------------
  // Start replicas loop
  //--------------------
  Printf("Looping over %d toys", gNtoy);
  for( Int_t toy=0; toy<gNtoy; toy++ ){

  Printf("  Toy series, toy %d", toy);

  //-------------------------------------------
  // Getting migration matrix and distributions
  //-------------------------------------------
  // Nominal
  MigMatrixToy = FillMigMatrix(gBMigMatrix, toy);
  RecoToy      = FillBSpectra(gBReco, toy);
  TrueToy      = FillBSpectra(gBTrue, toy);
  DataToy 	   = FillBSpectra(gBData, toy);

  //----------------------
  // Unfolding Pseudo-Data
  //----------------------
  // Nominal
  resultIDSNomToy = (TH1D*)GetIDSUnfoldedSpectrum(RecoToy, TrueToy, MigMatrixToy, DataToy, gNIterations);
  for (Int_t i = 0; i < gNBins; i++) {
  mToysIDSNom[toy][i] = resultIDSNomToy->GetBinContent(i+1);
  avgIDSNom[i] += resultIDSNomToy->GetBinContent(i+1)/gNtoy;
  }

  }// END: toy loop

  if(m_Debug) std::cout << "Toy loop finished" << std::endl;

  //------------------------------------------
  // Making bootstrap of the unfolded spectrum
  //------------------------------------------
  if(m_Debug) std::cout << "Making bootstrap of unfolded spectrum" << std::endl;

  // Nominal histograms
  // Nominal case
  TH1D resultIDSNom_H(*resultIDSNom);
  resultIDSNom_H.GetXaxis()->Set(gNBins, gBinLimits);

  // Replicas
  // Nominal case
  TVectorD **replicas_Nom_V = new TVectorD*[gNtoy];
  for (Int_t toy = 0; toy < gNtoy; ++toy) {
  replicas_Nom_V[toy] = new TVectorD(gNBins);
  for (Int_t i = 0; i < gNBins; ++i) {
  (*replicas_Nom_V[toy])[i] = mToysIDSNom[toy][i];
  }
  }
  TH1D **replicas_Nom_H = new TH1D*[gNtoy];
  for (Int_t toy = 0; toy < gNtoy; ++toy) {
  replicas_Nom_H[toy] = new TH1D(*replicas_Nom_V[toy]);
  replicas_Nom_H[toy]->GetXaxis()->Set(gNBins, gBinLimits);
  }

  TString Diffname = "DiffUnfoldedspectrum";
  TString Nomname = "NomUnfoldedspectra";
  TH1DBootstrap* resultNom = new TH1DBootstrap(Nomname.Data(), Nomname.Data(), &resultIDSNom_H, replicas_Nom_H, gNtoy);
  TH1DBootstrap* resultDiff = new TH1DBootstrap(Diffname.Data(), Diffname.Data(), &resultIDSJES_H, replicas_JES_H, gNtoy);

  // Making a bootstrap for the difference
  resultDiff->Add(resultNom,-1);

  resultDiff->SetErrBootstrapRMS();
  resultDiff->Divide(resultNom);

  // Saving the histogram into a ROOT file
  TFile* tout = new TFile("RelativeUncertainty.root","recreate");
  resultDiff->GetNominal()->Write();
  tout->Close();
  */

  std::cout << "Quitting PurityEff()" << std::endl;

}//END: RelativeUncertainty()



