#ifndef IDSUnfold_h
#define IDSUnfold_h

#include "Rtypes.h"
#include "TMatrixD.h"
#include "TVectorD.h"
#include "TLatex.h"
#include "TRandom3.h"
#include "TH1D.h"
#include "TH2D.h"
#include "TH3D.h"

//_________________________________________________________________________________________________
// C-C++ headers
#include <iostream>
#include <fstream>
#include <string>
#include <map>
#include <vector>

//----- Helper functions

void formatTex(TLatex *tex);
void rescaleAxis(TH1D *h, Double_t scale);
void divideBinWidth1D(TH1D *h);
void divideBinWidth2D(TH2D *h);
void divideBinWidth3D(TH3D *h);
void multiplyBinWidth1D(TH1D *h);
void multiplyBinWidth2D(TH2D *h);
void multiplyBinWidth3D(TH3D *h);

//-----   IDS code

TH1* GetIDSUnfoldedSpectrum(const TH1 *h_RecoMC, const TH1 *h_TruthMC, const TH2 *h_2DSmear, const TH1 *h_RecoData, Int_t iter);
Double_t Probability(Double_t deviation, Double_t sigma, Double_t lambda);
Double_t MCnormalizationCoeff(const TVectorD *vd, const TVectorD *errvd, const TVectorD *vRecmc, const Int_t dim, const Double_t estNknownd, const Double_t Nmc, const Double_t lambda, const TVectorD *soustr_ );
Double_t MCnormalizationCoeffIter(const TVectorD *vd, const TVectorD *errvd, const TVectorD *vRecmc, const Int_t dim, const Double_t estNknownd, const Double_t Nmc, const TVectorD *soustr_, Double_t lambdaN = 0., Int_t NiterMax = 5, Int_t messAct = 1);
void Unfold(const TVectorD &b, const TVectorD &errb, const TMatrixD &A, const Int_t dim, const Double_t lambda, TVectorD *soustr_, TVectorD *unf);
void ComputeSoustrTrue(const TMatrixD *A, const TVectorD *unfres, const TVectorD *unfresErr, Int_t N, TVectorD *soustr_, Double_t lambdaS);
void ModifyMatrix(TMatrixD *Am, const TMatrixD *A, const TVectorD *unfres, const TVectorD *unfresErr, Int_t N, const Double_t lambdaM_, TVectorD *soustr_, const Double_t lambdaS_);
void PerformIterations(const TVectorD &data, const TVectorD &dataErr, const TMatrixD &A_, const Int_t &N_, Double_t lambdaL_, Int_t NstepsOptMin_, Double_t lambdaU_, Double_t lambdaM_, Double_t lambdaS_, TVectorD* unfres1IDS_, TVectorD* unfres2IDS_);
TMatrixD* GetSqrtMatrix(const TMatrixD& covMat);
void GenGaussRnd(TArrayD& v, const TMatrixD& sqrtMat, TRandom3& R);

#endif
