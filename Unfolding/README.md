# Download BootstrapGenerator
(https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/StandardModelUnfoldingNew)
svn co svn+ssh://svn.cern.ch/reps/atlasphys-sm/Physics/StandardModel/Common/BootstrapGenerator/tags/BootstrapGenerator-01-10-03 BootstrapGenerator

# Get AtlasStyle
(https://twiki.cern.ch/twiki/bin/view/AtlasProtected/PubComPlotStyle#ROOT_Style_for_official_ATLAS_pl)
git clone ssh://git@gitlab.cern.ch:7999/atlas-publications-committee/atlasrootstyle.git AtlasStyle

# Get RootCore
svn co svn+ssh://svn.cern.ch/reps/atlasoff/PhysicsAnalysis/D3PDTools/RootCore/tags/RootCore-00-03-58 RootCore


# IDS Unfolding

Code for deconvolution of data distributions to correct for distortions caused by inefficiency and resolution effects
based on an Iterative Dynamically Stabilised Unfolding technique: (https://arxiv.org/abs/0907.3791)




## Table of contents

- [Packages needed/included](#packages)
- [Compilation](#compilation)
- [Running](#running)



## Packages needed/included

- RootCore-00-03-58
- BootstrapGenerator
- AtlasStyle 



## Compilation

source script.sh
rc find_packages
rc compile

## Running

mkdir StatUncFiles, unfoldedFiles, biasIDSFiles, Results_IDS
with the following structure in order to store the output:

e.g:

ls unfoldedFiles/
systematic_0/

ls systematic_0/
mjj/
incl/

ls incl/
ybin0/
ybin1/
ybin2/
ybin3/
ybin4/
ybin5/

- Open Unfolding_v5.config and set in PATH_1/PATH_2 the location of your data_tree_to_unfold.root and MC_tree.root inputs
- Set booleans of the methods you want to use:

Unfolding: true (Perform nominal unfolding)
Bootstrap: true (Use bootstraps. The inputs must have the bootstraps histograms.)
Closure: true (Perform Closure Test. You must have Bootstrap:true)
Covariance: true (Compute statistical uncertainty. You must have Bootstrap:true)

- Run_Closure_Covariance Unfolding_v5.config Data_tree_to_unfold.root MC_tree.root 0 mjj 0


