#!/bin/sh

########################################################################
#
# Purpose: generate the executable "run_exe" whose source code is run.C
# 
# (1) make sure that compile.sh is executable, if not do: 
#	> chmod 755 compile.sh 
# (2) compile run.C to make the executable "run_exe":
#	> ./compile.sh
# (3) run the code, for that try:
#	> ./run_exe --help
#
########################################################################

#rm run_exe *~
#g++ Calculation_CovMatrix.cxx -o exe_cov `root-config --cflags --libs --glibs` 
#g++ Calculation_StatError.cxx -o exe_stat `root-config --cflags --libs --glibs` 
g++ addError_systematics.cxx -o exe_syst `root-config --cflags --libs --glibs` 
#g++ merging_histos_prepChi2.cxx -o exe_rebin `root-config --cflags --libs --glibs` 

