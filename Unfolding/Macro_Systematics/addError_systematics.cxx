#include <iostream>
#include <map>
#include <string>
#include <vector>
#include "TString.h"
#include "TFile.h"
#include "TH1F.h"
#include "TH2D.h"
#include "TMath.h"
#include "TEnv.h"
#include "TVectorD.h"
#include "TCanvas.h"
#include "TH1D.h"
#include "TProfile.h"
#include "TLegend.h"
#include "TLine.h"
#include "TList.h"
#include "TMatrixD.h"
#include "TVectorD.h"
#include "TEnv.h"
#include "TObjArray.h"
#include "TObjString.h"
#include <stdlib.h>

using namespace std;

typedef std::vector<double> VecD;
typedef TString Str;
typedef std::vector<TString> StrV;

void define_dictionary();
void ReadInSettings(TEnv *settings, TString systName);
void AddError();
void SaveInFile();
void SaveInFile_highystar();
void FlipSign_and_StartBin();
void FlipSign_and_StartBin_highystar();
void StartBin();
void StartBin_and_SymJER();
int SumIn2_JES ();
int SumIn2_JES_toEsteban ();
int SumIn2_JES_toEsteban_highystar ();
int SumIn2_biasIDS_toEsteban ();
int SumIn2_JER_toEsteban ();
int SumIn2_JES_InputsForEICF();
int SumIn2_JER ();
void JES_mean();
void Sym_biasIDS();
void StartBin_and_SymJER_Dijets();
void LumiUnc();
void PrepInput ();
void PrepInput_EICF ();
void SumJER ();
void ScaleHist();
void SaveInFile_toBogdan();
void RebinData();
void RebinSyst();

static void Abort(Str msg) { printf("\nERROR:\n\n   %s\n\n",msg.Data()); }

StrV vectorize(Str str);
VecD vectorizeD(Str str);


TString PATH_1, PATH_2, fileName1, fileName2, jername1_up, jername1_dn, jername2_up, jername2_dn;

int ybin, jsyst;

int bin_to_merge_a;
int bin_to_merge_b;
int bin_to_merge_a2;
int bin_to_merge_b2;
int bin_to_merge_a3;
int bin_to_merge_b3;
int bin_to_merge_a4;
int bin_to_merge_b4;
int bin_to_merge_a5;
int bin_to_merge_b5;
int bin_to_merge_a6;
int bin_to_merge_b6;
int bin_to_merge_a7;
int bin_to_merge_b7;







Int_t gNBins;

std::vector<double> gBinLimits;

bool runDijet;

std::map<int, string> my_map;

int main( int argc, char* argv[] ) {

    define_dictionary();

    TString config("");

    for (int i=1;i<argc;++i) {
        if (TString(argv[i]).Contains(".config")){
            config=argv[i];
        }

        jsyst = atoi(argv[2]);

        if (TString(argv[3]).Contains("mjj")) {
            runDijet = true;
        }
        else if (TString(argv[3]).Contains("incl")) {
            runDijet = false;
        }

        ybin = atoi(argv[4]);

    }

    std::cout<<"jsyst: "<<jsyst<<std::endl;
    std::cout<<"runDijet: "<<runDijet<<std::endl;
    std::cout<<"ybin: "<<ybin<<std::endl;

    TString systName = (TString)my_map.find(jsyst)->second;
  //  TString systName = "tmp";

    TEnv *settings = new TEnv();
    int status = settings->ReadFile(config.Data(),EEnvLevel(0));
    if ( status != 0 ) Abort(Form("Cannot read file %s",config.Data()));

    ReadInSettings(settings,systName);

//    FlipSign_and_StartBin_highystar();
   // FlipSign_and_StartBin();
   
//    SaveInFile_highystar();
    //SaveInFile();

    //ScaleHist();

   // SaveInFile_toBogdan();

 //   SumIn2_JES_toEsteban();
    //SumIn2_JES_toEsteban_highystar();
    //JES_mean ();

    //AddError();

    //RebinData();
    //RebinSyst();
  //  SumIn2_JER_toEsteban();

    //StartBin_and_SymJER_Dijets();

    //LumiUnc();

   //Sym_biasIDS();

    //SumIn2_biasIDS_toEsteban();

    //StartBin_and_SymJER();

    //SumIn2_JES_InputsForEICF();

//    if (jsyst <= 152) {
//
//    SaveInFile();
//    }

    StartBin();

    //FlipSign_and_StartBin();
   // SaveInFile();
//    //SumIn2();
  //  PrepInput_EICF();
//   
//   if (jsyst>152) { 
//
//    SumJER();
//   }

    return 0;
}

StrV vectorize(Str str){
  StrV result; TObjArray *strings = str.Tokenize(" ");
  if (strings->GetEntries()==0) return result;
  TIter istr(strings);
  while (TObjString* os=(TObjString*)istr())
    result.push_back(os->GetString());
  return result;
}

VecD vectorizeD(Str str){
  VecD result; StrV vecS = vectorize(str);
  for (uint i=0;i<vecS.size();++i)
    result.push_back(atof(vecS[i]));
  return result;
}


void define_dictionary() {

  /*  Pre-recommendations:

    my_map.insert(pair<int, string>(0,"nominal"));
    my_map.insert(pair<int, string>(1,"JET_BJES_Response__1up"));
    my_map.insert(pair<int, string>(2,"JET_BJES_Response__1down"));
    my_map.insert(pair<int, string>(3,"JET_EtaIntercalibration_Modelling__1up"));
    my_map.insert(pair<int, string>(4,"JET_EtaIntercalibration_Modelling__1down"));
    my_map.insert(pair<int, string>(5,"JET_EtaIntercalibration_TotalStat__1up"));
    my_map.insert(pair<int, string>(6,"JET_EtaIntercalibration_TotalStat__1down"));
    my_map.insert(pair<int, string>(7,"JET_Flavor_Composition__1up"));
    my_map.insert(pair<int, string>(8,"JET_Flavor_Composition__1down"));
    my_map.insert(pair<int, string>(9,"JET_Flavor_Response__1up"));
    my_map.insert(pair<int, string>(10,"JET_Flavor_Response__1down"));
    my_map.insert(pair<int, string>(11,"JET_Gjet_Generator__1up"));
    my_map.insert(pair<int, string>(12,"JET_Gjet_Generator__1down"));
    my_map.insert(pair<int, string>(13,"JET_Gjet_OOC__1up"));
    my_map.insert(pair<int, string>(14,"JET_Gjet_OOC__1down"));

//    my_map.insert(pair<int, string>(15,"JET_Gjet_Stat6__1up")); //Already in 77

    my_map.insert(pair<int, string>(16,"JET_Gjet_Purity__1up"));
    my_map.insert(pair<int, string>(17,"JET_Gjet_Purity__1down"));
    my_map.insert(pair<int, string>(18,"JET_Gjet_Stat10__1up"));
    my_map.insert(pair<int, string>(19,"JET_Gjet_Stat10__1down"));
    my_map.insert(pair<int, string>(20,"JET_Gjet_Stat11__1up"));
    my_map.insert(pair<int, string>(21,"JET_Gjet_Stat11__1down"));
    my_map.insert(pair<int, string>(22,"JET_Gjet_Stat12__1up"));
    my_map.insert(pair<int, string>(23,"JET_Gjet_Stat12__1down"));
    my_map.insert(pair<int, string>(24,"JET_Gjet_Stat13__1up"));
    my_map.insert(pair<int, string>(25,"JET_Gjet_Stat13__1down"));
    my_map.insert(pair<int, string>(26,"JET_Gjet_Stat1__1up"));
    my_map.insert(pair<int, string>(27,"JET_Gjet_Stat1__1down"));
    my_map.insert(pair<int, string>(28,"JET_Gjet_Stat2__1up"));
    my_map.insert(pair<int, string>(29,"JET_Gjet_Stat2__1down"));
    my_map.insert(pair<int, string>(30,"JET_Gjet_Stat3__1up"));
    my_map.insert(pair<int, string>(31,"JET_Gjet_Stat3__1down"));
    my_map.insert(pair<int, string>(32,"JET_Gjet_Stat4__1up"));
    my_map.insert(pair<int, string>(33,"JET_Gjet_Stat4__1down"));
    my_map.insert(pair<int, string>(34,"JET_Gjet_Stat5__1up"));
    my_map.insert(pair<int, string>(35,"JET_Gjet_Stat5__1down"));
    my_map.insert(pair<int, string>(36,"JET_Xcalib_6__1up"));
    my_map.insert(pair<int, string>(37,"JET_Xcalib_6__1down"));
    my_map.insert(pair<int, string>(38,"JET_Zjet_JVF__1up"));
    my_map.insert(pair<int, string>(39,"JET_Zjet_JVF__1down"));
    my_map.insert(pair<int, string>(40,"JET_Zjet_KTerm__1up"));
    my_map.insert(pair<int, string>(41,"JET_Zjet_KTerm__1down"));
    my_map.insert(pair<int, string>(42,"JET_Zjet_MC__1up"));
    my_map.insert(pair<int, string>(43,"JET_Zjet_MC__1down"));
    my_map.insert(pair<int, string>(44,"JET_Zjet_MuScale__1up"));
    my_map.insert(pair<int, string>(45,"JET_Zjet_MuScale__1down"));
    my_map.insert(pair<int, string>(46,"JET_Zjet_MuSmearID__1up"));
    my_map.insert(pair<int, string>(47,"JET_Zjet_MuSmearID__1down"));
    my_map.insert(pair<int, string>(48,"JET_Zjet_MuSmearMS__1up"));
    my_map.insert(pair<int, string>(49,"JET_Zjet_MuSmearMS__1down"));
    my_map.insert(pair<int, string>(50,"JET_Zjet_Stat10__1up"));
    my_map.insert(pair<int, string>(51,"JET_Zjet_Stat10__1down"));
    my_map.insert(pair<int, string>(52,"JET_Zjet_Stat11__1up"));
    my_map.insert(pair<int, string>(53,"JET_Zjet_Stat11__1down"));
    my_map.insert(pair<int, string>(54,"JET_Zjet_Stat1__1up"));
    my_map.insert(pair<int, string>(55,"JET_Zjet_Stat1__1down"));
    my_map.insert(pair<int, string>(56,"JET_Zjet_Stat2__1up"));
    my_map.insert(pair<int, string>(57,"JET_Zjet_Stat2__1down"));
    my_map.insert(pair<int, string>(58,"JET_Zjet_Stat3__1up"));
    my_map.insert(pair<int, string>(59,"JET_Zjet_Stat3__1down"));
    my_map.insert(pair<int, string>(60,"JET_Zjet_Stat4__1up"));
    my_map.insert(pair<int, string>(61,"JET_Zjet_Stat4__1down"));
    my_map.insert(pair<int, string>(62,"JET_Zjet_Stat5__1up"));
    my_map.insert(pair<int, string>(63,"JET_Zjet_Stat5__1down"));
    my_map.insert(pair<int, string>(64,"JET_Zjet_Stat6__1up"));
    my_map.insert(pair<int, string>(65,"JET_Zjet_Stat6__1down"));
    my_map.insert(pair<int, string>(66,"JET_Zjet_Stat7__1up"));
    my_map.insert(pair<int, string>(67,"JET_Zjet_Stat7__1down"));
    my_map.insert(pair<int, string>(68,"JET_Zjet_Stat8__1up"));
    my_map.insert(pair<int, string>(69,"JET_Zjet_Stat8__1down"));
    my_map.insert(pair<int, string>(70,"JET_Zjet_Stat9__1up"));
    my_map.insert(pair<int, string>(71,"JET_Zjet_Stat9__1down"));
    my_map.insert(pair<int, string>(72,"JET_Zjet_Veto__1up"));
    my_map.insert(pair<int, string>(73,"JET_Zjet_Veto__1down"));
    my_map.insert(pair<int, string>(74,"JET_Zjet_dPhi__1up"));
    my_map.insert(pair<int, string>(75,"JET_Zjet_dPhi__1down"));
    my_map.insert(pair<int, string>(76,"JET_JER_SINGLE_NP__1up")); //--->> no hay JET_JER_SINGLE_NP__1down systematic, solo up!
    my_map.insert(pair<int, string>(77,"JET_Gjet_Stat6__1up")); 
    my_map.insert(pair<int, string>(78,"JET_Gjet_Stat6__1down"));
    my_map.insert(pair<int, string>(79,"JET_Gjet_Stat7__1up"));
    my_map.insert(pair<int, string>(80,"JET_Gjet_Stat7__1down"));
    my_map.insert(pair<int, string>(81,"JET_Gjet_Stat8__1up"));
    my_map.insert(pair<int, string>(82,"JET_Gjet_Stat8__1down"));
    my_map.insert(pair<int, string>(83,"JET_Gjet_Stat9__1up"));
    my_map.insert(pair<int, string>(84,"JET_Gjet_Stat9__1down"));
    my_map.insert(pair<int, string>(85,"JET_Gjet_Veto__1up"));
    my_map.insert(pair<int, string>(86,"JET_Gjet_Veto__1down"));
    my_map.insert(pair<int, string>(87,"JET_Gjet_dPhi__1up"));
    my_map.insert(pair<int, string>(88,"JET_Gjet_dPhi__1down"));
    my_map.insert(pair<int, string>(89,"JET_LAr_ESZee__1up"));
    my_map.insert(pair<int, string>(90,"JET_LAr_ESZee__1down"));
    my_map.insert(pair<int, string>(91,"JET_LAr_ESmaterial__1up"));
    my_map.insert(pair<int, string>(92,"JET_LAr_ESmaterial__1down"));
    my_map.insert(pair<int, string>(93,"JET_LAr_ESpresampler__1up"));
    my_map.insert(pair<int, string>(94,"JET_LAr_ESpresampler__1down"));
    my_map.insert(pair<int, string>(95,"JET_LAr_Esmear__1up"));
    my_map.insert(pair<int, string>(96,"JET_LAr_Esmear__1down"));
    my_map.insert(pair<int, string>(97,"JET_MJB_Alpha__1up"));
    my_map.insert(pair<int, string>(98,"JET_MJB_Alpha__1down"));
    my_map.insert(pair<int, string>(99,"JET_MJB_Asym__1up"));
    my_map.insert(pair<int, string>(100,"JET_MJB_Asym__1down"));
    my_map.insert(pair<int, string>(101,"JET_MJB_Beta__1up"));
    my_map.insert(pair<int, string>(102,"JET_MJB_Beta__1down"));
    my_map.insert(pair<int, string>(103,"JET_MJB_Fragmentation__1up"));
    my_map.insert(pair<int, string>(104,"JET_MJB_Fragmentation__1down"));
    my_map.insert(pair<int, string>(105,"JET_MJB_Stat10__1up"));
    my_map.insert(pair<int, string>(106,"JET_MJB_Stat10__1down"));
    my_map.insert(pair<int, string>(107,"JET_MJB_Stat1__1up"));
    my_map.insert(pair<int, string>(108,"JET_MJB_Stat1__1down"));
    my_map.insert(pair<int, string>(109,"JET_MJB_Stat2__1up"));
    my_map.insert(pair<int, string>(110,"JET_MJB_Stat2__1down"));
    my_map.insert(pair<int, string>(111,"JET_MJB_Stat3__1up"));
    my_map.insert(pair<int, string>(112,"JET_MJB_Stat3__1down"));
    my_map.insert(pair<int, string>(113,"JET_MJB_Stat4__1up"));
    my_map.insert(pair<int, string>(114,"JET_MJB_Stat4__1down"));
    my_map.insert(pair<int, string>(115,"JET_MJB_Stat5__1up"));
    my_map.insert(pair<int, string>(116,"JET_MJB_Stat5__1down"));
    my_map.insert(pair<int, string>(117,"JET_MJB_Stat6__1up"));
    my_map.insert(pair<int, string>(118,"JET_MJB_Stat6__1down"));
    my_map.insert(pair<int, string>(119,"JET_MJB_Stat7__1up"));
    my_map.insert(pair<int, string>(120,"JET_MJB_Stat7__1down"));
    my_map.insert(pair<int, string>(121,"JET_MJB_Stat8__1up"));
    my_map.insert(pair<int, string>(122,"JET_MJB_Stat8__1down"));
    my_map.insert(pair<int, string>(123,"JET_MJB_Stat9__1up"));
    my_map.insert(pair<int, string>(124,"JET_MJB_Stat9__1down"));
    my_map.insert(pair<int, string>(125,"JET_MJB_Threshold__1up"));
    my_map.insert(pair<int, string>(126,"JET_MJB_Threshold__1down"));
    my_map.insert(pair<int, string>(127,"JET_Pileup_OffsetMu__1up"));
    my_map.insert(pair<int, string>(128,"JET_Pileup_OffsetMu__1down"));
    my_map.insert(pair<int, string>(129,"JET_Pileup_OffsetNPV__1up"));
    my_map.insert(pair<int, string>(130,"JET_Pileup_OffsetNPV__1down"));
    my_map.insert(pair<int, string>(131,"JET_Pileup_PtTerm__1up"));
    my_map.insert(pair<int, string>(132,"JET_Pileup_PtTerm__1down"));
    my_map.insert(pair<int, string>(133,"JET_Pileup_RhoTopology__1up"));
    my_map.insert(pair<int, string>(134,"JET_Pileup_RhoTopology__1down"));
    my_map.insert(pair<int, string>(135,"JET_PunchThrough_MC15__1up"));
    my_map.insert(pair<int, string>(136,"JET_PunchThrough_MC15__1down"));
    my_map.insert(pair<int, string>(137,"JET_SingleParticle_HighPt__1up"));
    my_map.insert(pair<int, string>(138,"JET_SingleParticle_HighPt__1down"));
    my_map.insert(pair<int, string>(139,"JET_Xcalib_1__1up"));
    my_map.insert(pair<int, string>(140,"JET_Xcalib_1__1down"));
    my_map.insert(pair<int, string>(141,"JET_Xcalib_2__1up"));
    my_map.insert(pair<int, string>(142,"JET_Xcalib_2__1down"));
    my_map.insert(pair<int, string>(143,"JET_Xcalib_3__1up"));
    my_map.insert(pair<int, string>(144,"JET_Xcalib_3__1down"));
    my_map.insert(pair<int, string>(145,"JET_Xcalib_4__1up"));
    my_map.insert(pair<int, string>(146,"JET_Xcalib_4__1down"));
    my_map.insert(pair<int, string>(147,"JET_Xcalib_5__1up"));
    my_map.insert(pair<int, string>(148,"JET_Xcalib_5__1down"));
*/

//ScaleHist
/*
    my_map.insert(pair<int, string>(1,"EtaIntercalibration_Modelling_AntiKt4EMTopo"));
    my_map.insert(pair<int, string>(2,"EtaIntercalibration_TotalStat_AntiKt4EMTopo"));
    my_map.insert(pair<int, string>(3,"EtaIntercalibration_NonClosure_AntiKt4EMTopo"));
    my_map.insert(pair<int, string>(4,"flavorCompGlu_AntiKt4EMTopo"));
    my_map.insert(pair<int, string>(5,"flavorCompLight_AntiKt4EMTopo"));
    my_map.insert(pair<int, string>(6,"FlavorResponse_AntiKt4EMTopo"));


    my_map.insert(pair<int, string>(7,"InSituProp_FlavorComposition_AntiKt4EMTopo"));
    my_map.insert(pair<int, string>(8,"InSituProp_FlavorResponse_AntiKt4EMTopo"));
    my_map.insert(pair<int, string>(9,"InSituProp_EtaIntercalModelling_AntiKt4EMTopo"));
    my_map.insert(pair<int, string>(10,"InSituProp_EtaIntercalStat_AntiKt4EMTopo"));
    my_map.insert(pair<int, string>(11,"Gjet_Generator_AntiKt4EMTopo"));
    my_map.insert(pair<int, string>(12,"Gjet_OOC_AntiKt4EMTopo"));
    my_map.insert(pair<int, string>(13,"Gjet_Purity_AntiKt4EMTopo"));
    my_map.insert(pair<int, string>(14,"Gjet_Veto_AntiKt4EMTopo"));
    my_map.insert(pair<int, string>(15,"Zjet_ElecESZee_AntiKt4EMTopo"));
    my_map.insert(pair<int, string>(16,"Gjet_GamESZee_AntiKt4EMTopo"));
*/
/*
    my_map.insert(pair<int, string>(1,"JET_EtaIntercalibration_Modelling__1up"));
    my_map.insert(pair<int, string>(2,"JET_EtaIntercalibration_Modelling__1down"));
    my_map.insert(pair<int, string>(3,"JET_EtaIntercalibration_NonClosure__1up"));
    my_map.insert(pair<int, string>(4,"JET_EtaIntercalibration_NonClosure__1down"));
    my_map.insert(pair<int, string>(5,"JET_EtaIntercalibration_TotalStat__1up"));
    my_map.insert(pair<int, string>(6,"JET_EtaIntercalibration_TotalStat__1down"));
    my_map.insert(pair<int, string>(7,"JET_Flavor_Composition__1up"));
    my_map.insert(pair<int, string>(8,"JET_Flavor_Composition__1down"));
    my_map.insert(pair<int, string>(9,"JET_Flavor_Response__1up"));
    my_map.insert(pair<int, string>(10,"JET_Flavor_Response__1down"));
    my_map.insert(pair<int, string>(11,"JET_Gjet_Generator__1up"));
    my_map.insert(pair<int, string>(12,"JET_Gjet_Generator__1down"));
    my_map.insert(pair<int, string>(13,"JET_Gjet_OOC__1up"));
    my_map.insert(pair<int, string>(14,"JET_Gjet_OOC__1down"));
    my_map.insert(pair<int, string>(15,"JET_Gjet_Purity__1up"));
    my_map.insert(pair<int, string>(16,"JET_Gjet_Purity__1down"));
    my_map.insert(pair<int, string>(17,"JET_Gjet_Veto__1up"));
    my_map.insert(pair<int, string>(18,"JET_Gjet_Veto__1down"));
    my_map.insert(pair<int, string>(19,"JET_LAr_ESZee__1up"));
    my_map.insert(pair<int, string>(20,"JET_LAr_ESZee__1down"));

    
    my_map.insert(pair<int, string>(21,"JET_Gjet_Stat11__1up"));
    my_map.insert(pair<int, string>(22,"JET_Gjet_Stat11__1down"));
    my_map.insert(pair<int, string>(23,"JET_Gjet_Stat12__1up"));
    my_map.insert(pair<int, string>(24,"JET_Gjet_Stat12__1down"));
    my_map.insert(pair<int, string>(25,"JET_Gjet_Stat13__1up"));
    my_map.insert(pair<int, string>(26,"JET_Gjet_Stat13__1down"));
    my_map.insert(pair<int, string>(27,"JET_Gjet_Stat14__1up"));
    my_map.insert(pair<int, string>(28,"JET_Gjet_Stat14__1down"));
    my_map.insert(pair<int, string>(29,"JET_Gjet_Stat15__1up"));
    my_map.insert(pair<int, string>(30,"JET_Gjet_Stat15__1down"));
    my_map.insert(pair<int, string>(31,"JET_Gjet_Stat1__1up"));
    my_map.insert(pair<int, string>(32,"JET_Gjet_Stat1__1down"));
    my_map.insert(pair<int, string>(33,"JET_Gjet_Stat2__1up"));
    my_map.insert(pair<int, string>(34,"JET_Gjet_Stat2__1down"));
    my_map.insert(pair<int, string>(35,"JET_Gjet_Stat3__1up"));
    my_map.insert(pair<int, string>(36,"JET_Gjet_Stat3__1down"));
    my_map.insert(pair<int, string>(37,"JET_Gjet_Stat4__1up"));
    my_map.insert(pair<int, string>(38,"JET_Gjet_Stat4__1down"));
    my_map.insert(pair<int, string>(39,"JET_Gjet_Stat5__1up"));
    my_map.insert(pair<int, string>(40,"JET_Gjet_Stat5__1down"));
    my_map.insert(pair<int, string>(41,"JET_Gjet_Stat6__1up"));
    my_map.insert(pair<int, string>(42,"JET_Gjet_Stat6__1down"));
    my_map.insert(pair<int, string>(43,"JET_Gjet_Stat7__1up"));
    my_map.insert(pair<int, string>(44,"JET_Gjet_Stat7__1down"));
    my_map.insert(pair<int, string>(45,"JET_Gjet_Stat8__1up"));
    my_map.insert(pair<int, string>(46,"JET_Gjet_Stat8__1down"));
    my_map.insert(pair<int, string>(47,"JET_Gjet_Stat9__1up"));
    my_map.insert(pair<int, string>(48,"JET_Gjet_Stat9__1down"));   
    my_map.insert(pair<int, string>(49,"JET_Zjet_dPhi__1up"));
    my_map.insert(pair<int, string>(50,"JET_Zjet_dPhi__1down"));
    my_map.insert(pair<int, string>(51,"JET_Gjet_dPhi__1up"));
    my_map.insert(pair<int, string>(52,"JET_Gjet_dPhi__1down"));
    my_map.insert(pair<int, string>(53,"JET_Gjet_Stat10__1up"));
    my_map.insert(pair<int, string>(54,"JET_Gjet_Stat10__1down"));
    my_map.insert(pair<int, string>(55,"JET_LAr_Esmear__1up"));
    my_map.insert(pair<int, string>(56,"JET_LAr_Esmear__1down"));
    my_map.insert(pair<int, string>(57,"JET_LAr_JVT__1up"));
    my_map.insert(pair<int, string>(58,"JET_LAr_JVT__1down"));
    my_map.insert(pair<int, string>(59,"JET_MJB_Alpha__1up"));
    my_map.insert(pair<int, string>(60,"JET_MJB_Alpha__1down"));
    my_map.insert(pair<int, string>(61,"JET_MJB_Asym__1up"));
    my_map.insert(pair<int, string>(62,"JET_MJB_Asym__1down"));
    my_map.insert(pair<int, string>(63,"JET_MJB_Beta__1up"));
    my_map.insert(pair<int, string>(64,"JET_MJB_Beta__1down"));
    my_map.insert(pair<int, string>(65,"JET_MJB_Fragmentation__1up"));
    my_map.insert(pair<int, string>(66,"JET_MJB_Fragmentation__1down"));
    my_map.insert(pair<int, string>(67,"JET_MJB_Stat10__1up"));
    my_map.insert(pair<int, string>(68,"JET_MJB_Stat10__1down"));
    my_map.insert(pair<int, string>(69,"JET_MJB_Stat11__1up"));
    my_map.insert(pair<int, string>(70,"JET_MJB_Stat11__1down"));
    my_map.insert(pair<int, string>(71,"JET_MJB_Stat12__1up"));
    my_map.insert(pair<int, string>(72,"JET_MJB_Stat12__1down"));
    my_map.insert(pair<int, string>(73,"JET_MJB_Stat13__1up"));
    my_map.insert(pair<int, string>(74,"JET_MJB_Stat13__1down"));
    my_map.insert(pair<int, string>(75,"JET_MJB_Stat14__1up"));
    my_map.insert(pair<int, string>(76,"JET_MJB_Stat14__1down"));
    my_map.insert(pair<int, string>(77,"JET_MJB_Stat15__1up"));
    my_map.insert(pair<int, string>(78,"JET_MJB_Stat15__1down"));
    my_map.insert(pair<int, string>(79,"JET_MJB_Stat16__1up"));
    my_map.insert(pair<int, string>(80,"JET_MJB_Stat16__1down"));
    my_map.insert(pair<int, string>(81,"JET_MJB_Stat1__1up"));
    my_map.insert(pair<int, string>(82,"JET_MJB_Stat1__1down"));
    my_map.insert(pair<int, string>(83,"JET_MJB_Stat2__1up"));
    my_map.insert(pair<int, string>(84,"JET_MJB_Stat2__1down"));
    my_map.insert(pair<int, string>(85,"JET_MJB_Stat3__1up"));
    my_map.insert(pair<int, string>(86,"JET_MJB_Stat3__1down"));
    my_map.insert(pair<int, string>(87,"JET_MJB_Stat4__1up"));
    my_map.insert(pair<int, string>(88,"JET_MJB_Stat4__1down"));
    my_map.insert(pair<int, string>(89,"JET_MJB_Stat5__1up"));
    my_map.insert(pair<int, string>(90,"JET_MJB_Stat5__1down"));
    my_map.insert(pair<int, string>(91,"JET_MJB_Stat6__1up"));
    my_map.insert(pair<int, string>(92,"JET_MJB_Stat6__1down"));
    my_map.insert(pair<int, string>(93,"JET_MJB_Stat7__1up"));
    my_map.insert(pair<int, string>(94,"JET_MJB_Stat7__1down"));
    my_map.insert(pair<int, string>(95,"JET_MJB_Stat8__1up"));
    my_map.insert(pair<int, string>(96,"JET_MJB_Stat8__1down"));
    my_map.insert(pair<int, string>(97,"JET_MJB_Stat9__1up"));
    my_map.insert(pair<int, string>(98,"JET_MJB_Stat9__1down"));
    my_map.insert(pair<int, string>(99,"JET_MJB_Threshold__1up"));
    my_map.insert(pair<int, string>(100,"JET_MJB_Threshold__1down"));
    my_map.insert(pair<int, string>(101,"JET_Pileup_OffsetMu__1up"));
    my_map.insert(pair<int, string>(102,"JET_Pileup_OffsetMu__1down"));
    my_map.insert(pair<int, string>(103,"JET_Pileup_OffsetNPV__1up"));
    my_map.insert(pair<int, string>(104,"JET_Pileup_OffsetNPV__1down"));
    my_map.insert(pair<int, string>(105,"JET_Pileup_PtTerm__1up"));
    my_map.insert(pair<int, string>(106,"JET_Pileup_PtTerm__1down"));
    my_map.insert(pair<int, string>(107,"JET_Pileup_RhoTopology__1up"));
    my_map.insert(pair<int, string>(108,"JET_Pileup_RhoTopology__1down"));
    my_map.insert(pair<int, string>(109,"JET_PunchThrough_MC15__1up"));
    my_map.insert(pair<int, string>(110,"JET_PunchThrough_MC15__1down"));
    my_map.insert(pair<int, string>(111,"JET_SingleParticle_HighPt__1up"));
    my_map.insert(pair<int, string>(112,"JET_SingleParticle_HighPt__1down"));
    my_map.insert(pair<int, string>(113,"JET_Zjet_KTerm__1up"));
    my_map.insert(pair<int, string>(114,"JET_Zjet_KTerm__1down"));
    my_map.insert(pair<int, string>(115,"JET_Zjet_MC__1up"));
    my_map.insert(pair<int, string>(116,"JET_Zjet_MC__1down"));
    my_map.insert(pair<int, string>(117,"JET_Zjet_MuScale__1up"));
    my_map.insert(pair<int, string>(118,"JET_Zjet_MuScale__1down"));
    my_map.insert(pair<int, string>(119,"JET_Zjet_MuSmearID__1up"));
    my_map.insert(pair<int, string>(120,"JET_Zjet_MuSmearID__1down"));
    my_map.insert(pair<int, string>(121,"JET_Zjet_MuSmearMS__1up"));
    my_map.insert(pair<int, string>(122,"JET_Zjet_MuSmearMS__1down"));
    my_map.insert(pair<int, string>(123,"JET_Zjet_Stat10__1up"));
    my_map.insert(pair<int, string>(124,"JET_Zjet_Stat10__1down"));
    my_map.insert(pair<int, string>(125,"JET_Zjet_Stat11__1up"));
    my_map.insert(pair<int, string>(126,"JET_Zjet_Stat11__1down"));
    my_map.insert(pair<int, string>(127,"JET_Zjet_Stat12__1up"));
    my_map.insert(pair<int, string>(128,"JET_Zjet_Stat12__1down"));
    my_map.insert(pair<int, string>(129,"JET_Zjet_Stat13__1up"));
    my_map.insert(pair<int, string>(130,"JET_Zjet_Stat13__1down"));
    my_map.insert(pair<int, string>(131,"JET_Zjet_Stat1__1up"));
    my_map.insert(pair<int, string>(132,"JET_Zjet_Stat1__1down"));
    my_map.insert(pair<int, string>(133,"JET_Zjet_Stat2__1up"));
    my_map.insert(pair<int, string>(134,"JET_Zjet_Stat2__1down"));
    my_map.insert(pair<int, string>(135,"JET_Zjet_Stat3__1up"));
    my_map.insert(pair<int, string>(136,"JET_Zjet_Stat3__1down"));
    my_map.insert(pair<int, string>(137,"JET_Zjet_Stat4__1up"));
    my_map.insert(pair<int, string>(138,"JET_Zjet_Stat4__1down"));
    my_map.insert(pair<int, string>(139,"JET_Zjet_Stat5__1up"));
    my_map.insert(pair<int, string>(140,"JET_Zjet_Stat5__1down"));
    my_map.insert(pair<int, string>(141,"JET_Zjet_Stat6__1up"));
    my_map.insert(pair<int, string>(142,"JET_Zjet_Stat6__1down"));
    my_map.insert(pair<int, string>(143,"JET_Zjet_Stat7__1up"));
    my_map.insert(pair<int, string>(144,"JET_Zjet_Stat7__1down"));
    my_map.insert(pair<int, string>(145,"JET_Zjet_Stat8__1up"));
    my_map.insert(pair<int, string>(146,"JET_Zjet_Stat8__1down"));
    my_map.insert(pair<int, string>(147,"JET_Zjet_Stat9__1up"));
    my_map.insert(pair<int, string>(148,"JET_Zjet_Stat9__1down"));
    my_map.insert(pair<int, string>(149,"JET_Zjet_Veto__1up"));
    my_map.insert(pair<int, string>(150,"JET_Zjet_Veto__1down"));
*/

// Moriond:
/*
    my_map.insert(pair<int, string>(0,"nominal"));
    my_map.insert(pair<int, string>(1,"JET_BJES_Response__1up"));
    my_map.insert(pair<int, string>(2,"JET_BJES_Response__1down"));
    my_map.insert(pair<int, string>(3,"JET_EtaIntercalibration_Modelling__1up"));
    my_map.insert(pair<int, string>(4,"JET_EtaIntercalibration_Modelling__1down"));
    my_map.insert(pair<int, string>(5,"JET_EtaIntercalibration_NonClosure__1up"));
    my_map.insert(pair<int, string>(6,"JET_EtaIntercalibration_NonClosure__1down"));
    my_map.insert(pair<int, string>(7,"JET_EtaIntercalibration_TotalStat__1up"));
    my_map.insert(pair<int, string>(8,"JET_EtaIntercalibration_TotalStat__1down"));
    my_map.insert(pair<int, string>(9,"JET_Flavor_Composition__1up"));
    my_map.insert(pair<int, string>(10,"JET_Flavor_Composition__1down"));
    my_map.insert(pair<int, string>(11,"JET_Flavor_Response__1up"));
    my_map.insert(pair<int, string>(12,"JET_Flavor_Response__1down"));
    
    my_map.insert(pair<int, string>(13,"JET_Gjet_Generator__1up"));
    my_map.insert(pair<int, string>(14,"JET_Gjet_Generator__1down"));
    my_map.insert(pair<int, string>(15,"JET_Gjet_OOC__1up"));
    my_map.insert(pair<int, string>(16,"JET_Gjet_OOC__1down"));
    my_map.insert(pair<int, string>(17,"JET_Gjet_Purity__1up"));
    my_map.insert(pair<int, string>(18,"JET_Gjet_Purity__1down"));
    my_map.insert(pair<int, string>(19,"JET_Gjet_Stat10__1up"));
    my_map.insert(pair<int, string>(20,"JET_Gjet_Stat10__1down"));
    
    my_map.insert(pair<int, string>(21,"JET_Gjet_Stat11__1up"));
    my_map.insert(pair<int, string>(22,"JET_Gjet_Stat11__1down"));
    my_map.insert(pair<int, string>(23,"JET_Gjet_Stat12__1up"));
    my_map.insert(pair<int, string>(24,"JET_Gjet_Stat12__1down"));
    my_map.insert(pair<int, string>(25,"JET_Gjet_Stat13__1up"));
    my_map.insert(pair<int, string>(26,"JET_Gjet_Stat13__1down"));
    my_map.insert(pair<int, string>(27,"JET_Gjet_Stat14__1up"));
    my_map.insert(pair<int, string>(28,"JET_Gjet_Stat14__1down"));
    my_map.insert(pair<int, string>(29,"JET_Gjet_Stat15__1up"));
    my_map.insert(pair<int, string>(30,"JET_Gjet_Stat15__1down"));


    my_map.insert(pair<int, string>(31,"JET_Gjet_Stat1__1up"));
    my_map.insert(pair<int, string>(32,"JET_Gjet_Stat1__1down"));
    my_map.insert(pair<int, string>(33,"JET_Gjet_Stat2__1up"));
    my_map.insert(pair<int, string>(34,"JET_Gjet_Stat2__1down"));
    my_map.insert(pair<int, string>(35,"JET_Gjet_Stat3__1up"));
    my_map.insert(pair<int, string>(36,"JET_Gjet_Stat3__1down"));
    my_map.insert(pair<int, string>(37,"JET_Gjet_Stat4__1up"));
    my_map.insert(pair<int, string>(38,"JET_Gjet_Stat4__1down"));
    my_map.insert(pair<int, string>(39,"JET_Gjet_Stat5__1up"));
    my_map.insert(pair<int, string>(40,"JET_Gjet_Stat5__1down"));
    my_map.insert(pair<int, string>(41,"JET_Gjet_Stat6__1up"));
    my_map.insert(pair<int, string>(42,"JET_Gjet_Stat6__1down"));
    my_map.insert(pair<int, string>(43,"JET_Gjet_Stat7__1up"));
    my_map.insert(pair<int, string>(44,"JET_Gjet_Stat7__1down"));
    my_map.insert(pair<int, string>(45,"JET_Gjet_Stat8__1up"));
    my_map.insert(pair<int, string>(46,"JET_Gjet_Stat8__1down"));
    my_map.insert(pair<int, string>(47,"JET_Gjet_Stat9__1up"));
    my_map.insert(pair<int, string>(48,"JET_Gjet_Stat9__1down"));
    
    my_map.insert(pair<int, string>(49,"JET_Gjet_Veto__1up"));
    my_map.insert(pair<int, string>(50,"JET_Gjet_Veto__1down"));
    my_map.insert(pair<int, string>(51,"JET_Gjet_dPhi__1up"));
    my_map.insert(pair<int, string>(52,"JET_Gjet_dPhi__1down"));
    my_map.insert(pair<int, string>(53,"JET_LAr_ESZee__1up"));
    my_map.insert(pair<int, string>(54,"JET_LAr_ESZee__1down"));
    my_map.insert(pair<int, string>(55,"JET_LAr_Esmear__1up"));
    my_map.insert(pair<int, string>(56,"JET_LAr_Esmear__1down"));
    my_map.insert(pair<int, string>(57,"JET_LAr_JVT__1up"));
    my_map.insert(pair<int, string>(58,"JET_LAr_JVT__1down"));
    my_map.insert(pair<int, string>(59,"JET_MJB_Alpha__1up"));
    my_map.insert(pair<int, string>(60,"JET_MJB_Alpha__1down"));
    my_map.insert(pair<int, string>(61,"JET_MJB_Asym__1up"));
    my_map.insert(pair<int, string>(62,"JET_MJB_Asym__1down"));
    my_map.insert(pair<int, string>(63,"JET_MJB_Beta__1up"));
    my_map.insert(pair<int, string>(64,"JET_MJB_Beta__1down"));
    my_map.insert(pair<int, string>(65,"JET_MJB_Fragmentation__1up"));
    my_map.insert(pair<int, string>(66,"JET_MJB_Fragmentation__1down"));
    my_map.insert(pair<int, string>(67,"JET_MJB_Stat10__1up"));
    my_map.insert(pair<int, string>(68,"JET_MJB_Stat10__1down"));
    my_map.insert(pair<int, string>(69,"JET_MJB_Stat11__1up"));
    my_map.insert(pair<int, string>(70,"JET_MJB_Stat11__1down"));
    my_map.insert(pair<int, string>(71,"JET_MJB_Stat12__1up"));
    my_map.insert(pair<int, string>(72,"JET_MJB_Stat12__1down"));
    my_map.insert(pair<int, string>(73,"JET_MJB_Stat13__1up"));
    my_map.insert(pair<int, string>(74,"JET_MJB_Stat13__1down"));
    my_map.insert(pair<int, string>(75,"JET_MJB_Stat14__1up"));
    my_map.insert(pair<int, string>(76,"JET_MJB_Stat14__1down"));
    my_map.insert(pair<int, string>(77,"JET_MJB_Stat15__1up"));
    my_map.insert(pair<int, string>(78,"JET_MJB_Stat15__1down"));
    my_map.insert(pair<int, string>(79,"JET_MJB_Stat16__1up"));
    my_map.insert(pair<int, string>(80,"JET_MJB_Stat16__1down"));
    my_map.insert(pair<int, string>(81,"JET_MJB_Stat1__1up"));
    my_map.insert(pair<int, string>(82,"JET_MJB_Stat1__1down"));
    my_map.insert(pair<int, string>(83,"JET_MJB_Stat2__1up"));
    my_map.insert(pair<int, string>(84,"JET_MJB_Stat2__1down"));
    my_map.insert(pair<int, string>(85,"JET_MJB_Stat3__1up"));
    my_map.insert(pair<int, string>(86,"JET_MJB_Stat3__1down"));
    my_map.insert(pair<int, string>(87,"JET_MJB_Stat4__1up"));
    my_map.insert(pair<int, string>(88,"JET_MJB_Stat4__1down"));
    my_map.insert(pair<int, string>(89,"JET_MJB_Stat5__1up"));
    my_map.insert(pair<int, string>(90,"JET_MJB_Stat5__1down"));
    my_map.insert(pair<int, string>(91,"JET_MJB_Stat6__1up"));
    my_map.insert(pair<int, string>(92,"JET_MJB_Stat6__1down"));
    my_map.insert(pair<int, string>(93,"JET_MJB_Stat7__1up"));
    my_map.insert(pair<int, string>(94,"JET_MJB_Stat7__1down"));
    my_map.insert(pair<int, string>(95,"JET_MJB_Stat8__1up"));
    my_map.insert(pair<int, string>(96,"JET_MJB_Stat8__1down"));
    my_map.insert(pair<int, string>(97,"JET_MJB_Stat9__1up"));
    my_map.insert(pair<int, string>(98,"JET_MJB_Stat9__1down"));
    my_map.insert(pair<int, string>(99,"JET_MJB_Threshold__1up"));
    my_map.insert(pair<int, string>(100,"JET_MJB_Threshold__1down"));
    my_map.insert(pair<int, string>(101,"JET_Pileup_OffsetMu__1up"));
    my_map.insert(pair<int, string>(102,"JET_Pileup_OffsetMu__1down"));
    my_map.insert(pair<int, string>(103,"JET_Pileup_OffsetNPV__1up"));
    my_map.insert(pair<int, string>(104,"JET_Pileup_OffsetNPV__1down"));
    my_map.insert(pair<int, string>(105,"JET_Pileup_PtTerm__1up"));
    my_map.insert(pair<int, string>(106,"JET_Pileup_PtTerm__1down"));
    my_map.insert(pair<int, string>(107,"JET_Pileup_RhoTopology__1up"));
    my_map.insert(pair<int, string>(108,"JET_Pileup_RhoTopology__1down"));
    my_map.insert(pair<int, string>(109,"JET_PunchThrough_MC15__1up"));
    my_map.insert(pair<int, string>(110,"JET_PunchThrough_MC15__1down"));
    my_map.insert(pair<int, string>(111,"JET_SingleParticle_HighPt__1up"));
    my_map.insert(pair<int, string>(112,"JET_SingleParticle_HighPt__1down"));
    my_map.insert(pair<int, string>(113,"JET_Zjet_KTerm__1up"));
    my_map.insert(pair<int, string>(114,"JET_Zjet_KTerm__1down"));
    my_map.insert(pair<int, string>(115,"JET_Zjet_MC__1up"));
    my_map.insert(pair<int, string>(116,"JET_Zjet_MC__1down"));
    my_map.insert(pair<int, string>(117,"JET_Zjet_MuScale__1up"));
    my_map.insert(pair<int, string>(118,"JET_Zjet_MuScale__1down"));
    my_map.insert(pair<int, string>(119,"JET_Zjet_MuSmearID__1up"));
    my_map.insert(pair<int, string>(120,"JET_Zjet_MuSmearID__1down"));
    my_map.insert(pair<int, string>(121,"JET_Zjet_MuSmearMS__1up"));
    my_map.insert(pair<int, string>(122,"JET_Zjet_MuSmearMS__1down"));
    my_map.insert(pair<int, string>(123,"JET_Zjet_Stat10__1up"));
    my_map.insert(pair<int, string>(124,"JET_Zjet_Stat10__1down"));
    my_map.insert(pair<int, string>(125,"JET_Zjet_Stat11__1up"));
    my_map.insert(pair<int, string>(126,"JET_Zjet_Stat11__1down"));
    my_map.insert(pair<int, string>(127,"JET_Zjet_Stat12__1up"));
    my_map.insert(pair<int, string>(128,"JET_Zjet_Stat12__1down"));
    my_map.insert(pair<int, string>(129,"JET_Zjet_Stat13__1up"));
    my_map.insert(pair<int, string>(130,"JET_Zjet_Stat13__1down"));
    my_map.insert(pair<int, string>(131,"JET_Zjet_Stat1__1up"));
    my_map.insert(pair<int, string>(132,"JET_Zjet_Stat1__1down"));
    my_map.insert(pair<int, string>(133,"JET_Zjet_Stat2__1up"));
    my_map.insert(pair<int, string>(134,"JET_Zjet_Stat2__1down"));
    my_map.insert(pair<int, string>(135,"JET_Zjet_Stat3__1up"));
    my_map.insert(pair<int, string>(136,"JET_Zjet_Stat3__1down"));
    my_map.insert(pair<int, string>(137,"JET_Zjet_Stat4__1up"));
    my_map.insert(pair<int, string>(138,"JET_Zjet_Stat4__1down"));
    my_map.insert(pair<int, string>(139,"JET_Zjet_Stat5__1up"));
    my_map.insert(pair<int, string>(140,"JET_Zjet_Stat5__1down"));
    my_map.insert(pair<int, string>(141,"JET_Zjet_Stat6__1up"));
    my_map.insert(pair<int, string>(142,"JET_Zjet_Stat6__1down"));
    my_map.insert(pair<int, string>(143,"JET_Zjet_Stat7__1up"));
    my_map.insert(pair<int, string>(144,"JET_Zjet_Stat7__1down"));
    my_map.insert(pair<int, string>(145,"JET_Zjet_Stat8__1up"));
    my_map.insert(pair<int, string>(146,"JET_Zjet_Stat8__1down"));
    my_map.insert(pair<int, string>(147,"JET_Zjet_Stat9__1up"));
    my_map.insert(pair<int, string>(148,"JET_Zjet_Stat9__1down"));
    my_map.insert(pair<int, string>(149,"JET_Zjet_Veto__1up"));
    my_map.insert(pair<int, string>(150,"JET_Zjet_Veto__1down"));
    my_map.insert(pair<int, string>(151,"JET_Zjet_dPhi__1up"));
    my_map.insert(pair<int, string>(152,"JET_Zjet_dPhi__1down"));
*/
    //JER
//    my_map.insert(pair<int, string>(1,"JET_JER_SINGLE_NP__1up"));

// my_map.insert(pair<int, string>(0,"JetCleaningUnc"));
//  my_map.insert(pair<int, string>(0,"UData"));
// my_map.insert(pair<int, string>(1,"PRW_up"));
/*
    my_map.insert(pair<int, string>(1,"JET_JER_CROSS_CALIB_FORWARD__1up"));
 my_map.insert(pair<int, string>(2,"JET_JER_NOISE_FORWARD__1up"));
 my_map.insert(pair<int, string>(3,"JET_JER_NP0__1down"));
 my_map.insert(pair<int, string>(4,"JET_JER_NP0__1up"));
 my_map.insert(pair<int, string>(5,"JET_JER_NP1__1down"));
 my_map.insert(pair<int, string>(6,"JET_JER_NP1__1up"));
 my_map.insert(pair<int, string>(7,"JET_JER_NP2__1down"));
 my_map.insert(pair<int, string>(8,"JET_JER_NP2__1up"));
 my_map.insert(pair<int, string>(9,"JET_JER_NP3__1down"));
 my_map.insert(pair<int, string>(10,"JET_JER_NP3__1up"));
 my_map.insert(pair<int, string>(11,"JET_JER_NP4__1down"));
 my_map.insert(pair<int, string>(12,"JET_JER_NP4__1up"));
 my_map.insert(pair<int, string>(13,"JET_JER_NP5__1down"));
 my_map.insert(pair<int, string>(14,"JET_JER_NP5__1up"));
 my_map.insert(pair<int, string>(15,"JET_JER_NP6__1down"));
 my_map.insert(pair<int, string>(16,"JET_JER_NP6__1up"));
 my_map.insert(pair<int, string>(17,"JET_JER_NP7__1down"));
 my_map.insert(pair<int, string>(18,"JET_JER_NP7__1up"));
 my_map.insert(pair<int, string>(19,"JET_JER_NP8__1down"));
 my_map.insert(pair<int, string>(20,"JET_JER_NP8__1up"));
 my_map.insert(pair<int, string>(21,"JET_JER_CROSS_CALIB_FORWARD__1down"));
 my_map.insert(pair<int, string>(22,"JET_JER_NOISE_FORWARD__1down"));
*/

//    my_map.insert(pair<int, string>(154,"JET_JER_CROSS_CALIB_FORWARD__"));
//my_map.insert(pair<int, string>(155,"JET_JER_NOISE_FORWARD__"));
//my_map.insert(pair<int, string>(156,"JET_JER_NP0__"));
////    my_map.insert(pair<int, string>(4,"JET_JER_NP0__1up"));
//my_map.insert(pair<int, string>(157,"JET_JER_NP1__"));
////    my_map.insert(pair<int, string>(6,"JET_JER_NP1__1up"));
//my_map.insert(pair<int, string>(158,"JET_JER_NP2__"));
////    my_map.insert(pair<int, string>(8,"JET_JER_NP2__1up"));
//my_map.insert(pair<int, string>(159,"JET_JER_NP3__"));
////    my_map.insert(pair<int, string>(10,"JET_JER_NP3__1up"));
//my_map.insert(pair<int, string>(160,"JET_JER_NP4__"));
////    my_map.insert(pair<int, string>(12,"JET_JER_NP4__1up"));
//my_map.insert(pair<int, string>(161,"JET_JER_NP5__"));
////    my_map.insert(pair<int, string>(14,"JET_JER_NP5__1up"));
//my_map.insert(pair<int, string>(162,"JET_JER_NP6__"));
////    my_map.insert(pair<int, string>(16,"JET_JER_NP6__1up"));
//my_map.insert(pair<int, string>(163,"JET_JER_NP7__"));
////    my_map.insert(pair<int, string>(18,"JET_JER_NP7__1up"));
//my_map.insert(pair<int, string>(164,"JET_JER_NP8__"));
////    my_map.insert(pair<int, string>(20,"JET_JER_NP8__1up"));

//EICF

//    my_map.insert(pair<int, string>(1,"JET_EtaIntercalibration_TotalStat_MJB__1up"));
//    my_map.insert(pair<int, string>(2,"JET_EtaIntercalibration_TotalStat_MJB__1down"));
/*
    my_map.insert(pair<int, string>(0,"JET_EtaIntercalibration_Stat0__1up"));

// Mass JES correction

//    my_map.insert(pair<int, string>(0,"JET_EtaIntercalibration_TotalStat__1up"));
    
    my_map.insert(pair<int, string>(1,"JET_EtaIntercalibration_Stat1__1up"));
    my_map.insert(pair<int, string>(2,"JET_EtaIntercalibration_Stat2__1up"));
    my_map.insert(pair<int, string>(3,"JET_EtaIntercalibration_Stat3__1up"));
    my_map.insert(pair<int, string>(4,"JET_EtaIntercalibration_Stat4__1up"));
    my_map.insert(pair<int, string>(5,"JET_EtaIntercalibration_Stat5__1up"));
    my_map.insert(pair<int, string>(6,"JET_EtaIntercalibration_Stat6__1up"));
    my_map.insert(pair<int, string>(7,"JET_EtaIntercalibration_Stat7__1up"));
    my_map.insert(pair<int, string>(8,"JET_EtaIntercalibration_Stat8__1up"));
    my_map.insert(pair<int, string>(9,"JET_EtaIntercalibration_Stat9__1up"));
    my_map.insert(pair<int, string>(10,"JET_EtaIntercalibration_Stat10__1up"));
    my_map.insert(pair<int, string>(11,"JET_EtaIntercalibration_Stat11__1up"));
    my_map.insert(pair<int, string>(12,"JET_EtaIntercalibration_Stat12__1up"));
    my_map.insert(pair<int, string>(13,"JET_EtaIntercalibration_Stat13__1up"));
    my_map.insert(pair<int, string>(14,"JET_EtaIntercalibration_Stat14__1up"));
    my_map.insert(pair<int, string>(15,"JET_EtaIntercalibration_Stat15__1up"));
    my_map.insert(pair<int, string>(16,"JET_EtaIntercalibration_Stat16__1up"));
    my_map.insert(pair<int, string>(17,"JET_EtaIntercalibration_Stat17__1up"));
    my_map.insert(pair<int, string>(18,"JET_EtaIntercalibration_Stat18__1up"));
    my_map.insert(pair<int, string>(19,"JET_EtaIntercalibration_Stat19__1up"));
    my_map.insert(pair<int, string>(20,"JET_EtaIntercalibration_Stat20__1up"));
    my_map.insert(pair<int, string>(21,"JET_EtaIntercalibration_Stat21__1up"));
    my_map.insert(pair<int, string>(22,"JET_EtaIntercalibration_Stat22__1up"));
    my_map.insert(pair<int, string>(23,"JET_EtaIntercalibration_Stat23__1up"));
    my_map.insert(pair<int, string>(24,"JET_EtaIntercalibration_Stat24__1up"));
    my_map.insert(pair<int, string>(25,"JET_EtaIntercalibration_Stat25__1up"));
    my_map.insert(pair<int, string>(26,"JET_EtaIntercalibration_Stat26__1up"));
    my_map.insert(pair<int, string>(27,"JET_EtaIntercalibration_Stat27__1up"));
    my_map.insert(pair<int, string>(28,"JET_EtaIntercalibration_Stat28__1up"));
    my_map.insert(pair<int, string>(29,"JET_EtaIntercalibration_Stat29__1up"));
    my_map.insert(pair<int, string>(30,"JET_EtaIntercalibration_Stat30__1up"));
    my_map.insert(pair<int, string>(31,"JET_EtaIntercalibration_Stat31__1up"));
    my_map.insert(pair<int, string>(32,"JET_EtaIntercalibration_Stat32__1up"));
    my_map.insert(pair<int, string>(33,"JET_EtaIntercalibration_Stat33__1up"));
    my_map.insert(pair<int, string>(34,"JET_EtaIntercalibration_Stat34__1up"));
    my_map.insert(pair<int, string>(35,"JET_EtaIntercalibration_Stat35__1up"));
    my_map.insert(pair<int, string>(36,"JET_EtaIntercalibration_Stat36__1up"));
    my_map.insert(pair<int, string>(37,"JET_EtaIntercalibration_Stat37__1up"));
    my_map.insert(pair<int, string>(38,"JET_EtaIntercalibration_Stat38__1up"));
    my_map.insert(pair<int, string>(39,"JET_EtaIntercalibration_Stat39__1up"));
    my_map.insert(pair<int, string>(40,"JET_EtaIntercalibration_Stat40__1up"));
    my_map.insert(pair<int, string>(41,"JET_EtaIntercalibration_Stat41__1up"));
    my_map.insert(pair<int, string>(42,"JET_EtaIntercalibration_Stat42__1up"));
    my_map.insert(pair<int, string>(43,"JET_EtaIntercalibration_Stat43__1up"));
    my_map.insert(pair<int, string>(44,"JET_EtaIntercalibration_Stat44__1up"));
    my_map.insert(pair<int, string>(45,"JET_EtaIntercalibration_Stat45__1up"));
    my_map.insert(pair<int, string>(46,"JET_EtaIntercalibration_Stat46__1up"));
    my_map.insert(pair<int, string>(47,"JET_EtaIntercalibration_Stat47__1up"));
    my_map.insert(pair<int, string>(48,"JET_EtaIntercalibration_Stat48__1up"));
    my_map.insert(pair<int, string>(49,"JET_EtaIntercalibration_Stat49__1up"));
    my_map.insert(pair<int, string>(50,"JET_EtaIntercalibration_Stat50__1up"));
    my_map.insert(pair<int, string>(51,"JET_EtaIntercalibration_Stat51__1up"));
    my_map.insert(pair<int, string>(52,"JET_EtaIntercalibration_Stat52__1up"));
    my_map.insert(pair<int, string>(53,"JET_EtaIntercalibration_Stat53__1up"));
    my_map.insert(pair<int, string>(54,"JET_EtaIntercalibration_Stat54__1up"));
    my_map.insert(pair<int, string>(55,"JET_EtaIntercalibration_Stat55__1up"));
    my_map.insert(pair<int, string>(56,"JET_EtaIntercalibration_Stat56__1up"));
    my_map.insert(pair<int, string>(57,"JET_EtaIntercalibration_Stat57__1up"));
    my_map.insert(pair<int, string>(58,"JET_EtaIntercalibration_Stat58__1up"));
    my_map.insert(pair<int, string>(59,"JET_EtaIntercalibration_Stat59__1up"));
    my_map.insert(pair<int, string>(60,"JET_EtaIntercalibration_Stat60__1up"));
    my_map.insert(pair<int, string>(61,"JET_EtaIntercalibration_Stat61__1up"));
    my_map.insert(pair<int, string>(62,"JET_EtaIntercalibration_Stat62__1up"));
    my_map.insert(pair<int, string>(63,"JET_EtaIntercalibration_Stat63__1up"));
    my_map.insert(pair<int, string>(64,"JET_EtaIntercalibration_Stat64__1up"));
    my_map.insert(pair<int, string>(65,"JET_EtaIntercalibration_Stat65__1up"));
    my_map.insert(pair<int, string>(66,"JET_EtaIntercalibration_Stat66__1up"));
    my_map.insert(pair<int, string>(67,"JET_EtaIntercalibration_Stat67__1up"));
    my_map.insert(pair<int, string>(68,"JET_EtaIntercalibration_Stat68__1up"));
    my_map.insert(pair<int, string>(69,"JET_EtaIntercalibration_Stat69__1up"));
    my_map.insert(pair<int, string>(70,"JET_EtaIntercalibration_Stat70__1up"));
    my_map.insert(pair<int, string>(71,"JET_EtaIntercalibration_Stat71__1up"));
    my_map.insert(pair<int, string>(72,"JET_EtaIntercalibration_Stat72__1up"));
    my_map.insert(pair<int, string>(73,"JET_EtaIntercalibration_Stat73__1up"));
    my_map.insert(pair<int, string>(74,"JET_EtaIntercalibration_Stat74__1up"));
    my_map.insert(pair<int, string>(75,"JET_EtaIntercalibration_Stat75__1up"));
    my_map.insert(pair<int, string>(76,"JET_EtaIntercalibration_Stat76__1up"));
    my_map.insert(pair<int, string>(77,"JET_EtaIntercalibration_Stat77__1up"));
    my_map.insert(pair<int, string>(78,"JET_EtaIntercalibration_Stat78__1up"));
    my_map.insert(pair<int, string>(79,"JET_EtaIntercalibration_Stat79__1up"));
    my_map.insert(pair<int, string>(80,"JET_EtaIntercalibration_Stat80__1up"));
    my_map.insert(pair<int, string>(81,"JET_EtaIntercalibration_Stat81__1up"));
    my_map.insert(pair<int, string>(82,"JET_EtaIntercalibration_Stat82__1up"));
    my_map.insert(pair<int, string>(83,"JET_EtaIntercalibration_Stat83__1up"));
    my_map.insert(pair<int, string>(84,"JET_EtaIntercalibration_Stat84__1up"));
    my_map.insert(pair<int, string>(85,"JET_EtaIntercalibration_Stat85__1up"));
    my_map.insert(pair<int, string>(86,"JET_EtaIntercalibration_Stat86__1up"));
    my_map.insert(pair<int, string>(87,"JET_EtaIntercalibration_Stat87__1up"));
    my_map.insert(pair<int, string>(88,"JET_EtaIntercalibration_Stat88__1up"));
    my_map.insert(pair<int, string>(89,"JET_EtaIntercalibration_Stat89__1up"));
    my_map.insert(pair<int, string>(90,"JET_EtaIntercalibration_Stat90__1up"));
    my_map.insert(pair<int, string>(91,"JET_EtaIntercalibration_Stat91__1up"));
    my_map.insert(pair<int, string>(92,"JET_EtaIntercalibration_Stat92__1up"));
    my_map.insert(pair<int, string>(93,"JET_EtaIntercalibration_Stat93__1up"));
    my_map.insert(pair<int, string>(94,"JET_EtaIntercalibration_Stat94__1up"));
    my_map.insert(pair<int, string>(95,"JET_EtaIntercalibration_Stat95__1up"));
    my_map.insert(pair<int, string>(96,"JET_EtaIntercalibration_Stat96__1up"));
    my_map.insert(pair<int, string>(97,"JET_EtaIntercalibration_Stat97__1up"));
    my_map.insert(pair<int, string>(98,"JET_EtaIntercalibration_Stat98__1up"));
    my_map.insert(pair<int, string>(99,"JET_EtaIntercalibration_Stat99__1up"));
    my_map.insert(pair<int, string>(100,"JET_EtaIntercalibration_Stat100__1up"));
    my_map.insert(pair<int, string>(101,"JET_EtaIntercalibration_Stat101__1up"));
    my_map.insert(pair<int, string>(102,"JET_EtaIntercalibration_Stat102__1up"));
    my_map.insert(pair<int, string>(103,"JET_EtaIntercalibration_Stat103__1up"));
    my_map.insert(pair<int, string>(104,"JET_EtaIntercalibration_Stat104__1up"));
    my_map.insert(pair<int, string>(105,"JET_EtaIntercalibration_Stat105__1up"));
    my_map.insert(pair<int, string>(106,"JET_EtaIntercalibration_Stat106__1up"));
    my_map.insert(pair<int, string>(107,"JET_EtaIntercalibration_Stat107__1up"));
    my_map.insert(pair<int, string>(108,"JET_EtaIntercalibration_Stat108__1up"));
    my_map.insert(pair<int, string>(109,"JET_EtaIntercalibration_Stat109__1up"));
    my_map.insert(pair<int, string>(110,"JET_EtaIntercalibration_Stat110__1up"));
    my_map.insert(pair<int, string>(111,"JET_EtaIntercalibration_Stat111__1up"));
    my_map.insert(pair<int, string>(112,"JET_EtaIntercalibration_Stat112__1up"));
    my_map.insert(pair<int, string>(113,"JET_EtaIntercalibration_Stat113__1up"));
    my_map.insert(pair<int, string>(114,"JET_EtaIntercalibration_Stat114__1up"));
    my_map.insert(pair<int, string>(115,"JET_EtaIntercalibration_Stat115__1up"));
    my_map.insert(pair<int, string>(116,"JET_EtaIntercalibration_Stat116__1up"));
    my_map.insert(pair<int, string>(117,"JET_EtaIntercalibration_Stat117__1up"));
    my_map.insert(pair<int, string>(118,"JET_EtaIntercalibration_Stat118__1up"));
    my_map.insert(pair<int, string>(119,"JET_EtaIntercalibration_Stat119__1up"));
    my_map.insert(pair<int, string>(120,"JET_EtaIntercalibration_Stat120__1up"));
    my_map.insert(pair<int, string>(121,"JET_EtaIntercalibration_Stat121__1up"));
    my_map.insert(pair<int, string>(122,"JET_EtaIntercalibration_Stat122__1up"));
    my_map.insert(pair<int, string>(123,"JET_EtaIntercalibration_Stat123__1up"));
    my_map.insert(pair<int, string>(124,"JET_EtaIntercalibration_Stat124__1up"));
    my_map.insert(pair<int, string>(125,"JET_EtaIntercalibration_Stat125__1up"));
    my_map.insert(pair<int, string>(126,"JET_EtaIntercalibration_Stat126__1up"));
    my_map.insert(pair<int, string>(127,"JET_EtaIntercalibration_Stat127__1up"));
    my_map.insert(pair<int, string>(128,"JET_EtaIntercalibration_Stat128__1up"));
    my_map.insert(pair<int, string>(129,"JET_EtaIntercalibration_Stat129__1up"));
    my_map.insert(pair<int, string>(130,"JET_EtaIntercalibration_Stat130__1up"));
    my_map.insert(pair<int, string>(131,"JET_EtaIntercalibration_Stat131__1up"));
    my_map.insert(pair<int, string>(132,"JET_EtaIntercalibration_Stat132__1up"));
    my_map.insert(pair<int, string>(133,"JET_EtaIntercalibration_Stat133__1up"));
    my_map.insert(pair<int, string>(134,"JET_EtaIntercalibration_Stat134__1up"));
    my_map.insert(pair<int, string>(135,"JET_EtaIntercalibration_Stat135__1up"));
    my_map.insert(pair<int, string>(136,"JET_EtaIntercalibration_Stat136__1up"));
    my_map.insert(pair<int, string>(137,"JET_EtaIntercalibration_Stat137__1up"));
    my_map.insert(pair<int, string>(138,"JET_EtaIntercalibration_Stat138__1up"));
    my_map.insert(pair<int, string>(139,"JET_EtaIntercalibration_Stat139__1up"));
    my_map.insert(pair<int, string>(140,"JET_EtaIntercalibration_Stat140__1up"));
    my_map.insert(pair<int, string>(141,"JET_EtaIntercalibration_Stat141__1up"));
    my_map.insert(pair<int, string>(142,"JET_EtaIntercalibration_Stat142__1up"));
    my_map.insert(pair<int, string>(143,"JET_EtaIntercalibration_Stat143__1up"));
    my_map.insert(pair<int, string>(144,"JET_EtaIntercalibration_Stat144__1up"));
    my_map.insert(pair<int, string>(145,"JET_EtaIntercalibration_Stat145__1up"));
    my_map.insert(pair<int, string>(146,"JET_EtaIntercalibration_Stat146__1up"));
    my_map.insert(pair<int, string>(147,"JET_EtaIntercalibration_Stat147__1up"));
    my_map.insert(pair<int, string>(148,"JET_EtaIntercalibration_Stat148__1up"));
    my_map.insert(pair<int, string>(149,"JET_EtaIntercalibration_Stat149__1up"));
    my_map.insert(pair<int, string>(150,"JET_EtaIntercalibration_Stat150__1up"));
    my_map.insert(pair<int, string>(151,"JET_EtaIntercalibration_Stat151__1up"));
    my_map.insert(pair<int, string>(152,"JET_EtaIntercalibration_Stat152__1up"));
    my_map.insert(pair<int, string>(153,"JET_EtaIntercalibration_Stat153__1up"));
    my_map.insert(pair<int, string>(154,"JET_EtaIntercalibration_Stat154__1up"));
    my_map.insert(pair<int, string>(155,"JET_EtaIntercalibration_Stat155__1up"));
    my_map.insert(pair<int, string>(156,"JET_EtaIntercalibration_Stat156__1up"));
    my_map.insert(pair<int, string>(157,"JET_EtaIntercalibration_Stat157__1up"));
    my_map.insert(pair<int, string>(158,"JET_EtaIntercalibration_Stat158__1up"));
    my_map.insert(pair<int, string>(159,"JET_EtaIntercalibration_Stat159__1up"));
    my_map.insert(pair<int, string>(160,"JET_EtaIntercalibration_Stat160__1up"));
    my_map.insert(pair<int, string>(161,"JET_EtaIntercalibration_Stat161__1up"));
    my_map.insert(pair<int, string>(162,"JET_EtaIntercalibration_Stat162__1up"));
    my_map.insert(pair<int, string>(163,"JET_EtaIntercalibration_Stat163__1up"));
    my_map.insert(pair<int, string>(164,"JET_EtaIntercalibration_Stat164__1up"));
    my_map.insert(pair<int, string>(165,"JET_EtaIntercalibration_Stat165__1up"));
    my_map.insert(pair<int, string>(166,"JET_EtaIntercalibration_Stat166__1up"));
    my_map.insert(pair<int, string>(167,"JET_EtaIntercalibration_Stat167__1up"));
    my_map.insert(pair<int, string>(168,"JET_EtaIntercalibration_Stat168__1up"));
    my_map.insert(pair<int, string>(169,"JET_EtaIntercalibration_Stat169__1up"));
    my_map.insert(pair<int, string>(170,"JET_EtaIntercalibration_Stat170__1up"));
    my_map.insert(pair<int, string>(171,"JET_EtaIntercalibration_Stat171__1up"));
    my_map.insert(pair<int, string>(172,"JET_EtaIntercalibration_Stat172__1up"));
    my_map.insert(pair<int, string>(173,"JET_EtaIntercalibration_Stat173__1up"));
    my_map.insert(pair<int, string>(174,"JET_EtaIntercalibration_Stat174__1up"));
    my_map.insert(pair<int, string>(175,"JET_EtaIntercalibration_Stat175__1up"));
    my_map.insert(pair<int, string>(176,"JET_EtaIntercalibration_Stat176__1up"));
    my_map.insert(pair<int, string>(177,"JET_EtaIntercalibration_Stat177__1up"));
    my_map.insert(pair<int, string>(178,"JET_EtaIntercalibration_Stat178__1up"));
    my_map.insert(pair<int, string>(179,"JET_EtaIntercalibration_Stat179__1up"));
    my_map.insert(pair<int, string>(180,"JET_EtaIntercalibration_Stat180__1up"));
    my_map.insert(pair<int, string>(181,"JET_EtaIntercalibration_Stat181__1up"));
    my_map.insert(pair<int, string>(182,"JET_EtaIntercalibration_Stat182__1up"));
    my_map.insert(pair<int, string>(183,"JET_EtaIntercalibration_Stat183__1up"));
    my_map.insert(pair<int, string>(184,"JET_EtaIntercalibration_Stat184__1up"));
    my_map.insert(pair<int, string>(185,"JET_EtaIntercalibration_Stat185__1up"));
    my_map.insert(pair<int, string>(186,"JET_EtaIntercalibration_Stat186__1up"));
    my_map.insert(pair<int, string>(187,"JET_EtaIntercalibration_Stat187__1up"));
    my_map.insert(pair<int, string>(188,"JET_EtaIntercalibration_Stat188__1up"));
    my_map.insert(pair<int, string>(189,"JET_EtaIntercalibration_Stat189__1up"));
    my_map.insert(pair<int, string>(190,"JET_EtaIntercalibration_Stat190__1up"));
    my_map.insert(pair<int, string>(191,"JET_EtaIntercalibration_Stat191__1up"));
    my_map.insert(pair<int, string>(192,"JET_EtaIntercalibration_Stat192__1up"));
    my_map.insert(pair<int, string>(193,"JET_EtaIntercalibration_Stat193__1up"));
    my_map.insert(pair<int, string>(194,"JET_EtaIntercalibration_Stat194__1up"));
    my_map.insert(pair<int, string>(195,"JET_EtaIntercalibration_Stat195__1up"));
    my_map.insert(pair<int, string>(196,"JET_EtaIntercalibration_Stat196__1up"));
    my_map.insert(pair<int, string>(197,"JET_EtaIntercalibration_Stat197__1up"));
    my_map.insert(pair<int, string>(198,"JET_EtaIntercalibration_Stat198__1up"));
    my_map.insert(pair<int, string>(199,"JET_EtaIntercalibration_Stat199__1up"));
    my_map.insert(pair<int, string>(200,"JET_EtaIntercalibration_Stat200__1up"));
    my_map.insert(pair<int, string>(201,"JET_EtaIntercalibration_Stat201__1up"));
    my_map.insert(pair<int, string>(202,"JET_EtaIntercalibration_Stat202__1up"));
    my_map.insert(pair<int, string>(203,"JET_EtaIntercalibration_Stat203__1up"));
    my_map.insert(pair<int, string>(204,"JET_EtaIntercalibration_Stat204__1up"));
    my_map.insert(pair<int, string>(205,"JET_EtaIntercalibration_Stat205__1up"));
    my_map.insert(pair<int, string>(206,"JET_EtaIntercalibration_Stat206__1up"));
    my_map.insert(pair<int, string>(207,"JET_EtaIntercalibration_Stat207__1up"));
    my_map.insert(pair<int, string>(208,"JET_EtaIntercalibration_Stat208__1up"));
    my_map.insert(pair<int, string>(209,"JET_EtaIntercalibration_Stat209__1up"));
    my_map.insert(pair<int, string>(210,"JET_EtaIntercalibration_Stat210__1up"));
    my_map.insert(pair<int, string>(211,"JET_EtaIntercalibration_Stat211__1up"));
    my_map.insert(pair<int, string>(212,"JET_EtaIntercalibration_Stat212__1up"));
    my_map.insert(pair<int, string>(213,"JET_EtaIntercalibration_Stat213__1up"));
    my_map.insert(pair<int, string>(214,"JET_EtaIntercalibration_Stat214__1up"));
    my_map.insert(pair<int, string>(215,"JET_EtaIntercalibration_Stat215__1up"));
    my_map.insert(pair<int, string>(216,"JET_EtaIntercalibration_Stat216__1up"));
    my_map.insert(pair<int, string>(217,"JET_EtaIntercalibration_Stat217__1up"));
    my_map.insert(pair<int, string>(218,"JET_EtaIntercalibration_Stat218__1up"));
    my_map.insert(pair<int, string>(219,"JET_EtaIntercalibration_Stat219__1up"));
    my_map.insert(pair<int, string>(220,"JET_EtaIntercalibration_Stat220__1up"));
    my_map.insert(pair<int, string>(221,"JET_EtaIntercalibration_Stat221__1up"));
    my_map.insert(pair<int, string>(222,"JET_EtaIntercalibration_Stat222__1up"));
    my_map.insert(pair<int, string>(223,"JET_EtaIntercalibration_Stat223__1up"));
    my_map.insert(pair<int, string>(224,"JET_EtaIntercalibration_Stat224__1up"));
    my_map.insert(pair<int, string>(225,"JET_EtaIntercalibration_Stat225__1up"));
    my_map.insert(pair<int, string>(226,"JET_EtaIntercalibration_Stat226__1up"));
    my_map.insert(pair<int, string>(227,"JET_EtaIntercalibration_Stat227__1up"));
    my_map.insert(pair<int, string>(228,"JET_EtaIntercalibration_Stat228__1up"));
    my_map.insert(pair<int, string>(229,"JET_EtaIntercalibration_Stat229__1up"));
    my_map.insert(pair<int, string>(230,"JET_EtaIntercalibration_Stat230__1up"));
    my_map.insert(pair<int, string>(231,"JET_EtaIntercalibration_Stat231__1up"));
    my_map.insert(pair<int, string>(232,"JET_EtaIntercalibration_Stat232__1up"));
    my_map.insert(pair<int, string>(233,"JET_EtaIntercalibration_Stat233__1up"));
    my_map.insert(pair<int, string>(234,"JET_EtaIntercalibration_Stat234__1up"));
    my_map.insert(pair<int, string>(235,"JET_EtaIntercalibration_Stat235__1up"));
    my_map.insert(pair<int, string>(236,"JET_EtaIntercalibration_Stat236__1up"));
    my_map.insert(pair<int, string>(237,"JET_EtaIntercalibration_Stat237__1up"));
    my_map.insert(pair<int, string>(238,"JET_EtaIntercalibration_Stat238__1up"));
    my_map.insert(pair<int, string>(239,"JET_EtaIntercalibration_Stat239__1up"));
    my_map.insert(pair<int, string>(240,"JET_EtaIntercalibration_Stat240__1up"));
    my_map.insert(pair<int, string>(241,"JET_EtaIntercalibration_Stat241__1up"));
    my_map.insert(pair<int, string>(242,"JET_EtaIntercalibration_Stat242__1up"));
    my_map.insert(pair<int, string>(243,"JET_EtaIntercalibration_Stat243__1up"));
    my_map.insert(pair<int, string>(244,"JET_EtaIntercalibration_Stat244__1up"));
    my_map.insert(pair<int, string>(245,"JET_EtaIntercalibration_Stat245__1up"));

    my_map.insert(pair<int, string>(246,"JET_EtaIntercalibration_Stat0__1down"));
    
    //my_map.insert(pair<int, string>(246,"JET_EtaIntercalibration_TotalStat__1down"));
    my_map.insert(pair<int, string>(247,"JET_EtaIntercalibration_Stat1__1down"));
    my_map.insert(pair<int, string>(248,"JET_EtaIntercalibration_Stat2__1down"));
    my_map.insert(pair<int, string>(249,"JET_EtaIntercalibration_Stat3__1down"));
    my_map.insert(pair<int, string>(250,"JET_EtaIntercalibration_Stat4__1down"));
    my_map.insert(pair<int, string>(251,"JET_EtaIntercalibration_Stat5__1down"));
    my_map.insert(pair<int, string>(252,"JET_EtaIntercalibration_Stat6__1down"));
    my_map.insert(pair<int, string>(253,"JET_EtaIntercalibration_Stat7__1down"));
    my_map.insert(pair<int, string>(254,"JET_EtaIntercalibration_Stat8__1down"));
    my_map.insert(pair<int, string>(255,"JET_EtaIntercalibration_Stat9__1down"));
    my_map.insert(pair<int, string>(256,"JET_EtaIntercalibration_Stat10__1down"));
    my_map.insert(pair<int, string>(257,"JET_EtaIntercalibration_Stat11__1down"));
    my_map.insert(pair<int, string>(258,"JET_EtaIntercalibration_Stat12__1down"));
    my_map.insert(pair<int, string>(259,"JET_EtaIntercalibration_Stat13__1down"));
    my_map.insert(pair<int, string>(260,"JET_EtaIntercalibration_Stat14__1down"));
    my_map.insert(pair<int, string>(261,"JET_EtaIntercalibration_Stat15__1down"));
    my_map.insert(pair<int, string>(262,"JET_EtaIntercalibration_Stat16__1down"));
    my_map.insert(pair<int, string>(263,"JET_EtaIntercalibration_Stat17__1down"));
    my_map.insert(pair<int, string>(264,"JET_EtaIntercalibration_Stat18__1down"));
    my_map.insert(pair<int, string>(265,"JET_EtaIntercalibration_Stat19__1down"));
    my_map.insert(pair<int, string>(266,"JET_EtaIntercalibration_Stat20__1down"));
    my_map.insert(pair<int, string>(267,"JET_EtaIntercalibration_Stat21__1down"));
    my_map.insert(pair<int, string>(268,"JET_EtaIntercalibration_Stat22__1down"));
    my_map.insert(pair<int, string>(269,"JET_EtaIntercalibration_Stat23__1down"));
    my_map.insert(pair<int, string>(270,"JET_EtaIntercalibration_Stat24__1down"));
    my_map.insert(pair<int, string>(271,"JET_EtaIntercalibration_Stat25__1down"));
    my_map.insert(pair<int, string>(272,"JET_EtaIntercalibration_Stat26__1down"));
    my_map.insert(pair<int, string>(273,"JET_EtaIntercalibration_Stat27__1down"));
    my_map.insert(pair<int, string>(274,"JET_EtaIntercalibration_Stat28__1down"));
    my_map.insert(pair<int, string>(275,"JET_EtaIntercalibration_Stat29__1down"));
    my_map.insert(pair<int, string>(276,"JET_EtaIntercalibration_Stat30__1down"));
    my_map.insert(pair<int, string>(277,"JET_EtaIntercalibration_Stat31__1down"));
    my_map.insert(pair<int, string>(278,"JET_EtaIntercalibration_Stat32__1down"));
    my_map.insert(pair<int, string>(279,"JET_EtaIntercalibration_Stat33__1down"));
    my_map.insert(pair<int, string>(280,"JET_EtaIntercalibration_Stat34__1down"));
    my_map.insert(pair<int, string>(281,"JET_EtaIntercalibration_Stat35__1down"));
    my_map.insert(pair<int, string>(282,"JET_EtaIntercalibration_Stat36__1down"));
    my_map.insert(pair<int, string>(283,"JET_EtaIntercalibration_Stat37__1down"));
    my_map.insert(pair<int, string>(284,"JET_EtaIntercalibration_Stat38__1down"));
    my_map.insert(pair<int, string>(285,"JET_EtaIntercalibration_Stat39__1down"));
    my_map.insert(pair<int, string>(286,"JET_EtaIntercalibration_Stat40__1down"));
    my_map.insert(pair<int, string>(287,"JET_EtaIntercalibration_Stat41__1down"));
    my_map.insert(pair<int, string>(288,"JET_EtaIntercalibration_Stat42__1down"));
    my_map.insert(pair<int, string>(289,"JET_EtaIntercalibration_Stat43__1down"));
    my_map.insert(pair<int, string>(290,"JET_EtaIntercalibration_Stat44__1down"));
    my_map.insert(pair<int, string>(291,"JET_EtaIntercalibration_Stat45__1down"));
    my_map.insert(pair<int, string>(292,"JET_EtaIntercalibration_Stat46__1down"));
    my_map.insert(pair<int, string>(293,"JET_EtaIntercalibration_Stat47__1down"));
    my_map.insert(pair<int, string>(294,"JET_EtaIntercalibration_Stat48__1down"));
    my_map.insert(pair<int, string>(295,"JET_EtaIntercalibration_Stat49__1down"));
    my_map.insert(pair<int, string>(296,"JET_EtaIntercalibration_Stat50__1down"));
    my_map.insert(pair<int, string>(297,"JET_EtaIntercalibration_Stat51__1down"));
    my_map.insert(pair<int, string>(298,"JET_EtaIntercalibration_Stat52__1down"));
    my_map.insert(pair<int, string>(299,"JET_EtaIntercalibration_Stat53__1down"));
    my_map.insert(pair<int, string>(300,"JET_EtaIntercalibration_Stat54__1down"));
    my_map.insert(pair<int, string>(301,"JET_EtaIntercalibration_Stat55__1down"));
    my_map.insert(pair<int, string>(302,"JET_EtaIntercalibration_Stat56__1down"));
    my_map.insert(pair<int, string>(303,"JET_EtaIntercalibration_Stat57__1down"));
    my_map.insert(pair<int, string>(304,"JET_EtaIntercalibration_Stat58__1down"));
    my_map.insert(pair<int, string>(305,"JET_EtaIntercalibration_Stat59__1down"));
    my_map.insert(pair<int, string>(306,"JET_EtaIntercalibration_Stat60__1down"));
    my_map.insert(pair<int, string>(307,"JET_EtaIntercalibration_Stat61__1down"));
    my_map.insert(pair<int, string>(308,"JET_EtaIntercalibration_Stat62__1down"));
    my_map.insert(pair<int, string>(309,"JET_EtaIntercalibration_Stat63__1down"));
    my_map.insert(pair<int, string>(310,"JET_EtaIntercalibration_Stat64__1down"));
    my_map.insert(pair<int, string>(311,"JET_EtaIntercalibration_Stat65__1down"));
    my_map.insert(pair<int, string>(312,"JET_EtaIntercalibration_Stat66__1down"));
    my_map.insert(pair<int, string>(313,"JET_EtaIntercalibration_Stat67__1down"));
    my_map.insert(pair<int, string>(314,"JET_EtaIntercalibration_Stat68__1down"));
    my_map.insert(pair<int, string>(315,"JET_EtaIntercalibration_Stat69__1down"));
    my_map.insert(pair<int, string>(316,"JET_EtaIntercalibration_Stat70__1down"));
    my_map.insert(pair<int, string>(317,"JET_EtaIntercalibration_Stat71__1down"));
    my_map.insert(pair<int, string>(318,"JET_EtaIntercalibration_Stat72__1down"));
    my_map.insert(pair<int, string>(319,"JET_EtaIntercalibration_Stat73__1down"));
    my_map.insert(pair<int, string>(320,"JET_EtaIntercalibration_Stat74__1down"));
    my_map.insert(pair<int, string>(321,"JET_EtaIntercalibration_Stat75__1down"));
    my_map.insert(pair<int, string>(322,"JET_EtaIntercalibration_Stat76__1down"));
    my_map.insert(pair<int, string>(323,"JET_EtaIntercalibration_Stat77__1down"));
    my_map.insert(pair<int, string>(324,"JET_EtaIntercalibration_Stat78__1down"));
    my_map.insert(pair<int, string>(325,"JET_EtaIntercalibration_Stat79__1down"));
    my_map.insert(pair<int, string>(326,"JET_EtaIntercalibration_Stat80__1down"));
    my_map.insert(pair<int, string>(327,"JET_EtaIntercalibration_Stat81__1down"));
    my_map.insert(pair<int, string>(328,"JET_EtaIntercalibration_Stat82__1down"));
    my_map.insert(pair<int, string>(329,"JET_EtaIntercalibration_Stat83__1down"));
    my_map.insert(pair<int, string>(330,"JET_EtaIntercalibration_Stat84__1down"));
    my_map.insert(pair<int, string>(331,"JET_EtaIntercalibration_Stat85__1down"));
    my_map.insert(pair<int, string>(332,"JET_EtaIntercalibration_Stat86__1down"));
    my_map.insert(pair<int, string>(333,"JET_EtaIntercalibration_Stat87__1down"));
    my_map.insert(pair<int, string>(334,"JET_EtaIntercalibration_Stat88__1down"));
    my_map.insert(pair<int, string>(335,"JET_EtaIntercalibration_Stat89__1down"));
    my_map.insert(pair<int, string>(336,"JET_EtaIntercalibration_Stat90__1down"));
    my_map.insert(pair<int, string>(337,"JET_EtaIntercalibration_Stat91__1down"));
    my_map.insert(pair<int, string>(338,"JET_EtaIntercalibration_Stat92__1down"));
    my_map.insert(pair<int, string>(339,"JET_EtaIntercalibration_Stat93__1down"));
    my_map.insert(pair<int, string>(340,"JET_EtaIntercalibration_Stat94__1down"));
    my_map.insert(pair<int, string>(341,"JET_EtaIntercalibration_Stat95__1down"));
    my_map.insert(pair<int, string>(342,"JET_EtaIntercalibration_Stat96__1down"));
    my_map.insert(pair<int, string>(343,"JET_EtaIntercalibration_Stat97__1down"));
    my_map.insert(pair<int, string>(344,"JET_EtaIntercalibration_Stat98__1down"));
    my_map.insert(pair<int, string>(345,"JET_EtaIntercalibration_Stat99__1down"));
    my_map.insert(pair<int, string>(346,"JET_EtaIntercalibration_Stat100__1down"));
    my_map.insert(pair<int, string>(347,"JET_EtaIntercalibration_Stat101__1down"));
    my_map.insert(pair<int, string>(348,"JET_EtaIntercalibration_Stat102__1down"));
    my_map.insert(pair<int, string>(349,"JET_EtaIntercalibration_Stat103__1down"));
    my_map.insert(pair<int, string>(350,"JET_EtaIntercalibration_Stat104__1down"));
    my_map.insert(pair<int, string>(351,"JET_EtaIntercalibration_Stat105__1down"));
    my_map.insert(pair<int, string>(352,"JET_EtaIntercalibration_Stat106__1down"));
    my_map.insert(pair<int, string>(353,"JET_EtaIntercalibration_Stat107__1down"));
    my_map.insert(pair<int, string>(354,"JET_EtaIntercalibration_Stat108__1down"));
    my_map.insert(pair<int, string>(355,"JET_EtaIntercalibration_Stat109__1down"));
    my_map.insert(pair<int, string>(356,"JET_EtaIntercalibration_Stat110__1down"));
    my_map.insert(pair<int, string>(357,"JET_EtaIntercalibration_Stat111__1down"));
    my_map.insert(pair<int, string>(358,"JET_EtaIntercalibration_Stat112__1down"));
    my_map.insert(pair<int, string>(359,"JET_EtaIntercalibration_Stat113__1down"));
    my_map.insert(pair<int, string>(360,"JET_EtaIntercalibration_Stat114__1down"));
    my_map.insert(pair<int, string>(361,"JET_EtaIntercalibration_Stat115__1down"));
    my_map.insert(pair<int, string>(362,"JET_EtaIntercalibration_Stat116__1down"));
    my_map.insert(pair<int, string>(363,"JET_EtaIntercalibration_Stat117__1down"));
    my_map.insert(pair<int, string>(364,"JET_EtaIntercalibration_Stat118__1down"));
    my_map.insert(pair<int, string>(365,"JET_EtaIntercalibration_Stat119__1down"));
    my_map.insert(pair<int, string>(366,"JET_EtaIntercalibration_Stat120__1down"));
    my_map.insert(pair<int, string>(367,"JET_EtaIntercalibration_Stat121__1down"));
    my_map.insert(pair<int, string>(368,"JET_EtaIntercalibration_Stat122__1down"));
    my_map.insert(pair<int, string>(369,"JET_EtaIntercalibration_Stat123__1down"));
    my_map.insert(pair<int, string>(370,"JET_EtaIntercalibration_Stat124__1down"));
    my_map.insert(pair<int, string>(371,"JET_EtaIntercalibration_Stat125__1down"));
    my_map.insert(pair<int, string>(372,"JET_EtaIntercalibration_Stat126__1down"));
    my_map.insert(pair<int, string>(373,"JET_EtaIntercalibration_Stat127__1down"));
    my_map.insert(pair<int, string>(374,"JET_EtaIntercalibration_Stat128__1down"));
    my_map.insert(pair<int, string>(375,"JET_EtaIntercalibration_Stat129__1down"));
    my_map.insert(pair<int, string>(376,"JET_EtaIntercalibration_Stat130__1down"));
    my_map.insert(pair<int, string>(377,"JET_EtaIntercalibration_Stat131__1down"));
    my_map.insert(pair<int, string>(378,"JET_EtaIntercalibration_Stat132__1down"));
    my_map.insert(pair<int, string>(379,"JET_EtaIntercalibration_Stat133__1down"));
    my_map.insert(pair<int, string>(380,"JET_EtaIntercalibration_Stat134__1down"));
    my_map.insert(pair<int, string>(381,"JET_EtaIntercalibration_Stat135__1down"));
    my_map.insert(pair<int, string>(382,"JET_EtaIntercalibration_Stat136__1down"));
    my_map.insert(pair<int, string>(383,"JET_EtaIntercalibration_Stat137__1down"));
    my_map.insert(pair<int, string>(384,"JET_EtaIntercalibration_Stat138__1down"));
    my_map.insert(pair<int, string>(385,"JET_EtaIntercalibration_Stat139__1down"));
    my_map.insert(pair<int, string>(386,"JET_EtaIntercalibration_Stat140__1down"));
    my_map.insert(pair<int, string>(387,"JET_EtaIntercalibration_Stat141__1down"));
    my_map.insert(pair<int, string>(388,"JET_EtaIntercalibration_Stat142__1down"));
    my_map.insert(pair<int, string>(389,"JET_EtaIntercalibration_Stat143__1down"));
    my_map.insert(pair<int, string>(390,"JET_EtaIntercalibration_Stat144__1down"));
    my_map.insert(pair<int, string>(391,"JET_EtaIntercalibration_Stat145__1down"));
    my_map.insert(pair<int, string>(392,"JET_EtaIntercalibration_Stat146__1down"));
    my_map.insert(pair<int, string>(393,"JET_EtaIntercalibration_Stat147__1down"));
    my_map.insert(pair<int, string>(394,"JET_EtaIntercalibration_Stat148__1down"));
    my_map.insert(pair<int, string>(395,"JET_EtaIntercalibration_Stat149__1down"));
    my_map.insert(pair<int, string>(396,"JET_EtaIntercalibration_Stat150__1down"));
    my_map.insert(pair<int, string>(397,"JET_EtaIntercalibration_Stat151__1down"));
    my_map.insert(pair<int, string>(398,"JET_EtaIntercalibration_Stat152__1down"));
    my_map.insert(pair<int, string>(399,"JET_EtaIntercalibration_Stat153__1down"));
    my_map.insert(pair<int, string>(400,"JET_EtaIntercalibration_Stat154__1down"));
    my_map.insert(pair<int, string>(401,"JET_EtaIntercalibration_Stat155__1down"));
    my_map.insert(pair<int, string>(402,"JET_EtaIntercalibration_Stat156__1down"));
    my_map.insert(pair<int, string>(403,"JET_EtaIntercalibration_Stat157__1down"));
    my_map.insert(pair<int, string>(404,"JET_EtaIntercalibration_Stat158__1down"));
    my_map.insert(pair<int, string>(405,"JET_EtaIntercalibration_Stat159__1down"));
    my_map.insert(pair<int, string>(406,"JET_EtaIntercalibration_Stat160__1down"));
    my_map.insert(pair<int, string>(407,"JET_EtaIntercalibration_Stat161__1down"));
    my_map.insert(pair<int, string>(408,"JET_EtaIntercalibration_Stat162__1down"));
    my_map.insert(pair<int, string>(409,"JET_EtaIntercalibration_Stat163__1down"));
    my_map.insert(pair<int, string>(410,"JET_EtaIntercalibration_Stat164__1down"));
    my_map.insert(pair<int, string>(411,"JET_EtaIntercalibration_Stat165__1down"));
    my_map.insert(pair<int, string>(412,"JET_EtaIntercalibration_Stat166__1down"));
    my_map.insert(pair<int, string>(413,"JET_EtaIntercalibration_Stat167__1down"));
    my_map.insert(pair<int, string>(414,"JET_EtaIntercalibration_Stat168__1down"));
    my_map.insert(pair<int, string>(415,"JET_EtaIntercalibration_Stat169__1down"));
    my_map.insert(pair<int, string>(416,"JET_EtaIntercalibration_Stat170__1down"));
    my_map.insert(pair<int, string>(417,"JET_EtaIntercalibration_Stat171__1down"));
    my_map.insert(pair<int, string>(418,"JET_EtaIntercalibration_Stat172__1down"));
    my_map.insert(pair<int, string>(419,"JET_EtaIntercalibration_Stat173__1down"));
    my_map.insert(pair<int, string>(420,"JET_EtaIntercalibration_Stat174__1down"));
    my_map.insert(pair<int, string>(421,"JET_EtaIntercalibration_Stat175__1down"));
    my_map.insert(pair<int, string>(422,"JET_EtaIntercalibration_Stat176__1down"));
    my_map.insert(pair<int, string>(423,"JET_EtaIntercalibration_Stat177__1down"));
    my_map.insert(pair<int, string>(424,"JET_EtaIntercalibration_Stat178__1down"));
    my_map.insert(pair<int, string>(425,"JET_EtaIntercalibration_Stat179__1down"));
    my_map.insert(pair<int, string>(426,"JET_EtaIntercalibration_Stat180__1down"));
    my_map.insert(pair<int, string>(427,"JET_EtaIntercalibration_Stat181__1down"));
    my_map.insert(pair<int, string>(428,"JET_EtaIntercalibration_Stat182__1down"));
    my_map.insert(pair<int, string>(429,"JET_EtaIntercalibration_Stat183__1down"));
    my_map.insert(pair<int, string>(430,"JET_EtaIntercalibration_Stat184__1down"));
    my_map.insert(pair<int, string>(431,"JET_EtaIntercalibration_Stat185__1down"));
    my_map.insert(pair<int, string>(432,"JET_EtaIntercalibration_Stat186__1down"));
    my_map.insert(pair<int, string>(433,"JET_EtaIntercalibration_Stat187__1down"));
    my_map.insert(pair<int, string>(434,"JET_EtaIntercalibration_Stat188__1down"));
    my_map.insert(pair<int, string>(435,"JET_EtaIntercalibration_Stat189__1down"));
    my_map.insert(pair<int, string>(436,"JET_EtaIntercalibration_Stat190__1down"));
    my_map.insert(pair<int, string>(437,"JET_EtaIntercalibration_Stat191__1down"));
    my_map.insert(pair<int, string>(438,"JET_EtaIntercalibration_Stat192__1down"));
    my_map.insert(pair<int, string>(439,"JET_EtaIntercalibration_Stat193__1down"));
    my_map.insert(pair<int, string>(440,"JET_EtaIntercalibration_Stat194__1down"));
    my_map.insert(pair<int, string>(441,"JET_EtaIntercalibration_Stat195__1down"));
    my_map.insert(pair<int, string>(442,"JET_EtaIntercalibration_Stat196__1down"));
    my_map.insert(pair<int, string>(443,"JET_EtaIntercalibration_Stat197__1down"));
    my_map.insert(pair<int, string>(444,"JET_EtaIntercalibration_Stat198__1down"));
    my_map.insert(pair<int, string>(445,"JET_EtaIntercalibration_Stat199__1down"));
    my_map.insert(pair<int, string>(446,"JET_EtaIntercalibration_Stat200__1down"));
    my_map.insert(pair<int, string>(447,"JET_EtaIntercalibration_Stat201__1down"));
    my_map.insert(pair<int, string>(448,"JET_EtaIntercalibration_Stat202__1down"));
    my_map.insert(pair<int, string>(449,"JET_EtaIntercalibration_Stat203__1down"));
    my_map.insert(pair<int, string>(450,"JET_EtaIntercalibration_Stat204__1down"));
    my_map.insert(pair<int, string>(451,"JET_EtaIntercalibration_Stat205__1down"));
    my_map.insert(pair<int, string>(452,"JET_EtaIntercalibration_Stat206__1down"));
    my_map.insert(pair<int, string>(453,"JET_EtaIntercalibration_Stat207__1down"));
    my_map.insert(pair<int, string>(454,"JET_EtaIntercalibration_Stat208__1down"));
    my_map.insert(pair<int, string>(455,"JET_EtaIntercalibration_Stat209__1down"));
    my_map.insert(pair<int, string>(456,"JET_EtaIntercalibration_Stat210__1down"));
    my_map.insert(pair<int, string>(457,"JET_EtaIntercalibration_Stat211__1down"));
    my_map.insert(pair<int, string>(458,"JET_EtaIntercalibration_Stat212__1down"));
    my_map.insert(pair<int, string>(459,"JET_EtaIntercalibration_Stat213__1down"));
    my_map.insert(pair<int, string>(460,"JET_EtaIntercalibration_Stat214__1down"));
    my_map.insert(pair<int, string>(461,"JET_EtaIntercalibration_Stat215__1down"));
    my_map.insert(pair<int, string>(462,"JET_EtaIntercalibration_Stat216__1down"));
    my_map.insert(pair<int, string>(463,"JET_EtaIntercalibration_Stat217__1down"));
    my_map.insert(pair<int, string>(464,"JET_EtaIntercalibration_Stat218__1down"));
    my_map.insert(pair<int, string>(465,"JET_EtaIntercalibration_Stat219__1down"));
    my_map.insert(pair<int, string>(466,"JET_EtaIntercalibration_Stat220__1down"));
    my_map.insert(pair<int, string>(467,"JET_EtaIntercalibration_Stat221__1down"));
    my_map.insert(pair<int, string>(468,"JET_EtaIntercalibration_Stat222__1down"));
    my_map.insert(pair<int, string>(469,"JET_EtaIntercalibration_Stat223__1down"));
    my_map.insert(pair<int, string>(470,"JET_EtaIntercalibration_Stat224__1down"));
    my_map.insert(pair<int, string>(471,"JET_EtaIntercalibration_Stat225__1down"));
    my_map.insert(pair<int, string>(472,"JET_EtaIntercalibration_Stat226__1down"));
    my_map.insert(pair<int, string>(473,"JET_EtaIntercalibration_Stat227__1down"));
    my_map.insert(pair<int, string>(474,"JET_EtaIntercalibration_Stat228__1down"));
    my_map.insert(pair<int, string>(475,"JET_EtaIntercalibration_Stat229__1down"));
    my_map.insert(pair<int, string>(476,"JET_EtaIntercalibration_Stat230__1down"));
    my_map.insert(pair<int, string>(477,"JET_EtaIntercalibration_Stat231__1down"));
    my_map.insert(pair<int, string>(478,"JET_EtaIntercalibration_Stat232__1down"));
    my_map.insert(pair<int, string>(479,"JET_EtaIntercalibration_Stat233__1down"));
    my_map.insert(pair<int, string>(480,"JET_EtaIntercalibration_Stat234__1down"));
    my_map.insert(pair<int, string>(481,"JET_EtaIntercalibration_Stat235__1down"));
    my_map.insert(pair<int, string>(482,"JET_EtaIntercalibration_Stat236__1down"));
    my_map.insert(pair<int, string>(483,"JET_EtaIntercalibration_Stat237__1down"));
    my_map.insert(pair<int, string>(484,"JET_EtaIntercalibration_Stat238__1down"));
    my_map.insert(pair<int, string>(485,"JET_EtaIntercalibration_Stat239__1down"));
    my_map.insert(pair<int, string>(486,"JET_EtaIntercalibration_Stat240__1down"));
    my_map.insert(pair<int, string>(487,"JET_EtaIntercalibration_Stat241__1down"));
    my_map.insert(pair<int, string>(488,"JET_EtaIntercalibration_Stat242__1down"));
    my_map.insert(pair<int, string>(489,"JET_EtaIntercalibration_Stat243__1down"));
    my_map.insert(pair<int, string>(490,"JET_EtaIntercalibration_Stat244__1down"));
    my_map.insert(pair<int, string>(491,"JET_EtaIntercalibration_Stat245__1down"));

    
    my_map.insert(pair<int, string>(492,"JET_EtaIntercalibration_TotalStat_MJB__1up"));
    my_map.insert(pair<int, string>(493,"JET_EtaIntercalibration_TotalStat_MJB__1down"));

    

    //JES
    my_map.insert(pair<int, string>(1112,"JET_EtaIntercalibration_Modelling__1up"));
    my_map.insert(pair<int, string>(1113,"JET_EtaIntercalibration_Modelling__1down"));
    my_map.insert(pair<int, string>(1114,"JET_EtaIntercalibration_NonClosure__1up"));
    my_map.insert(pair<int, string>(1115,"JET_EtaIntercalibration_NonClosure__1down"));
    my_map.insert(pair<int, string>(1116,"JET_Flavor_Composition__1up"));
    my_map.insert(pair<int, string>(1117,"JET_Flavor_Composition__1down"));
    my_map.insert(pair<int, string>(1118,"JET_Flavor_Response__1up"));
    my_map.insert(pair<int, string>(1119,"JET_Flavor_Response__1down"));
    my_map.insert(pair<int, string>(1120,"JET_Gjet_Generator__1up"));
    my_map.insert(pair<int, string>(1121,"JET_Gjet_Generator__1down"));
    my_map.insert(pair<int, string>(1122,"JET_Gjet_OOC__1up"));
    my_map.insert(pair<int, string>(1123,"JET_Gjet_OOC__1down"));
    my_map.insert(pair<int, string>(1124,"JET_Gjet_Purity__1up"));
    my_map.insert(pair<int, string>(1125,"JET_Gjet_Purity__1down"));
    my_map.insert(pair<int, string>(1126,"JET_Gjet_Stat10__1up"));
    my_map.insert(pair<int, string>(1127,"JET_Gjet_Stat10__1down"));
    my_map.insert(pair<int, string>(1128,"JET_Gjet_Stat11__1up"));
    my_map.insert(pair<int, string>(1129,"JET_Gjet_Stat11__1down"));
    my_map.insert(pair<int, string>(1130,"JET_Gjet_Stat12__1up"));
    my_map.insert(pair<int, string>(1131,"JET_Gjet_Stat12__1down"));
    my_map.insert(pair<int, string>(1132,"JET_Gjet_Stat13__1up"));
    my_map.insert(pair<int, string>(1133,"JET_Gjet_Stat13__1down"));
    my_map.insert(pair<int, string>(1134,"JET_Gjet_Stat14__1up"));
    my_map.insert(pair<int, string>(1135,"JET_Gjet_Stat14__1down"));
    my_map.insert(pair<int, string>(1136,"JET_Gjet_Stat15__1up"));
    my_map.insert(pair<int, string>(1137,"JET_Gjet_Stat15__1down"));
    my_map.insert(pair<int, string>(1138,"JET_Gjet_Stat1__1up"));
    my_map.insert(pair<int, string>(1139,"JET_Gjet_Stat1__1down"));
    my_map.insert(pair<int, string>(1140,"JET_Gjet_Stat2__1up"));
    my_map.insert(pair<int, string>(1141,"JET_Gjet_Stat2__1down"));
    my_map.insert(pair<int, string>(1142,"JET_Gjet_Stat3__1up"));
    my_map.insert(pair<int, string>(1143,"JET_Gjet_Stat3__1down"));
    my_map.insert(pair<int, string>(1144,"JET_Gjet_Stat4__1up"));
    my_map.insert(pair<int, string>(1145,"JET_Gjet_Stat4__1down"));
    my_map.insert(pair<int, string>(1146,"JET_Gjet_Stat5__1up"));
    my_map.insert(pair<int, string>(1147,"JET_Gjet_Stat5__1down"));
    my_map.insert(pair<int, string>(1148,"JET_Gjet_Stat6__1up"));
    my_map.insert(pair<int, string>(1149,"JET_Gjet_Stat6__1down"));
    my_map.insert(pair<int, string>(1150,"JET_Gjet_Stat7__1up"));
    my_map.insert(pair<int, string>(1151,"JET_Gjet_Stat7__1down"));
    my_map.insert(pair<int, string>(1152,"JET_Gjet_Stat8__1up"));
    my_map.insert(pair<int, string>(1153,"JET_Gjet_Stat8__1down"));
    my_map.insert(pair<int, string>(1154,"JET_Gjet_Stat9__1up"));
    my_map.insert(pair<int, string>(1155,"JET_Gjet_Stat9__1down"));
    my_map.insert(pair<int, string>(1156,"JET_Gjet_Veto__1up"));
    my_map.insert(pair<int, string>(1157,"JET_Gjet_Veto__1down"));
    my_map.insert(pair<int, string>(1158,"JET_Gjet_dPhi__1up"));
    my_map.insert(pair<int, string>(1159,"JET_Gjet_dPhi__1down"));
    my_map.insert(pair<int, string>(1160,"JET_LAr_ESZee__1up"));
    my_map.insert(pair<int, string>(1161,"JET_LAr_ESZee__1down"));
    my_map.insert(pair<int, string>(1162,"JET_LAr_Esmear__1up"));
    my_map.insert(pair<int, string>(1163,"JET_LAr_Esmear__1down"));
    my_map.insert(pair<int, string>(1164,"JET_LAr_JVT__1up"));
    my_map.insert(pair<int, string>(1165,"JET_LAr_JVT__1down"));
    my_map.insert(pair<int, string>(1166,"JET_MJB_Alpha__1up"));
    my_map.insert(pair<int, string>(1167,"JET_MJB_Alpha__1down"));
    my_map.insert(pair<int, string>(1168,"JET_MJB_Asym__1up"));
    my_map.insert(pair<int, string>(1169,"JET_MJB_Asym__1down"));
    my_map.insert(pair<int, string>(1170,"JET_MJB_Beta__1up"));
    my_map.insert(pair<int, string>(1171,"JET_MJB_Beta__1down"));
    my_map.insert(pair<int, string>(1172,"JET_MJB_Fragmentation__1up"));
    my_map.insert(pair<int, string>(1173,"JET_MJB_Fragmentation__1down"));
    my_map.insert(pair<int, string>(1174,"JET_MJB_Stat10__1up"));
    my_map.insert(pair<int, string>(1175,"JET_MJB_Stat10__1down"));
    my_map.insert(pair<int, string>(1176,"JET_MJB_Stat11__1up"));
    my_map.insert(pair<int, string>(1177,"JET_MJB_Stat11__1down"));
    my_map.insert(pair<int, string>(1178,"JET_MJB_Stat12__1up"));
    my_map.insert(pair<int, string>(1179,"JET_MJB_Stat12__1down"));
    my_map.insert(pair<int, string>(1180,"JET_MJB_Stat13__1up"));
    my_map.insert(pair<int, string>(1181,"JET_MJB_Stat13__1down"));
    my_map.insert(pair<int, string>(1182,"JET_MJB_Stat14__1up"));
    my_map.insert(pair<int, string>(1183,"JET_MJB_Stat14__1down"));
    my_map.insert(pair<int, string>(1184,"JET_MJB_Stat15__1up"));
    my_map.insert(pair<int, string>(1185,"JET_MJB_Stat15__1down"));
    my_map.insert(pair<int, string>(1186,"JET_MJB_Stat16__1up"));
    my_map.insert(pair<int, string>(1187,"JET_MJB_Stat16__1down"));
    my_map.insert(pair<int, string>(1188,"JET_MJB_Stat1__1up"));
    my_map.insert(pair<int, string>(1189,"JET_MJB_Stat1__1down"));
    my_map.insert(pair<int, string>(1190,"JET_MJB_Stat2__1up"));
    my_map.insert(pair<int, string>(1191,"JET_MJB_Stat2__1down"));
    my_map.insert(pair<int, string>(1192,"JET_MJB_Stat3__1up"));
    my_map.insert(pair<int, string>(1193,"JET_MJB_Stat3__1down"));
    my_map.insert(pair<int, string>(1194,"JET_MJB_Stat4__1up"));
    my_map.insert(pair<int, string>(1195,"JET_MJB_Stat4__1down"));
    my_map.insert(pair<int, string>(1196,"JET_MJB_Stat5__1up"));
    my_map.insert(pair<int, string>(1197,"JET_MJB_Stat5__1down"));
    my_map.insert(pair<int, string>(1198,"JET_MJB_Stat6__1up"));
    my_map.insert(pair<int, string>(1199,"JET_MJB_Stat6__1down"));
    my_map.insert(pair<int, string>(1200,"JET_MJB_Stat7__1up"));
    my_map.insert(pair<int, string>(1201,"JET_MJB_Stat7__1down"));
    my_map.insert(pair<int, string>(1202,"JET_MJB_Stat8__1up"));
    my_map.insert(pair<int, string>(1203,"JET_MJB_Stat8__1down"));
    my_map.insert(pair<int, string>(1204,"JET_MJB_Stat9__1up"));
    my_map.insert(pair<int, string>(1205,"JET_MJB_Stat9__1down"));
    my_map.insert(pair<int, string>(1206,"JET_MJB_Threshold__1up"));
    my_map.insert(pair<int, string>(1207,"JET_MJB_Threshold__1down"));
    my_map.insert(pair<int, string>(1208,"JET_Pileup_OffsetMu__1up"));
    my_map.insert(pair<int, string>(1209,"JET_Pileup_OffsetMu__1down"));
    my_map.insert(pair<int, string>(1210,"JET_Pileup_OffsetNPV__1up"));
    my_map.insert(pair<int, string>(1211,"JET_Pileup_OffsetNPV__1down"));
    my_map.insert(pair<int, string>(1212,"JET_Pileup_PtTerm__1up"));
    my_map.insert(pair<int, string>(1213,"JET_Pileup_PtTerm__1down"));
    my_map.insert(pair<int, string>(1214,"JET_Pileup_RhoTopology__1up"));
    my_map.insert(pair<int, string>(1215,"JET_Pileup_RhoTopology__1down"));
    my_map.insert(pair<int, string>(1216,"JET_PunchThrough_MC15__1up"));
    my_map.insert(pair<int, string>(1217,"JET_PunchThrough_MC15__1down"));
    my_map.insert(pair<int, string>(1218,"JET_SingleParticle_HighPt__1up"));
    my_map.insert(pair<int, string>(1219,"JET_SingleParticle_HighPt__1down"));
    my_map.insert(pair<int, string>(1220,"JET_Zjet_KTerm__1up"));
    my_map.insert(pair<int, string>(1221,"JET_Zjet_KTerm__1down"));
    my_map.insert(pair<int, string>(1222,"JET_Zjet_MC__1up"));
    my_map.insert(pair<int, string>(1223,"JET_Zjet_MC__1down"));
    my_map.insert(pair<int, string>(1224,"JET_Zjet_MuScale__1up"));
    my_map.insert(pair<int, string>(1225,"JET_Zjet_MuScale__1down"));
    my_map.insert(pair<int, string>(1226,"JET_Zjet_MuSmearID__1up"));
    my_map.insert(pair<int, string>(1227,"JET_Zjet_MuSmearID__1down"));
    my_map.insert(pair<int, string>(1228,"JET_Zjet_MuSmearMS__1up"));
    my_map.insert(pair<int, string>(1229,"JET_Zjet_MuSmearMS__1down"));
    my_map.insert(pair<int, string>(1230,"JET_Zjet_Stat10__1up"));
    my_map.insert(pair<int, string>(1231,"JET_Zjet_Stat10__1down"));
    my_map.insert(pair<int, string>(1232,"JET_Zjet_Stat11__1up"));
    my_map.insert(pair<int, string>(1233,"JET_Zjet_Stat11__1down"));
    my_map.insert(pair<int, string>(1234,"JET_Zjet_Stat12__1up"));
    my_map.insert(pair<int, string>(1235,"JET_Zjet_Stat12__1down"));
    my_map.insert(pair<int, string>(1236,"JET_Zjet_Stat13__1up"));
    my_map.insert(pair<int, string>(1237,"JET_Zjet_Stat13__1down"));
    my_map.insert(pair<int, string>(1238,"JET_Zjet_Stat1__1up"));
    my_map.insert(pair<int, string>(1239,"JET_Zjet_Stat1__1down"));
    my_map.insert(pair<int, string>(1240,"JET_Zjet_Stat2__1up"));
    my_map.insert(pair<int, string>(1241,"JET_Zjet_Stat2__1down"));
    my_map.insert(pair<int, string>(1242,"JET_Zjet_Stat3__1up"));
    my_map.insert(pair<int, string>(1243,"JET_Zjet_Stat3__1down"));
    my_map.insert(pair<int, string>(1244,"JET_Zjet_Stat4__1up"));
    my_map.insert(pair<int, string>(1245,"JET_Zjet_Stat4__1down"));
    my_map.insert(pair<int, string>(1246,"JET_Zjet_Stat5__1up"));
    my_map.insert(pair<int, string>(1247,"JET_Zjet_Stat5__1down"));
    my_map.insert(pair<int, string>(1248,"JET_Zjet_Stat6__1up"));
    my_map.insert(pair<int, string>(1249,"JET_Zjet_Stat6__1down"));
    my_map.insert(pair<int, string>(1250,"JET_Zjet_Stat7__1up"));
    my_map.insert(pair<int, string>(1251,"JET_Zjet_Stat7__1down"));
    my_map.insert(pair<int, string>(1252,"JET_Zjet_Stat8__1up"));
    my_map.insert(pair<int, string>(1253,"JET_Zjet_Stat8__1down"));
    my_map.insert(pair<int, string>(1254,"JET_Zjet_Stat9__1up"));
    my_map.insert(pair<int, string>(1255,"JET_Zjet_Stat9__1down"));
    my_map.insert(pair<int, string>(1256,"JET_Zjet_Veto__1up"));
    my_map.insert(pair<int, string>(1257,"JET_Zjet_Veto__1down"));
    my_map.insert(pair<int, string>(1258,"JET_Zjet_dPhi__1up"));
    my_map.insert(pair<int, string>(1259,"JET_Zjet_dPhi__1down"));
*/
/*
    //Map for JES mean 
    my_map.insert(pair<int, string>(246,"JET_EtaIntercalibration_Stat0__"));
    my_map.insert(pair<int, string>(247,"JET_EtaIntercalibration_Stat1__"));
    my_map.insert(pair<int, string>(248,"JET_EtaIntercalibration_Stat2__"));
    my_map.insert(pair<int, string>(249,"JET_EtaIntercalibration_Stat3__"));
    my_map.insert(pair<int, string>(250,"JET_EtaIntercalibration_Stat4__"));
    my_map.insert(pair<int, string>(251,"JET_EtaIntercalibration_Stat5__"));
    my_map.insert(pair<int, string>(252,"JET_EtaIntercalibration_Stat6__"));
    my_map.insert(pair<int, string>(253,"JET_EtaIntercalibration_Stat7__"));
    my_map.insert(pair<int, string>(254,"JET_EtaIntercalibration_Stat8__"));
    my_map.insert(pair<int, string>(255,"JET_EtaIntercalibration_Stat9__"));
    my_map.insert(pair<int, string>(256,"JET_EtaIntercalibration_Stat10__"));
    my_map.insert(pair<int, string>(257,"JET_EtaIntercalibration_Stat11__"));
    my_map.insert(pair<int, string>(258,"JET_EtaIntercalibration_Stat12__"));
    my_map.insert(pair<int, string>(259,"JET_EtaIntercalibration_Stat13__"));
    my_map.insert(pair<int, string>(260,"JET_EtaIntercalibration_Stat14__"));
    my_map.insert(pair<int, string>(261,"JET_EtaIntercalibration_Stat15__"));
    my_map.insert(pair<int, string>(262,"JET_EtaIntercalibration_Stat16__"));
    my_map.insert(pair<int, string>(263,"JET_EtaIntercalibration_Stat17__"));
    my_map.insert(pair<int, string>(264,"JET_EtaIntercalibration_Stat18__"));
    my_map.insert(pair<int, string>(265,"JET_EtaIntercalibration_Stat19__"));
    my_map.insert(pair<int, string>(266,"JET_EtaIntercalibration_Stat20__"));
    my_map.insert(pair<int, string>(267,"JET_EtaIntercalibration_Stat21__"));
    my_map.insert(pair<int, string>(268,"JET_EtaIntercalibration_Stat22__"));
    my_map.insert(pair<int, string>(269,"JET_EtaIntercalibration_Stat23__"));
    my_map.insert(pair<int, string>(270,"JET_EtaIntercalibration_Stat24__"));
    my_map.insert(pair<int, string>(271,"JET_EtaIntercalibration_Stat25__"));
    my_map.insert(pair<int, string>(272,"JET_EtaIntercalibration_Stat26__"));
    my_map.insert(pair<int, string>(273,"JET_EtaIntercalibration_Stat27__"));
    my_map.insert(pair<int, string>(274,"JET_EtaIntercalibration_Stat28__"));
    my_map.insert(pair<int, string>(275,"JET_EtaIntercalibration_Stat29__"));
    my_map.insert(pair<int, string>(276,"JET_EtaIntercalibration_Stat30__"));
    my_map.insert(pair<int, string>(277,"JET_EtaIntercalibration_Stat31__"));
    my_map.insert(pair<int, string>(278,"JET_EtaIntercalibration_Stat32__"));
    my_map.insert(pair<int, string>(279,"JET_EtaIntercalibration_Stat33__"));
    my_map.insert(pair<int, string>(280,"JET_EtaIntercalibration_Stat34__"));
    my_map.insert(pair<int, string>(281,"JET_EtaIntercalibration_Stat35__"));
    my_map.insert(pair<int, string>(282,"JET_EtaIntercalibration_Stat36__"));
    my_map.insert(pair<int, string>(283,"JET_EtaIntercalibration_Stat37__"));
    my_map.insert(pair<int, string>(284,"JET_EtaIntercalibration_Stat38__"));
    my_map.insert(pair<int, string>(285,"JET_EtaIntercalibration_Stat39__"));
    my_map.insert(pair<int, string>(286,"JET_EtaIntercalibration_Stat40__"));
    my_map.insert(pair<int, string>(287,"JET_EtaIntercalibration_Stat41__"));
    my_map.insert(pair<int, string>(288,"JET_EtaIntercalibration_Stat42__"));
    my_map.insert(pair<int, string>(289,"JET_EtaIntercalibration_Stat43__"));
    my_map.insert(pair<int, string>(290,"JET_EtaIntercalibration_Stat44__"));
    my_map.insert(pair<int, string>(291,"JET_EtaIntercalibration_Stat45__"));
    my_map.insert(pair<int, string>(292,"JET_EtaIntercalibration_Stat46__"));
    my_map.insert(pair<int, string>(293,"JET_EtaIntercalibration_Stat47__"));
    my_map.insert(pair<int, string>(294,"JET_EtaIntercalibration_Stat48__"));
    my_map.insert(pair<int, string>(295,"JET_EtaIntercalibration_Stat49__"));
    my_map.insert(pair<int, string>(296,"JET_EtaIntercalibration_Stat50__"));
    my_map.insert(pair<int, string>(297,"JET_EtaIntercalibration_Stat51__"));
    my_map.insert(pair<int, string>(298,"JET_EtaIntercalibration_Stat52__"));
    my_map.insert(pair<int, string>(299,"JET_EtaIntercalibration_Stat53__"));
    my_map.insert(pair<int, string>(300,"JET_EtaIntercalibration_Stat54__"));
    my_map.insert(pair<int, string>(301,"JET_EtaIntercalibration_Stat55__"));
    my_map.insert(pair<int, string>(302,"JET_EtaIntercalibration_Stat56__"));
    my_map.insert(pair<int, string>(303,"JET_EtaIntercalibration_Stat57__"));
    my_map.insert(pair<int, string>(304,"JET_EtaIntercalibration_Stat58__"));
    my_map.insert(pair<int, string>(305,"JET_EtaIntercalibration_Stat59__"));
    my_map.insert(pair<int, string>(306,"JET_EtaIntercalibration_Stat60__"));
    my_map.insert(pair<int, string>(307,"JET_EtaIntercalibration_Stat61__"));
    my_map.insert(pair<int, string>(308,"JET_EtaIntercalibration_Stat62__"));
    my_map.insert(pair<int, string>(309,"JET_EtaIntercalibration_Stat63__"));
    my_map.insert(pair<int, string>(310,"JET_EtaIntercalibration_Stat64__"));
    my_map.insert(pair<int, string>(311,"JET_EtaIntercalibration_Stat65__"));
    my_map.insert(pair<int, string>(312,"JET_EtaIntercalibration_Stat66__"));
    my_map.insert(pair<int, string>(313,"JET_EtaIntercalibration_Stat67__"));
    my_map.insert(pair<int, string>(314,"JET_EtaIntercalibration_Stat68__"));
    my_map.insert(pair<int, string>(315,"JET_EtaIntercalibration_Stat69__"));
    my_map.insert(pair<int, string>(316,"JET_EtaIntercalibration_Stat70__"));
    my_map.insert(pair<int, string>(317,"JET_EtaIntercalibration_Stat71__"));
    my_map.insert(pair<int, string>(318,"JET_EtaIntercalibration_Stat72__"));
    my_map.insert(pair<int, string>(319,"JET_EtaIntercalibration_Stat73__"));
    my_map.insert(pair<int, string>(320,"JET_EtaIntercalibration_Stat74__"));
    my_map.insert(pair<int, string>(321,"JET_EtaIntercalibration_Stat75__"));
    my_map.insert(pair<int, string>(322,"JET_EtaIntercalibration_Stat76__"));
    my_map.insert(pair<int, string>(323,"JET_EtaIntercalibration_Stat77__"));
    my_map.insert(pair<int, string>(324,"JET_EtaIntercalibration_Stat78__"));
    my_map.insert(pair<int, string>(325,"JET_EtaIntercalibration_Stat79__"));
    my_map.insert(pair<int, string>(326,"JET_EtaIntercalibration_Stat80__"));
    my_map.insert(pair<int, string>(327,"JET_EtaIntercalibration_Stat81__"));
    my_map.insert(pair<int, string>(328,"JET_EtaIntercalibration_Stat82__"));
    my_map.insert(pair<int, string>(329,"JET_EtaIntercalibration_Stat83__"));
    my_map.insert(pair<int, string>(330,"JET_EtaIntercalibration_Stat84__"));
    my_map.insert(pair<int, string>(331,"JET_EtaIntercalibration_Stat85__"));
    my_map.insert(pair<int, string>(332,"JET_EtaIntercalibration_Stat86__"));
    my_map.insert(pair<int, string>(333,"JET_EtaIntercalibration_Stat87__"));
    my_map.insert(pair<int, string>(334,"JET_EtaIntercalibration_Stat88__"));
    my_map.insert(pair<int, string>(335,"JET_EtaIntercalibration_Stat89__"));
    my_map.insert(pair<int, string>(336,"JET_EtaIntercalibration_Stat90__"));
    my_map.insert(pair<int, string>(337,"JET_EtaIntercalibration_Stat91__"));
    my_map.insert(pair<int, string>(338,"JET_EtaIntercalibration_Stat92__"));
    my_map.insert(pair<int, string>(339,"JET_EtaIntercalibration_Stat93__"));
    my_map.insert(pair<int, string>(340,"JET_EtaIntercalibration_Stat94__"));
    my_map.insert(pair<int, string>(341,"JET_EtaIntercalibration_Stat95__"));
    my_map.insert(pair<int, string>(342,"JET_EtaIntercalibration_Stat96__"));
    my_map.insert(pair<int, string>(343,"JET_EtaIntercalibration_Stat97__"));
    my_map.insert(pair<int, string>(344,"JET_EtaIntercalibration_Stat98__"));
    my_map.insert(pair<int, string>(345,"JET_EtaIntercalibration_Stat99__"));
    my_map.insert(pair<int, string>(346,"JET_EtaIntercalibration_Stat100__"));
    my_map.insert(pair<int, string>(347,"JET_EtaIntercalibration_Stat101__"));
    my_map.insert(pair<int, string>(348,"JET_EtaIntercalibration_Stat102__"));
    my_map.insert(pair<int, string>(349,"JET_EtaIntercalibration_Stat103__"));
    my_map.insert(pair<int, string>(350,"JET_EtaIntercalibration_Stat104__"));
    my_map.insert(pair<int, string>(351,"JET_EtaIntercalibration_Stat105__"));
    my_map.insert(pair<int, string>(352,"JET_EtaIntercalibration_Stat106__"));
    my_map.insert(pair<int, string>(353,"JET_EtaIntercalibration_Stat107__"));
    my_map.insert(pair<int, string>(354,"JET_EtaIntercalibration_Stat108__"));
    my_map.insert(pair<int, string>(355,"JET_EtaIntercalibration_Stat109__"));
    my_map.insert(pair<int, string>(356,"JET_EtaIntercalibration_Stat110__"));
    my_map.insert(pair<int, string>(357,"JET_EtaIntercalibration_Stat111__"));
    my_map.insert(pair<int, string>(358,"JET_EtaIntercalibration_Stat112__"));
    my_map.insert(pair<int, string>(359,"JET_EtaIntercalibration_Stat113__"));
    my_map.insert(pair<int, string>(360,"JET_EtaIntercalibration_Stat114__"));
    my_map.insert(pair<int, string>(361,"JET_EtaIntercalibration_Stat115__"));
    my_map.insert(pair<int, string>(362,"JET_EtaIntercalibration_Stat116__"));
    my_map.insert(pair<int, string>(363,"JET_EtaIntercalibration_Stat117__"));
    my_map.insert(pair<int, string>(364,"JET_EtaIntercalibration_Stat118__"));
    my_map.insert(pair<int, string>(365,"JET_EtaIntercalibration_Stat119__"));
    my_map.insert(pair<int, string>(366,"JET_EtaIntercalibration_Stat120__"));
    my_map.insert(pair<int, string>(367,"JET_EtaIntercalibration_Stat121__"));
    my_map.insert(pair<int, string>(368,"JET_EtaIntercalibration_Stat122__"));
    my_map.insert(pair<int, string>(369,"JET_EtaIntercalibration_Stat123__"));
    my_map.insert(pair<int, string>(370,"JET_EtaIntercalibration_Stat124__"));
    my_map.insert(pair<int, string>(371,"JET_EtaIntercalibration_Stat125__"));
    my_map.insert(pair<int, string>(372,"JET_EtaIntercalibration_Stat126__"));
    my_map.insert(pair<int, string>(373,"JET_EtaIntercalibration_Stat127__"));
    my_map.insert(pair<int, string>(374,"JET_EtaIntercalibration_Stat128__"));
    my_map.insert(pair<int, string>(375,"JET_EtaIntercalibration_Stat129__"));
    my_map.insert(pair<int, string>(376,"JET_EtaIntercalibration_Stat130__"));
    my_map.insert(pair<int, string>(377,"JET_EtaIntercalibration_Stat131__"));
    my_map.insert(pair<int, string>(378,"JET_EtaIntercalibration_Stat132__"));
    my_map.insert(pair<int, string>(379,"JET_EtaIntercalibration_Stat133__"));
    my_map.insert(pair<int, string>(380,"JET_EtaIntercalibration_Stat134__"));
    my_map.insert(pair<int, string>(381,"JET_EtaIntercalibration_Stat135__"));
    my_map.insert(pair<int, string>(382,"JET_EtaIntercalibration_Stat136__"));
    my_map.insert(pair<int, string>(383,"JET_EtaIntercalibration_Stat137__"));
    my_map.insert(pair<int, string>(384,"JET_EtaIntercalibration_Stat138__"));
    my_map.insert(pair<int, string>(385,"JET_EtaIntercalibration_Stat139__"));
    my_map.insert(pair<int, string>(386,"JET_EtaIntercalibration_Stat140__"));
    my_map.insert(pair<int, string>(387,"JET_EtaIntercalibration_Stat141__"));
    my_map.insert(pair<int, string>(388,"JET_EtaIntercalibration_Stat142__"));
    my_map.insert(pair<int, string>(389,"JET_EtaIntercalibration_Stat143__"));
    my_map.insert(pair<int, string>(390,"JET_EtaIntercalibration_Stat144__"));
    my_map.insert(pair<int, string>(391,"JET_EtaIntercalibration_Stat145__"));
    my_map.insert(pair<int, string>(392,"JET_EtaIntercalibration_Stat146__"));
    my_map.insert(pair<int, string>(393,"JET_EtaIntercalibration_Stat147__"));
    my_map.insert(pair<int, string>(394,"JET_EtaIntercalibration_Stat148__"));
    my_map.insert(pair<int, string>(395,"JET_EtaIntercalibration_Stat149__"));
    my_map.insert(pair<int, string>(396,"JET_EtaIntercalibration_Stat150__"));
    my_map.insert(pair<int, string>(397,"JET_EtaIntercalibration_Stat151__"));
    my_map.insert(pair<int, string>(398,"JET_EtaIntercalibration_Stat152__"));
    my_map.insert(pair<int, string>(399,"JET_EtaIntercalibration_Stat153__"));
    my_map.insert(pair<int, string>(400,"JET_EtaIntercalibration_Stat154__"));
    my_map.insert(pair<int, string>(401,"JET_EtaIntercalibration_Stat155__"));
    my_map.insert(pair<int, string>(402,"JET_EtaIntercalibration_Stat156__"));
    my_map.insert(pair<int, string>(403,"JET_EtaIntercalibration_Stat157__"));
    my_map.insert(pair<int, string>(404,"JET_EtaIntercalibration_Stat158__"));
    my_map.insert(pair<int, string>(405,"JET_EtaIntercalibration_Stat159__"));
    my_map.insert(pair<int, string>(406,"JET_EtaIntercalibration_Stat160__"));
    my_map.insert(pair<int, string>(407,"JET_EtaIntercalibration_Stat161__"));
    my_map.insert(pair<int, string>(408,"JET_EtaIntercalibration_Stat162__"));
    my_map.insert(pair<int, string>(409,"JET_EtaIntercalibration_Stat163__"));
    my_map.insert(pair<int, string>(410,"JET_EtaIntercalibration_Stat164__"));
    my_map.insert(pair<int, string>(411,"JET_EtaIntercalibration_Stat165__"));
    my_map.insert(pair<int, string>(412,"JET_EtaIntercalibration_Stat166__"));
    my_map.insert(pair<int, string>(413,"JET_EtaIntercalibration_Stat167__"));
    my_map.insert(pair<int, string>(414,"JET_EtaIntercalibration_Stat168__"));
    my_map.insert(pair<int, string>(415,"JET_EtaIntercalibration_Stat169__"));
    my_map.insert(pair<int, string>(416,"JET_EtaIntercalibration_Stat170__"));
    my_map.insert(pair<int, string>(417,"JET_EtaIntercalibration_Stat171__"));
    my_map.insert(pair<int, string>(418,"JET_EtaIntercalibration_Stat172__"));
    my_map.insert(pair<int, string>(419,"JET_EtaIntercalibration_Stat173__"));
    my_map.insert(pair<int, string>(420,"JET_EtaIntercalibration_Stat174__"));
    my_map.insert(pair<int, string>(421,"JET_EtaIntercalibration_Stat175__"));
    my_map.insert(pair<int, string>(422,"JET_EtaIntercalibration_Stat176__"));
    my_map.insert(pair<int, string>(423,"JET_EtaIntercalibration_Stat177__"));
    my_map.insert(pair<int, string>(424,"JET_EtaIntercalibration_Stat178__"));
    my_map.insert(pair<int, string>(425,"JET_EtaIntercalibration_Stat179__"));
    my_map.insert(pair<int, string>(426,"JET_EtaIntercalibration_Stat180__"));
    my_map.insert(pair<int, string>(427,"JET_EtaIntercalibration_Stat181__"));
    my_map.insert(pair<int, string>(428,"JET_EtaIntercalibration_Stat182__"));
    my_map.insert(pair<int, string>(429,"JET_EtaIntercalibration_Stat183__"));
    my_map.insert(pair<int, string>(430,"JET_EtaIntercalibration_Stat184__"));
    my_map.insert(pair<int, string>(431,"JET_EtaIntercalibration_Stat185__"));
    my_map.insert(pair<int, string>(432,"JET_EtaIntercalibration_Stat186__"));
    my_map.insert(pair<int, string>(433,"JET_EtaIntercalibration_Stat187__"));
    my_map.insert(pair<int, string>(434,"JET_EtaIntercalibration_Stat188__"));
    my_map.insert(pair<int, string>(435,"JET_EtaIntercalibration_Stat189__"));
    my_map.insert(pair<int, string>(436,"JET_EtaIntercalibration_Stat190__"));
    my_map.insert(pair<int, string>(437,"JET_EtaIntercalibration_Stat191__"));
    my_map.insert(pair<int, string>(438,"JET_EtaIntercalibration_Stat192__"));
    my_map.insert(pair<int, string>(439,"JET_EtaIntercalibration_Stat193__"));
    my_map.insert(pair<int, string>(440,"JET_EtaIntercalibration_Stat194__"));
    my_map.insert(pair<int, string>(441,"JET_EtaIntercalibration_Stat195__"));
    my_map.insert(pair<int, string>(442,"JET_EtaIntercalibration_Stat196__"));
    my_map.insert(pair<int, string>(443,"JET_EtaIntercalibration_Stat197__"));
    my_map.insert(pair<int, string>(444,"JET_EtaIntercalibration_Stat198__"));
    my_map.insert(pair<int, string>(445,"JET_EtaIntercalibration_Stat199__"));
    my_map.insert(pair<int, string>(446,"JET_EtaIntercalibration_Stat200__"));
    my_map.insert(pair<int, string>(447,"JET_EtaIntercalibration_Stat201__"));
    my_map.insert(pair<int, string>(448,"JET_EtaIntercalibration_Stat202__"));
    my_map.insert(pair<int, string>(449,"JET_EtaIntercalibration_Stat203__"));
    my_map.insert(pair<int, string>(450,"JET_EtaIntercalibration_Stat204__"));
    my_map.insert(pair<int, string>(451,"JET_EtaIntercalibration_Stat205__"));
    my_map.insert(pair<int, string>(452,"JET_EtaIntercalibration_Stat206__"));
    my_map.insert(pair<int, string>(453,"JET_EtaIntercalibration_Stat207__"));
    my_map.insert(pair<int, string>(454,"JET_EtaIntercalibration_Stat208__"));
    my_map.insert(pair<int, string>(455,"JET_EtaIntercalibration_Stat209__"));
    my_map.insert(pair<int, string>(456,"JET_EtaIntercalibration_Stat210__"));
    my_map.insert(pair<int, string>(457,"JET_EtaIntercalibration_Stat211__"));
    my_map.insert(pair<int, string>(458,"JET_EtaIntercalibration_Stat212__"));
    my_map.insert(pair<int, string>(459,"JET_EtaIntercalibration_Stat213__"));
    my_map.insert(pair<int, string>(460,"JET_EtaIntercalibration_Stat214__"));
    my_map.insert(pair<int, string>(461,"JET_EtaIntercalibration_Stat215__"));
    my_map.insert(pair<int, string>(462,"JET_EtaIntercalibration_Stat216__"));
    my_map.insert(pair<int, string>(463,"JET_EtaIntercalibration_Stat217__"));
    my_map.insert(pair<int, string>(464,"JET_EtaIntercalibration_Stat218__"));
    my_map.insert(pair<int, string>(465,"JET_EtaIntercalibration_Stat219__"));
    my_map.insert(pair<int, string>(466,"JET_EtaIntercalibration_Stat220__"));
    my_map.insert(pair<int, string>(467,"JET_EtaIntercalibration_Stat221__"));
    my_map.insert(pair<int, string>(468,"JET_EtaIntercalibration_Stat222__"));
    my_map.insert(pair<int, string>(469,"JET_EtaIntercalibration_Stat223__"));
    my_map.insert(pair<int, string>(470,"JET_EtaIntercalibration_Stat224__"));
    my_map.insert(pair<int, string>(471,"JET_EtaIntercalibration_Stat225__"));
    my_map.insert(pair<int, string>(472,"JET_EtaIntercalibration_Stat226__"));
    my_map.insert(pair<int, string>(473,"JET_EtaIntercalibration_Stat227__"));
    my_map.insert(pair<int, string>(474,"JET_EtaIntercalibration_Stat228__"));
    my_map.insert(pair<int, string>(475,"JET_EtaIntercalibration_Stat229__"));
    my_map.insert(pair<int, string>(476,"JET_EtaIntercalibration_Stat230__"));
    my_map.insert(pair<int, string>(477,"JET_EtaIntercalibration_Stat231__"));
    my_map.insert(pair<int, string>(478,"JET_EtaIntercalibration_Stat232__"));
    my_map.insert(pair<int, string>(479,"JET_EtaIntercalibration_Stat233__"));
    my_map.insert(pair<int, string>(480,"JET_EtaIntercalibration_Stat234__"));
    my_map.insert(pair<int, string>(481,"JET_EtaIntercalibration_Stat235__"));
    my_map.insert(pair<int, string>(482,"JET_EtaIntercalibration_Stat236__"));
    my_map.insert(pair<int, string>(483,"JET_EtaIntercalibration_Stat237__"));
    my_map.insert(pair<int, string>(484,"JET_EtaIntercalibration_Stat238__"));
    my_map.insert(pair<int, string>(485,"JET_EtaIntercalibration_Stat239__"));
    my_map.insert(pair<int, string>(486,"JET_EtaIntercalibration_Stat240__"));
    my_map.insert(pair<int, string>(487,"JET_EtaIntercalibration_Stat241__"));
    my_map.insert(pair<int, string>(488,"JET_EtaIntercalibration_Stat242__"));
    my_map.insert(pair<int, string>(489,"JET_EtaIntercalibration_Stat243__"));
    my_map.insert(pair<int, string>(490,"JET_EtaIntercalibration_Stat244__"));
    my_map.insert(pair<int, string>(491,"JET_EtaIntercalibration_Stat245__"));
   
    my_map.insert(pair<int, string>(492,"JET_EtaIntercalibration_TotalStat_MJB__"));


    my_map.insert(pair<int, string>(1113,"JET_EtaIntercalibration_Modelling__"));
    my_map.insert(pair<int, string>(1115,"JET_EtaIntercalibration_NonClosure__"));
    my_map.insert(pair<int, string>(1117,"JET_Flavor_Composition__"));
    my_map.insert(pair<int, string>(1119,"JET_Flavor_Response__"));
    my_map.insert(pair<int, string>(1121,"JET_Gjet_Generator__"));
    my_map.insert(pair<int, string>(1122,"JET_Gjet_OOC__"));
    my_map.insert(pair<int, string>(1124,"JET_Gjet_Purity__"));
    my_map.insert(pair<int, string>(1126,"JET_Gjet_Stat10__"));
    my_map.insert(pair<int, string>(1128,"JET_Gjet_Stat11__"));
    my_map.insert(pair<int, string>(1130,"JET_Gjet_Stat12__"));
    my_map.insert(pair<int, string>(1132,"JET_Gjet_Stat13__"));
    my_map.insert(pair<int, string>(1134,"JET_Gjet_Stat14__"));
    my_map.insert(pair<int, string>(1136,"JET_Gjet_Stat15__"));
    my_map.insert(pair<int, string>(1138,"JET_Gjet_Stat1__"));
    my_map.insert(pair<int, string>(1140,"JET_Gjet_Stat2__"));
    my_map.insert(pair<int, string>(1142,"JET_Gjet_Stat3__"));
    my_map.insert(pair<int, string>(1144,"JET_Gjet_Stat4__"));
    my_map.insert(pair<int, string>(1146,"JET_Gjet_Stat5__"));
    my_map.insert(pair<int, string>(1148,"JET_Gjet_Stat6__"));
    my_map.insert(pair<int, string>(1150,"JET_Gjet_Stat7__"));
    my_map.insert(pair<int, string>(1152,"JET_Gjet_Stat8__"));
    my_map.insert(pair<int, string>(1154,"JET_Gjet_Stat9__"));
    my_map.insert(pair<int, string>(1156,"JET_Gjet_Veto__"));
    my_map.insert(pair<int, string>(1158,"JET_Gjet_dPhi__"));
    my_map.insert(pair<int, string>(1160,"JET_LAr_ESZee__"));
    my_map.insert(pair<int, string>(1162,"JET_LAr_Esmear__"));
    my_map.insert(pair<int, string>(1164,"JET_LAr_JVT__"));
    my_map.insert(pair<int, string>(1166,"JET_MJB_Alpha__"));
    my_map.insert(pair<int, string>(1168,"JET_MJB_Asym__"));
    my_map.insert(pair<int, string>(1170,"JET_MJB_Beta__"));
    my_map.insert(pair<int, string>(1172,"JET_MJB_Fragmentation__"));
    my_map.insert(pair<int, string>(1174,"JET_MJB_Stat10__"));
    my_map.insert(pair<int, string>(1176,"JET_MJB_Stat11__"));
    my_map.insert(pair<int, string>(1178,"JET_MJB_Stat12__"));
    my_map.insert(pair<int, string>(1180,"JET_MJB_Stat13__"));
    my_map.insert(pair<int, string>(1182,"JET_MJB_Stat14__"));
    my_map.insert(pair<int, string>(1184,"JET_MJB_Stat15__"));
    my_map.insert(pair<int, string>(1186,"JET_MJB_Stat16__"));
    my_map.insert(pair<int, string>(1188,"JET_MJB_Stat1__"));
    my_map.insert(pair<int, string>(1190,"JET_MJB_Stat2__"));
    my_map.insert(pair<int, string>(1192,"JET_MJB_Stat3__"));
    my_map.insert(pair<int, string>(1194,"JET_MJB_Stat4__"));
    my_map.insert(pair<int, string>(1196,"JET_MJB_Stat5__"));
    my_map.insert(pair<int, string>(1198,"JET_MJB_Stat6__"));
    my_map.insert(pair<int, string>(1200,"JET_MJB_Stat7__"));
    my_map.insert(pair<int, string>(1202,"JET_MJB_Stat8__"));
    my_map.insert(pair<int, string>(1204,"JET_MJB_Stat9__"));
    my_map.insert(pair<int, string>(1206,"JET_MJB_Threshold__"));
    my_map.insert(pair<int, string>(1208,"JET_Pileup_OffsetMu__"));
    my_map.insert(pair<int, string>(1210,"JET_Pileup_OffsetNPV__"));
    my_map.insert(pair<int, string>(1212,"JET_Pileup_PtTerm__"));
    my_map.insert(pair<int, string>(1214,"JET_Pileup_RhoTopology__"));
    my_map.insert(pair<int, string>(1216,"JET_PunchThrough_MC15__"));
    my_map.insert(pair<int, string>(1218,"JET_SingleParticle_HighPt__"));
    my_map.insert(pair<int, string>(1220,"JET_Zjet_KTerm__"));
    my_map.insert(pair<int, string>(1222,"JET_Zjet_MC__"));
    my_map.insert(pair<int, string>(1224,"JET_Zjet_MuScale__"));
    my_map.insert(pair<int, string>(1226,"JET_Zjet_MuSmearID__"));
    my_map.insert(pair<int, string>(1228,"JET_Zjet_MuSmearMS__"));
    my_map.insert(pair<int, string>(1230,"JET_Zjet_Stat10__"));
    my_map.insert(pair<int, string>(1232,"JET_Zjet_Stat11__"));
    my_map.insert(pair<int, string>(1234,"JET_Zjet_Stat12__"));
    my_map.insert(pair<int, string>(1236,"JET_Zjet_Stat13__"));
    my_map.insert(pair<int, string>(1238,"JET_Zjet_Stat1__"));
    my_map.insert(pair<int, string>(1240,"JET_Zjet_Stat2__"));
    my_map.insert(pair<int, string>(1242,"JET_Zjet_Stat3__"));
    my_map.insert(pair<int, string>(1244,"JET_Zjet_Stat4__"));
    my_map.insert(pair<int, string>(1246,"JET_Zjet_Stat5__"));
    my_map.insert(pair<int, string>(1248,"JET_Zjet_Stat6__"));
    my_map.insert(pair<int, string>(1250,"JET_Zjet_Stat7__"));
    my_map.insert(pair<int, string>(1252,"JET_Zjet_Stat8__"));
    my_map.insert(pair<int, string>(1254,"JET_Zjet_Stat9__"));
    my_map.insert(pair<int, string>(1256,"JET_Zjet_Veto__"));
    my_map.insert(pair<int, string>(1258,"JET_Zjet_dPhi__"));

*/
    
    //JetCleaning

//    my_map.insert(pair<int, string>(0,"JET_Cleaning_1up"));

    //Statistical Fix bin low edge

    my_map.insert(pair<int, string>(0,"Nominal_AvgRep"));
//    my_map.insert(pair<int, string>(1,"UData"));

    my_map.insert(pair<int, string>(1,"rep_0"));

    my_map.insert(pair<int, string>(2,"rep_1"));
    my_map.insert(pair<int, string>(3,"rep_2"));
    my_map.insert(pair<int, string>(4,"rep_3"));
    my_map.insert(pair<int, string>(5,"rep_4"));
    my_map.insert(pair<int, string>(6,"rep_5"));
    my_map.insert(pair<int, string>(7,"rep_6"));
    my_map.insert(pair<int, string>(8,"rep_7"));
    my_map.insert(pair<int, string>(9,"rep_8"));
    my_map.insert(pair<int, string>(10,"rep_9"));
    my_map.insert(pair<int, string>(11,"rep_10"));
    my_map.insert(pair<int, string>(12,"rep_11"));
    my_map.insert(pair<int, string>(13,"rep_12"));
    my_map.insert(pair<int, string>(14,"rep_13"));
    my_map.insert(pair<int, string>(15,"rep_14"));
    my_map.insert(pair<int, string>(16,"rep_15"));
    my_map.insert(pair<int, string>(17,"rep_16"));
    my_map.insert(pair<int, string>(18,"rep_17"));
    my_map.insert(pair<int, string>(19,"rep_18"));
    my_map.insert(pair<int, string>(20,"rep_19"));
    my_map.insert(pair<int, string>(21,"rep_20"));
    my_map.insert(pair<int, string>(22,"rep_21"));
    my_map.insert(pair<int, string>(23,"rep_22"));
    my_map.insert(pair<int, string>(24,"rep_23"));
    my_map.insert(pair<int, string>(25,"rep_24"));
    my_map.insert(pair<int, string>(26,"rep_25"));
    my_map.insert(pair<int, string>(27,"rep_26"));
    my_map.insert(pair<int, string>(28,"rep_27"));
    my_map.insert(pair<int, string>(29,"rep_28"));
    my_map.insert(pair<int, string>(30,"rep_29"));
    my_map.insert(pair<int, string>(31,"rep_30"));
    my_map.insert(pair<int, string>(32,"rep_31"));
    my_map.insert(pair<int, string>(33,"rep_32"));
    my_map.insert(pair<int, string>(34,"rep_33"));
    my_map.insert(pair<int, string>(35,"rep_34"));
    my_map.insert(pair<int, string>(36,"rep_35"));
    my_map.insert(pair<int, string>(37,"rep_36"));
    my_map.insert(pair<int, string>(38,"rep_37"));
    my_map.insert(pair<int, string>(39,"rep_38"));
    my_map.insert(pair<int, string>(40,"rep_39"));
    my_map.insert(pair<int, string>(41,"rep_40"));
    my_map.insert(pair<int, string>(42,"rep_41"));
    my_map.insert(pair<int, string>(43,"rep_42"));
    my_map.insert(pair<int, string>(44,"rep_43"));
    my_map.insert(pair<int, string>(45,"rep_44"));
    my_map.insert(pair<int, string>(46,"rep_45"));
    my_map.insert(pair<int, string>(47,"rep_46"));
    my_map.insert(pair<int, string>(48,"rep_47"));
    my_map.insert(pair<int, string>(49,"rep_48"));
    my_map.insert(pair<int, string>(50,"rep_49"));
    my_map.insert(pair<int, string>(51,"rep_50"));
    my_map.insert(pair<int, string>(52,"rep_51"));
    my_map.insert(pair<int, string>(53,"rep_52"));
    my_map.insert(pair<int, string>(54,"rep_53"));
    my_map.insert(pair<int, string>(55,"rep_54"));
    my_map.insert(pair<int, string>(56,"rep_55"));
    my_map.insert(pair<int, string>(57,"rep_56"));
    my_map.insert(pair<int, string>(58,"rep_57"));
    my_map.insert(pair<int, string>(59,"rep_58"));
    my_map.insert(pair<int, string>(60,"rep_59"));
    my_map.insert(pair<int, string>(61,"rep_60"));
    my_map.insert(pair<int, string>(62,"rep_61"));
    my_map.insert(pair<int, string>(63,"rep_62"));
    my_map.insert(pair<int, string>(64,"rep_63"));
    my_map.insert(pair<int, string>(65,"rep_64"));
    my_map.insert(pair<int, string>(66,"rep_65"));
    my_map.insert(pair<int, string>(67,"rep_66"));
    my_map.insert(pair<int, string>(68,"rep_67"));
    my_map.insert(pair<int, string>(69,"rep_68"));
    my_map.insert(pair<int, string>(70,"rep_69"));
    my_map.insert(pair<int, string>(71,"rep_70"));
    my_map.insert(pair<int, string>(72,"rep_71"));
    my_map.insert(pair<int, string>(73,"rep_72"));
    my_map.insert(pair<int, string>(74,"rep_73"));
    my_map.insert(pair<int, string>(75,"rep_74"));
    my_map.insert(pair<int, string>(76,"rep_75"));
    my_map.insert(pair<int, string>(77,"rep_76"));
    my_map.insert(pair<int, string>(78,"rep_77"));
    my_map.insert(pair<int, string>(79,"rep_78"));
    my_map.insert(pair<int, string>(80,"rep_79"));
    my_map.insert(pair<int, string>(81,"rep_80"));
    my_map.insert(pair<int, string>(82,"rep_81"));
    my_map.insert(pair<int, string>(83,"rep_82"));
    my_map.insert(pair<int, string>(84,"rep_83"));
    my_map.insert(pair<int, string>(85,"rep_84"));
    my_map.insert(pair<int, string>(86,"rep_85"));
    my_map.insert(pair<int, string>(87,"rep_86"));
    my_map.insert(pair<int, string>(88,"rep_87"));
    my_map.insert(pair<int, string>(89,"rep_88"));
    my_map.insert(pair<int, string>(90,"rep_89"));
    my_map.insert(pair<int, string>(91,"rep_90"));
    my_map.insert(pair<int, string>(92,"rep_91"));
    my_map.insert(pair<int, string>(93,"rep_92"));
    my_map.insert(pair<int, string>(94,"rep_93"));
    my_map.insert(pair<int, string>(95,"rep_94"));
    my_map.insert(pair<int, string>(96,"rep_95"));
    my_map.insert(pair<int, string>(97,"rep_96"));
    my_map.insert(pair<int, string>(98,"rep_97"));
    my_map.insert(pair<int, string>(99,"rep_98"));
    my_map.insert(pair<int, string>(100,"rep_99"));
    my_map.insert(pair<int, string>(101,"rep_100"));
    my_map.insert(pair<int, string>(102,"rep_101"));
    my_map.insert(pair<int, string>(103,"rep_102"));
    my_map.insert(pair<int, string>(104,"rep_103"));
    my_map.insert(pair<int, string>(105,"rep_104"));
    my_map.insert(pair<int, string>(106,"rep_105"));
    my_map.insert(pair<int, string>(107,"rep_106"));
    my_map.insert(pair<int, string>(108,"rep_107"));
    my_map.insert(pair<int, string>(109,"rep_108"));
    my_map.insert(pair<int, string>(110,"rep_109"));
    my_map.insert(pair<int, string>(111,"rep_110"));
    my_map.insert(pair<int, string>(112,"rep_111"));
    my_map.insert(pair<int, string>(113,"rep_112"));
    my_map.insert(pair<int, string>(114,"rep_113"));
    my_map.insert(pair<int, string>(115,"rep_114"));
    my_map.insert(pair<int, string>(116,"rep_115"));
    my_map.insert(pair<int, string>(117,"rep_116"));
    my_map.insert(pair<int, string>(118,"rep_117"));
    my_map.insert(pair<int, string>(119,"rep_118"));
    my_map.insert(pair<int, string>(120,"rep_119"));
    my_map.insert(pair<int, string>(121,"rep_120"));
    my_map.insert(pair<int, string>(122,"rep_121"));
    my_map.insert(pair<int, string>(123,"rep_122"));
    my_map.insert(pair<int, string>(124,"rep_123"));
    my_map.insert(pair<int, string>(125,"rep_124"));
    my_map.insert(pair<int, string>(126,"rep_125"));
    my_map.insert(pair<int, string>(127,"rep_126"));
    my_map.insert(pair<int, string>(128,"rep_127"));
    my_map.insert(pair<int, string>(129,"rep_128"));
    my_map.insert(pair<int, string>(130,"rep_129"));
    my_map.insert(pair<int, string>(131,"rep_130"));
    my_map.insert(pair<int, string>(132,"rep_131"));
    my_map.insert(pair<int, string>(133,"rep_132"));
    my_map.insert(pair<int, string>(134,"rep_133"));
    my_map.insert(pair<int, string>(135,"rep_134"));
    my_map.insert(pair<int, string>(136,"rep_135"));
    my_map.insert(pair<int, string>(137,"rep_136"));
    my_map.insert(pair<int, string>(138,"rep_137"));
    my_map.insert(pair<int, string>(139,"rep_138"));
    my_map.insert(pair<int, string>(140,"rep_139"));
    my_map.insert(pair<int, string>(141,"rep_140"));
    my_map.insert(pair<int, string>(142,"rep_141"));
    my_map.insert(pair<int, string>(143,"rep_142"));
    my_map.insert(pair<int, string>(144,"rep_143"));
    my_map.insert(pair<int, string>(145,"rep_144"));
    my_map.insert(pair<int, string>(146,"rep_145"));
    my_map.insert(pair<int, string>(147,"rep_146"));
    my_map.insert(pair<int, string>(148,"rep_147"));
    my_map.insert(pair<int, string>(149,"rep_148"));
    my_map.insert(pair<int, string>(150,"rep_149"));
    my_map.insert(pair<int, string>(151,"rep_150"));
    my_map.insert(pair<int, string>(152,"rep_151"));
    my_map.insert(pair<int, string>(153,"rep_152"));
    my_map.insert(pair<int, string>(154,"rep_153"));
    my_map.insert(pair<int, string>(155,"rep_154"));
    my_map.insert(pair<int, string>(156,"rep_155"));
    my_map.insert(pair<int, string>(157,"rep_156"));
    my_map.insert(pair<int, string>(158,"rep_157"));
    my_map.insert(pair<int, string>(159,"rep_158"));
    my_map.insert(pair<int, string>(160,"rep_159"));
    my_map.insert(pair<int, string>(161,"rep_160"));
    my_map.insert(pair<int, string>(162,"rep_161"));
    my_map.insert(pair<int, string>(163,"rep_162"));
    my_map.insert(pair<int, string>(164,"rep_163"));
    my_map.insert(pair<int, string>(165,"rep_164"));
    my_map.insert(pair<int, string>(166,"rep_165"));
    my_map.insert(pair<int, string>(167,"rep_166"));
    my_map.insert(pair<int, string>(168,"rep_167"));
    my_map.insert(pair<int, string>(169,"rep_168"));
    my_map.insert(pair<int, string>(170,"rep_169"));
    my_map.insert(pair<int, string>(171,"rep_170"));
    my_map.insert(pair<int, string>(172,"rep_171"));
    my_map.insert(pair<int, string>(173,"rep_172"));
    my_map.insert(pair<int, string>(174,"rep_173"));
    my_map.insert(pair<int, string>(175,"rep_174"));
    my_map.insert(pair<int, string>(176,"rep_175"));
    my_map.insert(pair<int, string>(177,"rep_176"));
    my_map.insert(pair<int, string>(178,"rep_177"));
    my_map.insert(pair<int, string>(179,"rep_178"));
    my_map.insert(pair<int, string>(180,"rep_179"));
    my_map.insert(pair<int, string>(181,"rep_180"));
    my_map.insert(pair<int, string>(182,"rep_181"));
    my_map.insert(pair<int, string>(183,"rep_182"));
    my_map.insert(pair<int, string>(184,"rep_183"));
    my_map.insert(pair<int, string>(185,"rep_184"));
    my_map.insert(pair<int, string>(186,"rep_185"));
    my_map.insert(pair<int, string>(187,"rep_186"));
    my_map.insert(pair<int, string>(188,"rep_187"));
    my_map.insert(pair<int, string>(189,"rep_188"));
    my_map.insert(pair<int, string>(190,"rep_189"));
    my_map.insert(pair<int, string>(191,"rep_190"));
    my_map.insert(pair<int, string>(192,"rep_191"));
    my_map.insert(pair<int, string>(193,"rep_192"));
    my_map.insert(pair<int, string>(194,"rep_193"));
    my_map.insert(pair<int, string>(195,"rep_194"));
    my_map.insert(pair<int, string>(196,"rep_195"));
    my_map.insert(pair<int, string>(197,"rep_196"));
    my_map.insert(pair<int, string>(198,"rep_197"));
    my_map.insert(pair<int, string>(199,"rep_198"));
    my_map.insert(pair<int, string>(200,"rep_199"));
    my_map.insert(pair<int, string>(201,"rep_200"));
    my_map.insert(pair<int, string>(202,"rep_201"));
    my_map.insert(pair<int, string>(203,"rep_202"));
    my_map.insert(pair<int, string>(204,"rep_203"));
    my_map.insert(pair<int, string>(205,"rep_204"));
    my_map.insert(pair<int, string>(206,"rep_205"));
    my_map.insert(pair<int, string>(207,"rep_206"));
    my_map.insert(pair<int, string>(208,"rep_207"));
    my_map.insert(pair<int, string>(209,"rep_208"));
    my_map.insert(pair<int, string>(210,"rep_209"));
    my_map.insert(pair<int, string>(211,"rep_210"));
    my_map.insert(pair<int, string>(212,"rep_211"));
    my_map.insert(pair<int, string>(213,"rep_212"));
    my_map.insert(pair<int, string>(214,"rep_213"));
    my_map.insert(pair<int, string>(215,"rep_214"));
    my_map.insert(pair<int, string>(216,"rep_215"));
    my_map.insert(pair<int, string>(217,"rep_216"));
    my_map.insert(pair<int, string>(218,"rep_217"));
    my_map.insert(pair<int, string>(219,"rep_218"));
    my_map.insert(pair<int, string>(220,"rep_219"));
    my_map.insert(pair<int, string>(221,"rep_220"));
    my_map.insert(pair<int, string>(222,"rep_221"));
    my_map.insert(pair<int, string>(223,"rep_222"));
    my_map.insert(pair<int, string>(224,"rep_223"));
    my_map.insert(pair<int, string>(225,"rep_224"));
    my_map.insert(pair<int, string>(226,"rep_225"));
    my_map.insert(pair<int, string>(227,"rep_226"));
    my_map.insert(pair<int, string>(228,"rep_227"));
    my_map.insert(pair<int, string>(229,"rep_228"));
    my_map.insert(pair<int, string>(230,"rep_229"));
    my_map.insert(pair<int, string>(231,"rep_230"));
    my_map.insert(pair<int, string>(232,"rep_231"));
    my_map.insert(pair<int, string>(233,"rep_232"));
    my_map.insert(pair<int, string>(234,"rep_233"));
    my_map.insert(pair<int, string>(235,"rep_234"));
    my_map.insert(pair<int, string>(236,"rep_235"));
    my_map.insert(pair<int, string>(237,"rep_236"));
    my_map.insert(pair<int, string>(238,"rep_237"));
    my_map.insert(pair<int, string>(239,"rep_238"));
    my_map.insert(pair<int, string>(240,"rep_239"));
    my_map.insert(pair<int, string>(241,"rep_240"));
    my_map.insert(pair<int, string>(242,"rep_241"));
    my_map.insert(pair<int, string>(243,"rep_242"));
    my_map.insert(pair<int, string>(244,"rep_243"));
    my_map.insert(pair<int, string>(245,"rep_244"));
    my_map.insert(pair<int, string>(246,"rep_245"));
    my_map.insert(pair<int, string>(247,"rep_246"));
    my_map.insert(pair<int, string>(248,"rep_247"));
    my_map.insert(pair<int, string>(249,"rep_248"));
    my_map.insert(pair<int, string>(250,"rep_249"));
    my_map.insert(pair<int, string>(251,"rep_250"));
    my_map.insert(pair<int, string>(252,"rep_251"));
    my_map.insert(pair<int, string>(253,"rep_252"));
    my_map.insert(pair<int, string>(254,"rep_253"));
    my_map.insert(pair<int, string>(255,"rep_254"));
    my_map.insert(pair<int, string>(256,"rep_255"));
    my_map.insert(pair<int, string>(257,"rep_256"));
    my_map.insert(pair<int, string>(258,"rep_257"));
    my_map.insert(pair<int, string>(259,"rep_258"));
    my_map.insert(pair<int, string>(260,"rep_259"));
    my_map.insert(pair<int, string>(261,"rep_260"));
    my_map.insert(pair<int, string>(262,"rep_261"));
    my_map.insert(pair<int, string>(263,"rep_262"));
    my_map.insert(pair<int, string>(264,"rep_263"));
    my_map.insert(pair<int, string>(265,"rep_264"));
    my_map.insert(pair<int, string>(266,"rep_265"));
    my_map.insert(pair<int, string>(267,"rep_266"));
    my_map.insert(pair<int, string>(268,"rep_267"));
    my_map.insert(pair<int, string>(269,"rep_268"));
    my_map.insert(pair<int, string>(270,"rep_269"));
    my_map.insert(pair<int, string>(271,"rep_270"));
    my_map.insert(pair<int, string>(272,"rep_271"));
    my_map.insert(pair<int, string>(273,"rep_272"));
    my_map.insert(pair<int, string>(274,"rep_273"));
    my_map.insert(pair<int, string>(275,"rep_274"));
    my_map.insert(pair<int, string>(276,"rep_275"));
    my_map.insert(pair<int, string>(277,"rep_276"));
    my_map.insert(pair<int, string>(278,"rep_277"));
    my_map.insert(pair<int, string>(279,"rep_278"));
    my_map.insert(pair<int, string>(280,"rep_279"));
    my_map.insert(pair<int, string>(281,"rep_280"));
    my_map.insert(pair<int, string>(282,"rep_281"));
    my_map.insert(pair<int, string>(283,"rep_282"));
    my_map.insert(pair<int, string>(284,"rep_283"));
    my_map.insert(pair<int, string>(285,"rep_284"));
    my_map.insert(pair<int, string>(286,"rep_285"));
    my_map.insert(pair<int, string>(287,"rep_286"));
    my_map.insert(pair<int, string>(288,"rep_287"));
    my_map.insert(pair<int, string>(289,"rep_288"));
    my_map.insert(pair<int, string>(290,"rep_289"));
    my_map.insert(pair<int, string>(291,"rep_290"));
    my_map.insert(pair<int, string>(292,"rep_291"));
    my_map.insert(pair<int, string>(293,"rep_292"));
    my_map.insert(pair<int, string>(294,"rep_293"));
    my_map.insert(pair<int, string>(295,"rep_294"));
    my_map.insert(pair<int, string>(296,"rep_295"));
    my_map.insert(pair<int, string>(297,"rep_296"));
    my_map.insert(pair<int, string>(298,"rep_297"));
    my_map.insert(pair<int, string>(299,"rep_298"));
    my_map.insert(pair<int, string>(300,"rep_299"));
    my_map.insert(pair<int, string>(301,"rep_300"));
    my_map.insert(pair<int, string>(302,"rep_301"));
    my_map.insert(pair<int, string>(303,"rep_302"));
    my_map.insert(pair<int, string>(304,"rep_303"));
    my_map.insert(pair<int, string>(305,"rep_304"));
    my_map.insert(pair<int, string>(306,"rep_305"));
    my_map.insert(pair<int, string>(307,"rep_306"));
    my_map.insert(pair<int, string>(308,"rep_307"));
    my_map.insert(pair<int, string>(309,"rep_308"));
    my_map.insert(pair<int, string>(310,"rep_309"));
    my_map.insert(pair<int, string>(311,"rep_310"));
    my_map.insert(pair<int, string>(312,"rep_311"));
    my_map.insert(pair<int, string>(313,"rep_312"));
    my_map.insert(pair<int, string>(314,"rep_313"));
    my_map.insert(pair<int, string>(315,"rep_314"));
    my_map.insert(pair<int, string>(316,"rep_315"));
    my_map.insert(pair<int, string>(317,"rep_316"));
    my_map.insert(pair<int, string>(318,"rep_317"));
    my_map.insert(pair<int, string>(319,"rep_318"));
    my_map.insert(pair<int, string>(320,"rep_319"));
    my_map.insert(pair<int, string>(321,"rep_320"));
    my_map.insert(pair<int, string>(322,"rep_321"));
    my_map.insert(pair<int, string>(323,"rep_322"));
    my_map.insert(pair<int, string>(324,"rep_323"));
    my_map.insert(pair<int, string>(325,"rep_324"));
    my_map.insert(pair<int, string>(326,"rep_325"));
    my_map.insert(pair<int, string>(327,"rep_326"));
    my_map.insert(pair<int, string>(328,"rep_327"));
    my_map.insert(pair<int, string>(329,"rep_328"));
    my_map.insert(pair<int, string>(330,"rep_329"));
    my_map.insert(pair<int, string>(331,"rep_330"));
    my_map.insert(pair<int, string>(332,"rep_331"));
    my_map.insert(pair<int, string>(333,"rep_332"));
    my_map.insert(pair<int, string>(334,"rep_333"));
    my_map.insert(pair<int, string>(335,"rep_334"));
    my_map.insert(pair<int, string>(336,"rep_335"));
    my_map.insert(pair<int, string>(337,"rep_336"));
    my_map.insert(pair<int, string>(338,"rep_337"));
    my_map.insert(pair<int, string>(339,"rep_338"));
    my_map.insert(pair<int, string>(340,"rep_339"));
    my_map.insert(pair<int, string>(341,"rep_340"));
    my_map.insert(pair<int, string>(342,"rep_341"));
    my_map.insert(pair<int, string>(343,"rep_342"));
    my_map.insert(pair<int, string>(344,"rep_343"));
    my_map.insert(pair<int, string>(345,"rep_344"));
    my_map.insert(pair<int, string>(346,"rep_345"));
    my_map.insert(pair<int, string>(347,"rep_346"));
    my_map.insert(pair<int, string>(348,"rep_347"));
    my_map.insert(pair<int, string>(349,"rep_348"));
    my_map.insert(pair<int, string>(350,"rep_349"));
    my_map.insert(pair<int, string>(351,"rep_350"));
    my_map.insert(pair<int, string>(352,"rep_351"));
    my_map.insert(pair<int, string>(353,"rep_352"));
    my_map.insert(pair<int, string>(354,"rep_353"));
    my_map.insert(pair<int, string>(355,"rep_354"));
    my_map.insert(pair<int, string>(356,"rep_355"));
    my_map.insert(pair<int, string>(357,"rep_356"));
    my_map.insert(pair<int, string>(358,"rep_357"));
    my_map.insert(pair<int, string>(359,"rep_358"));
    my_map.insert(pair<int, string>(360,"rep_359"));
    my_map.insert(pair<int, string>(361,"rep_360"));
    my_map.insert(pair<int, string>(362,"rep_361"));
    my_map.insert(pair<int, string>(363,"rep_362"));
    my_map.insert(pair<int, string>(364,"rep_363"));
    my_map.insert(pair<int, string>(365,"rep_364"));
    my_map.insert(pair<int, string>(366,"rep_365"));
    my_map.insert(pair<int, string>(367,"rep_366"));
    my_map.insert(pair<int, string>(368,"rep_367"));
    my_map.insert(pair<int, string>(369,"rep_368"));
    my_map.insert(pair<int, string>(370,"rep_369"));
    my_map.insert(pair<int, string>(371,"rep_370"));
    my_map.insert(pair<int, string>(372,"rep_371"));
    my_map.insert(pair<int, string>(373,"rep_372"));
    my_map.insert(pair<int, string>(374,"rep_373"));
    my_map.insert(pair<int, string>(375,"rep_374"));
    my_map.insert(pair<int, string>(376,"rep_375"));
    my_map.insert(pair<int, string>(377,"rep_376"));
    my_map.insert(pair<int, string>(378,"rep_377"));
    my_map.insert(pair<int, string>(379,"rep_378"));
    my_map.insert(pair<int, string>(380,"rep_379"));
    my_map.insert(pair<int, string>(381,"rep_380"));
    my_map.insert(pair<int, string>(382,"rep_381"));
    my_map.insert(pair<int, string>(383,"rep_382"));
    my_map.insert(pair<int, string>(384,"rep_383"));
    my_map.insert(pair<int, string>(385,"rep_384"));
    my_map.insert(pair<int, string>(386,"rep_385"));
    my_map.insert(pair<int, string>(387,"rep_386"));
    my_map.insert(pair<int, string>(388,"rep_387"));
    my_map.insert(pair<int, string>(389,"rep_388"));
    my_map.insert(pair<int, string>(390,"rep_389"));
    my_map.insert(pair<int, string>(391,"rep_390"));
    my_map.insert(pair<int, string>(392,"rep_391"));
    my_map.insert(pair<int, string>(393,"rep_392"));
    my_map.insert(pair<int, string>(394,"rep_393"));
    my_map.insert(pair<int, string>(395,"rep_394"));
    my_map.insert(pair<int, string>(396,"rep_395"));
    my_map.insert(pair<int, string>(397,"rep_396"));
    my_map.insert(pair<int, string>(398,"rep_397"));
    my_map.insert(pair<int, string>(399,"rep_398"));
    my_map.insert(pair<int, string>(400,"rep_399"));
    my_map.insert(pair<int, string>(401,"rep_400"));
    my_map.insert(pair<int, string>(402,"rep_401"));
    my_map.insert(pair<int, string>(403,"rep_402"));
    my_map.insert(pair<int, string>(404,"rep_403"));
    my_map.insert(pair<int, string>(405,"rep_404"));
    my_map.insert(pair<int, string>(406,"rep_405"));
    my_map.insert(pair<int, string>(407,"rep_406"));
    my_map.insert(pair<int, string>(408,"rep_407"));
    my_map.insert(pair<int, string>(409,"rep_408"));
    my_map.insert(pair<int, string>(410,"rep_409"));
    my_map.insert(pair<int, string>(411,"rep_410"));
    my_map.insert(pair<int, string>(412,"rep_411"));
    my_map.insert(pair<int, string>(413,"rep_412"));
    my_map.insert(pair<int, string>(414,"rep_413"));
    my_map.insert(pair<int, string>(415,"rep_414"));
    my_map.insert(pair<int, string>(416,"rep_415"));
    my_map.insert(pair<int, string>(417,"rep_416"));
    my_map.insert(pair<int, string>(418,"rep_417"));
    my_map.insert(pair<int, string>(419,"rep_418"));
    my_map.insert(pair<int, string>(420,"rep_419"));
    my_map.insert(pair<int, string>(421,"rep_420"));
    my_map.insert(pair<int, string>(422,"rep_421"));
    my_map.insert(pair<int, string>(423,"rep_422"));
    my_map.insert(pair<int, string>(424,"rep_423"));
    my_map.insert(pair<int, string>(425,"rep_424"));
    my_map.insert(pair<int, string>(426,"rep_425"));
    my_map.insert(pair<int, string>(427,"rep_426"));
    my_map.insert(pair<int, string>(428,"rep_427"));
    my_map.insert(pair<int, string>(429,"rep_428"));
    my_map.insert(pair<int, string>(430,"rep_429"));
    my_map.insert(pair<int, string>(431,"rep_430"));
    my_map.insert(pair<int, string>(432,"rep_431"));
    my_map.insert(pair<int, string>(433,"rep_432"));
    my_map.insert(pair<int, string>(434,"rep_433"));
    my_map.insert(pair<int, string>(435,"rep_434"));
    my_map.insert(pair<int, string>(436,"rep_435"));
    my_map.insert(pair<int, string>(437,"rep_436"));
    my_map.insert(pair<int, string>(438,"rep_437"));
    my_map.insert(pair<int, string>(439,"rep_438"));
    my_map.insert(pair<int, string>(440,"rep_439"));
    my_map.insert(pair<int, string>(441,"rep_440"));
    my_map.insert(pair<int, string>(442,"rep_441"));
    my_map.insert(pair<int, string>(443,"rep_442"));
    my_map.insert(pair<int, string>(444,"rep_443"));
    my_map.insert(pair<int, string>(445,"rep_444"));
    my_map.insert(pair<int, string>(446,"rep_445"));
    my_map.insert(pair<int, string>(447,"rep_446"));
    my_map.insert(pair<int, string>(448,"rep_447"));
    my_map.insert(pair<int, string>(449,"rep_448"));
    my_map.insert(pair<int, string>(450,"rep_449"));
    my_map.insert(pair<int, string>(451,"rep_450"));
    my_map.insert(pair<int, string>(452,"rep_451"));
    my_map.insert(pair<int, string>(453,"rep_452"));
    my_map.insert(pair<int, string>(454,"rep_453"));
    my_map.insert(pair<int, string>(455,"rep_454"));
    my_map.insert(pair<int, string>(456,"rep_455"));
    my_map.insert(pair<int, string>(457,"rep_456"));
    my_map.insert(pair<int, string>(458,"rep_457"));
    my_map.insert(pair<int, string>(459,"rep_458"));
    my_map.insert(pair<int, string>(460,"rep_459"));
    my_map.insert(pair<int, string>(461,"rep_460"));
    my_map.insert(pair<int, string>(462,"rep_461"));
    my_map.insert(pair<int, string>(463,"rep_462"));
    my_map.insert(pair<int, string>(464,"rep_463"));
    my_map.insert(pair<int, string>(465,"rep_464"));
    my_map.insert(pair<int, string>(466,"rep_465"));
    my_map.insert(pair<int, string>(467,"rep_466"));
    my_map.insert(pair<int, string>(468,"rep_467"));
    my_map.insert(pair<int, string>(469,"rep_468"));
    my_map.insert(pair<int, string>(470,"rep_469"));
    my_map.insert(pair<int, string>(471,"rep_470"));
    my_map.insert(pair<int, string>(472,"rep_471"));
    my_map.insert(pair<int, string>(473,"rep_472"));
    my_map.insert(pair<int, string>(474,"rep_473"));
    my_map.insert(pair<int, string>(475,"rep_474"));
    my_map.insert(pair<int, string>(476,"rep_475"));
    my_map.insert(pair<int, string>(477,"rep_476"));
    my_map.insert(pair<int, string>(478,"rep_477"));
    my_map.insert(pair<int, string>(479,"rep_478"));
    my_map.insert(pair<int, string>(480,"rep_479"));
    my_map.insert(pair<int, string>(481,"rep_480"));
    my_map.insert(pair<int, string>(482,"rep_481"));
    my_map.insert(pair<int, string>(483,"rep_482"));
    my_map.insert(pair<int, string>(484,"rep_483"));
    my_map.insert(pair<int, string>(485,"rep_484"));
    my_map.insert(pair<int, string>(486,"rep_485"));
    my_map.insert(pair<int, string>(487,"rep_486"));
    my_map.insert(pair<int, string>(488,"rep_487"));
    my_map.insert(pair<int, string>(489,"rep_488"));
    my_map.insert(pair<int, string>(490,"rep_489"));
    my_map.insert(pair<int, string>(491,"rep_490"));
    my_map.insert(pair<int, string>(492,"rep_491"));
    my_map.insert(pair<int, string>(493,"rep_492"));
    my_map.insert(pair<int, string>(494,"rep_493"));
    my_map.insert(pair<int, string>(495,"rep_494"));
    my_map.insert(pair<int, string>(496,"rep_495"));
    my_map.insert(pair<int, string>(497,"rep_496"));
    my_map.insert(pair<int, string>(498,"rep_497"));
    my_map.insert(pair<int, string>(499,"rep_498"));
    my_map.insert(pair<int, string>(500,"rep_499"));
    my_map.insert(pair<int, string>(501,"rep_500"));
    my_map.insert(pair<int, string>(502,"rep_501"));
    my_map.insert(pair<int, string>(503,"rep_502"));
    my_map.insert(pair<int, string>(504,"rep_503"));
    my_map.insert(pair<int, string>(505,"rep_504"));
    my_map.insert(pair<int, string>(506,"rep_505"));
    my_map.insert(pair<int, string>(507,"rep_506"));
    my_map.insert(pair<int, string>(508,"rep_507"));
    my_map.insert(pair<int, string>(509,"rep_508"));
    my_map.insert(pair<int, string>(510,"rep_509"));
    my_map.insert(pair<int, string>(511,"rep_510"));
    my_map.insert(pair<int, string>(512,"rep_511"));
    my_map.insert(pair<int, string>(513,"rep_512"));
    my_map.insert(pair<int, string>(514,"rep_513"));
    my_map.insert(pair<int, string>(515,"rep_514"));
    my_map.insert(pair<int, string>(516,"rep_515"));
    my_map.insert(pair<int, string>(517,"rep_516"));
    my_map.insert(pair<int, string>(518,"rep_517"));
    my_map.insert(pair<int, string>(519,"rep_518"));
    my_map.insert(pair<int, string>(520,"rep_519"));
    my_map.insert(pair<int, string>(521,"rep_520"));
    my_map.insert(pair<int, string>(522,"rep_521"));
    my_map.insert(pair<int, string>(523,"rep_522"));
    my_map.insert(pair<int, string>(524,"rep_523"));
    my_map.insert(pair<int, string>(525,"rep_524"));
    my_map.insert(pair<int, string>(526,"rep_525"));
    my_map.insert(pair<int, string>(527,"rep_526"));
    my_map.insert(pair<int, string>(528,"rep_527"));
    my_map.insert(pair<int, string>(529,"rep_528"));
    my_map.insert(pair<int, string>(530,"rep_529"));
    my_map.insert(pair<int, string>(531,"rep_530"));
    my_map.insert(pair<int, string>(532,"rep_531"));
    my_map.insert(pair<int, string>(533,"rep_532"));
    my_map.insert(pair<int, string>(534,"rep_533"));
    my_map.insert(pair<int, string>(535,"rep_534"));
    my_map.insert(pair<int, string>(536,"rep_535"));
    my_map.insert(pair<int, string>(537,"rep_536"));
    my_map.insert(pair<int, string>(538,"rep_537"));
    my_map.insert(pair<int, string>(539,"rep_538"));
    my_map.insert(pair<int, string>(540,"rep_539"));
    my_map.insert(pair<int, string>(541,"rep_540"));
    my_map.insert(pair<int, string>(542,"rep_541"));
    my_map.insert(pair<int, string>(543,"rep_542"));
    my_map.insert(pair<int, string>(544,"rep_543"));
    my_map.insert(pair<int, string>(545,"rep_544"));
    my_map.insert(pair<int, string>(546,"rep_545"));
    my_map.insert(pair<int, string>(547,"rep_546"));
    my_map.insert(pair<int, string>(548,"rep_547"));
    my_map.insert(pair<int, string>(549,"rep_548"));
    my_map.insert(pair<int, string>(550,"rep_549"));
    my_map.insert(pair<int, string>(551,"rep_550"));
    my_map.insert(pair<int, string>(552,"rep_551"));
    my_map.insert(pair<int, string>(553,"rep_552"));
    my_map.insert(pair<int, string>(554,"rep_553"));
    my_map.insert(pair<int, string>(555,"rep_554"));
    my_map.insert(pair<int, string>(556,"rep_555"));
    my_map.insert(pair<int, string>(557,"rep_556"));
    my_map.insert(pair<int, string>(558,"rep_557"));
    my_map.insert(pair<int, string>(559,"rep_558"));
    my_map.insert(pair<int, string>(560,"rep_559"));
    my_map.insert(pair<int, string>(561,"rep_560"));
    my_map.insert(pair<int, string>(562,"rep_561"));
    my_map.insert(pair<int, string>(563,"rep_562"));
    my_map.insert(pair<int, string>(564,"rep_563"));
    my_map.insert(pair<int, string>(565,"rep_564"));
    my_map.insert(pair<int, string>(566,"rep_565"));
    my_map.insert(pair<int, string>(567,"rep_566"));
    my_map.insert(pair<int, string>(568,"rep_567"));
    my_map.insert(pair<int, string>(569,"rep_568"));
    my_map.insert(pair<int, string>(570,"rep_569"));
    my_map.insert(pair<int, string>(571,"rep_570"));
    my_map.insert(pair<int, string>(572,"rep_571"));
    my_map.insert(pair<int, string>(573,"rep_572"));
    my_map.insert(pair<int, string>(574,"rep_573"));
    my_map.insert(pair<int, string>(575,"rep_574"));
    my_map.insert(pair<int, string>(576,"rep_575"));
    my_map.insert(pair<int, string>(577,"rep_576"));
    my_map.insert(pair<int, string>(578,"rep_577"));
    my_map.insert(pair<int, string>(579,"rep_578"));
    my_map.insert(pair<int, string>(580,"rep_579"));
    my_map.insert(pair<int, string>(581,"rep_580"));
    my_map.insert(pair<int, string>(582,"rep_581"));
    my_map.insert(pair<int, string>(583,"rep_582"));
    my_map.insert(pair<int, string>(584,"rep_583"));
    my_map.insert(pair<int, string>(585,"rep_584"));
    my_map.insert(pair<int, string>(586,"rep_585"));
    my_map.insert(pair<int, string>(587,"rep_586"));
    my_map.insert(pair<int, string>(588,"rep_587"));
    my_map.insert(pair<int, string>(589,"rep_588"));
    my_map.insert(pair<int, string>(590,"rep_589"));
    my_map.insert(pair<int, string>(591,"rep_590"));
    my_map.insert(pair<int, string>(592,"rep_591"));
    my_map.insert(pair<int, string>(593,"rep_592"));
    my_map.insert(pair<int, string>(594,"rep_593"));
    my_map.insert(pair<int, string>(595,"rep_594"));
    my_map.insert(pair<int, string>(596,"rep_595"));
    my_map.insert(pair<int, string>(597,"rep_596"));
    my_map.insert(pair<int, string>(598,"rep_597"));
    my_map.insert(pair<int, string>(599,"rep_598"));
    my_map.insert(pair<int, string>(600,"rep_599"));
    my_map.insert(pair<int, string>(601,"rep_600"));
    my_map.insert(pair<int, string>(602,"rep_601"));
    my_map.insert(pair<int, string>(603,"rep_602"));
    my_map.insert(pair<int, string>(604,"rep_603"));
    my_map.insert(pair<int, string>(605,"rep_604"));
    my_map.insert(pair<int, string>(606,"rep_605"));
    my_map.insert(pair<int, string>(607,"rep_606"));
    my_map.insert(pair<int, string>(608,"rep_607"));
    my_map.insert(pair<int, string>(609,"rep_608"));
    my_map.insert(pair<int, string>(610,"rep_609"));
    my_map.insert(pair<int, string>(611,"rep_610"));
    my_map.insert(pair<int, string>(612,"rep_611"));
    my_map.insert(pair<int, string>(613,"rep_612"));
    my_map.insert(pair<int, string>(614,"rep_613"));
    my_map.insert(pair<int, string>(615,"rep_614"));
    my_map.insert(pair<int, string>(616,"rep_615"));
    my_map.insert(pair<int, string>(617,"rep_616"));
    my_map.insert(pair<int, string>(618,"rep_617"));
    my_map.insert(pair<int, string>(619,"rep_618"));
    my_map.insert(pair<int, string>(620,"rep_619"));
    my_map.insert(pair<int, string>(621,"rep_620"));
    my_map.insert(pair<int, string>(622,"rep_621"));
    my_map.insert(pair<int, string>(623,"rep_622"));
    my_map.insert(pair<int, string>(624,"rep_623"));
    my_map.insert(pair<int, string>(625,"rep_624"));
    my_map.insert(pair<int, string>(626,"rep_625"));
    my_map.insert(pair<int, string>(627,"rep_626"));
    my_map.insert(pair<int, string>(628,"rep_627"));
    my_map.insert(pair<int, string>(629,"rep_628"));
    my_map.insert(pair<int, string>(630,"rep_629"));
    my_map.insert(pair<int, string>(631,"rep_630"));
    my_map.insert(pair<int, string>(632,"rep_631"));
    my_map.insert(pair<int, string>(633,"rep_632"));
    my_map.insert(pair<int, string>(634,"rep_633"));
    my_map.insert(pair<int, string>(635,"rep_634"));
    my_map.insert(pair<int, string>(636,"rep_635"));
    my_map.insert(pair<int, string>(637,"rep_636"));
    my_map.insert(pair<int, string>(638,"rep_637"));
    my_map.insert(pair<int, string>(639,"rep_638"));
    my_map.insert(pair<int, string>(640,"rep_639"));
    my_map.insert(pair<int, string>(641,"rep_640"));
    my_map.insert(pair<int, string>(642,"rep_641"));
    my_map.insert(pair<int, string>(643,"rep_642"));
    my_map.insert(pair<int, string>(644,"rep_643"));
    my_map.insert(pair<int, string>(645,"rep_644"));
    my_map.insert(pair<int, string>(646,"rep_645"));
    my_map.insert(pair<int, string>(647,"rep_646"));
    my_map.insert(pair<int, string>(648,"rep_647"));
    my_map.insert(pair<int, string>(649,"rep_648"));
    my_map.insert(pair<int, string>(650,"rep_649"));
    my_map.insert(pair<int, string>(651,"rep_650"));
    my_map.insert(pair<int, string>(652,"rep_651"));
    my_map.insert(pair<int, string>(653,"rep_652"));
    my_map.insert(pair<int, string>(654,"rep_653"));
    my_map.insert(pair<int, string>(655,"rep_654"));
    my_map.insert(pair<int, string>(656,"rep_655"));
    my_map.insert(pair<int, string>(657,"rep_656"));
    my_map.insert(pair<int, string>(658,"rep_657"));
    my_map.insert(pair<int, string>(659,"rep_658"));
    my_map.insert(pair<int, string>(660,"rep_659"));
    my_map.insert(pair<int, string>(661,"rep_660"));
    my_map.insert(pair<int, string>(662,"rep_661"));
    my_map.insert(pair<int, string>(663,"rep_662"));
    my_map.insert(pair<int, string>(664,"rep_663"));
    my_map.insert(pair<int, string>(665,"rep_664"));
    my_map.insert(pair<int, string>(666,"rep_665"));
    my_map.insert(pair<int, string>(667,"rep_666"));
    my_map.insert(pair<int, string>(668,"rep_667"));
    my_map.insert(pair<int, string>(669,"rep_668"));
    my_map.insert(pair<int, string>(670,"rep_669"));
    my_map.insert(pair<int, string>(671,"rep_670"));
    my_map.insert(pair<int, string>(672,"rep_671"));
    my_map.insert(pair<int, string>(673,"rep_672"));
    my_map.insert(pair<int, string>(674,"rep_673"));
    my_map.insert(pair<int, string>(675,"rep_674"));
    my_map.insert(pair<int, string>(676,"rep_675"));
    my_map.insert(pair<int, string>(677,"rep_676"));
    my_map.insert(pair<int, string>(678,"rep_677"));
    my_map.insert(pair<int, string>(679,"rep_678"));
    my_map.insert(pair<int, string>(680,"rep_679"));
    my_map.insert(pair<int, string>(681,"rep_680"));
    my_map.insert(pair<int, string>(682,"rep_681"));
    my_map.insert(pair<int, string>(683,"rep_682"));
    my_map.insert(pair<int, string>(684,"rep_683"));
    my_map.insert(pair<int, string>(685,"rep_684"));
    my_map.insert(pair<int, string>(686,"rep_685"));
    my_map.insert(pair<int, string>(687,"rep_686"));
    my_map.insert(pair<int, string>(688,"rep_687"));
    my_map.insert(pair<int, string>(689,"rep_688"));
    my_map.insert(pair<int, string>(690,"rep_689"));
    my_map.insert(pair<int, string>(691,"rep_690"));
    my_map.insert(pair<int, string>(692,"rep_691"));
    my_map.insert(pair<int, string>(693,"rep_692"));
    my_map.insert(pair<int, string>(694,"rep_693"));
    my_map.insert(pair<int, string>(695,"rep_694"));
    my_map.insert(pair<int, string>(696,"rep_695"));
    my_map.insert(pair<int, string>(697,"rep_696"));
    my_map.insert(pair<int, string>(698,"rep_697"));
    my_map.insert(pair<int, string>(699,"rep_698"));
    my_map.insert(pair<int, string>(700,"rep_699"));
    my_map.insert(pair<int, string>(701,"rep_700"));
    my_map.insert(pair<int, string>(702,"rep_701"));
    my_map.insert(pair<int, string>(703,"rep_702"));
    my_map.insert(pair<int, string>(704,"rep_703"));
    my_map.insert(pair<int, string>(705,"rep_704"));
    my_map.insert(pair<int, string>(706,"rep_705"));
    my_map.insert(pair<int, string>(707,"rep_706"));
    my_map.insert(pair<int, string>(708,"rep_707"));
    my_map.insert(pair<int, string>(709,"rep_708"));
    my_map.insert(pair<int, string>(710,"rep_709"));
    my_map.insert(pair<int, string>(711,"rep_710"));
    my_map.insert(pair<int, string>(712,"rep_711"));
    my_map.insert(pair<int, string>(713,"rep_712"));
    my_map.insert(pair<int, string>(714,"rep_713"));
    my_map.insert(pair<int, string>(715,"rep_714"));
    my_map.insert(pair<int, string>(716,"rep_715"));
    my_map.insert(pair<int, string>(717,"rep_716"));
    my_map.insert(pair<int, string>(718,"rep_717"));
    my_map.insert(pair<int, string>(719,"rep_718"));
    my_map.insert(pair<int, string>(720,"rep_719"));
    my_map.insert(pair<int, string>(721,"rep_720"));
    my_map.insert(pair<int, string>(722,"rep_721"));
    my_map.insert(pair<int, string>(723,"rep_722"));
    my_map.insert(pair<int, string>(724,"rep_723"));
    my_map.insert(pair<int, string>(725,"rep_724"));
    my_map.insert(pair<int, string>(726,"rep_725"));
    my_map.insert(pair<int, string>(727,"rep_726"));
    my_map.insert(pair<int, string>(728,"rep_727"));
    my_map.insert(pair<int, string>(729,"rep_728"));
    my_map.insert(pair<int, string>(730,"rep_729"));
    my_map.insert(pair<int, string>(731,"rep_730"));
    my_map.insert(pair<int, string>(732,"rep_731"));
    my_map.insert(pair<int, string>(733,"rep_732"));
    my_map.insert(pair<int, string>(734,"rep_733"));
    my_map.insert(pair<int, string>(735,"rep_734"));
    my_map.insert(pair<int, string>(736,"rep_735"));
    my_map.insert(pair<int, string>(737,"rep_736"));
    my_map.insert(pair<int, string>(738,"rep_737"));
    my_map.insert(pair<int, string>(739,"rep_738"));
    my_map.insert(pair<int, string>(740,"rep_739"));
    my_map.insert(pair<int, string>(741,"rep_740"));
    my_map.insert(pair<int, string>(742,"rep_741"));
    my_map.insert(pair<int, string>(743,"rep_742"));
    my_map.insert(pair<int, string>(744,"rep_743"));
    my_map.insert(pair<int, string>(745,"rep_744"));
    my_map.insert(pair<int, string>(746,"rep_745"));
    my_map.insert(pair<int, string>(747,"rep_746"));
    my_map.insert(pair<int, string>(748,"rep_747"));
    my_map.insert(pair<int, string>(749,"rep_748"));
    my_map.insert(pair<int, string>(750,"rep_749"));
    my_map.insert(pair<int, string>(751,"rep_750"));
    my_map.insert(pair<int, string>(752,"rep_751"));
    my_map.insert(pair<int, string>(753,"rep_752"));
    my_map.insert(pair<int, string>(754,"rep_753"));
    my_map.insert(pair<int, string>(755,"rep_754"));
    my_map.insert(pair<int, string>(756,"rep_755"));
    my_map.insert(pair<int, string>(757,"rep_756"));
    my_map.insert(pair<int, string>(758,"rep_757"));
    my_map.insert(pair<int, string>(759,"rep_758"));
    my_map.insert(pair<int, string>(760,"rep_759"));
    my_map.insert(pair<int, string>(761,"rep_760"));
    my_map.insert(pair<int, string>(762,"rep_761"));
    my_map.insert(pair<int, string>(763,"rep_762"));
    my_map.insert(pair<int, string>(764,"rep_763"));
    my_map.insert(pair<int, string>(765,"rep_764"));
    my_map.insert(pair<int, string>(766,"rep_765"));
    my_map.insert(pair<int, string>(767,"rep_766"));
    my_map.insert(pair<int, string>(768,"rep_767"));
    my_map.insert(pair<int, string>(769,"rep_768"));
    my_map.insert(pair<int, string>(770,"rep_769"));
    my_map.insert(pair<int, string>(771,"rep_770"));
    my_map.insert(pair<int, string>(772,"rep_771"));
    my_map.insert(pair<int, string>(773,"rep_772"));
    my_map.insert(pair<int, string>(774,"rep_773"));
    my_map.insert(pair<int, string>(775,"rep_774"));
    my_map.insert(pair<int, string>(776,"rep_775"));
    my_map.insert(pair<int, string>(777,"rep_776"));
    my_map.insert(pair<int, string>(778,"rep_777"));
    my_map.insert(pair<int, string>(779,"rep_778"));
    my_map.insert(pair<int, string>(780,"rep_779"));
    my_map.insert(pair<int, string>(781,"rep_780"));
    my_map.insert(pair<int, string>(782,"rep_781"));
    my_map.insert(pair<int, string>(783,"rep_782"));
    my_map.insert(pair<int, string>(784,"rep_783"));
    my_map.insert(pair<int, string>(785,"rep_784"));
    my_map.insert(pair<int, string>(786,"rep_785"));
    my_map.insert(pair<int, string>(787,"rep_786"));
    my_map.insert(pair<int, string>(788,"rep_787"));
    my_map.insert(pair<int, string>(789,"rep_788"));
    my_map.insert(pair<int, string>(790,"rep_789"));
    my_map.insert(pair<int, string>(791,"rep_790"));
    my_map.insert(pair<int, string>(792,"rep_791"));
    my_map.insert(pair<int, string>(793,"rep_792"));
    my_map.insert(pair<int, string>(794,"rep_793"));
    my_map.insert(pair<int, string>(795,"rep_794"));
    my_map.insert(pair<int, string>(796,"rep_795"));
    my_map.insert(pair<int, string>(797,"rep_796"));
    my_map.insert(pair<int, string>(798,"rep_797"));
    my_map.insert(pair<int, string>(799,"rep_798"));
    my_map.insert(pair<int, string>(800,"rep_799"));
    my_map.insert(pair<int, string>(801,"rep_800"));
    my_map.insert(pair<int, string>(802,"rep_801"));
    my_map.insert(pair<int, string>(803,"rep_802"));
    my_map.insert(pair<int, string>(804,"rep_803"));
    my_map.insert(pair<int, string>(805,"rep_804"));
    my_map.insert(pair<int, string>(806,"rep_805"));
    my_map.insert(pair<int, string>(807,"rep_806"));
    my_map.insert(pair<int, string>(808,"rep_807"));
    my_map.insert(pair<int, string>(809,"rep_808"));
    my_map.insert(pair<int, string>(810,"rep_809"));
    my_map.insert(pair<int, string>(811,"rep_810"));
    my_map.insert(pair<int, string>(812,"rep_811"));
    my_map.insert(pair<int, string>(813,"rep_812"));
    my_map.insert(pair<int, string>(814,"rep_813"));
    my_map.insert(pair<int, string>(815,"rep_814"));
    my_map.insert(pair<int, string>(816,"rep_815"));
    my_map.insert(pair<int, string>(817,"rep_816"));
    my_map.insert(pair<int, string>(818,"rep_817"));
    my_map.insert(pair<int, string>(819,"rep_818"));
    my_map.insert(pair<int, string>(820,"rep_819"));
    my_map.insert(pair<int, string>(821,"rep_820"));
    my_map.insert(pair<int, string>(822,"rep_821"));
    my_map.insert(pair<int, string>(823,"rep_822"));
    my_map.insert(pair<int, string>(824,"rep_823"));
    my_map.insert(pair<int, string>(825,"rep_824"));
    my_map.insert(pair<int, string>(826,"rep_825"));
    my_map.insert(pair<int, string>(827,"rep_826"));
    my_map.insert(pair<int, string>(828,"rep_827"));
    my_map.insert(pair<int, string>(829,"rep_828"));
    my_map.insert(pair<int, string>(830,"rep_829"));
    my_map.insert(pair<int, string>(831,"rep_830"));
    my_map.insert(pair<int, string>(832,"rep_831"));
    my_map.insert(pair<int, string>(833,"rep_832"));
    my_map.insert(pair<int, string>(834,"rep_833"));
    my_map.insert(pair<int, string>(835,"rep_834"));
    my_map.insert(pair<int, string>(836,"rep_835"));
    my_map.insert(pair<int, string>(837,"rep_836"));
    my_map.insert(pair<int, string>(838,"rep_837"));
    my_map.insert(pair<int, string>(839,"rep_838"));
    my_map.insert(pair<int, string>(840,"rep_839"));
    my_map.insert(pair<int, string>(841,"rep_840"));
    my_map.insert(pair<int, string>(842,"rep_841"));
    my_map.insert(pair<int, string>(843,"rep_842"));
    my_map.insert(pair<int, string>(844,"rep_843"));
    my_map.insert(pair<int, string>(845,"rep_844"));
    my_map.insert(pair<int, string>(846,"rep_845"));
    my_map.insert(pair<int, string>(847,"rep_846"));
    my_map.insert(pair<int, string>(848,"rep_847"));
    my_map.insert(pair<int, string>(849,"rep_848"));
    my_map.insert(pair<int, string>(850,"rep_849"));
    my_map.insert(pair<int, string>(851,"rep_850"));
    my_map.insert(pair<int, string>(852,"rep_851"));
    my_map.insert(pair<int, string>(853,"rep_852"));
    my_map.insert(pair<int, string>(854,"rep_853"));
    my_map.insert(pair<int, string>(855,"rep_854"));
    my_map.insert(pair<int, string>(856,"rep_855"));
    my_map.insert(pair<int, string>(857,"rep_856"));
    my_map.insert(pair<int, string>(858,"rep_857"));
    my_map.insert(pair<int, string>(859,"rep_858"));
    my_map.insert(pair<int, string>(860,"rep_859"));
    my_map.insert(pair<int, string>(861,"rep_860"));
    my_map.insert(pair<int, string>(862,"rep_861"));
    my_map.insert(pair<int, string>(863,"rep_862"));
    my_map.insert(pair<int, string>(864,"rep_863"));
    my_map.insert(pair<int, string>(865,"rep_864"));
    my_map.insert(pair<int, string>(866,"rep_865"));
    my_map.insert(pair<int, string>(867,"rep_866"));
    my_map.insert(pair<int, string>(868,"rep_867"));
    my_map.insert(pair<int, string>(869,"rep_868"));
    my_map.insert(pair<int, string>(870,"rep_869"));
    my_map.insert(pair<int, string>(871,"rep_870"));
    my_map.insert(pair<int, string>(872,"rep_871"));
    my_map.insert(pair<int, string>(873,"rep_872"));
    my_map.insert(pair<int, string>(874,"rep_873"));
    my_map.insert(pair<int, string>(875,"rep_874"));
    my_map.insert(pair<int, string>(876,"rep_875"));
    my_map.insert(pair<int, string>(877,"rep_876"));
    my_map.insert(pair<int, string>(878,"rep_877"));
    my_map.insert(pair<int, string>(879,"rep_878"));
    my_map.insert(pair<int, string>(880,"rep_879"));
    my_map.insert(pair<int, string>(881,"rep_880"));
    my_map.insert(pair<int, string>(882,"rep_881"));
    my_map.insert(pair<int, string>(883,"rep_882"));
    my_map.insert(pair<int, string>(884,"rep_883"));
    my_map.insert(pair<int, string>(885,"rep_884"));
    my_map.insert(pair<int, string>(886,"rep_885"));
    my_map.insert(pair<int, string>(887,"rep_886"));
    my_map.insert(pair<int, string>(888,"rep_887"));
    my_map.insert(pair<int, string>(889,"rep_888"));
    my_map.insert(pair<int, string>(890,"rep_889"));
    my_map.insert(pair<int, string>(891,"rep_890"));
    my_map.insert(pair<int, string>(892,"rep_891"));
    my_map.insert(pair<int, string>(893,"rep_892"));
    my_map.insert(pair<int, string>(894,"rep_893"));
    my_map.insert(pair<int, string>(895,"rep_894"));
    my_map.insert(pair<int, string>(896,"rep_895"));
    my_map.insert(pair<int, string>(897,"rep_896"));
    my_map.insert(pair<int, string>(898,"rep_897"));
    my_map.insert(pair<int, string>(899,"rep_898"));
    my_map.insert(pair<int, string>(900,"rep_899"));
    my_map.insert(pair<int, string>(901,"rep_900"));
    my_map.insert(pair<int, string>(902,"rep_901"));
    my_map.insert(pair<int, string>(903,"rep_902"));
    my_map.insert(pair<int, string>(904,"rep_903"));
    my_map.insert(pair<int, string>(905,"rep_904"));
    my_map.insert(pair<int, string>(906,"rep_905"));
    my_map.insert(pair<int, string>(907,"rep_906"));
    my_map.insert(pair<int, string>(908,"rep_907"));
    my_map.insert(pair<int, string>(909,"rep_908"));
    my_map.insert(pair<int, string>(910,"rep_909"));
    my_map.insert(pair<int, string>(911,"rep_910"));
    my_map.insert(pair<int, string>(912,"rep_911"));
    my_map.insert(pair<int, string>(913,"rep_912"));
    my_map.insert(pair<int, string>(914,"rep_913"));
    my_map.insert(pair<int, string>(915,"rep_914"));
    my_map.insert(pair<int, string>(916,"rep_915"));
    my_map.insert(pair<int, string>(917,"rep_916"));
    my_map.insert(pair<int, string>(918,"rep_917"));
    my_map.insert(pair<int, string>(919,"rep_918"));
    my_map.insert(pair<int, string>(920,"rep_919"));
    my_map.insert(pair<int, string>(921,"rep_920"));
    my_map.insert(pair<int, string>(922,"rep_921"));
    my_map.insert(pair<int, string>(923,"rep_922"));
    my_map.insert(pair<int, string>(924,"rep_923"));
    my_map.insert(pair<int, string>(925,"rep_924"));
    my_map.insert(pair<int, string>(926,"rep_925"));
    my_map.insert(pair<int, string>(927,"rep_926"));
    my_map.insert(pair<int, string>(928,"rep_927"));
    my_map.insert(pair<int, string>(929,"rep_928"));
    my_map.insert(pair<int, string>(930,"rep_929"));
    my_map.insert(pair<int, string>(931,"rep_930"));
    my_map.insert(pair<int, string>(932,"rep_931"));
    my_map.insert(pair<int, string>(933,"rep_932"));
    my_map.insert(pair<int, string>(934,"rep_933"));
    my_map.insert(pair<int, string>(935,"rep_934"));
    my_map.insert(pair<int, string>(936,"rep_935"));
    my_map.insert(pair<int, string>(937,"rep_936"));
    my_map.insert(pair<int, string>(938,"rep_937"));
    my_map.insert(pair<int, string>(939,"rep_938"));
    my_map.insert(pair<int, string>(940,"rep_939"));
    my_map.insert(pair<int, string>(941,"rep_940"));
    my_map.insert(pair<int, string>(942,"rep_941"));
    my_map.insert(pair<int, string>(943,"rep_942"));
    my_map.insert(pair<int, string>(944,"rep_943"));
    my_map.insert(pair<int, string>(945,"rep_944"));
    my_map.insert(pair<int, string>(946,"rep_945"));
    my_map.insert(pair<int, string>(947,"rep_946"));
    my_map.insert(pair<int, string>(948,"rep_947"));
    my_map.insert(pair<int, string>(949,"rep_948"));
    my_map.insert(pair<int, string>(950,"rep_949"));
    my_map.insert(pair<int, string>(951,"rep_950"));
    my_map.insert(pair<int, string>(952,"rep_951"));
    my_map.insert(pair<int, string>(953,"rep_952"));
    my_map.insert(pair<int, string>(954,"rep_953"));
    my_map.insert(pair<int, string>(955,"rep_954"));
    my_map.insert(pair<int, string>(956,"rep_955"));
    my_map.insert(pair<int, string>(957,"rep_956"));
    my_map.insert(pair<int, string>(958,"rep_957"));
    my_map.insert(pair<int, string>(959,"rep_958"));
    my_map.insert(pair<int, string>(960,"rep_959"));
    my_map.insert(pair<int, string>(961,"rep_960"));
    my_map.insert(pair<int, string>(962,"rep_961"));
    my_map.insert(pair<int, string>(963,"rep_962"));
    my_map.insert(pair<int, string>(964,"rep_963"));
    my_map.insert(pair<int, string>(965,"rep_964"));
    my_map.insert(pair<int, string>(966,"rep_965"));
    my_map.insert(pair<int, string>(967,"rep_966"));
    my_map.insert(pair<int, string>(968,"rep_967"));
    my_map.insert(pair<int, string>(969,"rep_968"));
    my_map.insert(pair<int, string>(970,"rep_969"));
    my_map.insert(pair<int, string>(971,"rep_970"));
    my_map.insert(pair<int, string>(972,"rep_971"));
    my_map.insert(pair<int, string>(973,"rep_972"));
    my_map.insert(pair<int, string>(974,"rep_973"));
    my_map.insert(pair<int, string>(975,"rep_974"));
    my_map.insert(pair<int, string>(976,"rep_975"));
    my_map.insert(pair<int, string>(977,"rep_976"));
    my_map.insert(pair<int, string>(978,"rep_977"));
    my_map.insert(pair<int, string>(979,"rep_978"));
    my_map.insert(pair<int, string>(980,"rep_979"));
    my_map.insert(pair<int, string>(981,"rep_980"));
    my_map.insert(pair<int, string>(982,"rep_981"));
    my_map.insert(pair<int, string>(983,"rep_982"));
    my_map.insert(pair<int, string>(984,"rep_983"));
    my_map.insert(pair<int, string>(985,"rep_984"));
    my_map.insert(pair<int, string>(986,"rep_985"));
    my_map.insert(pair<int, string>(987,"rep_986"));
    my_map.insert(pair<int, string>(988,"rep_987"));
    my_map.insert(pair<int, string>(989,"rep_988"));
    my_map.insert(pair<int, string>(990,"rep_989"));
    my_map.insert(pair<int, string>(991,"rep_990"));
    my_map.insert(pair<int, string>(992,"rep_991"));
    my_map.insert(pair<int, string>(993,"rep_992"));
    my_map.insert(pair<int, string>(994,"rep_993"));
    my_map.insert(pair<int, string>(995,"rep_994"));
    my_map.insert(pair<int, string>(996,"rep_995"));
    my_map.insert(pair<int, string>(997,"rep_996"));
    my_map.insert(pair<int, string>(998,"rep_997"));
    my_map.insert(pair<int, string>(999,"rep_998"));
    my_map.insert(pair<int, string>(1000,"rep_999"));
    
}

void ReadInSettings(TEnv *settings, TString systName) {

    PATH_1 = settings->GetValue("PATH_1","");
    PATH_2 = settings->GetValue("PATH_2","");

    if (!runDijet) {



        fileName1 = PATH_1;
       // fileName1 += jsyst;
        fileName1 += "/incl/ybin_";
        fileName1 += ybin;
        //fileName1 += "/Udata2015_PythiaMCAllStat_newbin_1000rep_v2.root";
        fileName1 += "/Udata2015_PythiaMC_newbin_withDS_v6.root";


//
        fileName2 = PATH_2;
        fileName2 += jsyst;
        fileName2 += "/incl/ybin_";
        fileName2 += ybin;
        fileName2 += "/Udata2015_PythiaMC_newbin_withDS_v6.root";


    }

    else {


        fileName1 = PATH_1;
      //  fileName1 += jsyst;
        fileName1 += "/mjj/ybin_";
        fileName1 += ybin;
        //fileName1 += "/Udata2015_PythiaMC_newbin_v4.root";
        fileName1 += "/Udata2015_PythiaMC_newbin_withDS_v8.root";

        //fileName1 = "Rebinned_Files_Systematics_toBogdan/JetCleaning_rebin_v3_mjj_ybin5.root";
        
//
        fileName2 = PATH_2;
        //fileName2 += jsyst;
        fileName2 += "/mjj/ybin_";
        fileName2 += ybin;
        fileName2 += "/Udata2015_PythiaMC_newbin_withDS_v6.root";

        fileName2 = "UData_inputs_to_chi2/UData_v7_mjj_ybin5.root";

    }


    if (!runDijet) {
        gBinLimits = vectorizeD(settings->GetValue("gBinLimits_inclusive",""));
        gNBins = settings->GetValue("gNbinsX",0);
        bin_to_merge_a = settings->GetValue("bin_to_merge_a_inclusive",0);
        bin_to_merge_b = settings->GetValue("bin_to_merge_b_inclusive",0);
    }
    else {
        gBinLimits = vectorizeD(settings->GetValue("gBinLimits_dijet",""));
        gNBins = settings->GetValue("gNbinsX",0);
        bin_to_merge_a = settings->GetValue("bin_to_merge_a_dijet",0);
        bin_to_merge_b = settings->GetValue("bin_to_merge_b_dijet",0);
        bin_to_merge_a2 = settings->GetValue("bin_to_merge_a_dijet2",0);
        bin_to_merge_b2 = settings->GetValue("bin_to_merge_b_dijet2",0);
        bin_to_merge_a3 = settings->GetValue("bin_to_merge_a_dijet3",0);
        bin_to_merge_b3 = settings->GetValue("bin_to_merge_b_dijet3",0);
        bin_to_merge_a4 = settings->GetValue("bin_to_merge_a_dijet4",0);
        bin_to_merge_b4 = settings->GetValue("bin_to_merge_b_dijet4",0);
        bin_to_merge_a5 = settings->GetValue("bin_to_merge_a_dijet5",0);
        bin_to_merge_b5 = settings->GetValue("bin_to_merge_b_dijet5",0);
        bin_to_merge_a6 = settings->GetValue("bin_to_merge_a_dijet6",0);
        bin_to_merge_b6 = settings->GetValue("bin_to_merge_b_dijet6",0);
        bin_to_merge_a7 = settings->GetValue("bin_to_merge_a_dijet7",0);
        bin_to_merge_b7 = settings->GetValue("bin_to_merge_b_dijet7",0);

    }

}


void AddError () {


    TFile *ferr = new TFile(fileName1,"read");
   
    TH1F* herr = (TH1F*)ferr->Get("rel_syst_mu_rms");

    gNBins = herr->GetNbinsX();

    TFile *fnom = new TFile(fileName2,"read");

    //cout<<"filename1: "<<fileName1<<endl;
    //cout<<"filename2: "<<fileName2<<endl;

    TString histName = "syst_";
    histName += jsyst;
    
    if (!runDijet) {
        histName += "_incl";
    }
    else {
        histName += "_mjj";
    }

    TH1F* hnom = (TH1F*)fnom->Get(histName);
    

    TH1F *h_syst = new TH1F("h_syst","h_syst",gNBins,&gBinLimits[0]);
    TH1F *h_error = new TH1F("h_error","h_error",gNBins,&gBinLimits[0]);
    
    for (int i = 1; i<=gNBins; i++) {

        float nom = hnom->GetBinContent(i);
        float err = herr->GetBinError(i);

        h_syst->SetBinContent(i,nom);
        h_syst->SetBinError(i,err);
        
        h_error->SetBinContent(i,err);

    }

    TString systName = (TString)my_map.find(jsyst)->second;

    TString output = "systematics_2015Data_newbin_JES_";

    if (systName.Contains("1up")) {
        output += "up/";
    }
    else if (systName.Contains("1down")) {
        output += "down/";
    }

    output += systName;

    if (!runDijet) {
        output += "_incl_ybin";
    }
    else {
        output += "_mjj_ybin";
    }

    output += ybin;
    output += ".root";

    //TFile *f = new TFile("BootstrapErrorSet.root","recreate");
    TFile *f = new TFile(output,"recreate");
    f->cd();

    h_syst->SetName("syst");
    h_error->SetName("error");
    h_syst->Write();
    h_error->Write();
    f->Write();

}

void RebinData () {

    TFile *file = new TFile(fileName1,"read");   
    TFile *file2 = new TFile(fileName2,"read");

    TH1F* udat = (TH1F*)file->Get("UData");
    TH1F* ustat = (TH1F*)file2->Get("Nominal_AvgRep");

    TH1F *newudat = new TH1F("newudat","newudat",gNBins,&gBinLimits[0]);
    TH1F *newustat = new TH1F("newustat","newustat",gNBins,&gBinLimits[0]);


    for (int i = 1; i<=gNBins; i++) {

        float nom = udat->GetBinContent(i);
        float err = ustat->GetBinError(i);
        float err_nom = ustat->GetBinContent(i);

            if (i < bin_to_merge_a) {

                newudat->SetBinContent(i,nom);
                newustat->SetBinContent(i,err_nom);
                newustat->SetBinError(i,err);
            }

            else if (i==bin_to_merge_a) {

                double input_staterr=0;
                double input_statnom=0;
                double input_udata=0;

                for (int l=bin_to_merge_a; l<=bin_to_merge_b; l++) {

                    input_udata += udat->GetBinContent(l);
                    input_statnom += ustat->GetBinContent(l);
                    input_staterr += pow(ustat->GetBinError(l),2);
                }

                input_staterr = TMath::Sqrt(input_staterr);

                newudat->SetBinContent(i,input_udata);
                newustat->SetBinContent(i,input_statnom);
                newustat->SetBinError(i,input_staterr);

            }

            else if (i==bin_to_merge_a2) {

                double input_staterr=0;
                double input_statnom=0;
                double input_udata=0;

                for (int l=bin_to_merge_a2; l<=bin_to_merge_b2; l++) {

                    input_udata += udat->GetBinContent(l);
                    input_statnom += ustat->GetBinContent(l);
                    input_staterr += pow(ustat->GetBinError(l),2);
                }

                input_staterr = TMath::Sqrt(input_staterr);

                newudat->SetBinContent(i,input_udata);
                newustat->SetBinContent(i,input_statnom);
                newustat->SetBinError(i,input_staterr);

            }

            else {

                newudat->SetBinContent(i,nom);
                newustat->SetBinContent(i,err_nom);
                newustat->SetBinError(i,err);
            }

    }   

    TString output = "Rebinned_Files_";
    TString output2 = "Rebinned_Files_";

    
    output += "udata/udata";
    output2 += "stat/stat";

    if (!runDijet) {
        output += "_incl_ybin";
        output2 += "_incl_ybin";
    }
    else {
        output += "_mjj_ybin";
        output2 += "_mjj_ybin";
    }

    output += ybin;
    output2 += ybin;
    output += ".root";
    output2 += ".root";

    //TFile *f = new TFile("BootstrapErrorSet.root","recreate");
    TFile *f = new TFile(output,"update");
    f->cd();

    newudat->SetName("UData");
    newudat->Write();
    f->Write();

    TFile *f2 = new TFile(output2,"update");
    f2->cd();

    newustat->SetName("error");
    newustat->Write();
    f2->Write();


}


void RebinSyst () {

    TFile *file = new TFile(fileName2,"read");

    TH1F* udat = (TH1F*)file->Get("UData");



    TFile *ferr = new TFile(fileName1,"read");

    TString systName = (TString)my_map.find(jsyst)->second;

   
    TH1F* herr = (TH1F*)ferr->Get(systName);


    TH1F *h_JES = new TH1F("h_JES","h_JES",gNBins,&gBinLimits[0]);


    for (int i = 1; i<=gNBins; i++) {

        float jesnom = herr->GetBinContent(i);

      //  cout<<"jesNom: "<<jesnom<<endl;

            if (i < bin_to_merge_a) {

                h_JES->SetBinContent(i,jesnom);
            }

            else if (i==bin_to_merge_a) {

                double input_udata=0;
                double input_jes_up=0;

                for (int l=bin_to_merge_a; l<=bin_to_merge_b; l++) {

                    input_udata += udat->GetBinContent(l);

                 //   cout<<"udat1: "<<input_udata<<" Low Edge: "<<udat->GetBinLowEdge(l)<<endl;

                    input_jes_up += udat->GetBinContent(l)*herr->GetBinContent(l);
                }

                input_jes_up /= input_udata;


                h_JES->SetBinContent(i,input_jes_up);
            }

            else if (i==bin_to_merge_a2-1) {

                double input_udata=0;
                double input_jes_up=0;

                for (int l=bin_to_merge_a2-1; l<=bin_to_merge_b2-1; l++) {

                    input_udata += udat->GetBinContent(l);

//                    cout<<"udata2: "<<input_udata<<" Low Edge: "<<udat->GetBinLowEdge(l)<<endl;

                    input_jes_up += udat->GetBinContent(l)*herr->GetBinContent(l);
                }

                input_jes_up /= input_udata;


                h_JES->SetBinContent(i,input_jes_up);
            }

            else if (i==bin_to_merge_a3-2) {

                double input_udata=0;
                double input_jes_up=0;

                for (int l=bin_to_merge_a3-2; l<=bin_to_merge_b3-2; l++) {

                    input_udata += udat->GetBinContent(l);

                    //cout<<"udata3: "<<input_udata<<" Low Edge: "<<udat->GetBinLowEdge(l)<<endl;

                    input_jes_up += udat->GetBinContent(l)*herr->GetBinContent(l);
                }

                input_jes_up /= input_udata;


                h_JES->SetBinContent(i,input_jes_up);
            }

        
            else if (i==bin_to_merge_a4-3) {

                double input_udata=0;
                double input_jes_up=0;

                for (int l=bin_to_merge_a4-3; l<=bin_to_merge_b4-3; l++) {

                    input_udata += udat->GetBinContent(l);

                    //cout<<"udata3: "<<input_udata<<" Low Edge: "<<udat->GetBinLowEdge(l)<<endl;

                    input_jes_up += udat->GetBinContent(l)*herr->GetBinContent(l);
                }

                input_jes_up /= input_udata;


                h_JES->SetBinContent(i,input_jes_up);
            }

            else if (i==bin_to_merge_a5-4) {

                double input_udata=0;
                double input_jes_up=0;

                for (int l=bin_to_merge_a5-4; l<=bin_to_merge_b5-4; l++) {

                    input_udata += udat->GetBinContent(l);

                    //cout<<"udata3: "<<input_udata<<" Low Edge: "<<udat->GetBinLowEdge(l)<<endl;

                    input_jes_up += udat->GetBinContent(l)*herr->GetBinContent(l);
                }

                input_jes_up /= input_udata;


                h_JES->SetBinContent(i,input_jes_up);
            }

            else if (i==bin_to_merge_a6-5) {

                double input_udata=0;
                double input_jes_up=0;

                for (int l=bin_to_merge_a6-5; l<=bin_to_merge_b6-5; l++) {

                    input_udata += udat->GetBinContent(l);

                    //cout<<"udata3: "<<input_udata<<" Low Edge: "<<udat->GetBinLowEdge(l)<<endl;

                    input_jes_up += udat->GetBinContent(l)*herr->GetBinContent(l);
                }

                input_jes_up /= input_udata;


                h_JES->SetBinContent(i,input_jes_up);
            }

            else if (i==bin_to_merge_a7-6) {

                double input_udata=0;
                double input_jes_up=0;

                for (int l=bin_to_merge_a7-6; l<=bin_to_merge_b7-6; l++) {

                    input_udata += udat->GetBinContent(l);

                    //cout<<"udata3: "<<input_udata<<" Low Edge: "<<udat->GetBinLowEdge(l)<<endl;

                    input_jes_up += udat->GetBinContent(l)*herr->GetBinContent(l);
                }

                input_jes_up /= input_udata;


                h_JES->SetBinContent(i,input_jes_up);
            }

            else {
                h_JES->SetBinContent(i,jesnom);
            }

//    cout<<"JES: "<<endl;
//    cout<<"i" <<i<<"\t"<<"bin: "<<herr->GetBinLowEdge(i)<<"\t"<<"before merging: "<<jesnom<<"\t"<<"after: "<<h_JES->GetBinContent(i)<<endl;
//    cout<<endl;

    }


    TString output = "Rebinned_Files_Systematics_toBogdan/JetCleaning_rebin_v5";

//    if (systName.Contains("1up")) {
//        output += "JES_up/";
//    }
//    else if (systName.Contains("1down")) {
//        output += "JES_down/";
//    }

    
  //  output += "_Systematics/PRW_Systematics";

    if (!runDijet) {
        output += "_incl_ybin";
    }
    else {
        output += "_mjj_ybin";
    }

    output += ybin;
    output += ".root";

    //TFile *f = new TFile("BootstrapErrorSet.root","recreate");
    TFile *f = new TFile(output,"update");
    f->cd();

    h_JES->SetName(systName);
    h_JES->Write();
    f->Write();

}


void SumJER () {


    TFile *ferr_false_up = new TFile(jername1_up,"read");
    TFile *ferr_true_up = new TFile(jername2_up,"read");

    TFile *ferr_false_dn = new TFile(jername1_dn,"read");
    TFile *ferr_true_dn = new TFile(jername2_dn,"read");


    TH1F* herr_false_up = (TH1F*)ferr_false_up->Get("smoothed");
    TH1F* herr_true_up = (TH1F*)ferr_true_up->Get("smoothed");

    TH1F* herr_false_dn = (TH1F*)ferr_false_dn->Get("smoothed");
    TH1F* herr_true_dn = (TH1F*)ferr_true_dn->Get("smoothed");


    gNBins = herr_false_up->GetNbinsX();

    TH1F *h_total_up = new TH1F("h_total_up","h_total_JER_up",gNBins,&gBinLimits[0]);
    TH1F *h_total_dn = new TH1F("h_total_dn","h_total_JER_dn",gNBins,&gBinLimits[0]);


    for (int i = 1; i<=gNBins; i++) {

        float nom1 = herr_false_up->GetBinContent(i);
        float nom2 = herr_true_up->GetBinContent(i);

        float nom3 = herr_false_dn->GetBinContent(i);
        float nom4 = herr_true_dn->GetBinContent(i);

        float xup = nom1 + nom2;
        float xdn = nom3 + nom4;

     //   cout<<"jsyst: "<<jsyst<<"\t"<<xup<<endl;
     //   cout<<"jsyst: "<<jsyst<<"\t"<<xdn<<endl;

        if (jsyst==154 || jsyst==155) {
            xdn = (-1)*(nom3+nom4);
        }

        h_total_up->SetBinContent(i,xup);
        h_total_dn->SetBinContent(i,xdn);

    }

    TString systName = (TString)my_map.find(jsyst)->second;

    TString output = "systematics_2015Data_newbin_JES_up/JES_and_JER_Systematics";

    if (!runDijet) {
        output += "_incl_ybin";
    }
    else {
        output += "_mjj_ybin";
    }

    output += ybin;
    output += ".root";

    TFile *f = new TFile(output,"update");
    f->cd();

    TString name_up = systName;
    name_up +="_up";
    TString name_dn = systName;
    name_dn +="_down";


    h_total_up->SetName(name_up);
    h_total_dn->SetName(name_dn);
    h_total_up->Write();
    h_total_dn->Write();
    f->Write();

}

void LumiUnc() {

    //TString systName = (TString)my_map.find(jsyst)->second;

//    TString input = "systematic_2015Data_newbin_JERFull_10Smear_";

//    TString input = "JetCleaningUnc_inputs_to_chi2/JetCleaning";
    TString output = "LumiUnc_inputs_to_chi2/LumiUnc";

    if (!runDijet) {
//        input += "_incl_ybin";
        output += "_incl_ybin";
    }
    else {
//        input += "_mjj_ybin";
        output += "_mjj_ybin";
    }

//    input += ybin;
//    input += ".root";
    output += ybin;
    output += ".root";



//    TFile *fin = new TFile(input,"open");
    TFile *fout = new TFile(output,"update");


//    TH1F* h = (TH1F*)fin->Get("JetCleaningUnc");


    TH1F *h_out = new TH1F("h_out","h_out",gNBins,&gBinLimits[0]);


    for (int i = 1; i<=gNBins; i++) {

//        float num = h->GetBinContent(i);

        h_out->SetBinContent(i,0.021);

    }


    h_out->SetName("LumiUnc");

    fout->cd();

    //h_out->Write();

    fout->Write();

}

void StartBin() {

    TString systName = (TString)my_map.find(jsyst)->second;

    TString input = fileName1;

    TString output = "StatUnc_inputs_to_chi2/UData_v9";

    if (!runDijet) {
       // input += "_incl_ybin";
        output += "_incl_ybin";
    }
    else {
       // input += "_mjj_ybin";
        output += "_mjj_ybin";
    }

  //  input += ybin;
  //  input += ".root";
    output += ybin;
    output += ".root";



    TFile *fin = new TFile(input,"open");
    TFile *fout = new TFile(output,"update");


    TH1F* h = (TH1F*)fin->Get(systName);
    //TH1F* h = (TH1F*)fin->Get("smoothed_2sig");


    TH1F *h_out = new TH1F("h_out","h_out",gNBins,&gBinLimits[0]);


    for (int i = 1; i<=gNBins; i++) {

        float num = h->GetBinContent(h->FindBin(h_out->GetBinCenter(i)));

        float err=0;

        if (jsyst==0) {
            err = h->GetBinError(h->FindBin(h_out->GetBinCenter(i)));
        }


        //std::cout<<"pt: "<<h_out->GetBinLowEdge(i)<<"\t"<<"num: "<<num<<std::endl;

        h_out->SetBinContent(i,num);
        h_out->SetBinError(i,0);

        if (jsyst==0) {
            h_out->SetBinError(i,err);
        }

    }


    h_out->SetName(systName);
    //h_out->SetName("smoothed_2sig");

    fout->cd();

    //h_out->Write();

    fout->Write();

}


void StartBin_and_SymJER_Dijets() {

    TString systName = (TString)my_map.find(jsyst)->second;

    TString input = "systematic_2015Data_newbin_JERFull_10Smear_";
    TString input2 = "systematics_2015Data_newbin_Scalex10JER_Total_";

    if (systName.Contains("1up")) {
        //  output += "up/";
        input += "up/";
        input2 += "up/";
    }
    else if (systName.Contains("1down")) {
        //   output += "down/";
        input += "down/";
        input2 += "down/";
    }


    TString output = "systematics_2015Data_newbin_JER_inputs_to_chi2/JER_Total";

    input += systName;
    input2 += systName;

    if (!runDijet) {
        input += "_incl_ybin";
        input2 += "_incl_ybin";
        output += "_incl_ybin";
    }
    else {
        input += "_mjj_ybin";
        input2 += "_mjj_ybin";
        output += "_mjj_ybin";
    }

    input += ybin;
    input += ".root";
    input2 += ybin;
    input2 += ".root";
    output += ybin;
    output += ".root";


    if (ybin==0 && (systName.Contains("NP1") || systName.Contains("NP2") || systName.Contains("NP3") || systName.Contains("NP4") || systName.Contains("NP6") || systName.Contains("NP8"))) {
        input = input2;
    }


    if (ybin==1 && (systName.Contains("NP3") || systName.Contains("NP4") || systName.Contains("NP5") || systName.Contains("NP6") || systName.Contains("NP8"))) {
        input = input2;
    }


    if (ybin==2 && (systName.Contains("NP1") || systName.Contains("NP3"))) {
        input = input2;
    }


    if (ybin==3 && (systName.Contains("NP1") || systName.Contains("NP2") || systName.Contains("NP3"))) {
        input = input2;
    }


    if (ybin==4 && (systName.Contains("CROSS_CALIB") || systName.Contains("NP0") || systName.Contains("NP1") || systName.Contains("NP2") || systName.Contains("NP3") || systName.Contains("NP6")) || systName.Contains("NP8")) {
        input = input2;
    }


    if (ybin==5 && (systName.Contains("CROSS_CALIB") || systName.Contains("NOISE") || systName.Contains("NP0") ||  systName.Contains("NP1"))) {
        input = input2;
    }


    TFile *fin = new TFile(input,"open");
    TFile *fout = new TFile(output,"update");


    TH1F* h = (TH1F*)fin->Get("smoothed");


    TH1F *h_out = new TH1F("h_out","h_out",gNBins,&gBinLimits[0]);
    TH1F *h_out2 = new TH1F("h_out2","h_out2",gNBins,&gBinLimits[0]);


    if (!(systName.Contains("JET_JER_NOISE_FORWARD") || systName.Contains("JET_JER_CROSS_CALIB_FORWARD"))){

        h_out2->Delete();
    }


    for (int i = 1; i<=gNBins; i++) {

        float num = h->GetBinContent(h->FindBin(h_out->GetBinCenter(i)));


        if (systName.Contains("JET_JER_NOISE_FORWARD") || systName.Contains("JET_JER_CROSS_CALIB_FORWARD")){
                h_out->SetBinContent(i,num);
                h_out2->SetBinContent(i,(-1)*num);
        }
        else {
            h_out->SetBinContent(i,num);
        }


    }


    if (systName.Contains("JET_JER_NOISE_FORWARD")) {
        h_out->SetName(systName);
        h_out2->SetName("JET_JER_NOISE_FORWARD__1down");
    }
    else if (systName.Contains("JET_JER_CROSS_CALIB_FORWARD")) {
        h_out->SetName(systName);
        h_out2->SetName("JET_JER_CROSS_CALIB_FORWARD__1down");
    }
    else {
        h_out->SetName(systName);
    }



    fout->cd();

    //h_out->Write();

    fout->Write();

}




void StartBin_and_SymJER() {

    TString systName = (TString)my_map.find(jsyst)->second;

    TString input = "systematic_2015Data_newbin_JERFull_10Smear_";

    if (systName.Contains("1up")) {
        //  output += "up/";
        input += "up/";
    }
    else if (systName.Contains("1down")) {
        //   output += "down/";
        input += "down/";
    }


    TString output = "systematics_2015Data_newbin_JER_inputs_to_chi2/JER_Total";

    input += systName;

    if (!runDijet) {
        input += "_incl_ybin";
        output += "_incl_ybin";
    }
    else {
        input += "_mjj_ybin";
        output += "_mjj_ybin";
    }

    input += ybin;
    input += ".root";
    output += ybin;
    output += ".root";



    TFile *fin = new TFile(input,"open");
    TFile *fout = new TFile(output,"update");


    TH1F* h = (TH1F*)fin->Get("smoothed");


    TH1F *h_out = new TH1F("h_out","h_out",gNBins,&gBinLimits[0]);
    TH1F *h_out2 = new TH1F("h_out2","h_out2",gNBins,&gBinLimits[0]);


    if (!(systName.Contains("JET_JER_NOISE_FORWARD") || systName.Contains("JET_JER_CROSS_CALIB_FORWARD"))){

        h_out2->Delete();
    }


    for (int i = 1; i<=gNBins; i++) {

        float num = h->GetBinContent(h->FindBin(h_out->GetBinCenter(i)));


        if (systName.Contains("JET_JER_NOISE_FORWARD") || systName.Contains("JET_JER_CROSS_CALIB_FORWARD")){
                h_out->SetBinContent(i,num);
                h_out2->SetBinContent(i,(-1)*num);
        }
        else {
            h_out->SetBinContent(i,num);
        }


    }


    if (systName.Contains("JET_JER_NOISE_FORWARD")) {
        h_out->SetName(systName);
        h_out2->SetName("JET_JER_NOISE_FORWARD__1down");
    }
    else if (systName.Contains("JET_JER_CROSS_CALIB_FORWARD")) {
        h_out->SetName(systName);
        h_out2->SetName("JET_JER_CROSS_CALIB_FORWARD__1down");
    }
    else {
        h_out->SetName(systName);
    }



    fout->cd();

    //h_out->Write();

    fout->Write();

}


void Sym_biasIDS () {

    //TString systName = (TString)my_map.find(jsyst)->second;

    TString input = "PRW_inputs_to_chi2/PRW_v4";
    TString output = "PRW_inputs_to_chi2_toBogdan/PRW";
    
   // input += systName;

    if (!runDijet) {
        input += "_incl_ybin";
        output += "_incl_ybin";
    }
    else {
        input += "_mjj_ybin";
        output += "_mjj_ybin";
    }

    input += ybin;
    input += ".root";
    output += ybin;
    output += ".root";



    TFile *fin = new TFile(input,"open");
    TFile *fout = new TFile(output,"recreate");

    TH1F* h = (TH1F*)fin->Get("smoothed_2sig");

    gNBins = h->GetNbinsX();
    
    
    TH1F *h_out = new TH1F("h_out","h_out",gNBins,&gBinLimits[0]);


    for (int i = 1; i<=gNBins; i++) {

        float num = (-1)*h->GetBinContent(i);

        h_out->SetBinContent(i,num);

    }


    h_out->SetName("PRW_up");
    h->SetName("PRW_down");

    fout->cd();

    //h_out->Write();
    h->Write();

    fout->Write();

}

void FlipSign_and_StartBin_highystar () {

    TString systName = (TString)my_map.find(jsyst)->second;

    TString input = "systematics_Scalex10JES_v1_";
    
    TString input2 = "systematics_2015Data_newbin_JES_";

    if (systName.Contains("1up")) {
      //  output += "up/";
        input += "up/";
        input2 += "up/";
    }
    else if (systName.Contains("1down")) {
     //   output += "down/";
        input += "down/";
        input2 += "down/";
    }

    input += systName;
    input2 += systName;



    if (!runDijet) {
        input += "_incl_ybin";
        input2 += "_incl_ybin";
    }
    else {
        input += "_mjj_ybin";
        input2 += "_mjj_ybin";
    }

    input += ybin;
    input += ".root";
    input2 += ybin;
    input2 += ".root";



    if (jsyst>20) {

        input = input2;
    }


    TFile *fin = new TFile(input,"update");

    TH1F* h = (TH1F*)fin->Get("smoothed");

    
    TH1F *h_out = new TH1F("h_out","h_out",gNBins,&gBinLimits[0]);


    for (int i = 1; i<=gNBins; i++) {

        float num = (-1)*h->GetBinContent(h->FindBin(h_out->GetBinCenter(i)));

        h_out->SetBinContent(i,num);

    }


    h_out->SetName("smoothed_flip");

    fin->cd();

    h_out->Write();

    fin->Write();

}

void FlipSign_and_StartBin () {

    TString systName = (TString)my_map.find(jsyst)->second;

    TString input = "systematics_2015Data_newbin_EICF_1Sig_";
    
    TString input2 = "systematics_2015Data_newbin_JES_";

    if (systName.Contains("1up")) {
      //  output += "up/";
        input += "up/";
        input2 += "up/";
    }
    else if (systName.Contains("1down")) {
     //   output += "down/";
        input += "down/";
        input2 += "down/";
    }

    input += systName;
    input2 += systName;

    if (systName.Contains("1down") && (jsyst>=330 && jsyst <=379)) {
        input += "_v2";
    }


    if (!runDijet) {
        input += "_incl_ybin";
        input2 += "_incl_ybin";
    }
    else {
        input += "_mjj_ybin";
        input2 += "_mjj_ybin";
    }

    input += ybin;
    input += ".root";
    input2 += ybin;
    input2 += ".root";



    if (jsyst>=1112) {

        input = input2;
    }


    TFile *fin = new TFile(input2,"update");

    TH1F* h = (TH1F*)fin->Get("smoothed");

    
    TH1F *h_out = new TH1F("h_out","h_out",gNBins,&gBinLimits[0]);


    for (int i = 1; i<=gNBins; i++) {

        float num = (-1)*h->GetBinContent(h->FindBin(h_out->GetBinCenter(i)));

        h_out->SetBinContent(i,num);

    }


    h_out->SetName("smoothed_flip");

    fin->cd();

    h_out->Write();

    fin->Write();

}



void JES_mean () {

    TString systName = (TString)my_map.find(jsyst)->second;

    TString input = "systematics_2015Data_newbin_JES_Total_inputs_to_chi2_mjj/JES_Systematics_mjj_ybin5.root";
    
    TString output = "systematics_2015Data_newbin_JES_mean_Total_inputs_to_chi2/JES_Systematics_v2_mjj_ybin5.root";

//    if (systName.Contains("1up")) {
      //  output += "up/";
//        input += "up/";
//        input2 += "up/";
//    }
//    else if (systName.Contains("1down")) {
     //   output += "down/";
//        input += "down/";
//        input2 += "down/";
//    }

//    input += systName;
//    input2 += systName;
//
//    if (systName.Contains("1down") && (jsyst>=330 && jsyst <=379)) {
//        input += "_v2";
//    }
//
//
//    if (!runDijet) {
//        input += "_incl_ybin";
//        input2 += "_incl_ybin";
//    }
//    else {
//        input += "_mjj_ybin";
//        input2 += "_mjj_ybin";
//    }
//
//    input += ybin;
//    input += ".root";
//    input2 += ybin;
//    input2 += ".root";



//    if (jsyst>=1112) {
//
//        input = input2;
//    }


    TString systName_up = systName;
    systName_up +="1up";

    TString systName_dn = systName;
    systName_dn += "1down";


    TFile *fin = new TFile(input,"open");
    TFile *fou = new TFile(output,"update");

    TH1F* hup = (TH1F*)fin->Get(systName_up);
    TH1F* hdn = (TH1F*)fin->Get(systName_dn);

    
    TH1F *h_out_up = new TH1F("h_out_up","h_out_up",gNBins,&gBinLimits[0]);
    TH1F *h_out_dn = new TH1F("h_out_dn","h_out_dn",gNBins,&gBinLimits[0]);


    for (int i = 1; i<=gNBins; i++) {


        if (i<gNBins) {

            h_out_up->SetBinContent(i,hup->GetBinContent(i));
            h_out_dn->SetBinContent(i,hdn->GetBinContent(i));
        }

        else if (i==gNBins) {

            float num_up = hup->GetBinContent(i);
            float num_dn = hdn->GetBinContent(i);
            float mean = (fabs(num_up) + fabs(num_dn))/2;


            h_out_up->SetBinContent(i,(-1)*hdn->GetBinContent(i));
            h_out_dn->SetBinContent(i,hdn->GetBinContent(i));

  //          else if (num_up <0) {
  //              h_out_up->SetBinContent(i,(-1)*mean);
  //          }
  //          else if (num_up==0) {
  //              h_out_up->SetBinContent(i,0);
  //          }


  //          if (num_dn >0) {
  //              h_out_dn->SetBinContent(i,mean);
  //          }
  //          else if (num_dn <0) {
  //            h_out_dn->SetBinContent(i,(-1)*mean);
  //          }
  //          else if (num_dn==0) {
  //              h_out_dn->SetBinContent(i,0);
  //          }
        }

    }


    h_out_up->SetName(systName_up);
    h_out_dn->SetName(systName_dn);

    fou->cd();

    h_out_up->Write();
    h_out_dn->Write();

//    fou->Write();

}

void ScaleHist () {

    TString systName = (TString)my_map.find(jsyst)->second;

    TString output = "JESUncertainty_AllComponents_Moriond2016_Scaledx20.root";
   
    TString input = "/afs/cern.ch/user/g/gmarceca/WORK/Analysis_SM/xAH_v1/JetUncertainties/share/JES_2015/Moriond2016/JESUncertainty_AllComponents_Moriond2016_cp.root";


    TFile *fin = new TFile(input,"open");

    
    if (jsyst >6) {
        TH1D* h1 = (TH1D*)fin->Get(systName);
        h1->Scale(20);

        TFile *fou = new TFile(output,"update");
        fou->cd();

        h1->SetName(systName);
        h1->Write();

        fou->Write();

    }
    else if (jsyst<=6) {
        TH2D* h2 = (TH2D*)fin->Get(systName);
        h2->Scale(20);

        TFile *fou = new TFile(output,"update");
        fou->cd();

        h2->SetName(systName);
        h2->Write();

        fou->Write();
    }
    
}


void SaveInFile_toBogdan () {

    TString systName = (TString)my_map.find(jsyst)->second;

    TString output = "systematics_2015Data_newbin_JES_Total_inputs_to_chi2_mjj/";
   
    TString input = "systematics_Scalex10JES_v1_";
    TString input2 = "systematics_2015Data_newbin_JES_";
    TString input3 = "systematics_2015Data_newbin_EICF_1Sig_";

    if (systName.Contains("1up")) {
      //  output += "up/";
        input += "up/";
        input2 += "up/";
        input3 += "up/";
    }
    else if (systName.Contains("1down")) {
     //   output += "down/";
        input += "down/";
        input2 += "down/";
        input3 += "down/";
    }


    input += systName;
    input2 += systName;
    input3 += systName;


    if (systName.Contains("1down") && (jsyst>=330 && jsyst <=379)) {
        input3 += "_v2";
    }

    output += "JES_Systematics";

    if (!runDijet) {
        output += "_incl_ybin";
        input += "_incl_ybin";
        input2 += "_incl_ybin";
        input3 += "_incl_ybin";
    }
    else {
        output += "_mjj_ybin";
        input += "_mjj_ybin";
        input2 += "_mjj_ybin";
        input3 += "_mjj_ybin";
    }


    output += ybin;
    output += ".root";
    input += ybin;
    input += ".root";
    input2 += ybin;
    input2 += ".root";
    input3 += ybin;
    input3 += ".root";




   if (jsyst==0 || jsyst==246 || (jsyst>=1112 && jsyst<=1125) || jsyst==1156 || jsyst==1157 || jsyst==1160 || jsyst==1161) {

       input2 = input;
   }

   if ((jsyst>=1 && jsyst <=245) || (jsyst>=247 && jsyst<=493)) {
       input2 = input3;
   }


    TFile *fin = new TFile(input2,"update");

    TH1F* h = (TH1F*)fin->Get("smoothed_flip");
   
    if (jsyst==0) {

        h->SetName("JET_EtaIntercalibration_Stat0__1up");
    }

    else if (jsyst==246) {

        h->SetName("JET_EtaIntercalibration_Stat0__1down");
    }

    else {
        h->SetName(systName);
    }


    if ((jsyst>=1 && jsyst<=245) || (jsyst>=247 && jsyst<=493)) {

        for (int i = 1; i<=gNBins; i++) {

            h->SetBinContent(i,0);
        }
    }


    TFile *fou = new TFile(output,"update");
    fou->cd();

    h->Write();

    fou->Write();

}

void SaveInFile_highystar () {

    TString systName = (TString)my_map.find(jsyst)->second;

    TString output = "systematics_2015Data_newbin_JES_Total_inputs_to_chi2_Scalex10/";
   
    TString input = "systematics_Scalex10JES_v1_";
    TString input2 = "systematics_2015Data_newbin_JES_";

    if (systName.Contains("1up")) {
      //  output += "up/";
        input += "up/";
        input2 += "up/";
    }
    else if (systName.Contains("1down")) {
     //   output += "down/";
        input += "down/";
        input2 += "down/";
    }


    input += systName;
    input2 += systName;


    output += "JES_Systematics";

    if (!runDijet) {
        output += "_incl_ybin";
        input += "_incl_ybin";
        input2 += "_incl_ybin";
    }
    else {
        output += "_mjj_ybin";
        input += "_mjj_ybin";
        input2 += "_mjj_ybin";
    }

    output += ybin;
    output += ".root";
    input += ybin;
    input += ".root";
    input2 += ybin;
    input2 += ".root";



   if (jsyst>20) {

       input = input2;
   }

    TFile *fin = new TFile(input,"update");

    TH1F* h = (TH1F*)fin->Get("smoothed_flip");
    
    h->SetName(systName);

    TFile *fou = new TFile(output,"update");
    fou->cd();

    h->Write();

    fou->Write();

}


void SaveInFile () {

    TString systName = (TString)my_map.find(jsyst)->second;

    TString output = "systematics_2015Data_newbin_JES_Total_inputs_to_chi2_v2/";
   
    TString input = "systematics_2015Data_newbin_EICF_1Sig_";
    TString input2 = "systematics_2015Data_newbin_JES_";

    if (systName.Contains("1up")) {
      //  output += "up/";
        input += "up/";
        input2 += "up/";
    }
    else if (systName.Contains("1down")) {
     //   output += "down/";
        input += "down/";
        input2 += "down/";
    }


    input += systName;
    input2 += systName;

    if (systName.Contains("1down") && (jsyst>=330 && jsyst <=379)) {
        input += "_v2";
    }

    output += "JES_Systematics";

    if (!runDijet) {
        output += "_incl_ybin";
        input += "_incl_ybin";
        input2 += "_incl_ybin";
    }
    else {
        output += "_mjj_ybin";
        input += "_mjj_ybin";
        input2 += "_mjj_ybin";
    }

    output += ybin;
    output += ".root";
    input += ybin;
    input += ".root";
    input2 += ybin;
    input2 += ".root";



   if (jsyst>=1112) {

       input = input2;
   }

    TFile *fin = new TFile(input,"update");

    TH1F* h = (TH1F*)fin->Get("smoothed_flip");
    
    h->SetName(systName);

    TFile *fou = new TFile(output,"update");
    fou->cd();

    h->Write();

    fou->Write();

}

int SumIn2_JES () {


    TString systName = (TString)my_map.find(jsyst)->second;

    TString input = "systematics_2015Data_newbin_EICF_1Sig_";
    
    TString output = "systematics_2015Data_newbin_EICF_1Sig_";

    if (systName.Contains("1up")) {
      //  output += "up/";
        input += "up/";
    }
    else if (systName.Contains("1down")) {
     //   output += "down/";
        input += "down/";
    }

  output += "up/";
  output += "EICF_Uncertainty_Total_v3_NoSmooth_NoMJB_";
  
  input += systName;

    if (systName.Contains("1down") && (jsyst>=330 && jsyst <=379)) {
        input += "_v2";
    }


    if (!runDijet) {
        output += "_incl_ybin";
        input += "_incl_ybin";
    }
    else {
        output += "_mjj_ybin";
        input += "_mjj_ybin";
    }

    output += ybin;
    output += ".root";
    input += ybin;
    input += ".root";



    TFile *fin = new TFile(input,"open");
    TFile *fout = new TFile(output,"update");
  
    TH1F* h = (TH1F*)fin->Get("syst");

    gNBins = h->GetNbinsX();

    if (jsyst==0) {

        TH1F *h_base_p = new TH1F("h_base_p","h_base_p",gNBins,&gBinLimits[0]);
        TH1F *h_base_n = new TH1F("h_base_n","h_base_n",gNBins,&gBinLimits[0]);


        for (int i = 1; i<=gNBins; i++) {

      //      std::cout<<"starting systName: "<<"\t"<<systName<<"\t"<<"h_content: "<<h->GetBinLowEdge(i)<<std::endl;

            if (systName.Contains("1down")) 
                h_base_p->SetBinContent(i,h->GetBinContent(i));
            else if (systName.Contains("1up"))
                h_base_n->SetBinContent(i,h->GetBinContent(i)); 

        //    std::cout<<"starting systName: "<<"\t"<<systName<<"\t"<<"h_base: "<<h_base_n->GetBinLowEdge(i)<<std::endl;

        }

        fout->cd();

        h_base_p->Write();
        h_base_n->Write();
        fout->Write();

        return 0;
    }

    
    TH1F* h_tot_p = (TH1F*)fout->Get("h_base_p");
    TH1F* h_tot_n = (TH1F*)fout->Get("h_base_n");


 //   std::cout<<"systName: "<<systName<<endl;

    for (int i = 1; i<=gNBins; i++) {
        
        float num2_p = pow(h_tot_p->GetBinContent(i),2) + pow(h->GetBinContent(i),2);
        float num2_n = pow(h_tot_n->GetBinContent(i),2) + pow(h->GetBinContent(i),2);

        float num_p = sqrt(num2_p);
        float num_n = (-1)*sqrt(num2_n);

      //  std::cout<<"h_tot_p: "<<h_tot_p->GetBinLowEdge(i)<<"\t"<<"h_tot_n: "<<h_tot_n->GetBinContent(i)<<"\t"<<"h: "<<h->GetBinLowEdge(i)<<std::endl;

        if (systName.Contains("1down"))
            h_tot_p->SetBinContent(i,num_p);
        else if (systName.Contains("1up"))
            h_tot_n->SetBinContent(i,num_n);

    }

    if (jsyst==491) {

        h_tot_p->SetName("total_down");
        h_tot_n->SetName("total_up");

        fout->cd();

        h_tot_p->Write();
        h_tot_n->Write();

        return 2;


    }

    else {


        h_tot_p->SetName("h_base_p");
        h_tot_n->SetName("h_base_n");

        fout->cd();

        h_tot_p->Write("",TObject::kOverwrite);
        h_tot_n->Write("",TObject::kOverwrite);

        //    fout->Write();

        return 1;
    }

}


int SumIn2_biasIDS_toEsteban () {


    TString systName = (TString)my_map.find(jsyst)->second;

    TString input = "Rebinned_Files_Systematics_toBogdan/PRW_Systematics_v5";

    if (!runDijet) {
        input += "_incl_ybin";
    }
    else {
        input += "_mjj_ybin";
    }

    input += ybin;
    input += ".root";

    TFile *fin = new TFile(input,"open");

    
    TString output = "Rebinned_Files_Systematics_toEsteban/PRW_rebin_v5";
    TString output2 = "Rebinned_Files_Systematics_toEsteban/PRW_v5";


    if (!runDijet) {
        output += "_incl_ybin";
        output2 += "_incl_ybin";
    }
    else {
        output += "_mjj_ybin";
        output2 += "_mjj_ybin";
    }

    output += ybin;
    output += ".root";
    output2 += ybin;
    output2 += ".root";


    TFile *fout = new TFile(output,"update");
  
    TH1F* h = (TH1F*)fin->Get(systName);


    if (jsyst==0) {

        TH1F *h_base_p = new TH1F("h_base_p","h_base_p",gNBins,&gBinLimits[0]);
        TH1F *h_base_n = new TH1F("h_base_n","h_base_n",gNBins,&gBinLimits[0]);


        for (int i = 1; i<=gNBins; i++) {

      //      std::cout<<"starting systName: "<<"\t"<<systName<<"\t"<<"h_content: "<<h->GetBinLowEdge(i)<<std::endl;

            if (h->GetBinContent(i)>0) 
                h_base_p->SetBinContent(i,h->GetBinContent(i));
            else if (h->GetBinContent(i)<0)
                h_base_n->SetBinContent(i,h->GetBinContent(i)); 

        //    std::cout<<"starting systName: "<<"\t"<<systName<<"\t"<<"h_base: "<<h_base_n->GetBinLowEdge(i)<<std::endl;

        }

        fout->cd();

        h_base_p->Write();
        h_base_n->Write();
        fout->Write();

        return 0;
    }

    
    TH1F* h_tot_p = (TH1F*)fout->Get("h_base_p");
    TH1F* h_tot_n = (TH1F*)fout->Get("h_base_n");


 //   std::cout<<"systName: "<<systName<<endl;

    for (int i = 1; i<=gNBins; i++) {
        
        float num2_p = pow(h_tot_p->GetBinContent(i),2) + pow(h->GetBinContent(i),2);
        float num2_n = pow(h_tot_n->GetBinContent(i),2) + pow(h->GetBinContent(i),2);

        float num_p = sqrt(num2_p);
        float num_n = (-1)*sqrt(num2_n);

      //  std::cout<<"h_tot_p: "<<h_tot_p->GetBinLowEdge(i)<<"\t"<<"h_tot_n: "<<h_tot_n->GetBinContent(i)<<"\t"<<"h: "<<h->GetBinLowEdge(i)<<std::endl;

        if (h->GetBinContent(i)>0)
            h_tot_p->SetBinContent(i,num_p);
        else if (h->GetBinContent(i)<0)
            h_tot_n->SetBinContent(i,num_n);

    }

    if (jsyst==1) {

        h_tot_p->SetName("total_pos");
        h_tot_n->SetName("total_neg");

        TFile *fout2 = new TFile(output2,"recreate");

        fout2->cd();

        h_tot_p->Write();
        h_tot_n->Write();

        return 2;


    }

    else {


        h_tot_p->SetName("h_base_p");
        h_tot_n->SetName("h_base_n");

        fout->cd();

        h_tot_p->Write("",TObject::kOverwrite);
        h_tot_n->Write("",TObject::kOverwrite);

        //    fout->Write();

        return 1;
    }

}




int SumIn2_JER_toEsteban () {


    TString systName = (TString)my_map.find(jsyst)->second;

    TString input = "Rebinned_Files_Systematics_toBogdan/JER_Total_rebin_v5";

    if (!runDijet) {
        input += "_incl_ybin";
    }
    else {
        input += "_mjj_ybin";
    }

    input += ybin;
    input += ".root";

    TFile *fin = new TFile(input,"open");

    
    TString output = "Rebinned_Files_Systematics_toEsteban/JER_Systematics_v5";
    TString output2 = "Rebinned_Files_Systematics_toEsteban/JER_Systematics_SumIn2_v5";


    if (!runDijet) {
        output += "_incl_ybin";
        output2 += "_incl_ybin";
    }
    else {
        output += "_mjj_ybin";
        output2 += "_mjj_ybin";
    }

    output += ybin;
    output += ".root";
    output2 += ybin;
    output2 += ".root";



    TFile *fout = new TFile(output,"update");
  
    TH1F* h = (TH1F*)fin->Get(systName);


    if (jsyst==1) {

        TH1F *h_base_p = new TH1F("h_base_p","h_base_p",gNBins,&gBinLimits[0]);
        TH1F *h_base_n = new TH1F("h_base_n","h_base_n",gNBins,&gBinLimits[0]);


        for (int i = 1; i<=gNBins; i++) {

      //      std::cout<<"starting systName: "<<"\t"<<systName<<"\t"<<"h_content: "<<h->GetBinLowEdge(i)<<std::endl;

            if (h->GetBinContent(i)>0) 
                h_base_p->SetBinContent(i,h->GetBinContent(i));
            else if (h->GetBinContent(i)<0)
                h_base_n->SetBinContent(i,h->GetBinContent(i)); 

        //    std::cout<<"starting systName: "<<"\t"<<systName<<"\t"<<"h_base: "<<h_base_n->GetBinLowEdge(i)<<std::endl;

        }

        fout->cd();

        h_base_p->Write();
        h_base_n->Write();
        fout->Write();

        return 0;
    }

    
    TH1F* h_tot_p = (TH1F*)fout->Get("h_base_p");
    TH1F* h_tot_n = (TH1F*)fout->Get("h_base_n");


 //   std::cout<<"systName: "<<systName<<endl;

    for (int i = 1; i<=gNBins; i++) {
        
        float num2_p = pow(h_tot_p->GetBinContent(i),2) + pow(h->GetBinContent(i),2);
        float num2_n = pow(h_tot_n->GetBinContent(i),2) + pow(h->GetBinContent(i),2);

        float num_p = sqrt(num2_p);
        float num_n = (-1)*sqrt(num2_n);

      //  std::cout<<"h_tot_p: "<<h_tot_p->GetBinLowEdge(i)<<"\t"<<"h_tot_n: "<<h_tot_n->GetBinContent(i)<<"\t"<<"h: "<<h->GetBinLowEdge(i)<<std::endl;

        if (h->GetBinContent(i)>0)
            h_tot_p->SetBinContent(i,num_p);
        else if (h->GetBinContent(i)<0)
            h_tot_n->SetBinContent(i,num_n);

    }

    if (jsyst==22) {

        h_tot_p->SetName("total_pos");
        h_tot_n->SetName("total_neg");

        TFile *fout2 = new TFile(output2,"recreate");

        fout2->cd();

        h_tot_p->Write();
        h_tot_n->Write();

        return 2;


    }

    else {


        h_tot_p->SetName("h_base_p");
        h_tot_n->SetName("h_base_n");

        fout->cd();

        h_tot_p->Write("",TObject::kOverwrite);
        h_tot_n->Write("",TObject::kOverwrite);

        //    fout->Write();

        return 1;
    }

}


int SumIn2_JES_toEsteban_highystar () {


    TString systName = (TString)my_map.find(jsyst)->second;

    TString input = "Rebinned_Files_Systematics_toBogdan/JES_Systematics_v5";

    if (!runDijet) {
        input += "_incl_ybin";
    }
    else {
        input += "_mjj_ybin";
    }

    input += ybin;
    input += ".root";

    TFile *fin = new TFile(input,"open");

    
    TString output = "Rebinned_Files_Systematics_toEsteban/JES_Systematics";
    TString output2 = "Rebinned_Files_Systematics_toEsteban/JES_Systematics_SumIn2";


    if (!runDijet) {
        output += "_incl_ybin";
        output2 += "_incl_ybin";
    }
    else {
        output += "_mjj_ybin";
        output2 += "_mjj_ybin";
    }

    output += ybin;
    output += ".root";
    output2 += ybin;
    output2 += ".root";



    TFile *fout = new TFile(output,"update");
  
    TH1F* h = (TH1F*)fin->Get(systName);


    if (jsyst==1) {

        TH1F *h_base_p = new TH1F("h_base_p","h_base_p",gNBins,&gBinLimits[0]);
        TH1F *h_base_n = new TH1F("h_base_n","h_base_n",gNBins,&gBinLimits[0]);


        for (int i = 1; i<=gNBins; i++) {

      //      std::cout<<"starting systName: "<<"\t"<<systName<<"\t"<<"h_content: "<<h->GetBinLowEdge(i)<<std::endl;

            if (h->GetBinContent(i)>0) 
                h_base_p->SetBinContent(i,h->GetBinContent(i));
            else if (h->GetBinContent(i)<0)
                h_base_n->SetBinContent(i,h->GetBinContent(i)); 

        //    std::cout<<"starting systName: "<<"\t"<<systName<<"\t"<<"h_base: "<<h_base_n->GetBinLowEdge(i)<<std::endl;

        }

        fout->cd();

        h_base_p->Write();
        h_base_n->Write();
        fout->Write();

        return 0;
    }

    
    TH1F* h_tot_p = (TH1F*)fout->Get("h_base_p");
    TH1F* h_tot_n = (TH1F*)fout->Get("h_base_n");


//    std::cout<<"systName: "<<systName<<endl;

    for (int i = 1; i<=gNBins; i++) {
        
        float num2_p = pow(h_tot_p->GetBinContent(i),2) + pow(h->GetBinContent(i),2);
        float num2_n = pow(h_tot_n->GetBinContent(i),2) + pow(h->GetBinContent(i),2);

        float num_p = sqrt(num2_p);
        float num_n = (-1)*sqrt(num2_n);


        if (h->GetBinContent(i)>0)
            h_tot_p->SetBinContent(i,num_p);
        else if (h->GetBinContent(i)<0)
            h_tot_n->SetBinContent(i,num_n);

    }

    if (jsyst==150) {

        h_tot_p->SetName("total_pos");
        h_tot_n->SetName("total_neg");

        TFile *fout2 = new TFile(output2,"recreate");

        fout2->cd();

        h_tot_p->Write();
        h_tot_n->Write();

        return 2;


    }

    else {


        h_tot_p->SetName("h_base_p");
        h_tot_n->SetName("h_base_n");

        fout->cd();

        h_tot_p->Write("",TObject::kOverwrite);
        h_tot_n->Write("",TObject::kOverwrite);

        //    fout->Write();

        return 1;
    }

}


int SumIn2_JES_toEsteban () {


    TString systName = (TString)my_map.find(jsyst)->second;

    TString input = "Rebinned_Files_Systematics_toBogdan/JES_Systematics_v5";

    if (!runDijet) {
        input += "_incl_ybin";
    }
    else {
        input += "_mjj_ybin";
    }

    input += ybin;
    input += ".root";

    TFile *fin = new TFile(input,"open");

    
    TString output = "Rebinned_Files_Systematics_toEsteban/JES_Systematics_v5";
    TString output2 = "Rebinned_Files_Systematics_toEsteban/JES_Systematics_SumIn2_v5";


    if (!runDijet) {
        output += "_incl_ybin";
        output2 += "_incl_ybin";
    }
    else {
        output += "_mjj_ybin";
        output2 += "_mjj_ybin";
    }

    output += ybin;
    output += ".root";
    output2 += ybin;
    output2 += ".root";



    TFile *fout = new TFile(output,"update");
  
    TH1F* h = (TH1F*)fin->Get(systName);


    if (jsyst==0) {

        TH1F *h_base_p = new TH1F("h_base_p","h_base_p",gNBins,&gBinLimits[0]);
        TH1F *h_base_n = new TH1F("h_base_n","h_base_n",gNBins,&gBinLimits[0]);


        for (int i = 1; i<=gNBins; i++) {

      //      std::cout<<"starting systName: "<<"\t"<<systName<<"\t"<<"h_content: "<<h->GetBinLowEdge(i)<<std::endl;

            if (h->GetBinContent(i)>0) 
                h_base_p->SetBinContent(i,h->GetBinContent(i));
            else if (h->GetBinContent(i)<0)
                h_base_n->SetBinContent(i,h->GetBinContent(i)); 

        //    std::cout<<"starting systName: "<<"\t"<<systName<<"\t"<<"h_base: "<<h_base_n->GetBinLowEdge(i)<<std::endl;

        }

        fout->cd();

        h_base_p->Write();
        h_base_n->Write();
        fout->Write();

        return 0;
    }

    
    TH1F* h_tot_p = (TH1F*)fout->Get("h_base_p");
    TH1F* h_tot_n = (TH1F*)fout->Get("h_base_n");


//    std::cout<<"systName: "<<systName<<endl;

    for (int i = 1; i<=gNBins; i++) {
        
        float num2_p = pow(h_tot_p->GetBinContent(i),2) + pow(h->GetBinContent(i),2);
        float num2_n = pow(h_tot_n->GetBinContent(i),2) + pow(h->GetBinContent(i),2);

        float num_p = sqrt(num2_p);
        float num_n = (-1)*sqrt(num2_n);


     //   if (jsyst==1112 || jsyst==1113)
     //   std::cout<<"h_tot_p: "<<h_tot_p->GetBinLowEdge(i)<<"\t"<<"h_tot_n: "<<h_tot_n->GetBinContent(i)<<"\t"<<"h: "<<h->GetBinLowEdge(i)<<std::endl;

        if (h->GetBinContent(i)>0)
            h_tot_p->SetBinContent(i,num_p);
        else if (h->GetBinContent(i)<0)
            h_tot_n->SetBinContent(i,num_n);

    }

    if (jsyst==1259) {

        h_tot_p->SetName("total_pos");
        h_tot_n->SetName("total_neg");

        TFile *fout2 = new TFile(output2,"recreate");

        fout2->cd();

        h_tot_p->Write();
        h_tot_n->Write();

        return 2;


    }

    else {


        h_tot_p->SetName("h_base_p");
        h_tot_n->SetName("h_base_n");

        fout->cd();

        h_tot_p->Write("",TObject::kOverwrite);
        h_tot_n->Write("",TObject::kOverwrite);

        //    fout->Write();

        return 1;
    }

}


int SumIn2_JES_InputsForEICF () {


//    TString systName = (TString)my_map.find(jsyst)->second;

    TString input_file = "Steven_Inputs/JESUncertainty_AllComponents_AllEtaIntStat_ICHEP2016.root";
    TString output = "inputs_to_EICF_Unc/";

    
    TString input = "EtaIntercalibration_Stat";
    input += jsyst;
    input += "_AntiKt4EMTopo";
    
    output += "EICF_Uncertainty_Total_v4.root";

//    std::cout<<"input: "<<input<<std::endl;

    TFile *fin = new TFile(input_file,"open");
    TFile *fout = new TFile(output,"update");
  
    TH2D* h = (TH2D*)fin->Get(input);

    TH1F* h2 = (TH1F*)h->ProjectionX("h2",31,31);

    gNBins = h2->GetXaxis()->GetNbins();

    if (jsyst==0) {

        TH1F *h_base_p = (TH1F*)h2->Clone("h_base_p");
        
      //  TH1F *h_base_p = new TH1F("h_base_p","h_base_p",gNBins,15,2375);
      //  TH1F *h_base_n = new TH1F("h_base_n","h_base_n",gNBins,15,2375);


        for (int i = 1; i<=gNBins; i++) {

      //      std::cout<<"starting systName: "<<"\t"<<systName<<"\t"<<"h_content: "<<h->GetBinContent(i)<<std::endl;

            if (h2->GetBinContent(i)>0) 
                h_base_p->SetBinContent(i,h2->GetBinContent(i));
           // else if (h2->GetBinContent(h2->FindBin(h_base_n->GetBinCenter(i)))>0)
           //     h_base_n->SetBinContent(i,h2->GetBinContent(h2->FindBin(h_base_p->GetBinCenter(i)))); 

        }

        fout->cd();

        h_base_p->Write();
        //h_base_n->Write();
        fout->Write();

        return 0;
    }

    
    TH1F* h_tot_p = (TH1F*)fout->Get("h_base_p");
   // TH1F* h_tot_n = (TH1F*)fout->Get("h_base_n");


    std::cout<<"my jsyst: "<<jsyst<<endl;

    for (int i = 1; i<=gNBins; i++) {
        
        float num2_p = pow(h_tot_p->GetBinContent(i),2) + pow(h2->GetBinContent(i),2);
    //    float num2_n = pow(h_tot_n->GetBinContent(i),2) + pow(h2->GetBinContent(h2->FindBin(h_tot_p->GetBinCenter(i))),2);

        float num_p = sqrt(num2_p);
     //   float num_n = (-1)*sqrt(num2_n);

     //   std::cout<<"index: "<<i<<" lowEdge: "<<h_tot_p->GetBinLowEdge(i)<<std::endl;

        if (i==60) {// pT_low ~ 306.889
        std::cout<<"h_tot_p: "<<h_tot_p->GetBinContent(i)<<"\t"<<"h2: "<<h2->GetBinContent(i)<<std::endl;
        }

        if (h2->GetBinContent(i)>0)
            h_tot_p->SetBinContent(i,num_p);
     //   else if (h2->GetBinContent(h2->FindBin(h_tot_p->GetBinCenter(i)))<0)
     //       h_tot_n->SetBinContent(i,num_n);

    }

    if (jsyst==245) {

        h_tot_p->SetName("total_up");
      //  h_tot_n->SetName("total_down");

        fout->cd();

        h_tot_p->Write();
      //  h_tot_n->Write();

        return 2;


    }

    else {


        h_tot_p->SetName("h_base_p");
      //  h_tot_n->SetName("h_base_n");

        fout->cd();

        h_tot_p->Write("",TObject::kOverwrite);
      //  h_tot_n->Write("",TObject::kOverwrite);

        //    fout->Write();

        return 1;
    }

}



void PrepInput () {

    TString udataS = "/afs/cern.ch/user/g/gmarceca/WORK/Analysis_SM/Unfolding/Unfolding/dijets_inclusive_v7/unfoldedFiles/systematic_0/";
    TString statS = "/afs/cern.ch/user/g/gmarceca/WORK/Analysis_SM/Unfolding/Unfolding/dijets_inclusive_v7/StatUncFiles/systematic_0/";

    TString jerS = "systematics_2015Data_newbin_JES_up/";

    TString outS = "inputs_to_Jona/";

    if (!runDijet) {
        udataS += "incl/ybin_";
        outS += "udata_incl_ybin_";
        statS += "incl/ybin_";
        jerS += "JES_Uncertainty_Total_incl_ybin";
    }
    else {
        udataS += "mjj/ybin_";
        outS += "udata_mjj_ybin_";
        statS += "mjj/ybin_";
        jerS += "JES_Uncertainty_Total_mjj_ybin";
    }


    udataS += ybin;
    udataS += "/Udata2015_PythiaMC_newbin_v1.root";
    statS += ybin;
    statS += "/Udata2015_PythiaMC_newbin_1000rep_v1.root";
    jerS += ybin;
    jerS += ".root";
    outS += ybin;
    outS += ".root";


    std::cout<<"udataS: "<<udataS<<std::endl;
    std::cout<<"statS: "<<statS<<std::endl;
    std::cout<<"jerS: "<<jerS<<std::endl;
    std::cout<<"outS: "<<outS<<std::endl;


    TFile *f_udat = new TFile(udataS,"open");
    TFile *f_stat = new TFile(statS,"open");
    TFile *f_jer = new TFile(jerS,"open");

    TH1F *h_udat = (TH1F*)f_udat->Get("UData");

    TH1F *h_stat = (TH1F*)f_stat->Get("Nominal_AvgRep");

    TH1F *h_jer_up = (TH1F*)f_jer->Get("total_up");
    TH1F *h_jer_down = (TH1F*)f_jer->Get("total_down");

    
    gNBins = h_udat->GetNbinsX();

    TH1F *h_out = new TH1F("udata","h_out",gNBins,&gBinLimits[0]);


    for (int i=1;i<=h_udat->GetNbinsX();i++) {

        h_out->SetBinContent(i,h_udat->GetBinContent(i));
        h_out->SetBinError(i,h_stat->GetBinError(i));

    }

    TFile *f_out = new TFile(outS,"recreate");

    f_out->cd();

    h_out->Write();

    h_jer_up->SetName("jes_up");
    h_jer_down->SetName("jes_down");

    h_jer_up->Write();
    h_jer_down->Write();

}


void PrepInput_EICF () {

    TString udataS = "/afs/cern.ch/user/g/gmarceca/WORK/Analysis_SM/Unfolding/Unfolding/dijets_inclusive_v7/EICF_Jacob/Feb16_EtaIntercalibration13TeV25ns_Eta2016_MM_AntiKt4EMTopo_Calibration.root";

    TString outS = "inputs_to_EICF_Unc/output.root";
    
    TFile *f_udat = new TFile(udataS,"open");

    TString histname = "StatNuisanceParams/Stat";
    TString histname_out = "EtaIntercalibration_Stat";

    histname_out += jsyst;
    histname_out += "_AntiKt4EMTopo";

    histname += jsyst;

    f_udat->cd();

    TH2D *h_udat = (TH2D*)f_udat->Get(histname);

    h_udat->SetName(histname_out);


    TFile *f_out = new TFile(outS,"update");

    f_out->cd();

    h_udat->Write();

    if(jsyst==0)
	    f_out->Write();

}




