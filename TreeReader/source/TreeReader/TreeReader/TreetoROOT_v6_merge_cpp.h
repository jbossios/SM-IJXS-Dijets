#ifndef run_h
#define run_h

#include <iostream>
#include <vector>
#include <stdlib.h>
#include <algorithm>
#include <string>
#include <map>
#include <fstream>
#include <utility>
#include <math.h>

#include "TROOT.h"
#include "TTree.h"
#include "TChain.h"
#include "TMath.h"
#include "TFile.h"
#include "TH1.h"
#include "TLorentzVector.h"
#include "TGraphAsymmErrors.h"
#include "TStopwatch.h"

#include <stdio.h>

// Bootstrap
#include "BootstrapGenerator/TH2DBootstrap.h"
#include "BootstrapGenerator/TH1DBootstrap.h"
#include "BootstrapGenerator/BootstrapGenerator.h"

using namespace std;

int RecoMatch_index(TLorentzVector myjet, vector<TLorentzVector> jets, TLorentzVector &matchingJet, int &index, double &DRmin);
int Match_index_inv(TLorentzVector myjet, vector<TLorentzVector> jets, int index_veto, double DRmin);
double DRmin(TLorentzVector myjet, vector<TLorentzVector> jets, double PtMin);
int GetYStarBin(double yStar);

BootstrapGenerator *fGenerator;

#endif

