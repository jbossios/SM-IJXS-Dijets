//
// TreetoHists
//
// Tree Reader for inclusive jet and dijet analyses
//
// Jonathan Bossio - 28 Sep 2016
//
// Change lumi table Nov 2018 T Carli
/////////////////////////////////////////////

#include "TreeReader/global.h"
#include "TreeReader/TreetoROOT_v6_merge_cpp.h"

//--------------------
// Global variables 
//--------------------

TString m_PATH            = "";
TString m_inputFile       = "";
TString m_outputFile      = "";
TString m_lumiFile        = "";
TString m_generator       = "Pythia";
TString m_firstTrigger    = "HLT_j380"; // set by steering
TString m_unprescaledTrig = "HLT_j450"; // will be recalculated
bool MC           = false;
bool m_debug      = false;
bool m_runDijets  = false;
bool m_rel21      = false;
bool m_PRW        = false;
bool m_unfolding  = true;
bool m_bootstraps = true;
int  m_nReplicas  = 100;
double m_intermediateMu = 13.5; // Needs to be updated for 2016 TEMPORARY
// Dijet Selection
double m_j0min  = 75;
double m_j1min  = 75;
double m_HT2min = 200;

//------------------------------------------------------------------------
// Functions
//------------------------------------------------------------------------

void EnableBranches();
void SetBranches();
void SetBinning();
int GetYStarBin(double);

//------------------------------------------------------------------------
// Main
//------------------------------------------------------------------------

int main(int argc, char **argv) {

  //---------------------------
  // Decoding the user settings
  //---------------------------

  std::string path         = "";
  std::string inputFile    = "";
  std::string outputFile   = "";
  std::string lumiFile     = "";
  std::string generator    = "Pythia";
  //  std::string firstTrigger = "HLT_j380";
  std::string firstTrigger = "HLT_j360";

  for (int i=1; i< argc; i++){

    std::string opt(argv[i]); std::vector< std::string > v;

    std::istringstream iss(opt);

    std::string item;
    char delim = '=';

    while (std::getline(iss, item, delim)){
        v.push_back(item);
    }

    if ( opt.find("--path=")   != std::string::npos) path = v[1];

    if ( opt.find("--inputFile=")   != std::string::npos) inputFile = v[1];
    
    if ( opt.find("--outputFile=")   != std::string::npos) outputFile = v[1];
    
    if ( opt.find("--lumiFile=")   != std::string::npos) lumiFile = v[1];

    if ( opt.find("--mcGenerator=")   != std::string::npos) generator = v[1];
    
    if ( opt.find("--firstTrigger=")   != std::string::npos) firstTrigger = v[1];
    
    std::string::size_type sz;   // alias of size_t
    if ( opt.find("--nReplicas=")   != std::string::npos) m_nReplicas = std::stoi(v[1],&sz);
    if (m_debug && m_unfolding) std::cout << "Number of replicas: " << m_nReplicas << std::endl;

    if ( opt.find("--unfolding=")      != std::string::npos) {
      if (v[1].find("TRUE") != std::string::npos) m_unfolding= true;
      if (v[1].find("FALSE") != std::string::npos) m_unfolding= false;
    }
    
    if ( opt.find("--isMC=")      != std::string::npos) {
      if (v[1].find("TRUE") != std::string::npos) MC= true;
      if (v[1].find("FALSE") != std::string::npos) MC= false;
    }
    
    if ( opt.find("--debug=")      != std::string::npos) {
      if (v[1].find("TRUE") != std::string::npos) m_debug= true;
      if (v[1].find("FALSE") != std::string::npos) m_debug= false;
    }

    if ( opt.find("--dijets=")      != std::string::npos) {
      if (v[1].find("TRUE") != std::string::npos) m_runDijets= true;
      if (v[1].find("FALSE") != std::string::npos) m_runDijets= false;
    }

    if ( opt.find("rel21=")      != std::string::npos) {
      if (v[1].find("TRUE") != std::string::npos) m_rel21= true;
      if (v[1].find("FALSE") != std::string::npos) m_rel21= false;
    }

    if ( opt.find("prw=")      != std::string::npos) {
      if (v[1].find("TRUE") != std::string::npos) m_PRW= true;
      if (v[1].find("FALSE") != std::string::npos) m_PRW= false;
    }

  }//End: Loop over input options

  m_PATH         = path;
  m_inputFile    = inputFile;
  m_outputFile   = outputFile;
  m_lumiFile     = lumiFile;
  m_generator    = generator;
  m_firstTrigger = firstTrigger;

  if(m_PATH == ""){
    std::cout << "ERROR: no path specified, exiting" << std::endl;
    return 1;
  }
  if(m_inputFile == ""){
    std::cout << "ERROR: no inputFile specified, exiting" << std::endl;
    return 1;
  }
  if(m_outputFile == ""){
    std::cout << "ERROR: no outputFile specified, exiting" << std::endl;
    return 1;
  }
  if(!MC && m_lumiFile == ""){
    std::cout << "ERROR: no lumiFile specified, exiting" << std::endl;
    return 1;
  }

  if(m_unfolding && m_nReplicas==0) m_bootstraps = false;
  if(!MC){
   m_bootstraps = false;
   m_unfolding  = false;
  }
  
  if (m_debug) cout<<"m_debug is on "<<endl;

  std::vector <TH1D*> vhisto;
  TH1D *hevent= new TH1D("hevent","Event info",6,0.,6.);
  hevent->GetXaxis()->SetBinLabel(1,"Nfile");
  hevent->GetXaxis()->SetBinLabel(2,"Nevtot");
  hevent->GetXaxis()->SetBinLabel(3,"skipped events");
  hevent->GetXaxis()->SetBinLabel(4,"Skipped events");  
  hevent->GetXaxis()->SetBinLabel(5,"Njet |y|<3");
  hevent->GetXaxis()->SetBinLabel(6,"Njet |y|<0.5");
  vhisto.push_back(hevent);
 
  TH1D *hoptions= new TH1D("hoptions","Options info",6,0.,6.);
  hoptions->GetXaxis()->SetBinLabel(1,m_inputFile.Data());
  hoptions->GetXaxis()->SetBinLabel(2,m_lumiFile.Data());
  hoptions->GetXaxis()->SetBinLabel(3,m_generator.Data());
  hoptions->GetXaxis()->SetBinLabel(4,m_firstTrigger.Data());
  if (m_PRW) hoptions->GetXaxis()->SetBinLabel(5,"prw");
  else      hoptions->GetXaxis()->SetBinLabel(5,"No prw");    
  vhisto.push_back(hoptions);

  std::vector <int> vrun;
  
  //
  //----------
  // Triggers
  //----------
  //
  // changed this code to make is simpler and less error prone
  // (changed format of luminosity file)
  // See warning below: Since Jona needs a matrix at one place
  //
  //
  //std::map<std::string, float > Lumiwgt;
  //std::map<std::string,double> minPtTriggers;  
  //std::map<std::string,double> maxPtTriggers;
  //

  std::map<std::string, std::map<std::string, float >  > Lumiwgt;
  
  if(!MC){
    if (m_debug) std::cout<<"Filling triggers "<<std::endl;
    // Input File
    std::ifstream in;
    TString lumifile = "";
    lumifile += m_lumiFile;
    
    std::cout << "Reading luminosity from: " << lumifile << std::endl;
    in.open(lumifile,std::ifstream::in);
    if (!in.is_open()) {std::cout<<"File "<<lumifile<<" not found"<<std::endl; return 1;}
    else {std::cout<<"File "<<lumifile<<" opened !"<<std::endl;}


    std::string trigname;
    float lumi=0., pt=0.;
    //while (in.good()) {
    while (in >> trigname >> lumi >> pt) {    
     //in >> trigname >> lumi >> pt;
     if (m_debug) std::cout<<" name= "<<trigname<<" lumi= "<<lumi<<" pt= "<<pt<<std::endl;
     if (TString(trigname).Contains("eta")) continue;
     Lumiwgt[trigname]["lumi"]=lumi;
     Lumiwgt[trigname]["ptmin"]=pt;
     Triggers.push_back(trigname);
    }
  
    in.close();

    // filling ptmax and determined lowest unprescaled trigger
    double vallumiold=-1.; double vallumi=0;
    for (int i=0; i<(int)Triggers.size(); i++) {
      double ptmax=-1.;
      if (i<(int)Triggers.size()-1)
        ptmax=Lumiwgt[Triggers.at(i+1)]["ptmin"];
      else
       ptmax=7500.;

     vallumi=Lumiwgt[Triggers.at(i)]["lumi"];
     if (vallumi>vallumiold and vallumi!=vallumiold) {
      vallumiold=vallumi;
      m_unprescaledTrig=Triggers.at(i);
     }
     Lumiwgt[Triggers.at(i)]["ptmax"]=ptmax;
    }

    cout<<"Determined lowest unprescaled trigger = "<<m_unprescaledTrig.Data()<<endl;

    //
    // Print and store Trigger map
    //

    TH1D *hybins= new TH1D("hybins","Number of ybins",nyBins+1,yBins[0],yBins[nyBins]);
    vhisto.push_back(hybins);    
    for (int i=0; i<nyBins+1; i++) {
      //cout<<i<<" "<<yBins[i]<<endl; 
     hybins->SetBinContent(i+1,yBins[i]);
    }
    //hybins->Print("all");
    //return 1; 

    int ntrigger=Triggers.size(); 
    cout<<"Number of triggers= "<<ntrigger<<endl;
    TH1D *htriggerptmin= new TH1D("htriggerptmin","ptmin for each tirgger",ntrigger,0.,float(ntrigger));
    vhisto.push_back(htriggerptmin);

    TH1D *htriggerptmax= new TH1D("htriggerptmax","ptmax for each trigger",ntrigger,0.,float(ntrigger));
    vhisto.push_back(htriggerptmax);
    
    TH1D *htriggerlumin= new TH1D("htriggerlumin","Luminosity per trigger",ntrigger,0.,float(ntrigger));
    vhisto.push_back(htriggerlumin);

    cout<<"Trigger read from "<<lumifile<<endl; 
    std::cout<<" Name     Lumi   ptmin  ptmax "<<std::endl;

    for (int i=0; i<ntrigger; i++) {
     std::string name=Triggers.at(i);

     std::cout<<" "<<name<<" "<<Lumiwgt[name]["lumi"]<<" "<<Lumiwgt[name]["ptmin"]<<" "<<Lumiwgt[name]["ptmax"]<<std::endl;

     htriggerptmin->GetXaxis()->SetBinLabel(i+1,name.c_str());
     htriggerptmax->GetXaxis()->SetBinLabel(i+1,name.c_str());
     htriggerlumin->GetXaxis()->SetBinLabel(i+1,name.c_str());
     
     htriggerptmin->SetBinContent(i+1,Lumiwgt[name]["ptmin"]);
     htriggerptmax->SetBinContent(i+1,Lumiwgt[name]["ptmax"]);
     htriggerlumin->SetBinContent(i+1,Lumiwgt[name]["lumi"]);          
    }  

    
    //std::cout<<" Print trigger map size= "<<Lumiwgt.size()<<std::endl;
    //for(std::map<std::string, std::map<std::string, float >  > ::iterator it = Lumiwgt.begin(); it != Lumiwgt.end(); it++) {
    // for ( std::map<std::string, float > ::iterator it2 =(it->second).begin(); it2!= (it->second).end(); it2++) {
    //  std::cout<<" Lumiwgt["<<it->first<<"]["<<it2->first<<"]= "<<it2->second<<std::endl;
    // }
    //}

    //if (Lumiwgt[m_firstTrigger.Data().c_str()]["lumi"]
    //if (Lumiwgt[unprescaledTrig.Data().c_str()]["lumi"]

 }  

  
  //----------
  // Triggers
  //----------

/*
//Triggers.push_back("L1_RD0_FILLED");
  Triggers.push_back("HLT_j15");
  Triggers.push_back("HLT_j25");
  Triggers.push_back("HLT_j35");
  Triggers.push_back("HLT_j45");
  Triggers.push_back("HLT_j60");
  Triggers.push_back("HLT_j85");
  Triggers.push_back("HLT_j110");
  Triggers.push_back("HLT_j175");
  Triggers.push_back("HLT_j260");
  Triggers.push_back("HLT_j360");
  Triggers.push_back("HLT_j400");
*/
  //Triggers.push_back("HLT_j460");

  /*
  Triggers.push_back("HLT_j15_320eta490");
  Triggers.push_back("HLT_j25_320eta490");
  Triggers.push_back("HLT_j35_320eta490");
  Triggers.push_back("HLT_j45_320eta490");
  Triggers.push_back("HLT_j60_320eta490");
  Triggers.push_back("HLT_j85_320eta490");
  Triggers.push_back("HLT_j110_320eta490");
  Triggers.push_back("HLT_j175_320eta490");
  Triggers.push_back("HLT_j260_320eta490");
  Triggers.push_back("HLT_j360_320eta490");
  Triggers.push_back("HLT_j460_320eta400");
  Triggers.push_back("HLT_j460_320eta490");
  */

 /* 
  std::map<std::string,double> minPtTriggers;
  minPtTriggers["HLT_j15"]  = 0.; 
  minPtTriggers["HLT_j25"]  = 35.; 
  minPtTriggers["HLT_j35"]  = 46.;
  minPtTriggers["HLT_j45"]  = 56.;
  minPtTriggers["HLT_j60"]  = 79.;
  minPtTriggers["HLT_j85"]  = 100.; // not measured
  minPtTriggers["HLT_j110"] = 128.;
  minPtTriggers["HLT_j175"] = 196.;
  minPtTriggers["HLT_j260"] = 298.;
  minPtTriggers["HLT_j360"] = 425.;
  minPtTriggers["HLT_j400"] = 500.;
//minPtTriggers["HLT_j460"] = 500.;  // 400 should be the lowest unprescaled trigger

  std::map<std::string,double> maxPtTriggers;
  maxPtTriggers["HLT_j15"]  = 35.; 
  maxPtTriggers["HLT_j25"]  = 46.; 
  maxPtTriggers["HLT_j35"]  = 56.;
  maxPtTriggers["HLT_j45"]  = 79.;
  maxPtTriggers["HLT_j60"]  = 100.;
  maxPtTriggers["HLT_j85"]  = 128.;
  maxPtTriggers["HLT_j110"] = 196.;
  maxPtTriggers["HLT_j175"] = 298.;
  maxPtTriggers["HLT_j260"] = 425.;
  maxPtTriggers["HLT_j360"] = 500.;
  maxPtTriggers["HLT_j400"] = 7500.;
*/
  /*
  if (minPtTriggers.size()!=maxPtTriggers.size()) {
   std::cout << "Problem in trigger set-up " << minPtTriggers.size()<<" maxPtTriggers= "<<maxPtTriggers.size()<<std::endl;
   return 1;
  }

  if (Triggers.size()!=maxPtTriggers.size()) {
   std::cout << "Problem in trigger: " << Triggers.size()<<" maxPtTriggers= "<<maxPtTriggers.size()<<std::endl;
   return 1;
  }     
  */
  /*
  minPtTriggers["HLT_j15_320eta490"]  = 25.; 
  minPtTriggers["HLT_j25_320eta490"]  = 35.; 
  maxPtTriggers["HLT_j35_320eta490"]  = 50.;
  maxPtTriggers["HLT_j45_320eta490"]  = 65.;
  minPtTriggers["HLT_j60_320eta490"]  = 85.;
  maxPtTriggers["HLT_j85_320eta490"]  = 110.;
  minPtTriggers["HLT_j110_320eta490"] = 125.;
  minPtTriggers["HLT_j175_320eta490"] = 195.;
  maxPtTriggers["HLT_j260_320eta490"] = 280.;
  minPtTriggers["HLT_j360_320eta490"] = 380.;
  minPtTriggers["HLT_j460_320eta490"] = 475.;
  */
  
  std::map<std::string,int> TriggertoInt;
  //TriggertoInt["HLT_j25"]  = 0;
  //TriggertoInt["HLT_j60"]  = 1;
  //TriggertoInt["HLT_j110"] = 2;
  //TriggertoInt["HLT_j175"] = 3;
  //TriggertoInt["HLT_j380"] = 4;
  //TriggertoInt["HLT_j450"] = 0;

  int firstTrig = TriggertoInt[m_firstTrigger.Data()]; // Default: HLT_j380
  
  for (int i=0; i<(int)Triggers.size(); i++){
   TriggertoInt[Triggers.at(i)]  = i;
  }

  //----------------------------
  // Reading Luminosity Matrix
  //----------------------------
/*
  std::vector<std::vector<double> > wgtDijets;  
  if(!MC){
    // Input File
    std::ifstream in;
    //    TString lumifile = "Luminosities/";
    TString lumifile = "";
    lumifile += m_lumiFile;
    std::cout << "Reading luminosity from: " << lumifile << std::endl;
    in.open(lumifile,std::ifstream::in);

    double t0, t1, t2, t3, t4;

    // Reading values
    while (in >> t0 >> t1 >> t2 >> t3 >> t4) {
      std::vector<double> temp;
      temp.push_back(t0);
      temp.push_back(t1);
      temp.push_back(t2);
      temp.push_back(t3);
      temp.push_back(t4);
      wgtDijets.push_back(temp);
    }
    in.close();
  }
*/
  //-----------------------
  // Getting File
  //-----------------------
  
  TString File = m_PATH; File += "/"; File += m_inputFile;
  std::cout << "\nOpening " << File << std::endl;

  //-----------------------
  // Getting Tree
  //-----------------------

  TFile* ft;
  if(!File.Contains("eos")) ft = new TFile(File,"read");
  else{ ft = TFile::Open(File,"read");}
  if(ft==NULL){
    std::cout << "File "<<File<<" does not exist! Exiting" << std::endl;
    return 1;
  }
  if(m_debug) std::cout << "File opened:" << File << std::endl;
  ft->cd();
  TString treeName = "nominal";
  TString directoryName = "treeAlgo";
  if(m_rel21){
    TDirectory* TDirF_tmp = ft->GetDirectory(directoryName.Data());
    //TDirF_tmp->GetObject(treeName.Data(),tree);
    //if (m_debug) cout<<"treeName= "<<treeName<<endl;

    tree = (TTree*) TDirF_tmp->Get(treeName.Data());
  } else{
    tree = (TTree*)ft->Get(treeName.Data());
  }
  if(!tree){
    std::cout << "Tree does not exist! Exiting" << std::endl;
    return 1;
  }
  if(m_debug) std::cout << "Tree opened: " << treeName << std::endl;

  EnableBranches(); 
  SetBranches();
  
  int entries = tree->GetEntries();  // Number of events
  if(m_debug) entries = 50;          // Run over only 10 events in debug mode

  //-----------------------------
  // Defining eta, phi and y bins
  //-----------------------------
  
  if(m_debug) std::cout << "Setting Binning" << std::endl;
  SetBinning();

  //--------------------------------------
  // Histograms
  //--------------------------------------

  if(m_debug) std::cout << "Creating Histograms" << std::endl;
  TString histname_tmp; // General auxiliar histogram name

  //----------------------
  // Dijet distributions
  //----------------------

  // Bootstraps  
  fGenerator = new BootstrapGenerator("Generator", "Generator", 0);
  TH2DBootstrap    *fBootstrap_m12[nystarBins];
  TH1DBootstrap    *fRecoBootstrap_m12[nystarBins];
  TH1DBootstrap    *fTruthBootstrap_m12[nystarBins];

  // Binned in y*
  TH1D *h_m12_reco[nystarBins];
  TH1D *h_m12_reco_finer_bins[nystarBins];
  TH1D *h_m12_reco_lowMu[nystarBins];
  TH1D *h_m12_reco_highMu[nystarBins];
  TH1D *h_m12_truth[nystarBins];
  TH1D *h_m12_truth_finer_bins[nystarBins];
  TH2D *h_m12_TM[nystarBins]; // Transfer Matrix
  for(int ystarbin=0;ystarbin<nystarBins;++ystarbin){
    histname_tmp  = "h_m12_reco_";
    histname_tmp += ystarbin;
    h_m12_reco[ystarbin] = new TH1D(histname_tmp.Data(),"",nMassBins[ystarbin],massBins_0);
    h_m12_reco[ystarbin]->Sumw2();
    
    histname_tmp  = "h_m12_reco_finer_bins_";
    histname_tmp += ystarbin;
    h_m12_reco_finer_bins[ystarbin] = new TH1D(histname_tmp.Data(),"",1300,0,13000);
    h_m12_reco_finer_bins[ystarbin]->Sumw2();

    histname_tmp  = "h_m12_reco_lowMu_";
    histname_tmp += ystarbin;
    h_m12_reco_lowMu[ystarbin] = new TH1D(histname_tmp.Data(),"",nMassBins[ystarbin],massBins_0);
    h_m12_reco_lowMu[ystarbin]->Sumw2();

    histname_tmp  = "h_m12_reco_highMu_";
    histname_tmp += ystarbin;
    h_m12_reco_highMu[ystarbin] = new TH1D(histname_tmp.Data(),"",nMassBins[ystarbin],massBins_0);
    h_m12_reco_highMu[ystarbin]->Sumw2();

    histname_tmp  = "h_m12_truth_";
    histname_tmp += ystarbin;
    h_m12_truth[ystarbin] = new TH1D(histname_tmp.Data(),"",nMassBins[ystarbin],massBins_0);
    h_m12_truth[ystarbin]->Sumw2();

    histname_tmp  = "h_m12_truth_finer_bins_";
    histname_tmp += ystarbin;
    h_m12_truth_finer_bins[ystarbin] = new TH1D(histname_tmp.Data(),"",1300,0,13000);
    h_m12_truth_finer_bins[ystarbin]->Sumw2();
    
    if (m_unfolding) {
      histname_tmp  = "h_m12_TM_";
      histname_tmp += ystarbin;
      h_m12_TM[ystarbin] = new TH2D(histname_tmp.Data(),"",nMassBins[ystarbin],massBins_0,nMassBins[ystarbin],massBins_0);
      h_m12_TM[ystarbin]->Sumw2();
      if(m_bootstraps){
        histname_tmp  = "boot_m12_";
        histname_tmp += ystarbin;
        fBootstrap_m12[ystarbin] = new TH2DBootstrap(histname_tmp.Data(), "", nMassBins[ystarbin], massBins_0, nMassBins[ystarbin], massBins_0, m_nReplicas, fGenerator);
        fBootstrap_m12[ystarbin]->Sumw2();

        histname_tmp  = "boot_m12_reco_";
        histname_tmp += ystarbin;
        fRecoBootstrap_m12[ystarbin] = new TH1DBootstrap(histname_tmp.Data(), "", nMassBins[ystarbin], massBins_0, m_nReplicas, fGenerator);
        fRecoBootstrap_m12[ystarbin]->Sumw2();

        histname_tmp  = "boot_m12_truth_";
        histname_tmp += ystarbin;
        fTruthBootstrap_m12[ystarbin] = new TH1DBootstrap(histname_tmp.Data(), "", nMassBins[ystarbin], massBins_0, m_nReplicas, fGenerator);
        fTruthBootstrap_m12[ystarbin]->Sumw2();
      }
    }
  }// END: Loop over ystar bins

  // |y|<3.0
  TH1D *h_m12_reco_ystar_inclusive = new TH1D("h_m12_reco_ystar_inclusive","reco distribution",nMassBins[0],massBins_0);
  h_m12_reco_ystar_inclusive->Sumw2();
  TH1D *h_m12_reco_ystar_inclusive_mu_14_21 = new TH1D("h_m12_reco_ystar_inclusive_mu_14_21","reco distribution mu1",nMassBins[0],massBins_0);
  h_m12_reco_ystar_inclusive_mu_14_21->Sumw2();
  TH1D *h_m12_reco_ystar_inclusive_mu_21_28 = new TH1D("h_m12_reco_ystar_inclusive_mu_21_28","reco distribution mu2",nMassBins[0],massBins_0);
  h_m12_reco_ystar_inclusive_mu_21_28->Sumw2();
  // |y|<0.5
  TH1D *h_m12_reco_centralregion = new TH1D("h_m12_reco_centralregion","reco distribution",nMassBins[0],massBins_0);
  h_m12_reco_centralregion->Sumw2();
  TH1D *h_m12_reco_centralregion_mu_14_21 = new TH1D("h_m12_reco_centralregion_mu_14_21","reco distribution mu1",nMassBins[0],massBins_0);
  h_m12_reco_centralregion_mu_14_21->Sumw2();
  TH1D *h_m12_reco_centralregion_mu_21_28 = new TH1D("h_m12_reco_centralregion_mu_21_28","reco distribution mu2",nMassBins[0],massBins_0);
  h_m12_reco_centralregion_mu_21_28->Sumw2();
 
  //---------------- 
  // y* distribution
  //---------------- 
  TH1D *h_ystar = new TH1D("h_ystar","",nDelYBins,delYBins);
  h_ystar->Sumw2();

  //---------------------
  // Histogram of weights
  //---------------------
  TH1D *h_wgt = new TH1D("h_wgt","",7e4,0.,7e5); // Pythia
  //TH1D *h_wgt = new TH1D("h_wgt","",7e4,0.,1); // Powheg
  //TH1D *h_wgt = new TH1D("h_wgt","",7e4,0.,1000000); // Powheg

  //-----------------------------
  // Inclusive jets Distributions
  //-----------------------------
  
  TH1D *h_njet = new TH1D("h_njet","",20,0,20);
  h_njet->Sumw2();

  //----------------------------------------------------------
  // pT distributions (All jets - Leading and subleading jets)
  //----------------------------------------------------------
  
  // y inclusive (|y|<3)
  // All Jets
  TH1D *h_pt_with_wgt_y_inclusive = new TH1D("h_pt_with_wgt_y_inclusive","",nptBins[0],ptBins_0);
  h_pt_with_wgt_y_inclusive->Sumw2();

  TH1D *h_pt_with_wgt_y_05_08 = new TH1D("h_pt_with_wgt_y_05_08","",nptBins[0],ptBins_0);
  h_pt_with_wgt_y_05_08->Sumw2();
  TH1D *h_pt_with_wgt_eta_05_after_calibration = new TH1D("h_pt_with_wgt_eta_05_after_calibration","",nptBins[0],ptBins_0);
  h_pt_with_wgt_eta_05_after_calibration->Sumw2();
  TH1D *h_pt_with_wgt_eta_05_before_calibration = new TH1D("h_pt_with_wgt_eta_05_before_calibration","",nptBins[0],ptBins_0);
  h_pt_with_wgt_eta_05_before_calibration->Sumw2();
  TH1D *h_pt_with_wgt_y_inclusive_mu_14_21 = new TH1D("h_pt_with_wgt_y_inclusive_mu_14_21","",nptBins[0],ptBins_0);
  h_pt_with_wgt_y_inclusive_mu_14_21->Sumw2();
  TH1D *h_pt_with_wgt_y_inclusive_mu_21_28 = new TH1D("h_pt_with_wgt_y_inclusive_mu_21_28","",nptBins[0],ptBins_0);
  h_pt_with_wgt_y_inclusive_mu_21_28->Sumw2();

  // All Jets for turn on curves
  std::map<std::string,TH1D*> h_pt_y_inclusive_triggers;
  TString BaseName = "h_pt_y_inclusive_";
  TString TempName = "";
  for(unsigned int m=0;m<Triggers.size();m++){
    TempName = BaseName + Triggers.at(m);
    h_pt_y_inclusive_triggers[Triggers.at(m)] = new TH1D(TempName.Data(),"",1000.,0.,5000.);  
    h_pt_y_inclusive_triggers[Triggers.at(m)]->Sumw2();  
  }

  // Leading Jets
  TH1D *h_j0_pt_with_wgt_y_inclusive = new TH1D("h_j0_pt_with_wgt_y_inclusive","",nptBins[0],ptBins_0);
  h_j0_pt_with_wgt_y_inclusive->Sumw2();
  TH1D *h_j0_pt_with_wgt_y_05_08 = new TH1D("h_j0_pt_with_wgt_y_05_08","",nptBins[0],ptBins_0);
  h_j0_pt_with_wgt_y_05_08->Sumw2();
  TH1D *h_j0_pt_with_wgt_eta_05_before_calibration = new TH1D("h_j0_pt_with_wgt_eta_05_before_calibration","",nptBins[0],ptBins_0);
  h_j0_pt_with_wgt_eta_05_before_calibration->Sumw2();
  TH1D *h_j0_pt_with_wgt_eta_05_after_calibration = new TH1D("h_j0_pt_with_wgt_eta_05_after_calibration","",nptBins[0],ptBins_0);
  h_j0_pt_with_wgt_eta_05_after_calibration->Sumw2();
  TH1D *h_j0_pt_with_wgt_y_inclusive_mu_14_21 = new TH1D("h_j0_pt_with_wgt_y_inclusive_mu_14_21","",nptBins[0],ptBins_0);
  h_j0_pt_with_wgt_y_inclusive_mu_14_21->Sumw2();
  TH1D *h_j0_pt_with_wgt_y_inclusive_mu_21_28 = new TH1D("h_j0_pt_with_wgt_y_inclusive_mu_21_28","",nptBins[0],ptBins_0);
  h_j0_pt_with_wgt_y_inclusive_mu_21_28->Sumw2();
  TH1D *h_j0_pt_with_wgt_y_inclusive_FinerBins = new TH1D("h_j0_pt_with_wgt_y_inclusive_FinerBins","",nptFinerBins,ptFinerBins);
  h_j0_pt_with_wgt_y_inclusive_FinerBins->Sumw2();


  // SubLeading Jets
  TH1D *h_j1_pt_with_wgt_y_inclusive = new TH1D("h_j1_pt_with_wgt_y_inclusive","",nptBins[0],ptBins_0);
  h_j1_pt_with_wgt_y_inclusive->Sumw2();
  TH1D *h_j1_pt_with_wgt_y_05_08 = new TH1D("h_j1_pt_with_wgt_y_05_08","",nptBins[0],ptBins_0);
  h_j1_pt_with_wgt_y_05_08->Sumw2();
  TH1D *h_j1_pt_with_wgt_eta_05_before_calibration = new TH1D("h_j1_pt_with_wgt_eta_05_before_calibration","",nptBins[0],ptBins_0);
  h_j1_pt_with_wgt_eta_05_before_calibration->Sumw2();
  TH1D *h_j1_pt_with_wgt_eta_05_after_calibration = new TH1D("h_j1_pt_with_wgt_eta_05_after_calibration","",nptBins[0],ptBins_0);
  h_j1_pt_with_wgt_eta_05_after_calibration->Sumw2();
  TH1D *h_j1_pt_with_wgt_y_inclusive_mu_14_21 = new TH1D("h_j1_pt_with_wgt_y_inclusive_mu_14_21","",nptBins[0],ptBins_0);
  h_j1_pt_with_wgt_y_inclusive_mu_14_21->Sumw2();
  TH1D *h_j1_pt_with_wgt_y_inclusive_mu_21_28 = new TH1D("h_j1_pt_with_wgt_y_inclusive_mu_21_28","",nptBins[0],ptBins_0);
  h_j1_pt_with_wgt_y_inclusive_mu_21_28->Sumw2();

  // Third jets
  TH1D *h_j2_pt_with_wgt_y_inclusive = new TH1D("h_j2_pt_with_wgt_y_inclusive","",nptBins[0],ptBins_0);
  h_j2_pt_with_wgt_y_inclusive->Sumw2();
    
  // Binned in y
  TH1D *h_pt_with_wgt[nyBins];
  TH1D *h_pt_finebins_with_wgt[nyBins];  
  TH1D *h_pt_truth[nyBins];
  TH1D *h_j0_pt_with_wgt[nyBins];
  TH1D *h_j1_pt_with_wgt[nyBins];
  TH1D *h_j0_pt_mjj[nyBins];
  TH1D *h_j1_pt_mjj[nyBins];
  TH1D *h_pt_lowMu[nyBins];
  TH1D *h_pt_highMu[nyBins];
  TH2D *h_pt_TM[nyBins]; // Transfer Matrix
  TH2DBootstrap    *fBootstrap_inc[nyBins];
  TH1DBootstrap    *fRecoBootstrap_inc[nyBins];
  TH1DBootstrap    *fTruthBootstrap_inc[nyBins];
  for(int ybin=0;ybin<nyBins;++ybin){
    histname_tmp  = "h_pt_with_wgt_";
    histname_tmp += ybin;
    h_pt_with_wgt[ybin] = new TH1D(histname_tmp.Data(),"",nptBins[ybin],ptBins_0);
    h_pt_with_wgt[ybin]->Sumw2();

    histname_tmp  = "h_pt_finebins_with_wgt_";
    histname_tmp += ybin;
    // h_pt_finebins_with_wgt[ybin] = new TH1D(histname_tmp.Data(),"",nptFinerBins,ptFinerBins);
    h_pt_finebins_with_wgt[ybin] = new TH1D(histname_tmp.Data(),"",3000,15.,3015);
    h_pt_finebins_with_wgt[ybin]->Sumw2();    
    vhisto.push_back( h_pt_finebins_with_wgt[ybin]);
    
    histname_tmp  = "h_pt_truth_";
    histname_tmp += ybin;
    h_pt_truth[ybin] = new TH1D(histname_tmp.Data(),"",nptBins[ybin],ptBins_0);
    h_pt_truth[ybin]->Sumw2();

    histname_tmp  = "h_j0_pt_with_wgt_";
    histname_tmp += ybin;
    h_j0_pt_with_wgt[ybin] = new TH1D(histname_tmp.Data(),"",nptBins[ybin],ptBins_0);
    h_j0_pt_with_wgt[ybin]->Sumw2();

    histname_tmp  = "h_j1_pt_with_wgt_";
    histname_tmp += ybin;
    h_j1_pt_with_wgt[ybin] = new TH1D(histname_tmp.Data(),"",nptBins[ybin],ptBins_0);
    h_j1_pt_with_wgt[ybin]->Sumw2();

    histname_tmp  = "h_j0_pt_mjj_";
    histname_tmp += ybin;
    h_j0_pt_mjj[ybin] = new TH1D(histname_tmp.Data(),"",650,0,6500);
    h_j0_pt_mjj[ybin]->Sumw2();

    histname_tmp  = "h_j1_pt_mjj_";
    histname_tmp += ybin;
    h_j1_pt_mjj[ybin] = new TH1D(histname_tmp.Data(),"",650,0,6500);
    h_j1_pt_mjj[ybin]->Sumw2();

    histname_tmp  = "h_pt_lowMu_";
    histname_tmp += ybin;
    h_pt_lowMu[ybin] = new TH1D(histname_tmp.Data(),"",nptBins[ybin],ptBins_0);
    h_pt_lowMu[ybin]->Sumw2();

    histname_tmp  = "h_pt_highMu_";
    histname_tmp += ybin;
    h_pt_highMu[ybin] = new TH1D(histname_tmp.Data(),"",nptBins[ybin],ptBins_0);
    h_pt_highMu[ybin]->Sumw2();

    if (m_unfolding) {
      histname_tmp  = "h_pt_TM_";
      histname_tmp += ybin;
      h_pt_TM[ybin] = new TH2D(histname_tmp.Data(),"",nptBins[0],ptBins_0,nptBins[0],ptBins_0);
      h_pt_TM[ybin]->Sumw2();
      if(m_bootstraps){
        histname_tmp  = "boot_inc_";
        histname_tmp += ybin;
        fBootstrap_inc[ybin] = new TH2DBootstrap(histname_tmp.Data(), "", nptBins[0], ptBins_0, nptBins[0], ptBins_0, m_nReplicas, fGenerator);
        fBootstrap_inc[ybin]->Sumw2();

        histname_tmp  = "boot_inc_reco_";
        histname_tmp += ybin;
        fRecoBootstrap_inc[ybin] = new TH1DBootstrap(histname_tmp.Data(), "", nptBins[0], ptBins_0,m_nReplicas, fGenerator);
        fRecoBootstrap_inc[ybin]->Sumw2();

        histname_tmp  = "boot_inc_truth_";
        histname_tmp += ybin;
        fTruthBootstrap_inc[ybin] = new TH1DBootstrap(histname_tmp.Data(), "", nptBins[0], ptBins_0,m_nReplicas, fGenerator);
        fTruthBootstrap_inc[ybin]->Sumw2();
      }
    }
  }// END: Loop over |y| bins

  // pT distributions by trigger
  std::map<std::string,TH1D*> h_pT_Triggers;
  std::map<std::string,TH1D*> h_pT_j0_Triggers;
  std::map<std::string,TH1D*> h_pT_j1_Triggers;
  std::map<std::string,TH1D*> h_pT_j0_Triggers_eta3;
  std::map<std::string,TH1D*> h_pT_j0_Triggers_eta2;
  std::map<std::string,TH1D*> h_pT_j0_Triggers_eta1;
  TString BaseName_pT = "h_pt_";
  TString BaseName_pT_j0 = "h_pt_j0_";
  TString BaseName_pT_j0_eta3 = "h_pt_j0_eta3_";
  TString BaseName_pT_j0_eta2 = "h_pt_j0_eta2_";
  TString BaseName_pT_j0_eta1 = "h_pt_j0_eta1_";
  TString BaseName_pT_j1 = "h_pt_j1_";
  TString TempName_pT = "";
  TString TempName_pT_j0 = "";
  TString TempName_pT_j0_eta3 = "";
  TString TempName_pT_j0_eta2 = "";
  TString TempName_pT_j0_eta1 = "";
  TString TempName_pT_j1 = "";
  for(unsigned int m=0;m<Triggers.size();m++){
    TempName_pT    = BaseName_pT    + Triggers.at(m);
    TempName_pT_j0 = BaseName_pT_j0 + Triggers.at(m);
    TempName_pT_j0_eta3 = BaseName_pT_j0_eta3 + Triggers.at(m);
    TempName_pT_j0_eta2 = BaseName_pT_j0_eta2 + Triggers.at(m);
    TempName_pT_j0_eta1 = BaseName_pT_j0_eta1 + Triggers.at(m);
    TempName_pT_j1 = BaseName_pT_j1 + Triggers.at(m);
    h_pT_Triggers[Triggers.at(m)] = new TH1D(TempName_pT.Data(),"",500,0,2000);
    h_pT_Triggers[Triggers.at(m)]->Sumw2();
    h_pT_j0_Triggers[Triggers.at(m)] = new TH1D(TempName_pT_j0.Data(),"",500,0,2000);
    h_pT_j0_Triggers[Triggers.at(m)]->Sumw2();
    h_pT_j0_Triggers_eta3[Triggers.at(m)] = new TH1D(TempName_pT_j0_eta3.Data(),"",500,0,2000);
    h_pT_j0_Triggers_eta3[Triggers.at(m)]->Sumw2();
    h_pT_j0_Triggers_eta2[Triggers.at(m)] = new TH1D(TempName_pT_j0_eta2.Data(),"",500,0,2000);
    h_pT_j0_Triggers_eta2[Triggers.at(m)]->Sumw2();
    h_pT_j0_Triggers_eta1[Triggers.at(m)] = new TH1D(TempName_pT_j0_eta1.Data(),"",500,0,2000);
    h_pT_j0_Triggers_eta1[Triggers.at(m)]->Sumw2();
    h_pT_j1_Triggers[Triggers.at(m)] = new TH1D(TempName_pT_j1.Data(),"",500,0,2000);
    h_pT_j1_Triggers[Triggers.at(m)]->Sumw2();
  }

  //------------------
  // NPV distributions
  //------------------
  TH1D *h_NPV = new TH1D("h_NPV","",100,0,100);
  h_NPV->Sumw2();
  TH1D *h_NPV_wJetSelection = new TH1D("h_NPV_wJetSelection","",100,0,100);
  h_NPV_wJetSelection->Sumw2();
  TH1D *h_NPV_mu_14_21 = new TH1D("h_NPV_mu_14_21","",200,0,100);
  h_NPV_mu_14_21->Sumw2();
  TH1D *h_NPV_mu_21_28 = new TH1D("h_NPV_mu_21_28","",200,0,100);
  h_NPV_mu_21_28->Sumw2();
  TH1D *h_NPV_inclusive = new TH1D("h_NPV_inclusive","",100,0,100);
  h_NPV_inclusive->Sumw2();

  //-------------------
  // <mu> distributions
  //-------------------
  TH1D *h_mu = new TH1D("h_mu","",100,0,100);
  h_mu->Sumw2();
  TH1D *h_mu_wJetSelection = new TH1D("h_mu_wJetSelection","",100,0,100);
  h_mu_wJetSelection->Sumw2();
  TH1D *h_mu_inclusive = new TH1D("h_mu_inclusive","",100,0,100);
  h_mu_inclusive->Sumw2();
  TH1D *h_mu_dijet = new TH1D("h_mu_dijet","",100,0,100);
  h_mu_dijet->Sumw2();

  //------------------
  // rho distributions
  //------------------
  TH1D *h_rho = new TH1D("h_rho","",200,0,50);
  h_rho->Sumw2();
  TH1D *h_rho_without_wgt = new TH1D("h_rho_without_wgt","",200,0,50);
  TH1D *h_rho_wJetSelection = new TH1D("h_rho_wJetSelection","",200,0,50);
  h_rho_wJetSelection->Sumw2();
  TH1D *h_rho_inclusive = new TH1D("h_rho_inclusive","",200,0,50);
  h_rho_inclusive->Sumw2();

  //---------------------
  // jet_TrackWidthPt1000
  //---------------------
  TH1D *h_trackWIDTH = new TH1D("h_trackWIDTH","",200,0,0.4);
  h_trackWIDTH->Sumw2();
  TH1D *h_trackWIDTH_pT_1000plus = new TH1D("h_trackWIDTH_pT_1000plus","",100,0,0.4);
  h_trackWIDTH_pT_1000plus->Sumw2();
  TH1D *h_trackWIDTH_pT_900plus = new TH1D("h_trackWIDTH_pT_900plus","",100,0,0.4);
  h_trackWIDTH_pT_900plus->Sumw2();
  TH1D *h_trackWIDTH_pT_800plus = new TH1D("h_trackWIDTH_pT_800plus","",100,0,0.4);
  h_trackWIDTH_pT_800plus->Sumw2();
  TH1D *h_trackWIDTH_pT_894_1310 = new TH1D("h_trackWIDTH_pT_894_1310","",100,0,0.4);
  h_trackWIDTH_pT_894_1310->Sumw2();
  TH1D *h_trackWIDTH_pT_1310_1992 = new TH1D("h_trackWIDTH_pT_1310_1992","",100,0,0.4);
  h_trackWIDTH_pT_1310_1992->Sumw2();

  //---------------------
  // jet_NumTrkPt1000
  //---------------------
  TH1D *h_nTrk = new TH1D("h_nTrk","",400,0,40);
  h_nTrk->Sumw2();
  TH1D *h_nTrk_pT_1000plus = new TH1D("h_nTrk_pT_1000plus","",20,0,40);
  h_nTrk_pT_1000plus->Sumw2();
  TH1D *h_nTrk_pT_900plus = new TH1D("h_nTrk_pT_900plus","",20,0,40);
  h_nTrk_pT_900plus->Sumw2();
  TH1D *h_nTrk_pT_800plus = new TH1D("h_nTrk_pT_800plus","",20,0,40);
  h_nTrk_pT_800plus->Sumw2();
  TH1D *h_nTrk_pT_894_1310 = new TH1D("h_nTrk_pT_894_1310","",20,0,40);
  h_nTrk_pT_894_1310->Sumw2();
  TH1D *h_nTrk_pT_1310_1992 = new TH1D("h_nTrk_pT_1310_1992","",20,0,40);
  h_nTrk_pT_1310_1992->Sumw2();

  //------------------------
  // Jet_width distributions
  //------------------------
  
  TH1D *h_jet_width = new TH1D("h_jet_width","",24,0,0.3);
  h_jet_width->Sumw2();
  TH1D *h_jet_width_pT642 = new TH1D("h_jet_width_pT642","",24,0,0.3);
  h_jet_width_pT642->Sumw2();
  /*
  TH1D *h_jet_width_mu_14_21 = new TH1D("h_jet_width_mu_14_21","",24,0,0.3);
  h_jet_width_mu_14_21->Sumw2();
  TH1D *h_jet_width_mu_21_28 = new TH1D("h_jet_width_mu_21_28","",24,0,0.3);
  h_jet_width_mu_21_28->Sumw2();
  */

  //------------------------
  // Nsegments distribution
  //------------------------
  
  TH1D *h_jet_Nsegments = new TH1D("h_jet_Nsegments","",1000,0,1000);
  h_jet_Nsegments->Sumw2();
  
  //------------------------
  // EM3 distribution
  //------------------------
  //TH1D *h_jet_EM3 = new TH1D("h_jet_EM3","",300,-0.05,0.2);
  //h_jet_EM3->Sumw2();

  //------------------------
  // Tile0 distribution
  //------------------------
  //TH1D *h_jet_Tile0 = new TH1D("h_jet_Tile0","",300,-0.1,1.0);
  //h_jet_Tile0->Sumw2();

  
  //-------------------------
  // Jet_EMFrac distributions
  //-------------------------

  TH1D *h_jet_EMFrac = new TH1D("h_jet_EMFrac","",100,0,1);
  h_jet_EMFrac->Sumw2();
  TH1D *h_jet_EMFrac_pT642 = new TH1D("h_jet_EMFrac_pT642","",100,0,1);
  h_jet_EMFrac_pT642->Sumw2();
  /*
  TH1D *h_jet_EMFrac_mu_14_21 = new TH1D("h_jet_EMFrac_mu_14_21","",100,0,1);
  h_jet_EMFrac_mu_14_21->Sumw2();
  TH1D *h_jet_EMFrac_mu_21_28 = new TH1D("h_jet_EMFrac_mu_21_28","",100,0,1);
  h_jet_EMFrac_mu_21_28->Sumw2();
  */

  //----------------
  // y distributions
  //----------------

  TH1D *h_y_152pT216 = new TH1D("h_y_152pT216","",nyBinsyHisto,yBinsyHisto);
  h_y_152pT216->Sumw2();
  TH1D *h_y_216pT408 = new TH1D("h_y_216pT408","",nyBinsyHisto,yBinsyHisto);
  h_y_216pT408->Sumw2();
  TH1D *h_y_408pT642 = new TH1D("h_y_408pT642","",nyBinsyHisto,yBinsyHisto);
  h_y_408pT642->Sumw2();
  TH1D *h_y_642pT1992 = new TH1D("h_y_642pT1992","",nyBinsyHisto,yBinsyHisto);
  h_y_642pT1992->Sumw2();

  //------------------
  // eta distributions
  //------------------
  TH1D *h_eta = new TH1D("h_eta","",netaBins,etaBins);
  h_eta->Sumw2();
  TH1D *h_eta_j0 = new TH1D("h_eta_j0","",netaBins,etaBins);
  h_eta_j0->Sumw2();
  TH1D *h_eta_j1 = new TH1D("h_eta_j1","",netaBins,etaBins);
  h_eta_j1->Sumw2();
  TH1D *h_eta_j0andj1 = new TH1D("h_eta_j0andj1","",netaBins,etaBins);
  h_eta_j0andj1->Sumw2();
  TH1D *h_eta_pT_150 = new TH1D("h_eta_pT_150","",netaBins,etaBins);
  h_eta_pT_150->Sumw2();
  TH1D *h_eta_pT_250 = new TH1D("h_eta_pT_250","",netaBins,etaBins);
  h_eta_pT_250->Sumw2();
  TH1D *h_eta_pT_500 = new TH1D("h_eta_pT_500","",netaBins,etaBins);
  h_eta_pT_500->Sumw2();
  TH1D *h_eta_pT_642 = new TH1D("h_eta_pT_642","",netaBins,etaBins);
  h_eta_pT_642->Sumw2();
  TH1D *h_eta_mu_14_21 = new TH1D("h_eta_mu_14_21","",netaBins,etaBins);
  h_eta_mu_14_21->Sumw2();
  TH1D *h_eta_mu_21_28 = new TH1D("h_eta_mu_21_28","",netaBins,etaBins);
  h_eta_mu_21_28->Sumw2();
  TH1D *h_eta_GabrielSelection = new TH1D("h_eta_GabrielSelection","",netaBins,etaBins);
  h_eta_GabrielSelection->Sumw2();
  TH1D *h_eta_300_400 = new TH1D("h_eta_300_400","",netaBins,etaBins);
  h_eta_300_400->Sumw2();
  TH1D *h_eta_400_500 = new TH1D("h_eta_400_500","",netaBins,etaBins);
  h_eta_400_500->Sumw2();
  TH1D *h_eta_500_600 = new TH1D("h_eta_500_600","",netaBins,etaBins);
  h_eta_500_600->Sumw2();
  TH1D *h_eta_600_700 = new TH1D("h_eta_600_700","",netaBins,etaBins);
  h_eta_600_700->Sumw2();
  TH1D *h_eta_700_900 = new TH1D("h_eta_700_900","",netaBins,etaBins);
  h_eta_700_900->Sumw2();
  TH1D *h_eta_500_700 = new TH1D("h_eta_500_700","",netaBins,etaBins);
  h_eta_500_700->Sumw2();
  TH1D *h_eta_900_1100 = new TH1D("h_eta_900_1100","",netaBins,etaBins);
  h_eta_900_1100->Sumw2();
  TH1D *h_eta_1100plus = new TH1D("h_eta_1100plus","",netaBins,etaBins);
  h_eta_1100plus->Sumw2();
  TH1D *h_eta_j0_300_400 = new TH1D("h_eta_j0_300_400","",netaBins,etaBins);
  h_eta_j0_300_400->Sumw2();
  TH1D *h_eta_j0_400_500 = new TH1D("h_eta_j0_400_500","",netaBins,etaBins);
  h_eta_j0_400_500->Sumw2();
  TH1D *h_eta_j0_500_600 = new TH1D("h_eta_j0_500_600","",netaBins,etaBins);
  h_eta_j0_500_600->Sumw2();
  TH1D *h_eta_j0_600_700 = new TH1D("h_eta_j0_600_700","",netaBins,etaBins);
  h_eta_j0_600_700->Sumw2();
  TH1D *h_eta_j0_700_900 = new TH1D("h_eta_j0_700_900","",netaBins,etaBins);
  h_eta_j0_700_900->Sumw2();
  TH1D *h_eta_j1_300_400 = new TH1D("h_eta_j1_300_400","",netaBins,etaBins);
  h_eta_j1_300_400->Sumw2();
  TH1D *h_eta_j1_400_500 = new TH1D("h_eta_j1_400_500","",netaBins,etaBins);
  h_eta_j1_400_500->Sumw2();
  TH1D *h_eta_j1_500_600 = new TH1D("h_eta_j1_500_600","",netaBins,etaBins);
  h_eta_j1_500_600->Sumw2();
  TH1D *h_eta_j1_600_700 = new TH1D("h_eta_j1_600_700","",netaBins,etaBins);
  h_eta_j1_600_700->Sumw2();
  TH1D *h_eta_j1_700_900 = new TH1D("h_eta_j1_700_900","",netaBins,etaBins);
  h_eta_j1_700_900->Sumw2();
  
  std::map<std::string,TH1D*> h_eta_Triggers;
  std::map<std::string,TH1D*> h_eta_j0_Triggers;
  std::map<std::string,TH1D*> h_eta_j1_Triggers;
  TString BaseName_Eta = "h_eta_";
  TString BaseName_Eta_j0 = "h_eta_j0_";
  TString BaseName_Eta_j1 = "h_eta_j1_";
  TString TempName_Eta = "";
  TString TempName_Eta_j0 = "";
  TString TempName_Eta_j1 = "";
  for(unsigned int m=firstTrig;m<Triggers.size();m++){
    TempName_Eta = BaseName_Eta + Triggers.at(m);
    TempName_Eta_j0 = BaseName_Eta_j0 + Triggers.at(m);
    TempName_Eta_j1 = BaseName_Eta_j1 + Triggers.at(m);
    h_eta_Triggers[Triggers.at(m)] = new TH1D(TempName_Eta.Data(),"",netaBins,etaBins);  
    h_eta_Triggers[Triggers.at(m)]->Sumw2();  
    h_eta_j0_Triggers[Triggers.at(m)] = new TH1D(TempName_Eta_j0.Data(),"",netaBins,etaBins);  
    h_eta_j0_Triggers[Triggers.at(m)]->Sumw2();  
    h_eta_j1_Triggers[Triggers.at(m)] = new TH1D(TempName_Eta_j1.Data(),"",netaBins,etaBins);  
    h_eta_j1_Triggers[Triggers.at(m)]->Sumw2();  
  }

  std::map<int, TH1D*> h_eta_pTBins;
  std::map<int, TH1D*> h_eta_j0_pTBins;
  std::map<int, TH1D*> h_eta_j1_pTBins;
  TString BaseName_Eta_pTBins = "h_eta_pTBin";
  TString BaseName_Eta_j0_pTBins = "h_eta_j0_pTBin";
  TString BaseName_Eta_j1_pTBins = "h_eta_j1_pTBin";
  TempName_Eta = "";
  TempName_Eta_j0 = "";
  TempName_Eta_j1 = "";
  for(int m=22;m<43;m++){
    TempName_Eta = BaseName_Eta_pTBins; TempName_Eta += m;
    TempName_Eta_j0 = BaseName_Eta_j0_pTBins; TempName_Eta_j0 += m;
    TempName_Eta_j1 = BaseName_Eta_j1_pTBins; TempName_Eta_j1 += m;
    h_eta_pTBins[m-22] = new TH1D(TempName_Eta.Data(),"",netaBins,etaBins);  
    h_eta_pTBins[m-22]->Sumw2();  
    h_eta_j0_pTBins[m-22] = new TH1D(TempName_Eta_j0.Data(),"",netaBins,etaBins);  
    h_eta_j0_pTBins[m-22]->Sumw2();  
    h_eta_j1_pTBins[m-22] = new TH1D(TempName_Eta_j1.Data(),"",netaBins,etaBins);  
    h_eta_j1_pTBins[m-22]->Sumw2();  
  }

  std::map<std::string,TH1D*> h_eta_Triggers_phi_0_16;
  std::map<std::string,TH1D*> h_eta_Triggers_phi_16_32;
  std::map<std::string,TH1D*> h_eta_Triggers_phi_32_48;
  std::map<std::string,TH1D*> h_eta_Triggers_phi_48_64;
  std::map<std::string,TH1D*> h_eta_j0_Triggers_phi_0_16;
  std::map<std::string,TH1D*> h_eta_j0_Triggers_phi_16_32;
  std::map<std::string,TH1D*> h_eta_j0_Triggers_phi_32_48;
  std::map<std::string,TH1D*> h_eta_j0_Triggers_phi_48_64;
  std::map<std::string,TH1D*> h_eta_j1_Triggers_phi_0_16;
  std::map<std::string,TH1D*> h_eta_j1_Triggers_phi_16_32;
  std::map<std::string,TH1D*> h_eta_j1_Triggers_phi_32_48;
  std::map<std::string,TH1D*> h_eta_j1_Triggers_phi_48_64;
  TString BaseName_Eta_phi_0_16 = "h_eta_phi_0_16_";
  TString BaseName_Eta_phi_16_32 = "h_eta_phi_16_32_";
  TString BaseName_Eta_phi_32_48 = "h_eta_phi_32_48_";
  TString BaseName_Eta_phi_48_64 = "h_eta_phi_48_64_";
  TString BaseName_Eta_j0_phi_0_16 = "h_eta_j0_phi_0_16_";
  TString BaseName_Eta_j0_phi_16_32 = "h_eta_j0_phi_16_32_";
  TString BaseName_Eta_j0_phi_32_48 = "h_eta_j0_phi_32_48_";
  TString BaseName_Eta_j0_phi_48_64 = "h_eta_j0_phi_48_64_";
  TString BaseName_Eta_j1_phi_0_16 = "h_eta_j1_phi_0_16_";
  TString BaseName_Eta_j1_phi_16_32 = "h_eta_j1_phi_16_32_";
  TString BaseName_Eta_j1_phi_32_48 = "h_eta_j1_phi_32_48_";
  TString BaseName_Eta_j1_phi_48_64 = "h_eta_j1_phi_48_64_";
  TempName_Eta = "";
  TempName_Eta_j0 = "";
  TempName_Eta_j1 = "";
  for(unsigned int m=firstTrig;m<Triggers.size();m++){
    TempName_Eta = BaseName_Eta_phi_0_16 + Triggers.at(m);
    h_eta_Triggers_phi_0_16[Triggers.at(m)] = new TH1D(TempName_Eta.Data(),"",netaBins,etaBins);  
    h_eta_Triggers_phi_0_16[Triggers.at(m)]->Sumw2();  
    TempName_Eta = BaseName_Eta_j0_phi_0_16 + Triggers.at(m);
    h_eta_j0_Triggers_phi_0_16[Triggers.at(m)] = new TH1D(TempName_Eta.Data(),"",netaBins,etaBins);  
    h_eta_j0_Triggers_phi_0_16[Triggers.at(m)]->Sumw2();  
    TempName_Eta = BaseName_Eta_j1_phi_0_16 + Triggers.at(m);
    h_eta_j1_Triggers_phi_0_16[Triggers.at(m)] = new TH1D(TempName_Eta.Data(),"",netaBins,etaBins);  
    h_eta_j1_Triggers_phi_0_16[Triggers.at(m)]->Sumw2();  
    TempName_Eta = BaseName_Eta_phi_16_32 + Triggers.at(m);
    h_eta_Triggers_phi_16_32[Triggers.at(m)] = new TH1D(TempName_Eta.Data(),"",netaBins,etaBins);  
    h_eta_Triggers_phi_16_32[Triggers.at(m)]->Sumw2();  
    TempName_Eta = BaseName_Eta_j0_phi_16_32 + Triggers.at(m);
    h_eta_j0_Triggers_phi_16_32[Triggers.at(m)] = new TH1D(TempName_Eta.Data(),"",netaBins,etaBins);  
    h_eta_j0_Triggers_phi_16_32[Triggers.at(m)]->Sumw2();  
    TempName_Eta = BaseName_Eta_j1_phi_16_32 + Triggers.at(m);
    h_eta_j1_Triggers_phi_16_32[Triggers.at(m)] = new TH1D(TempName_Eta.Data(),"",netaBins,etaBins);  
    h_eta_j1_Triggers_phi_16_32[Triggers.at(m)]->Sumw2();  
    TempName_Eta = BaseName_Eta_phi_32_48 + Triggers.at(m);
    h_eta_Triggers_phi_32_48[Triggers.at(m)] = new TH1D(TempName_Eta.Data(),"",netaBins,etaBins);  
    h_eta_Triggers_phi_32_48[Triggers.at(m)]->Sumw2();  
    TempName_Eta = BaseName_Eta_j0_phi_32_48 + Triggers.at(m);
    h_eta_j0_Triggers_phi_32_48[Triggers.at(m)] = new TH1D(TempName_Eta.Data(),"",netaBins,etaBins);  
    h_eta_j0_Triggers_phi_32_48[Triggers.at(m)]->Sumw2();  
    TempName_Eta = BaseName_Eta_j1_phi_32_48 + Triggers.at(m);
    h_eta_j1_Triggers_phi_32_48[Triggers.at(m)] = new TH1D(TempName_Eta.Data(),"",netaBins,etaBins);  
    h_eta_j1_Triggers_phi_32_48[Triggers.at(m)]->Sumw2();  
    TempName_Eta = BaseName_Eta_phi_48_64 + Triggers.at(m);
    h_eta_Triggers_phi_48_64[Triggers.at(m)] = new TH1D(TempName_Eta.Data(),"",netaBins,etaBins);  
    h_eta_Triggers_phi_48_64[Triggers.at(m)]->Sumw2();  
    TempName_Eta = BaseName_Eta_j0_phi_48_64 + Triggers.at(m);
    h_eta_j0_Triggers_phi_48_64[Triggers.at(m)] = new TH1D(TempName_Eta.Data(),"",netaBins,etaBins);  
    h_eta_j0_Triggers_phi_48_64[Triggers.at(m)]->Sumw2();  
    TempName_Eta = BaseName_Eta_j1_phi_48_64 + Triggers.at(m);
    h_eta_j1_Triggers_phi_48_64[Triggers.at(m)] = new TH1D(TempName_Eta.Data(),"",netaBins,etaBins);  
    h_eta_j1_Triggers_phi_48_64[Triggers.at(m)]->Sumw2();  
  }
  
  std::map<std::string,TH1D*> h_eta_Triggers_finerbins;
  std::map<std::string,TH1D*> h_eta_j0_Triggers_finerbins;
  std::map<std::string,TH1D*> h_eta_j1_Triggers_finerbins;
  TString BaseName_Eta_finerbins = "h_eta_finerBins_";
  TString BaseName_Eta_j0_finerbins = "h_eta_j0_finerBins_";
  TString BaseName_Eta_j1_finerbins = "h_eta_j1_finerBins_";
  TString TempName_Eta_finerbins = "";
  TString TempName_Eta_j0_finerbins = "";
  TString TempName_Eta_j1_finerbins = "";
  for(unsigned int m=firstTrig;m<Triggers.size();m++){
    TempName_Eta_finerbins = BaseName_Eta_finerbins + Triggers.at(m);
    TempName_Eta_j0_finerbins = BaseName_Eta_j0_finerbins + Triggers.at(m);
    TempName_Eta_j1_finerbins = BaseName_Eta_j1_finerbins + Triggers.at(m);
    h_eta_Triggers_finerbins[Triggers.at(m)] = new TH1D(TempName_Eta_finerbins.Data(),"",nfineretaBins,fineretaBins);  
    h_eta_Triggers_finerbins[Triggers.at(m)]->Sumw2();  
    h_eta_j0_Triggers_finerbins[Triggers.at(m)] = new TH1D(TempName_Eta_j0_finerbins.Data(),"",nfineretaBins,fineretaBins);  
    h_eta_j0_Triggers_finerbins[Triggers.at(m)]->Sumw2();  
    h_eta_j1_Triggers_finerbins[Triggers.at(m)] = new TH1D(TempName_Eta_j1_finerbins.Data(),"",nfineretaBins,fineretaBins);  
    h_eta_j1_Triggers_finerbins[Triggers.at(m)]->Sumw2();  
  }
  
  //------------------
  // phi distributions
  //------------------
  TH1D *h_phi  = new TH1D("h_phi","",nphiBins,phiBins);
  h_phi->Sumw2();
  TH1D *h_phi_pT_150 = new TH1D("h_phi_pT_150","",nphiBins,phiBins);
  h_phi_pT_150->Sumw2();
  TH1D *h_phi_pT_250 = new TH1D("h_phi_pT_250","",nphiBins,phiBins);
  h_phi_pT_250->Sumw2();
  TH1D *h_phi_pT_500 = new TH1D("h_phi_pT_500","",nphiBins,phiBins);
  h_phi_pT_500->Sumw2();
  TH1D *h_phi_pT_642 = new TH1D("h_phi_pT_642","",nphiBins,phiBins);
  h_phi_pT_642->Sumw2();
  TH1D *h_phi_mu_14_21  = new TH1D("h_phi_mu_14_21","",nphiBins,phiBins);
  h_phi_mu_14_21->Sumw2();
  TH1D *h_phi_mu_21_28  = new TH1D("h_phi_mu_21_28","",nphiBins,phiBins);
  h_phi_mu_21_28->Sumw2();
  TH1D *h_phi_yBins[nyBins];
  for(int ybin=0;ybin<nyBins;++ybin){
    histname_tmp  = "h_phi_yBin_";
    histname_tmp += ybin;
    h_phi_yBins[ybin] = new TH1D(histname_tmp.Data(),"",nphiBins,phiBins);
    h_phi_yBins[ybin]->Sumw2();
  }

  TH1D *h_phi_eta_15_25 = new TH1D("h_phi_eta_15_25","",nphiBins,phiBins);
  h_phi_eta_15_25->Sumw2();
  TH1D *h_phi_eta_20_25 = new TH1D("h_phi_eta_20_25","",nphiBins,phiBins);
  h_phi_eta_20_25->Sumw2();
  TH1D *h_phi_eta_175_225 = new TH1D("h_phi_eta_175_225","",nphiBins,phiBins);
  h_phi_eta_175_225->Sumw2();

  TH1D *h_phi_eta_15_25_pT_100_300 = new TH1D("h_phi_eta_15_25_pT_100_300","",nphiBins,phiBins);
  h_phi_eta_15_25_pT_100_300->Sumw2();
  TH1D *h_phi_eta_20_25_pT_100_300 = new TH1D("h_phi_eta_20_25_pT_100_300","",nphiBins,phiBins);
  h_phi_eta_20_25_pT_100_300->Sumw2();
  TH1D *h_phi_eta_175_225_pT_100_300 = new TH1D("h_phi_eta_175_225_pT_100_300","",nphiBins,phiBins);
  h_phi_eta_175_225_pT_100_300->Sumw2();

  TH1D *h_phi_eta_15_25_pT_300_500 = new TH1D("h_phi_eta_15_25_pT_300_500","",nphiBins,phiBins);
  h_phi_eta_15_25_pT_300_500->Sumw2();
  TH1D *h_phi_eta_20_25_pT_300_500 = new TH1D("h_phi_eta_20_25_pT_300_500","",nphiBins,phiBins);
  h_phi_eta_20_25_pT_300_500->Sumw2();
  TH1D *h_phi_eta_175_225_pT_300_500 = new TH1D("h_phi_eta_175_225_pT_300_500","",nphiBins,phiBins);
  h_phi_eta_175_225_pT_300_500->Sumw2();

  TH1D *h_phi_eta_15_25_pT_642_1992 = new TH1D("h_phi_eta_15_25_pT_642_1992","",nphiBins,phiBins);
  h_phi_eta_15_25_pT_642_1992->Sumw2();
  TH1D *h_phi_eta_20_25_pT_642_1992 = new TH1D("h_phi_eta_20_25_pT_642_1992","",nphiBins,phiBins);
  h_phi_eta_20_25_pT_642_1992->Sumw2();
  TH1D *h_phi_eta_175_225_pT_642_1992 = new TH1D("h_phi_eta_175_225_pT_642_1992","",nphiBins,phiBins);
  h_phi_eta_175_225_pT_642_1992->Sumw2();

  TH1D *h_phi_eta_24_25_pT_442 = new TH1D("h_phi_eta_24_25_pT_442","",nphiBins,phiBins);
  h_phi_eta_24_25_pT_442->Sumw2();
  TH1D *h_phi_eta_24_25_pT_408 = new TH1D("h_phi_eta_24_25_pT_408","",nphiBins,phiBins);
  h_phi_eta_24_25_pT_408->Sumw2();
  TH1D *h_phi_eta_24_25_pT_642 = new TH1D("h_phi_eta_24_25_pT_642","",nphiBins,phiBins);
  h_phi_eta_24_25_pT_642->Sumw2();
  TH1D *h_phi_eta_24_25_pT_264 = new TH1D("h_phi_eta_24_25_pT_264","",nphiBins,phiBins);
  h_phi_eta_24_25_pT_264->Sumw2();
  TH1D *h_phi_eta_24_25_pT_346 = new TH1D("h_phi_eta_24_25_pT_346","",nphiBins,phiBins);
  h_phi_eta_24_25_pT_346->Sumw2();

  // Additional phi plots to study disagreement in Data/MC around phi~0 

  TH1D *h_phi_abseta_15_20_pT150 = new TH1D("h_phi_abseta_15_20_pT150","",nphiBins,phiBins);
  h_phi_abseta_15_20_pT150->Sumw2();
  TH1D *h_phi_abseta_15_20_pT250 = new TH1D("h_phi_abseta_15_20_pT250","",nphiBins,phiBins);
  h_phi_abseta_15_20_pT250->Sumw2();
  TH1D *h_phi_abseta_15_20_pT500 = new TH1D("h_phi_abseta_15_20_pT500","",nphiBins,phiBins);
  h_phi_abseta_15_20_pT500->Sumw2();
  TH1D *h_phi_abseta_15_20_pT642 = new TH1D("h_phi_abseta_15_20_pT642","",nphiBins,phiBins);
  h_phi_abseta_15_20_pT642->Sumw2();
  TH1D *h_phi_abseta_15_20_pT1100 = new TH1D("h_phi_abseta_15_20_pT1100","",nphiBins,phiBins);
  h_phi_abseta_15_20_pT1100->Sumw2();


  TH1D *h_phi_abseta_15_175 = new TH1D("h_phi_abseta_15_175","",nphiBins,phiBins);
  h_phi_abseta_15_175->Sumw2();
  TH1D *h_phi_abseta_175_20 = new TH1D("h_phi_abseta_175_20","",nphiBins,phiBins);
  h_phi_abseta_175_20->Sumw2();
  TH1D *h_phi_poseta_15_175 = new TH1D("h_phi_poseta_15_175","",nphiBins,phiBins);
  h_phi_poseta_15_175->Sumw2();
  TH1D *h_phi_poseta_175_20 = new TH1D("h_phi_poseta_175_20","",nphiBins,phiBins);
  h_phi_poseta_175_20->Sumw2();
  TH1D *h_phi_negeta_15_175 = new TH1D("h_phi_negeta_15_175","",nphiBins,phiBins);
  h_phi_negeta_15_175->Sumw2();
  TH1D *h_phi_negeta_175_20 = new TH1D("h_phi_negeta_175_20","",nphiBins,phiBins);
  h_phi_negeta_175_20->Sumw2();

  TH1D *h_phi_abseta_15_16 = new TH1D("h_phi_abseta_15_16","",nphiBins,phiBins);
  h_phi_abseta_15_16->Sumw2();
  TH1D *h_phi_abseta_16_17 = new TH1D("h_phi_abseta_16_17","",nphiBins,phiBins);
  h_phi_abseta_16_17->Sumw2();
  TH1D *h_phi_abseta_17_18 = new TH1D("h_phi_abseta_17_18","",nphiBins,phiBins);
  h_phi_abseta_17_18->Sumw2();
  TH1D *h_phi_abseta_18_19 = new TH1D("h_phi_abseta_18_19","",nphiBins,phiBins);
  h_phi_abseta_18_19->Sumw2();
  TH1D *h_phi_abseta_19_20 = new TH1D("h_phi_abseta_19_20","",nphiBins,phiBins);
  h_phi_abseta_19_20->Sumw2();

  TH1D *h_phi_poseta_15_16 = new TH1D("h_phi_poseta_15_16","",nphiBins,phiBins);
  h_phi_poseta_15_16->Sumw2();
  TH1D *h_phi_poseta_16_17 = new TH1D("h_phi_poseta_16_17","",nphiBins,phiBins);
  h_phi_poseta_16_17->Sumw2();
  TH1D *h_phi_poseta_17_18 = new TH1D("h_phi_poseta_17_18","",nphiBins,phiBins);
  h_phi_poseta_17_18->Sumw2();
  TH1D *h_phi_poseta_18_19 = new TH1D("h_phi_poseta_18_19","",nphiBins,phiBins);
  h_phi_poseta_18_19->Sumw2();
  TH1D *h_phi_poseta_19_20 = new TH1D("h_phi_poseta_19_20","",nphiBins,phiBins);
  h_phi_poseta_19_20->Sumw2();

  TH1D *h_phi_negeta_15_16 = new TH1D("h_phi_negeta_15_16","",nphiBins,phiBins);
  h_phi_negeta_15_16->Sumw2();
  TH1D *h_phi_negeta_16_17 = new TH1D("h_phi_negeta_16_17","",nphiBins,phiBins);
  h_phi_negeta_16_17->Sumw2();
  TH1D *h_phi_negeta_17_18 = new TH1D("h_phi_negeta_17_18","",nphiBins,phiBins);
  h_phi_negeta_17_18->Sumw2();
  TH1D *h_phi_negeta_18_19 = new TH1D("h_phi_negeta_18_19","",nphiBins,phiBins);
  h_phi_negeta_18_19->Sumw2();
  TH1D *h_phi_negeta_19_20 = new TH1D("h_phi_negeta_19_20","",nphiBins,phiBins);
  h_phi_negeta_19_20->Sumw2();


  //-------------
  // Eta-Phi Maps
  //-------------

  TH2D *h_eta_phi = new TH2D("h_eta_phi","",netaBins,etaBins,nphiBins,phiBins);
  h_eta_phi->Sumw2();
  TH2D *h_eta_phi_150pT200 = new TH2D("h_eta_phi_150pT200","",netaBins,etaBins,nphiBins,phiBins);
  h_eta_phi_150pT200->Sumw2();
  TH2D *h_eta_phi_300pT500 = new TH2D("h_eta_phi_300pT500","",netaBins,etaBins,nphiBins,phiBins);
  h_eta_phi_300pT500->Sumw2();
  TH2D *h_eta_phi_442pT642 = new TH2D("h_eta_phi_442pT642","",netaBins,etaBins,nphiBins,phiBins);
  h_eta_phi_442pT642->Sumw2();
  TH2D *h_eta_phi_442pT1992 = new TH2D("h_eta_phi_442pT1992","",netaBins,etaBins,nphiBins,phiBins);
  h_eta_phi_442pT1992->Sumw2();
  TH2D *h_eta_phi_642pT1992 = new TH2D("h_eta_phi_642pT1992","",netaBins,etaBins,nphiBins,phiBins);
  h_eta_phi_642pT1992->Sumw2();
  TH2D *h_eta_phi_mu_14_21 = new TH2D("h_eta_phi_mu_14_21","",netaBins,etaBins,nphiBins,phiBins);
  h_eta_phi_mu_14_21->Sumw2();
  TH2D *h_eta_phi_mu_21_28 = new TH2D("h_eta_phi_mu_21_28","",netaBins,etaBins,nphiBins,phiBins);
  h_eta_phi_mu_21_28->Sumw2();

  // Initializing Number of Final Jets
  int nInclusiveJets_CentralRegion = 0;
  int nInclusiveJets_y_Inclusive = 0;
  int nDijets_y_Inclusive = 0;

  float weight = 1.;
  
  // Vector of jets for Dijets
  std::vector<TLorentzVector> RecoJets_y30;
  std::vector<TLorentzVector> TruthJets_y30;

  // Vector of jets for Inclusive jets
  std::vector<TLorentzVector> RecoJets;
  std::vector<TLorentzVector> TruthJets;

  // Cleaning
  int Skippedevents_Cleaning = 0; // Counter
  bool should_skip;

  int runnumberold=-999999;
  int runnumber;
  //----------------------------------------------------------------------------------------
  // Loop over entries
  //----------------------------------------------------------------------------------------

  std::cout << "Loop over entries" << std::endl;

  for (int i=0; i<entries; ++i) {

    should_skip = false;
    RecoJets_y30.clear();
    TruthJets_y30.clear();

    RecoJets.clear();
    TruthJets.clear();

    tree->GetEntry(i);

    // Show status
    if (i <100000)     {if(i % 10000== 0) std::cout << "Entry = " << i << " of " << entries << std::endl;}
    else if (i <10000) {if(i % 1000 == 0) std::cout << "Entry = " << i << " of " << entries << std::endl;}
    else if (i % 1000000 == 0) std::cout << "Entry = " << i << " of " << entries << std::endl;

    //
    // Filling Weight
    if(MC){
      if(m_PRW) weight = wgt_pileup*wgt;
      else{weight = wgt;}
    } else{weight = 1;}

    if(m_rel21) njet = jet_pt->size();
    if(m_rel21 && MC) ntruthjets = truthjet_pt->size();

    if(njet<1) continue; // no jets: skip event

    // Remove Pileup Events in MC
    if(MC){
      double pTavg = jet_pt->at(0);
      if(njet>1) pTavg = ( jet_pt->at(0) + jet_pt->at(1) ) / 2.0;
      if( ntruthjets == 0 || (pTavg/truthjet_pt->at(0)>1.4) ){
        continue; // skip event
      }
      if(m_generator=="Powheg" && runNumber==426005 && mceventNumber==1652845) continue; // See if it's still needed in MC15c TEMPORARY
    } 

    //-----------------
    // Event Cleaning
    //-----------------

    bool passJetCleaning;
    if(m_debug)std::cout << "Applying Event Jet Cleaning" << std::endl;
    for(int j=0;j<njet;++j){ // Loop over jets
      if(j==0) passJetCleaning = jet_clean_passTightBad->at(j); // TightBad for leading jets
      else{ passJetCleaning = jet_clean_passLooseBad->at(j);}   // LooseBad for the rest of jets
      if(jet_pt->at(j)>60 && passJetCleaning!=1){ // jet not passing cleaning criteria
	should_skip = true;
        if (m_debug) std::cout << "Skipped due to jet cleaning jet= "<<j<<" Entry = " << i << " of " << entries << std::endl;
        break;
      }
    }


    if(m_debug)std::cout << "Checking if event should be skipped" << std::endl;
    if(should_skip){
      Skippedevents_Cleaning++;
      continue; // skip event due to a non clean jet
    }

    //--------------------
    // Filling Histograms
    //--------------------
    
    // Fill Event Histograms
    if(m_debug) std::cout << "Filling Event histograms NPV= " <<NPV <<" weight= "<<weight<<" rho= "<<rho<< std::endl;
    
    h_NPV->Fill(NPV,weight);
    h_mu->Fill(mu,weight);
    if(mu>=14 && mu<14) h_NPV_mu_14_21->Fill(NPV,weight);
    if(mu>=21 && mu<28) h_NPV_mu_21_28->Fill(NPV,weight);
    h_rho->Fill(rho*0.001,weight);
    h_rho_without_wgt->Fill(rho*0.001,1);
    h_njet->Fill(njet,1);

    // Fill pT distributions for each trigger
    if(m_debug) std::cout << "Filling pT distributions for each trigger" << std::endl;
    if(!MC){
      for(unsigned int m=0;m<Triggers.size();m++){
        bool trigger_passed = false;
        for(unsigned int lll=0;lll<passedTriggers->size();lll++){// Loop over passed triggers
          if(passedTriggers->at(lll)==Triggers.at(m)) trigger_passed = true;
        }
        if(!trigger_passed) continue; // skip Trigger
        for(int j=0;j<njet;++j){
	 if (m_debug) std::cout << "Filling Triggers.at("<<m<<")]->Fill("<<jet_pt->at(j)<< std::endl;      	  
          h_pt_y_inclusive_triggers[Triggers.at(m)]->Fill(jet_pt->at(j));
        }
      }
    }
    
    // Fill Truth Jet Vector for Dijet Distributions
    if(MC){
      for(int j=0;j<ntruthjets;++j){
        TLorentzVector jet;
        jet.SetPtEtaPhiE( truthjet_pt->at(j), truthjet_eta->at(j), truthjet_phi->at(j), truthjet_E->at(j) );
        if(fabs(truthjet_rapidity->at(j))<3.0) TruthJets_y30.push_back(jet); // Saving jets for Dijets distributions
      }
    }

    if(m_unfolding && m_bootstraps){
      if(MC) fGenerator->Generate(runNumber, eventNumber, runNumber);
      else fGenerator->Generate(runNumber, eventNumber, -1);
    } 


    //---------------------------------
    // Filling Inclusive Distributions
    //---------------------------------

    if(m_debug) std::cout << "Filling Inclusive histograms" << std::endl;

    double pT_min = Lumiwgt[m_firstTrigger.Data()]["ptmin"];

    //    if(m_debug) std::cout << "pT_min = "<<pT_min<<" minPtTriggers["<<m_firstTrigger.Data()<<"]= "<<minPtTriggers[m_firstTrigger.Data()]<< std::endl;
    if(m_debug) std::cout << "pT_min = "<<pT_min<<" Lumiwgt["<<m_firstTrigger.Data()<<"]= "<<Lumiwgt[m_firstTrigger.Data()]["ptmin"]<< std::endl;        

    // Loop over truth jets to fill inclusive-jet distributions
    if(MC){
      for(int j=0;j<ntruthjets;++j){
        if(truthjet_pt->at(j)>=pT_min && fabs(truthjet_rapidity->at(j))<3.0){// Inclusive-jet selection

	  TLorentzVector Jet; 
	  Jet.SetPtEtaPhiE(truthjet_pt->at(j), truthjet_eta->at(j), truthjet_phi->at(j), truthjet_E->at(j));	  
	  TruthJets.push_back(Jet);

          for(unsigned int l=0;l<nyBins;++l){ // Looping over different y bins
            if( fabs(truthjet_rapidity->at(j)) >= yBins[l] && fabs(truthjet_rapidity->at(j))<yBins[l+1] ){
              h_pt_truth[l]->Fill(truthjet_pt->at(j),weight);
	      if(m_unfolding && m_bootstraps) fTruthBootstrap_inc[l]->Fill(truthjet_pt->at(j),weight);
            }
          }  
        }
      }  
    }

    int trigUnprescaled = TriggertoInt[m_unprescaledTrig.Data()]; 

    if(m_debug) std::cout << "trigUnprescaled= "<<trigUnprescaled<<std::endl;
    //-----------------------------------------------------------------------------------------------------
    // Loop over Jets
    //-----------------------------------------------------------------------------------------------------

    std::string trigger = "";

    if(m_debug) std::cout << "Loop over Jets njet= " << njet << std::endl;

    for(int j=0;j<njet;++j){

      // Why do we fill eta not y here ? 
      TLorentzVector jet;
      if(m_debug) std::cout << "Fill pt= " << jet_pt->at(j)<<" eta= "<<jet_eta->at(j)<<" phi= "<<jet_phi->at(j)<< std::endl;
      
      jet.SetPtEtaPhiE( jet_pt->at(j), jet_eta->at(j), jet_phi->at(j), jet_E->at(j) );

      if(fabs(jet_rapidity->at(j))<3.0) RecoJets_y30.push_back(jet); // Saving jets for Dijets distributions

      int trig = 0;

      if(m_debug) std::cout << "Applying Inclusive Jet Selection pt= "<<jet_pt->at(j)<<" y= "<<fabs(jet_rapidity->at(j))<< std::endl;

      if(jet_pt->at(j)>=pT_min && fabs(jet_rapidity->at(j))<3){ // Due to efficiency lowest-pt trigger

	if(!MC){ // Data
          // Deciding Trigger and Weight
          for(unsigned int m=firstTrig;m<Triggers.size();m++){
	 //if(jet_pt->at(j)>=minPtTriggers[Triggers.at(m)] && jet_pt->at(j)<maxPtTriggers[Triggers.at(m)]) trigger = Triggers.at(m);
	   if(jet_pt->at(j)>=Lumiwgt[Triggers.at(m)]["ptmin"] && jet_pt->at(j)<Lumiwgt[Triggers.at(m)]["ptmax"]) trigger = Triggers.at(m);	    
          }
	  if(m_debug) std::cout << "After deciding trigger: " << trigger << std::endl;
	  trig = TriggertoInt[trigger];

	  if(m_debug) std::cout << "Corresponding number for the trigger: " << trig<<" trigger= "<<trigger<< std::endl;

	    
	  if ((int)Lumiwgt.size()<trigUnprescaled) {
	    std::cout<<" wtDijets too small "<<trigUnprescaled<<std::endl; return 0; }

	  if ((int)Lumiwgt.size()<trig) {
	    std::cout<<" wtDijets too small trig= "<<trig<<std::endl; return 0; }
	       
	  
	  if(trigUnprescaled == trig) weight = 1.;
	  //else{weight = wgtDijets.at(trigUnprescaled).at(trigUnprescaled)/wgtDijets.at(trig).at(trig);}
	  else{weight = Lumiwgt[Triggers.at(trigUnprescaled)]["lumi"]/Lumiwgt[Triggers.at(trig)]["lumi"];}

	  if(m_debug) std::cout << "weight decided: " << weight << std::endl;

  	  bool trigger_passed = false;
          for(unsigned int lll=0;lll<passedTriggers->size();lll++){// Loop over passed triggers
            if(passedTriggers->at(lll)==trigger) trigger_passed = true;

	    if(m_debug) std::cout <<"passedTriggers->at("<<lll<<")= "<<passedTriggers->at(lll)<<std::endl;
          }

	  if(m_debug) std::cout << "trigger_passed: " << trigger_passed<< std::endl;
	  
          if(!trigger_passed) continue; // skip jet
	  if(m_debug) std::cout << "Trigger passed!" << std::endl;
	  
	  h_eta_Triggers[trigger]->Fill(jet_eta->at(j),weight);
	  if(j==0) h_eta_j0_Triggers[trigger]->Fill(jet_eta->at(j),weight); //leading jet
	  if(j==1) h_eta_j1_Triggers[trigger]->Fill(jet_eta->at(j),weight); //subleading jet
	  
          if(jet_phi->at(j)>=-3.2 && jet_phi->at(j)<-1.6){
            h_eta_Triggers_phi_0_16[trigger]->Fill(jet_eta->at(j),weight);
	    if(j==0) h_eta_j0_Triggers_phi_0_16[trigger]->Fill(jet_eta->at(j),weight);
	    if(j==1) h_eta_j1_Triggers_phi_0_16[trigger]->Fill(jet_eta->at(j),weight);
	  } else if(jet_phi->at(j)>=-1.6 && jet_phi->at(j)<0){
            h_eta_Triggers_phi_16_32[trigger]->Fill(jet_eta->at(j),weight);
	    if(j==0) h_eta_j0_Triggers_phi_16_32[trigger]->Fill(jet_eta->at(j),weight);
	    if(j==1) h_eta_j1_Triggers_phi_16_32[trigger]->Fill(jet_eta->at(j),weight);
	  } else if(jet_phi->at(j)>=0 && jet_phi->at(j)<1.6){
            h_eta_Triggers_phi_32_48[trigger]->Fill(jet_eta->at(j),weight);
	    if(j==0) h_eta_j0_Triggers_phi_32_48[trigger]->Fill(jet_eta->at(j),weight);
	    if(j==1) h_eta_j1_Triggers_phi_32_48[trigger]->Fill(jet_eta->at(j),weight);
	  } else if(jet_phi->at(j)>=1.6 && jet_phi->at(j)<=3.2){
            h_eta_Triggers_phi_48_64[trigger]->Fill(jet_eta->at(j),weight);
	    if(j==0) h_eta_j0_Triggers_phi_48_64[trigger]->Fill(jet_eta->at(j),weight);
	    if(j==1) h_eta_j1_Triggers_phi_48_64[trigger]->Fill(jet_eta->at(j),weight);
	  }
	  
	  h_eta_Triggers_finerbins[trigger]->Fill(jet_eta->at(j),weight);
	  if(j==0) h_eta_j0_Triggers_finerbins[trigger]->Fill(jet_eta->at(j),weight);
	  if(j==1) h_eta_j1_Triggers_finerbins[trigger]->Fill(jet_eta->at(j),weight);
	  
	}//Data only
	if(MC){
	  if(m_debug) std::cout << "Eta distributions in pT Bins (Triggers)" << std::endl;
          for(unsigned int m=firstTrig;m<Triggers.size();m++){
            if(jet_pt->at(j)>=Lumiwgt[Triggers.at(m)]["ptmin"] && jet_pt->at(j)<Lumiwgt[Triggers.at(m)]["ptmax"]){  
              h_eta_Triggers[Triggers.at(m)]->Fill(jet_eta->at(j),weight);
	      if(j==0) h_eta_j0_Triggers[Triggers.at(m)]->Fill(jet_eta->at(j),weight);
              if(j==1) h_eta_j1_Triggers[Triggers.at(m)]->Fill(jet_eta->at(j),weight);
  	      h_eta_Triggers_finerbins[Triggers.at(m)]->Fill(jet_eta->at(j),weight);
  	      if(j==0) h_eta_j0_Triggers_finerbins[Triggers.at(m)]->Fill(jet_eta->at(j),weight);
	      if(j==1) h_eta_j1_Triggers_finerbins[Triggers.at(m)]->Fill(jet_eta->at(j),weight);
 	      if(jet_phi->at(j)>=-3.2 && jet_phi->at(j)<-1.6){
                h_eta_Triggers_phi_0_16[Triggers.at(m)]->Fill(jet_eta->at(j),weight);
	        if(j==0) h_eta_j0_Triggers_phi_0_16[Triggers.at(m)]->Fill(jet_eta->at(j),weight);
	        if(j==1) h_eta_j1_Triggers_phi_0_16[Triggers.at(m)]->Fill(jet_eta->at(j),weight);
	      } else if(jet_phi->at(j)>=-1.6 && jet_phi->at(j)<0){
                h_eta_Triggers_phi_16_32[Triggers.at(m)]->Fill(jet_eta->at(j),weight);
	        if(j==0) h_eta_j0_Triggers_phi_16_32[Triggers.at(m)]->Fill(jet_eta->at(j),weight);
	        if(j==1) h_eta_j1_Triggers_phi_16_32[Triggers.at(m)]->Fill(jet_eta->at(j),weight);
	      } else if(jet_phi->at(j)>=0 && jet_phi->at(j)<1.6){
                h_eta_Triggers_phi_32_48[Triggers.at(m)]->Fill(jet_eta->at(j),weight);
	        if(j==0) h_eta_j0_Triggers_phi_32_48[Triggers.at(m)]->Fill(jet_eta->at(j),weight);
	        if(j==1) h_eta_j1_Triggers_phi_32_48[Triggers.at(m)]->Fill(jet_eta->at(j),weight);
	      } else if(jet_phi->at(j)>=1.6 && jet_phi->at(j)<=3.2){
                 h_eta_Triggers_phi_48_64[Triggers.at(m)]->Fill(jet_eta->at(j),weight);
	         if(j==0) h_eta_j0_Triggers_phi_48_64[Triggers.at(m)]->Fill(jet_eta->at(j),weight);
	         if(j==1) h_eta_j1_Triggers_phi_48_64[Triggers.at(m)]->Fill(jet_eta->at(j),weight);
	      }
	    }
	  }
	  if(m_debug) std::cout << "Eta distributions for high pT bins" << std::endl;
	}//MC only

	// Eta distributions at high pT for different pT bins
        for(unsigned int m=22;m<43;m++){
	  if(jet_pt->at(j)>=ptBins_0[m] && jet_pt->at(j)<ptBins_0[m+1]){ 
  	    h_eta_pTBins[m-22]->Fill(jet_eta->at(j),weight);
  	    if(j==0) h_eta_j0_pTBins[m-22]->Fill(jet_eta->at(j),weight);
	    if(j==1) h_eta_j1_pTBins[m-22]->Fill(jet_eta->at(j),weight);
	  }
	}

	if(m_debug) std::cout << "Eta distributions for different scales" << std::endl;

	if(fabs(jet_eta->at(j))>=1.5 && fabs(jet_eta->at(j))<1.75){
          h_phi_abseta_15_175->Fill(jet_phi->at(j),weight);
	}
	if(fabs(jet_eta->at(j))>=1.75 && fabs(jet_eta->at(j))<2.0){
          h_phi_abseta_175_20->Fill(jet_phi->at(j),weight);
          if(jet_pt->at(j)>150){
	    h_phi_abseta_15_20_pT150->Fill(jet_phi->at(j),weight);
	  }
          if(jet_pt->at(j)>250){
	    h_phi_abseta_15_20_pT250->Fill(jet_phi->at(j),weight);
	  }
          if(jet_pt->at(j)>500){
	    h_phi_abseta_15_20_pT500->Fill(jet_phi->at(j),weight);
	  }
          if(jet_pt->at(j)>642){
	    h_phi_abseta_15_20_pT642->Fill(jet_phi->at(j),weight);
	  }
          if(jet_pt->at(j)>1100){
	    h_phi_abseta_15_20_pT1100->Fill(jet_phi->at(j),weight);
	  }

	}
	if(fabs(jet_eta->at(j))>=1.5 && fabs(jet_eta->at(j))<1.6){
          h_phi_abseta_15_16->Fill(jet_phi->at(j),weight);
	}
	if(fabs(jet_eta->at(j))>=1.6 && fabs(jet_eta->at(j))<1.7){
          h_phi_abseta_16_17->Fill(jet_phi->at(j),weight);
	}
	if(fabs(jet_eta->at(j))>=1.7 && fabs(jet_eta->at(j))<1.8){
          h_phi_abseta_17_18->Fill(jet_phi->at(j),weight);
	}
	if(fabs(jet_eta->at(j))>=1.8 && fabs(jet_eta->at(j))<1.9){
          h_phi_abseta_18_19->Fill(jet_phi->at(j),weight);
	}
	if(fabs(jet_eta->at(j))>=1.9 && fabs(jet_eta->at(j))<2.0){
          h_phi_abseta_19_20->Fill(jet_phi->at(j),weight);
	}

        if(jet_eta->at(j)>=1.5 && jet_eta->at(j)<1.75){
          h_phi_poseta_15_175->Fill(jet_phi->at(j),weight);
	}
	if(jet_eta->at(j)>=1.75 && jet_eta->at(j)<2.0){
          h_phi_poseta_175_20->Fill(jet_phi->at(j),weight);
	}
	if(jet_eta->at(j)>=1.5 && jet_eta->at(j)<1.6){
          h_phi_poseta_15_16->Fill(jet_phi->at(j),weight);
	}
	if(jet_eta->at(j)>=1.6 && jet_eta->at(j)<1.7){
          h_phi_poseta_16_17->Fill(jet_phi->at(j),weight);
	}
	if(jet_eta->at(j)>=1.7 && jet_eta->at(j)<1.8){
          h_phi_poseta_17_18->Fill(jet_phi->at(j),weight);
	}
	if(jet_eta->at(j)>=1.8 && jet_eta->at(j)<1.9){
          h_phi_poseta_18_19->Fill(jet_phi->at(j),weight);
	}
	if(jet_eta->at(j)>=1.9 && jet_eta->at(j)<2.0){
          h_phi_poseta_19_20->Fill(jet_phi->at(j),weight);
	}

        if(jet_eta->at(j)>=-1.75 && jet_eta->at(j)<-1.5){
          h_phi_negeta_15_175->Fill(jet_phi->at(j),weight);
	}
	if(jet_eta->at(j)>=-2.0 && jet_eta->at(j)<-1.75){
          h_phi_negeta_175_20->Fill(jet_phi->at(j),weight);
	}
	if(jet_eta->at(j)>=-1.6 && jet_eta->at(j)<-1.5){
          h_phi_negeta_15_16->Fill(jet_phi->at(j),weight);
	}
	if(jet_eta->at(j)>=-1.7 && jet_eta->at(j)<-1.6){
          h_phi_negeta_16_17->Fill(jet_phi->at(j),weight);
	}
	if(jet_eta->at(j)>=-1.8 && jet_eta->at(j)<-1.7){
          h_phi_negeta_17_18->Fill(jet_phi->at(j),weight);
	}
	if(jet_eta->at(j)>=-1.9 && jet_eta->at(j)<-1.8){
          h_phi_negeta_18_19->Fill(jet_phi->at(j),weight);
	}
	if(jet_eta->at(j)>=-2.0 && jet_eta->at(j)<-1.9){
          h_phi_negeta_19_20->Fill(jet_phi->at(j),weight);
	}

	// Phi distributions for 2.4<eta<2.5 for different pT thresholds
	if(fabs(jet_eta->at(j))>=2.4 && fabs(jet_eta->at(j))<2.5){
	  if(jet_pt->at(j)>442) h_phi_eta_24_25_pT_442->Fill(jet_phi->at(j),weight);
	  if(jet_pt->at(j)>408) h_phi_eta_24_25_pT_408->Fill(jet_phi->at(j),weight);
	  if(jet_pt->at(j)>346) h_phi_eta_24_25_pT_346->Fill(jet_phi->at(j),weight);
	  if(jet_pt->at(j)>264) h_phi_eta_24_25_pT_264->Fill(jet_phi->at(j),weight);
	  if(jet_pt->at(j)>642) h_phi_eta_24_25_pT_642->Fill(jet_phi->at(j),weight);
	}

        if(jet_eta->at(j)>=1.5 && jet_eta->at(j)<2.5) h_phi_eta_15_25->Fill(jet_phi->at(j),weight);
        if(jet_eta->at(j)>=2.0 && jet_eta->at(j)<2.5) h_phi_eta_20_25->Fill(jet_phi->at(j),weight);
        if(jet_eta->at(j)>=1.75 && jet_eta->at(j)<2.25) h_phi_eta_175_225->Fill(jet_phi->at(j),weight);
	if(jet_pt->at(j)>=100 && jet_pt->at(j)<300){
          if(jet_eta->at(j)>=1.5 && jet_eta->at(j)<2.5) h_phi_eta_15_25_pT_100_300->Fill(jet_phi->at(j),weight);
          if(jet_eta->at(j)>=2.0 && jet_eta->at(j)<2.5) h_phi_eta_20_25_pT_100_300->Fill(jet_phi->at(j),weight);
          if(jet_eta->at(j)>=1.75 && jet_eta->at(j)<2.25) h_phi_eta_175_225_pT_100_300->Fill(jet_phi->at(j),weight);
	}
        if(jet_pt->at(j)>=300 && jet_pt->at(j)<500){
          if(jet_eta->at(j)>=1.5 && jet_eta->at(j)<2.5) h_phi_eta_15_25_pT_300_500->Fill(jet_phi->at(j),weight);
          if(jet_eta->at(j)>=2.0 && jet_eta->at(j)<2.5) h_phi_eta_20_25_pT_300_500->Fill(jet_phi->at(j),weight);
          if(jet_eta->at(j)>=1.75 && jet_eta->at(j)<2.25) h_phi_eta_175_225_pT_300_500->Fill(jet_phi->at(j),weight);
	}
        if(jet_pt->at(j)>=642 && jet_pt->at(j)<1992){
          if(jet_eta->at(j)>=1.5 && jet_eta->at(j)<2.5) h_phi_eta_15_25_pT_642_1992->Fill(jet_phi->at(j),weight);
          if(jet_eta->at(j)>=2.0 && jet_eta->at(j)<2.5) h_phi_eta_20_25_pT_642_1992->Fill(jet_phi->at(j),weight);
          if(jet_eta->at(j)>=1.75 && jet_eta->at(j)<2.25) h_phi_eta_175_225_pT_642_1992->Fill(jet_phi->at(j),weight);
	}

	TLorentzVector Jet; 
	Jet.SetPtEtaPhiE(jet_pt->at(j), jet_eta->at(j), jet_phi->at(j), jet_E->at(j));	
	RecoJets.push_back(Jet);

        h_wgt->Fill(weight);
        nInclusiveJets_y_Inclusive++; 
        h_NPV_wJetSelection->Fill(NPV,weight);
        h_rho_wJetSelection->Fill(rho*0.001,weight);
        h_mu_wJetSelection->Fill(mu,weight);
	if(!m_rel21){
	  h_nTrk->Fill(jet_NumTrkPt1000->at(j).at(0),weight);
	  h_trackWIDTH->Fill(jet_TrackWidthPt1000->at(j).at(0),weight);
	}
        h_pt_with_wgt_y_inclusive->Fill(jet_pt->at(j),weight); 

	h_eta->Fill(jet_eta->at(j),weight);
        h_phi->Fill(jet_phi->at(j),weight);
        h_eta_phi->Fill(jet_eta->at(j),jet_phi->at(j),weight);
        h_jet_width->Fill(jet_width->at(j),weight);
        h_jet_EMFrac->Fill(jet_EMFrac->at(j),weight);
        h_jet_Nsegments->Fill(jet_Nsegments->at(j),weight);
	// TEMPORARY (Only valid for EM jets, if LC emscale should be used)
        //h_jet_EM3->Fill((jet_EnergyPerSampling->at(j).at(3) + jet_EnergyPerSampling->at(j).at(7)) / jet_emScaleE->at(j),weight);
        //h_jet_EM3->Fill((jet_EnergyPerSampling->at(j).at(3) + jet_EnergyPerSampling->at(j).at(7)) / jet_constitScaleE->at(j),weight);
	//h_jet_Tile0->Fill((jet_EnergyPerSampling->at(j).at(12) + jet_EnergyPerSampling->at(j).at(18)) / jet_constitScaleE->at(j),weight);
	if(j==0){ // Leading pT jets
          h_j0_pt_with_wgt_y_inclusive->Fill(jet_pt->at(j),weight); 
          h_j0_pt_with_wgt_y_inclusive_FinerBins->Fill(jet_pt->at(j),weight);
          h_mu_inclusive->Fill(mu,weight);
          h_NPV_inclusive->Fill(NPV,weight);
          h_rho_inclusive->Fill(rho*0.001,weight);
	  h_eta_j0->Fill(jet_eta->at(j),weight);
	  h_eta_j0andj1->Fill(jet_eta->at(j),weight);
        }
        if(j==1){ // Secondary leading pT jets
          h_j1_pt_with_wgt_y_inclusive->Fill(jet_pt->at(j),weight); 
	  h_eta_j1->Fill(jet_eta->at(j),weight);
	  h_eta_j0andj1->Fill(jet_eta->at(j),weight);
        }
        if(j==2){ // Third leading pT jets
          h_j2_pt_with_wgt_y_inclusive->Fill(jet_pt->at(j),weight);
        }
	if(mu>=14 && mu<21){
          //h_jet_width_mu_14_21->Fill(jet_width->at(j),weight);
          //h_jet_EMFrac_mu_14_21->Fill(jet_EMFrac->at(j),weight);
          h_pt_with_wgt_y_inclusive_mu_14_21->Fill(jet_pt->at(j),weight);
          h_eta_mu_14_21->Fill(jet_eta->at(j),weight);
          h_phi_mu_14_21->Fill(jet_phi->at(j),weight);
          h_eta_phi_mu_14_21->Fill(jet_eta->at(j),jet_phi->at(j),weight);
          if(j==0) h_j0_pt_with_wgt_y_inclusive_mu_14_21->Fill(jet_pt->at(j),weight);
          if(j==1) h_j1_pt_with_wgt_y_inclusive_mu_14_21->Fill(jet_pt->at(j),weight);
        }
        else if(mu>=21 && mu<28){
          //h_jet_width_mu_21_28->Fill(jet_width->at(j),weight);
          //h_jet_EMFrac_mu_21_28->Fill(jet_EMFrac->at(j),weight);
          h_pt_with_wgt_y_inclusive_mu_21_28->Fill(jet_pt->at(j),weight);
          h_eta_mu_21_28->Fill(jet_eta->at(j),weight);
          h_phi_mu_21_28->Fill(jet_phi->at(j),weight);
          h_eta_phi_mu_21_28->Fill(jet_eta->at(j),jet_phi->at(j),weight);
          if(j==0) h_j0_pt_with_wgt_y_inclusive_mu_21_28->Fill(jet_pt->at(j),weight);
          if(j==1) h_j1_pt_with_wgt_y_inclusive_mu_21_28->Fill(jet_pt->at(j),weight);
        }
	if(!m_rel21){
 	  if(jet_pt->at(j)>=1000){
	    h_nTrk_pT_1000plus->Fill(jet_NumTrkPt1000->at(j).at(0),weight);
 	    h_trackWIDTH_pT_1000plus->Fill(jet_TrackWidthPt1000->at(j).at(0),weight);
	  }
	  if(jet_pt->at(j)>=900){
	    h_nTrk_pT_900plus->Fill(jet_NumTrkPt1000->at(j).at(0),weight);
 	    h_trackWIDTH_pT_900plus->Fill(jet_TrackWidthPt1000->at(j).at(0),weight);
	  }
	  if(jet_pt->at(j)>=800){
	    h_nTrk_pT_800plus->Fill(jet_NumTrkPt1000->at(j).at(0),weight);
 	    h_trackWIDTH_pT_800plus->Fill(jet_TrackWidthPt1000->at(j).at(0),weight);
	  }	
	  if(jet_pt->at(j)>=894 && jet_pt->at(j)<1310){
	    h_nTrk_pT_894_1310->Fill(jet_NumTrkPt1000->at(j).at(0),weight);
 	    h_trackWIDTH_pT_894_1310->Fill(jet_TrackWidthPt1000->at(j).at(0),weight);
	  }
	  if(jet_pt->at(j)>=1310 && jet_pt->at(j)<1992){
	    h_nTrk_pT_1310_1992->Fill(jet_NumTrkPt1000->at(j).at(0),weight);
 	    h_trackWIDTH_pT_1310_1992->Fill(jet_TrackWidthPt1000->at(j).at(0),weight);
	  }
	}
	if(jet_pt->at(j)>=642){
          h_jet_EMFrac_pT642->Fill(jet_EMFrac->at(j),weight);
	  h_jet_width_pT642->Fill(jet_width->at(j),weight);
          h_eta_pT_642->Fill(jet_eta->at(j),weight);
          h_phi_pT_642->Fill(jet_phi->at(j),weight);
	}
        if(jet_pt->at(j)>=150){
	  h_eta_pT_150->Fill(jet_eta->at(j),weight);
          h_phi_pT_150->Fill(jet_phi->at(j),weight);
	}
        if(jet_pt->at(j)>=250){
	  h_eta_pT_250->Fill(jet_eta->at(j),weight);
          h_phi_pT_250->Fill(jet_phi->at(j),weight);
	}
        if(jet_pt->at(j)>=500){
	  h_eta_pT_500->Fill(jet_eta->at(j),weight);
          h_phi_pT_500->Fill(jet_phi->at(j),weight);
	}
	if(jet_pt->at(j)>=300 && jet_pt->at(j)<400){
	  h_eta_300_400->Fill(jet_eta->at(j),weight);
	  if(j==0) h_eta_j0_300_400->Fill(jet_eta->at(j),weight);
	  if(j==1) h_eta_j1_300_400->Fill(jet_eta->at(j),weight);
	}
	if(jet_pt->at(j)>=400 && jet_pt->at(j)<500){
	  h_eta_400_500->Fill(jet_eta->at(j),weight);
	  if(j==0) h_eta_j0_400_500->Fill(jet_eta->at(j),weight);
	  if(j==1) h_eta_j1_400_500->Fill(jet_eta->at(j),weight);
	}
	if(jet_pt->at(j)>=500 && jet_pt->at(j)<600){
	  h_eta_500_600->Fill(jet_eta->at(j),weight);
	  if(j==0) h_eta_j0_500_600->Fill(jet_eta->at(j),weight);
	  if(j==1) h_eta_j1_500_600->Fill(jet_eta->at(j),weight);
	}
 	if(jet_pt->at(j)>=600 && jet_pt->at(j)<700){
	  h_eta_600_700->Fill(jet_eta->at(j),weight);
	  if(j==0) h_eta_j0_600_700->Fill(jet_eta->at(j),weight);
	  if(j==1) h_eta_j1_600_700->Fill(jet_eta->at(j),weight);
	}
	if(jet_pt->at(j)>=700 && jet_pt->at(j)<900){
	  h_eta_700_900->Fill(jet_eta->at(j),weight);
	  if(j==0) h_eta_j0_700_900->Fill(jet_eta->at(j),weight);
	  if(j==1) h_eta_j1_700_900->Fill(jet_eta->at(j),weight);
	}
	if(jet_pt->at(j)>=500 && jet_pt->at(j)<700){
	  h_eta_500_700->Fill(jet_eta->at(j),weight);
	}
	if(jet_pt->at(j)>=900 && jet_pt->at(j)<1100){
	  h_eta_900_1100->Fill(jet_eta->at(j),weight);
	}
	if(jet_pt->at(j)>=1100){
	  h_eta_1100plus->Fill(jet_eta->at(j),weight);
	}
        if(jet_pt->at(j)>150 && jet_pt->at(j)<200) h_eta_phi_150pT200->Fill(jet_eta->at(j),jet_phi->at(j),weight);
        if(jet_pt->at(j)>300 && jet_pt->at(j)<500) h_eta_phi_300pT500->Fill(jet_eta->at(j),jet_phi->at(j),weight);
        if(jet_pt->at(j)>442 && jet_pt->at(j)<642){
          h_eta_phi_442pT642->Fill(jet_eta->at(j),jet_phi->at(j),weight);
        }
	if(jet_pt->at(j)>442 && jet_pt->at(j)<1992){
          h_eta_phi_442pT1992->Fill(jet_eta->at(j),jet_phi->at(j),weight);
        }
        if(jet_pt->at(j)>642 && jet_pt->at(j)<1992){
          h_eta_phi_642pT1992->Fill(jet_eta->at(j),jet_phi->at(j),weight);
          h_y_642pT1992->Fill(jet_rapidity->at(j),weight);
        }
        if(jet_pt->at(j)>=152 && jet_pt->at(j)<216) h_y_152pT216->Fill(jet_rapidity->at(j),weight);
        if(jet_pt->at(j)>=216 && jet_pt->at(j)<408) h_y_216pT408->Fill(jet_rapidity->at(j),weight);
        if(jet_pt->at(j)>=408 && jet_pt->at(j)<642) h_y_408pT642->Fill(jet_rapidity->at(j),weight);
	if(fabs(jet_eta->at(j))<0.5){
	  nInclusiveJets_CentralRegion++;
	  h_pt_with_wgt_eta_05_after_calibration->Fill(jet_pt->at(j),weight);
	  if(j==0) h_j0_pt_with_wgt_eta_05_after_calibration->Fill(jet_pt->at(j),weight);
	  if(j==1) h_j1_pt_with_wgt_eta_05_after_calibration->Fill(jet_pt->at(j),weight);
	}
	if(fabs(jet_rapidity->at(j))>=0.5 && fabs(jet_rapidity->at(j))<0.8){
          h_pt_with_wgt_y_05_08->Fill(jet_pt->at(j),weight);
          if(j==0) h_j0_pt_with_wgt_y_05_08->Fill(jet_pt->at(j),weight);
          if(j==1) h_j1_pt_with_wgt_y_05_08->Fill(jet_pt->at(j),weight);
	}
        for(unsigned int l=0;l<nyBins;++l){ // Looping over different y bins
          if( fabs(jet_rapidity->at(j)) >= yBins[l] && fabs(jet_rapidity->at(j))<yBins[l+1] ){
            h_pt_with_wgt[l]->Fill(jet_pt->at(j),weight);
            h_pt_finebins_with_wgt[l]->Fill(jet_pt->at(j),weight);
	    
	    float tmpWgt = weight;
            if(mu<m_intermediateMu) h_pt_lowMu[l]->Fill(jet_pt->at(j),weight);
	    else{h_pt_highMu[l]->Fill(jet_pt->at(j),tmpWgt);}
	    if(jet_pt->at(j)>=100) h_phi_yBins[l]->Fill(jet_phi->at(j),weight); //pT>100GeV to remove pileup jets with low pT
            if(j==0) h_j0_pt_with_wgt[l]->Fill(jet_pt->at(j),weight);
            if(j==1) h_j1_pt_with_wgt[l]->Fill(jet_pt->at(j),weight);
	    if(m_unfolding && m_bootstraps) fRecoBootstrap_inc[l]->Fill(jet_pt->at(j),weight);
          }
        } // End: Loop over y bins

	if(MC && m_unfolding){
	    // Let's find the matching reco jet
	    TLorentzVector Jet; 
	    Jet.SetPtEtaPhiE(RecoJets[j].Pt(), RecoJets[j].Eta(), RecoJets[j].Phi(), RecoJets[j].E());

	    TLorentzVector truthJet;

	    int index(0);
	    double DRmin(0);
	    int Nmatches = RecoMatch_index(Jet,TruthJets,truthJet,index,DRmin);
	    int Nmatches_inv = Match_index_inv(truthJet,RecoJets,j,DRmin);

	    // skip truth jets that don't match any reco jets
	    if (Nmatches == 0) continue;
	    // skip non biyective matching
	    if (Nmatches_inv != 0) continue;

	    for(unsigned int l=0;l<nyBins;++l){ // Looping over different y bins
	      if( fabs(truthJet.Rapidity()) >= yBins[l] && fabs(Jet.Rapidity()) >= yBins[l] && fabs(truthJet.Rapidity())<yBins[l+1] && fabs(Jet.Rapidity())<yBins[l+1]){
		if (m_bootstraps) fBootstrap_inc[l]->Fill(Jet.Pt(),truthJet.Pt(),weight);
		h_pt_TM[l]->Fill(Jet.Pt(),truthJet.Pt(),weight);
	      }
	    } // End: Loop over y bins
	}

      }// End: Inclusive-jet selection

      //cout<<"Run number= "<<runNumber<<endl;
     runnumber=runNumber;
     //cout<<"runnumber= "<<runnumber<<" runnumberold= "<<runnumberold<<endl;
    
     if (runnumber!=runnumberold) {runnumberold=runnumber; vrun.push_back(runnumber);};
     //cout<<"after runnumber= "<<runnumber<<" runnumberold= "<<runnumberold<<endl;
     //for (int i=0; i<(int)vrun.size(); i++) cout<<vrun.at(i)<<endl; 
    
    }// End: Loop over jets

    //------------------------------
    // Filling Dijets distributions
    //------------------------------

    if(!MC) weight = 1;

    if(m_runDijets){
    
      if(m_debug) std::cout << "Filling dijets histograms" << std::endl;

      double massReco=-1,yStarReco=-1;
      double massTruth=-1,yStarTruth=-1;
      int yStarRecoBin=-1;
      int yStarTruthBin=-1;
      std::string trigger_j0 = "";
      std::string trigger_j1 = "";
      int trigj0 = 0;
      int trigj1 = 0;
      bool passReco_dijets  = false;
      bool passTruth_dijets = false;
      if(RecoJets_y30.size()>1){ // At least two Reco Jets

        // Trigger
        if(!MC){
          // Deciding Trigger and Weight
          for(unsigned int m=firstTrig;m<Triggers.size();m++){
	    //            if(RecoJets_y30.at(0).Pt()>=minPtTriggers[Triggers.at(m)] && RecoJets_y30.at(0).Pt()<maxPtTriggers[Triggers.at(m)]) trigger_j0 = Triggers.at(m);
            //if(RecoJets_y30.at(1).Pt()>=minPtTriggers[Triggers.at(m)] && RecoJets_y30.at(1).Pt()<maxPtTriggers[Triggers.at(m)]) trigger_j1 = Triggers.at(m);
	    if(RecoJets_y30.at(0).Pt()>=Lumiwgt[Triggers.at(m)]["ptmin"] && RecoJets_y30.at(0).Pt()<Lumiwgt[Triggers.at(m)]["ptmax"]) trigger_j0 = Triggers.at(m);
            if(RecoJets_y30.at(1).Pt()>=Lumiwgt[Triggers.at(m)]["ptmin"] && RecoJets_y30.at(1).Pt()<Lumiwgt[Triggers.at(m)]["ptmax"]) trigger_j1 = Triggers.at(m);	    
          }
	  if(trigger_j0=="" || trigger_j1=="") continue; // skip event
          trigj0 = TriggertoInt[trigger_j0];
          trigj1 = TriggertoInt[trigger_j1];
	  if(trigUnprescaled == trigj1) weight = 1.;
	  //else{weight = wgtDijets.at(trigUnprescaled).at(trigUnprescaled)/wgtDijets.at(trigj0).at(trigj1);}
          // Warning here Jone needs a matrix !!
          else{weight = Lumiwgt[Triggers.at(trigUnprescaled)]["lumi"]/Lumiwgt[Triggers.at(trigj0)]["lumi"];};
          bool trigger_j0_passed = false;
          bool trigger_j1_passed = false;
          for(unsigned int lll=0;lll<passedTriggers->size();lll++){// Loop over passed triggers
            if(passedTriggers->at(lll)==trigger_j0) trigger_j0_passed = true;
            if(passedTriggers->at(lll)==trigger_j1) trigger_j1_passed = true;
          }
          if(!(trigger_j0_passed || trigger_j1_passed)) continue; // skip event
        } else{ // MC
	  //if( ! (RecoJets_y30.at(1).Pt()>=minPtTriggers[Triggers.at(firstTrig)]) ) continue; // skip event to be consistent with Data
	  if( ! (RecoJets_y30.at(1).Pt()>=Lumiwgt[Triggers.at(firstTrig)]["ptmin"]) ) continue; // skip event to be consistent with Data
	}

        // Dijet selection
        if(RecoJets_y30.at(0).Pt()>m_j0min && ((RecoJets_y30.at(0).Pt())+(RecoJets_y30.at(1).Pt()))>m_HT2min && RecoJets_y30.at(1).Pt()>m_j1min){
  	  passReco_dijets = true;
          for(unsigned int l=0;l<nyBins;++l){ // Looping over different y bins
            if( fabs(RecoJets_y30.at(0).Rapidity()) >= yBins[l] && fabs(RecoJets_y30.at(0).Rapidity())<yBins[l+1] ){
	      h_j0_pt_mjj[l]->Fill(RecoJets_y30.at(0).Pt(),weight);
            }
	    if( fabs(RecoJets_y30.at(1).Rapidity()) >= yBins[l] && fabs(RecoJets_y30.at(1).Rapidity())<yBins[l+1] ){
	      h_j1_pt_mjj[l]->Fill(RecoJets_y30.at(1).Pt(),weight);
            }
          }
          h_mu_dijet->Fill(mu,weight);
          yStarReco = fabs(RecoJets_y30.at(0).Rapidity()-RecoJets_y30.at(1).Rapidity())/2.0;
          yStarRecoBin=GetYStarBin(yStarReco);
          h_ystar->Fill(yStarReco,weight);
          massReco = ((RecoJets_y30.at(0))+(RecoJets_y30.at(1))).M();
          h_m12_reco_ystar_inclusive->Fill(massReco,weight);//y*<3
          if(mu>=14 && mu<21) h_m12_reco_ystar_inclusive_mu_14_21->Fill(massReco,weight);
          if(mu>=21 && mu<28) h_m12_reco_ystar_inclusive_mu_21_28->Fill(massReco,weight);
          h_m12_reco[yStarRecoBin]->Fill(massReco,weight);
	  double tmpWgt = weight;
          if(mu<m_intermediateMu) h_m12_reco_lowMu[yStarRecoBin]->Fill(massReco,weight);
  	  else{h_m12_reco_highMu[yStarRecoBin]->Fill(massReco,tmpWgt);}
          h_m12_reco_finer_bins[yStarRecoBin]->Fill(massReco,weight);
          if(yStarRecoBin==6) std::cout << "yStarRecoBin==6" << std::endl;
          nDijets_y_Inclusive++;
	  if(m_unfolding && m_bootstraps) fRecoBootstrap_m12[yStarRecoBin]->Fill(massReco,weight);
        }

      }// At least two Reco Jets

      if(TruthJets_y30.size()>1){ // At least two Truth Jets

        // Dijet selection
        if(TruthJets_y30.at(0).Pt()>m_j0min && ((TruthJets_y30.at(0).Pt())+(TruthJets_y30.at(1).Pt()))>m_HT2min && TruthJets_y30.at(1).Pt()>m_j1min){
  	  passTruth_dijets = true;
          yStarTruth = fabs(TruthJets_y30.at(0).Rapidity()-TruthJets_y30.at(1).Rapidity())/2.0;
          yStarTruthBin=GetYStarBin(yStarTruth);
          massTruth = ((TruthJets_y30.at(0))+(TruthJets_y30.at(1))).M();
          h_m12_truth[yStarTruthBin]->Fill(massTruth,weight);
          h_m12_truth_finer_bins[yStarTruthBin]->Fill(massTruth,weight);
	  if(m_unfolding && m_bootstraps) fTruthBootstrap_m12[yStarTruthBin]->Fill(massTruth,weight);
        }
      
      }// At least two Truth Jets

      // Fill Transfer Matrix
      if(MC && m_unfolding && passReco_dijets && passTruth_dijets){
        if(yStarRecoBin==yStarTruthBin) {
	    if(m_bootstraps) fBootstrap_m12[yStarRecoBin]->Fill(massReco,massTruth,weight); 
	    h_m12_TM[yStarRecoBin]->Fill(massReco,massTruth,weight);
        }
      }

    }// Dijets

    runNumber = -999;
    eventNumber = -999;
    mceventNumber = -999;
    mcEventWeight = -999;
    lumiBlock = -999;
    bcid = -999;
    wgt = -999.;
    wgt_pileup = -999.;
    NPV = -999.;
    mu = -999.;
    rho = -999.;
    njet = -1;
    ntruthjets = -1;
    jet_pt->clear();
    jet_eta->clear();
    jet_phi->clear();
    jet_E->clear();
    jet_rapidity->clear();
    jet_clean_passLooseBad->clear();
    jet_clean_passTightBad->clear();
    if(!m_rel21){
      jet_NumTrkPt1000->clear();
      jet_TrackWidthPt1000->clear();
    }
    jet_width->clear();
    jet_EMFrac->clear();
    jet_Nsegments->clear();
    if(MC){
      truthjet_pt->clear();
      truthjet_eta->clear();
      truthjet_phi->clear();
      truthjet_E->clear();
      truthjet_rapidity->clear();
    }
    if(!MC) passedTriggers->clear();

  } // End: Loop over entries

  //
  // Store statistics
  //
  hevent->SetBinContent(1,1.);  
  hevent->SetBinContent(2, double(entries));
  hevent->SetBinContent(3, double(Skippedevents_Cleaning));
  hevent->SetBinContent(4, double(nInclusiveJets_y_Inclusive));
  hevent->SetBinContent(5, double(nInclusiveJets_CentralRegion));
  hevent->SetBinContent(6, double(nDijets_y_Inclusive));  
  
  //------------------------------------
  // Saving Histogramas into a ROOT file
  //------------------------------------
  
  std::cout << "Saving plots into a ROOT file..." << std::endl;
  TFile* tout = new TFile(m_outputFile,"recreate");
  tout->cd();
  //
  // Opening output file
  //TFile* tout;
  //cout<<"Saving to file "<<outputFile<<endl;
  //if(!m_debug){ tout = new TFile(m_outputFile,"recreate");
  //std::cout << "output file = " << m_outputFile << std::endl;
  //tout->cd();

  
  //TVectorD *vRunNumbers = new TVectorD((int) vrun.size());
  TVectorD vRunNumbers((int) vrun.size());  
  cout<<"Run number in this file: "<<endl; 
  for (int i=0; i<(int)vrun.size(); i++) {
   cout<<vrun.at(i)<<endl;
   vRunNumbers[i]=(double) vrun.at(i);
  }   
  vRunNumbers.Write();
  
  // Writing histograms
  for (int i=0; i<(int)vhisto.size(); i++) {
   vhisto.at(i)->Write();
  }
  
  h_wgt->Write();
  h_eta->Write();
  h_ystar->Write();
  h_njet->Write();
  //h_jet_Tile0->Write();
  //h_jet_EM3->Write();
  h_jet_Nsegments->Write();
  h_eta_j0->Write();
  h_eta_j1->Write();
  h_eta_j0andj1->Write();
  h_eta_pT_150->Write();
  h_eta_pT_250->Write();
  h_eta_pT_500->Write();
  h_eta_pT_642->Write();
  h_eta_mu_14_21->Write();
  h_eta_mu_21_28->Write();
  h_eta_GabrielSelection->Write();
  h_eta_300_400->Write();
  h_eta_400_500->Write();
  h_eta_500_600->Write();
  h_eta_600_700->Write();
  h_eta_500_700->Write();
  h_eta_700_900->Write();
  h_eta_900_1100->Write();
  h_eta_1100plus->Write();
  h_eta_j0_300_400->Write();
  h_eta_j0_400_500->Write();
  h_eta_j0_500_600->Write();
  h_eta_j0_600_700->Write();
  h_eta_j0_700_900->Write();
  h_eta_j1_300_400->Write();
  h_eta_j1_400_500->Write();
  h_eta_j1_500_600->Write();
  h_eta_j1_600_700->Write();
  h_eta_j1_700_900->Write();
  h_phi->Write();
  h_phi_pT_150->Write();
  h_phi_pT_250->Write();
  h_phi_pT_500->Write();
  h_phi_pT_642->Write();
  h_phi_mu_14_21->Write();
  h_phi_mu_21_28->Write();
  h_phi_abseta_15_20_pT150->Write();
  h_phi_abseta_15_20_pT250->Write();
  h_phi_abseta_15_20_pT500->Write();
  h_phi_abseta_15_20_pT642->Write();
  h_phi_abseta_15_20_pT1100->Write();
  h_phi_abseta_15_175->Write();
  h_phi_abseta_175_20->Write();
  h_phi_abseta_15_16->Write();
  h_phi_abseta_16_17->Write();
  h_phi_abseta_17_18->Write();
  h_phi_abseta_18_19->Write();
  h_phi_abseta_19_20->Write();
  h_phi_poseta_15_175->Write();
  h_phi_poseta_175_20->Write();
  h_phi_poseta_15_16->Write();
  h_phi_poseta_16_17->Write();
  h_phi_poseta_17_18->Write();
  h_phi_poseta_18_19->Write();
  h_phi_poseta_19_20->Write();
  h_phi_negeta_15_175->Write();
  h_phi_negeta_175_20->Write();
  h_phi_negeta_15_16->Write();
  h_phi_negeta_16_17->Write();
  h_phi_negeta_17_18->Write();
  h_phi_negeta_18_19->Write();
  h_phi_negeta_19_20->Write();
  h_eta_phi->Write();
  h_eta_phi_150pT200->Write();
  h_eta_phi_300pT500->Write();
  h_eta_phi_442pT642->Write();
  h_eta_phi_442pT1992->Write();
  h_eta_phi_642pT1992->Write();
  h_eta_phi_mu_14_21->Write();
  h_eta_phi_mu_21_28->Write();
  h_NPV->Write();
  h_NPV_inclusive->Write();
  h_NPV_wJetSelection->Write();
  h_NPV_mu_14_21->Write();
  h_NPV_mu_21_28->Write();
  h_y_152pT216->Write();
  h_y_216pT408->Write();
  h_y_408pT642->Write();
  h_y_642pT1992->Write();
  h_mu->Write();
  h_mu_wJetSelection->Write();
  h_mu_inclusive->Write();
  h_mu_dijet->Write();
  h_rho->Write();
  h_rho_inclusive->Write();
  h_rho_wJetSelection->Write();
  h_rho_without_wgt->Write();
  h_jet_width->Write();
  h_jet_width_pT642->Write();
  /*
  h_jet_width_mu_14_21->Write();
  h_jet_width_mu_21_28->Write();
  */
  h_jet_EMFrac->Write();
  h_jet_EMFrac_pT642->Write();
  /*
  h_jet_EMFrac_mu_14_21->Write();
  h_jet_EMFrac_mu_21_28->Write();
  */
  h_nTrk->Write();
  h_nTrk_pT_1000plus->Write();
  h_nTrk_pT_900plus->Write();
  h_nTrk_pT_800plus->Write();
  h_nTrk_pT_894_1310->Write();
  h_nTrk_pT_1310_1992->Write();
  h_trackWIDTH->Write();
  h_trackWIDTH_pT_1000plus->Write();
  h_trackWIDTH_pT_900plus->Write();
  h_trackWIDTH_pT_800plus->Write();
  h_trackWIDTH_pT_894_1310->Write();
  h_trackWIDTH_pT_1310_1992->Write();
  h_pt_with_wgt_y_inclusive->Write();
  h_pt_with_wgt_y_05_08->Write();
  h_pt_with_wgt_eta_05_before_calibration->Write();
  h_pt_with_wgt_eta_05_after_calibration->Write();
  h_pt_with_wgt_y_inclusive_mu_14_21->Write();
  h_pt_with_wgt_y_inclusive_mu_21_28->Write();
  h_j0_pt_with_wgt_y_inclusive->Write();
  h_j0_pt_with_wgt_y_inclusive_FinerBins->Write();
  h_j0_pt_with_wgt_y_05_08->Write();
  h_j0_pt_with_wgt_eta_05_before_calibration->Write();
  h_j0_pt_with_wgt_eta_05_after_calibration->Write();
  h_j0_pt_with_wgt_y_inclusive_mu_14_21->Write();
  h_j0_pt_with_wgt_y_inclusive_mu_21_28->Write();
  h_j1_pt_with_wgt_y_inclusive->Write();
  h_j1_pt_with_wgt_y_05_08->Write();
  h_j1_pt_with_wgt_eta_05_before_calibration->Write();
  h_j1_pt_with_wgt_eta_05_after_calibration->Write();
  h_j1_pt_with_wgt_y_inclusive_mu_14_21->Write();
  h_j1_pt_with_wgt_y_inclusive_mu_21_28->Write();
  h_j2_pt_with_wgt_y_inclusive->Write();

  for(unsigned int i=0;i<nyBins;++i){
    h_pt_with_wgt[i]->Write();
    if(MC) h_pt_truth[i]->Write();
    h_pt_lowMu[i]->Write();
    h_pt_highMu[i]->Write();
    h_j0_pt_with_wgt[i]->Write();
    h_j1_pt_with_wgt[i]->Write();
    h_phi_yBins[i]->Write();
    h_j0_pt_mjj[i]->Write();
    h_j1_pt_mjj[i]->Write();
    if(m_unfolding){
      if(MC){
	if(m_bootstraps){
          fTruthBootstrap_inc[i]->Write();
          fBootstrap_inc[i]->Write();
	}
	h_pt_TM[i]->Write();
      }
      if(m_bootstraps){    
        fRecoBootstrap_inc[i]->Write();
      }
    }
  }
  if(m_runDijets){
    for(unsigned int i=0;i<nystarBins;++i){
      h_m12_reco[i]->Write();
      h_m12_reco_finer_bins[i]->Write();
      h_m12_reco_lowMu[i]->Write();
      h_m12_reco_highMu[i]->Write();
      h_m12_truth_finer_bins[i]->Write();
      h_m12_truth[i]->Write();
      if(m_unfolding){
	if(m_bootstraps) fRecoBootstrap_m12[i]->Write();
        if(MC) {
	  if(m_bootstraps){
            fTruthBootstrap_m12[i]->Write();
            fBootstrap_m12[i]->Write();
	  }
	  h_m12_TM[i]->Write();
	  
        }
      }
    }
    h_m12_reco_ystar_inclusive->Write();
    h_m12_reco_ystar_inclusive_mu_14_21->Write();
    h_m12_reco_ystar_inclusive_mu_21_28->Write();
    h_m12_reco_centralregion->Write();
    h_m12_reco_centralregion_mu_14_21->Write();
    h_m12_reco_centralregion_mu_21_28->Write();
  }
  for(unsigned int m=firstTrig;m<Triggers.size();m++){
    h_pt_y_inclusive_triggers[Triggers.at(m)]->Write();
    h_eta_Triggers[Triggers.at(m)]->Write();
    h_eta_j0_Triggers[Triggers.at(m)]->Write();
    h_eta_j1_Triggers[Triggers.at(m)]->Write();
    h_eta_Triggers_finerbins[Triggers.at(m)]->Write();
    h_eta_j0_Triggers_finerbins[Triggers.at(m)]->Write();
    h_eta_j1_Triggers_finerbins[Triggers.at(m)]->Write();
    h_eta_Triggers_phi_0_16[Triggers.at(m)]->Write();
    h_eta_j0_Triggers_phi_0_16[Triggers.at(m)]->Write();
    h_eta_j1_Triggers_phi_0_16[Triggers.at(m)]->Write();
    h_eta_Triggers_phi_16_32[Triggers.at(m)]->Write();
    h_eta_j0_Triggers_phi_16_32[Triggers.at(m)]->Write();
    h_eta_j1_Triggers_phi_16_32[Triggers.at(m)]->Write();
    h_eta_Triggers_phi_32_48[Triggers.at(m)]->Write();
    h_eta_j0_Triggers_phi_32_48[Triggers.at(m)]->Write();
    h_eta_j1_Triggers_phi_32_48[Triggers.at(m)]->Write();
    h_eta_Triggers_phi_48_64[Triggers.at(m)]->Write();
    h_eta_j0_Triggers_phi_48_64[Triggers.at(m)]->Write();
    h_eta_j1_Triggers_phi_48_64[Triggers.at(m)]->Write();
  }
  for(unsigned int m=0;m<Triggers.size();m++){
    h_pT_Triggers[Triggers.at(m)]->Write();
    h_pT_j0_Triggers[Triggers.at(m)]->Write();
    h_pT_j0_Triggers_eta3[Triggers.at(m)]->Write();
    h_pT_j0_Triggers_eta2[Triggers.at(m)]->Write();
    h_pT_j0_Triggers_eta1[Triggers.at(m)]->Write();
    h_pT_j1_Triggers[Triggers.at(m)]->Write();
  }
  for(unsigned int m=22;m<43;m++){
    h_eta_pTBins[m-22]->Write();
    h_eta_j0_pTBins[m-22]->Write();
    h_eta_j1_pTBins[m-22]->Write();
  }
  h_phi_eta_15_25->Write();
  h_phi_eta_20_25->Write();
  h_phi_eta_175_225->Write();
  h_phi_eta_15_25_pT_100_300->Write();
  h_phi_eta_20_25_pT_100_300->Write();
  h_phi_eta_175_225_pT_100_300->Write();
  h_phi_eta_15_25_pT_300_500->Write();
  h_phi_eta_20_25_pT_300_500->Write();
  h_phi_eta_175_225_pT_300_500->Write();
  h_phi_eta_15_25_pT_642_1992->Write();
  h_phi_eta_20_25_pT_642_1992->Write();
  h_phi_eta_175_225_pT_642_1992->Write();
  h_phi_eta_24_25_pT_442->Write();
  h_phi_eta_24_25_pT_408->Write();
  h_phi_eta_24_25_pT_346->Write();
  h_phi_eta_24_25_pT_264->Write();
  h_phi_eta_24_25_pT_642->Write();
  tout->Close();
  //  }

  // Creating logfile
  /*
  std::string logOutName("");
  if(MC) logOutName += "../../../MCHists/";
  else{ logOutName += "../../../DataHists/";}
  logOutName += version;
  logOutName += "/";
  if(!MC){
    logOutName += DataPeriod;
  }
  logOutName += CodeVersion;
  logOutName += "_log.txt";
  std::cout << "Logfile: " << logOutName << std::endl;

  freopen(logOutName.c_str(),"w",stdout);

  */
  
  std::cout << "Number of Jets (|y|<3.0): " << nInclusiveJets_y_Inclusive << std::endl; 
  std::cout << "Number of Jets (|y|<0.5): " << nInclusiveJets_CentralRegion << std::endl; 
  std::cout << "Number of Dijets (|y|<3.0): " << nDijets_y_Inclusive << std::endl; 
  std::cout << "Skipped events due to cleaning: " << Skippedevents_Cleaning << std::endl; 

  for(int i=0;i<h_pt_with_wgt_y_inclusive->GetNbinsX();++i){
    std::cout << "Number of jets with " << ptBins_0[i] << "<pT<" << ptBins_0[i+1] << ": " << h_pt_with_wgt_y_inclusive->GetBinContent(i+1) << std::endl;
  }

  std::cout << "Done!" << std::endl;

  return 0;

}

int GetYStarBin(double yStar) {
  if (yStar<0.5) return 0;
  if (yStar<1.0) return 1;
  if (yStar<1.5) return 2;
  if (yStar<2.0) return 3;
  if (yStar<2.5) return 4;
  if (yStar<3.0) return 5;
  if (yStar>=3.0) return 6;
  return 6;
}


void EnableBranches(){

  tree->SetBranchStatus("*",0); //disable all branches
  if(MC){
    tree->SetBranchStatus("mcChannelNumber",1);
    tree->SetBranchStatus("mcEventNumber",1);
    tree->SetBranchStatus("mcEventWeight",1);
  }
  if(!MC){ 
    tree->SetBranchStatus("runNumber",1);
    tree->SetBranchStatus("eventNumber",1);
    tree->SetBranchStatus("lumiBlock",1);
    tree->SetBranchStatus("bcid",1);
    tree->SetBranchStatus("passedTriggers",1);
  }
  tree->SetBranchStatus("NPV",1);
  tree->SetBranchStatus("averageInteractionsPerCrossing",1);

  if(MC) tree->SetBranchStatus("weight",1);
  if(MC && m_PRW) tree->SetBranchStatus("weight_pileup",1);
  if(!m_rel21) tree->SetBranchStatus("njets",1);
  tree->SetBranchStatus("jet_pt",1);
  tree->SetBranchStatus("jet_eta",1);
  tree->SetBranchStatus("jet_phi",1);
  tree->SetBranchStatus("jet_E",1);
  tree->SetBranchStatus("jet_rapidity",1);
  if(!m_rel21){
    tree->SetBranchStatus("jet_NumTrkPt1000",1);
    tree->SetBranchStatus("jet_TrackWidthPt1000",1);
  }
  tree->SetBranchStatus("jet_clean_passTightBad",1);
  tree->SetBranchStatus("jet_clean_passLooseBad",1);
  tree->SetBranchStatus("jet_EnergyPerSampling",1);
  if(!m_rel21){
   tree->SetBranchStatus("jet__Width",1);
   tree->SetBranchStatus("jet__EMFrac",1);
   tree->SetBranchStatus("jet__GhostMuonSegmentCount",1);
  }else{
   tree->SetBranchStatus("jet_Width",1);
   tree->SetBranchStatus("jet_EMFrac",1);
   tree->SetBranchStatus("jet_GhostMuonSegmentCount",1);
  }
  if(MC){
    if(!m_rel21) tree->SetBranchStatus("ntruthJets",1);
    tree->SetBranchStatus("truthJet_pt",1);
    tree->SetBranchStatus("truthJet_eta",1);
    tree->SetBranchStatus("truthJet_phi",1);
    tree->SetBranchStatus("truthJet_E",1);
    tree->SetBranchStatus("truthJet_rapidity",1);
  }

}// EnableBranches()

void SetBranches(){

  // Event
  if(MC){
    tree->SetBranchAddress("mcChannelNumber",&runNumber);
    tree->SetBranchAddress("mcEventNumber",&mceventNumber);
    tree->SetBranchAddress("mcEventWeight",&mcEventWeight);
  }
  if(!MC){
    tree->SetBranchAddress("runNumber",&runNumber);
    tree->SetBranchAddress("eventNumber",&eventNumber);
    tree->SetBranchAddress("lumiBlock",&lumiBlock);
    tree->SetBranchAddress("bcid",&bcid);
    tree->SetBranchAddress("passedTriggers",&passedTriggers);
  }
  tree->SetBranchAddress("NPV",&NPV);
  tree->SetBranchAddress("averageInteractionsPerCrossing",&mu);

  // Reco Jets  
  if(MC) tree->SetBranchAddress("weight",&wgt);
  if(MC && m_PRW) tree->SetBranchAddress("weight",&wgt_pileup);
  if(!m_rel21) tree->SetBranchAddress("njets",&njet);
  tree->SetBranchAddress("jet_pt",&jet_pt);
  tree->SetBranchAddress("jet_eta",&jet_eta);
  tree->SetBranchAddress("jet_phi",&jet_phi);
  tree->SetBranchAddress("jet_E",&jet_E);
  tree->SetBranchAddress("jet_rapidity",&jet_rapidity);
  if(!m_rel21){
    tree->SetBranchAddress("jet_NumTrkPt1000",&jet_NumTrkPt1000);
    tree->SetBranchAddress("jet_TrackWidthPt1000",&jet_TrackWidthPt1000);
  }
  tree->SetBranchAddress("jet_EnergyPerSampling",&jet_EnergyPerSampling);
  tree->SetBranchAddress("jet_clean_passTightBad",&jet_clean_passTightBad);
  tree->SetBranchAddress("jet_clean_passLooseBad",&jet_clean_passLooseBad);
  if(!m_rel21){
    tree->SetBranchAddress("jet__Width",&jet_width);
    tree->SetBranchAddress("jet__EMFrac",&jet_EMFrac); 
    tree->SetBranchAddress("jet__GhostMuonSegmentCount",&jet_Nsegments);
  }else{
    tree->SetBranchAddress("jet_Width",&jet_width);
    tree->SetBranchAddress("jet_EMFrac",&jet_EMFrac); 
    tree->SetBranchAddress("jet_GhostMuonSegmentCount",&jet_Nsegments);
  }
  // Truth jets
  if(MC){
    if(!m_rel21) tree->SetBranchAddress("ntruthJets",&ntruthjets);
    tree->SetBranchAddress("truthJet_pt",&truthjet_pt);
    tree->SetBranchAddress("truthJet_eta",&truthjet_eta);
    tree->SetBranchAddress("truthJet_phi",&truthjet_phi);
    tree->SetBranchAddress("truthJet_E",&truthjet_E);
    tree->SetBranchAddress("truthJet_rapidity",&truthjet_rapidity);
  }

}// SetBranches()

void SetBinning(){

  // eta bins for eta histogram
  for (unsigned int i = 0; i <= netaBins; i++ ) {
    etaBins[i]= -4.5+(i/10.0);
  }

  // Finer eta bins for eta histogram
  for (unsigned int i = 0; i <= nfineretaBins; i++ ) {
    fineretaBins[i]= -4.5+(i/50.0);
  }

  // EMFrac bins
  for (unsigned int i = 0; i <= nEMFracBins; i++ ) {
    EMFracBins[i]= i/nEMFracBins;
  }

  // phi bins for phi histogram
  for (unsigned int i = 0; i <= nphiBins; i++ ) {
    phiBins[i]= -3.2+(i/10.0);
  }

  // y bins for y histograms
  for (unsigned int i = 0; i <= nyBinsyHisto; i++ ) {
    yBinsyHisto[i]= -3.+(i/10.0);
  }

}// SetBinning()

int RecoMatch_index(TLorentzVector myjet, vector<TLorentzVector> jets, TLorentzVector &matchingJet, int &index, double &DRmin) {
  int Nmatches=0;
  double drmin = 999;
  int Min_index=-1;
  for (unsigned int ji=0;ji<jets.size();++ji) {
    double dr = myjet.DeltaR(jets[ji]);
    if (dr < 0.3) ++Nmatches;
    //find minimum:
    if (dr < drmin) {
      drmin = dr;
      Min_index = ji;
    }
  }
  DRmin = drmin;
  if (drmin<0.3) { //FIXME
    matchingJet=jets[Min_index]; index=Min_index;
  }
  return Nmatches;
}

int Match_index_inv(TLorentzVector myjet, vector<TLorentzVector> jets, int index_veto, double DRmin) {
  int Nmatches=0;
  double drmin = 999;
  for (unsigned int ji=0;ji<jets.size();++ji) {
    double dr = myjet.DeltaR(jets[ji]);
    if (ji == (unsigned int)index_veto) continue;
    if (dr < drmin) {
      drmin = dr;
    }
  }
  if (drmin < DRmin) ++Nmatches;
  return Nmatches;
}

double DRmin(TLorentzVector myjet, vector<TLorentzVector> jets, double PtMin) {
  double DRmin=9999;
  for (unsigned int ji=0;ji<jets.size();++ji) {
    if (PtMin>0&&jets[ji].Pt()<PtMin) continue;
    double DR=myjet.DeltaR(jets[ji]);
    if ( DR>0.001 && DR<DRmin ) DRmin=DR;
  }
  return DRmin;
}
