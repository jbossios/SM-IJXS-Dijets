#!/usr/bin/python

import os, sys

param=sys.argv

print str(sys.argv)

PATH=''

Debug     = True # No output, small number of events, only many couts used for debugging

#logfile=False
logfile=True

datasets=['datalowmu2017', 'datalowmu2018']

JOBname='datatest'
if len(sys.argv)>1:
 JOBname=sys.argv[1]

if JOBname != 'datatest': 
 if JOBname not in datasets:
  print '------> dataset ',JOBname,' not found '
  print '------> use one of these options: ',datasets
  exit (1)
  
#print '------> dataset ',JOBname

if JOBname == 'datatest': 
 PATH = "/data/run355389test"
 logfile=False 
else:
 #PATH = "/data/run355389"
 Debug=False
 if JOBname == 'datalowmu2017':
  PATH = "/disk/tree2017/"
 elif JOBname == 'datalowmu2018':
  PATH = "/disk/tree2018/"
 else:
  print 'JOBname= ',JOBname,' not known use ',datasets
  exit (1)

print 'PATH= ',PATH
print 'JOBname= ',JOBname


MC        = False
PRW       = False

# Unfolding inputs
UnfoldingInputs = False
nReplicas       = "0" # 0: still produces unfolding inputs (TMs, etc) but with no bootstraps (default: 100)

Generator = "Pythia"
#Generator = "Powheg"
#Generator = "PowhegHerwig"
#Generator = "Sherpa"

OutputFile = "OUTPUT_"+JOBname
OutputFile+='/'
#OutputFile = "OUTPUT_unfolding_100Replicas/"

#os.system('rmdir OutputFile')
#os.system('mkdir OutputFile')

#LumiFile = "lumi_2016data_finalGRL_All_02Dec.txt" # Only for Data
#LumiFile = "/home/tcarli/docker-21.2.52/SM-IJXS-Dijets/TreeReader/source/TreeReader/Luminosities/lumi_test.txt"
LumiFile = "/home/tcarli/docker-21.2.52/SM-IJXS-Dijets/TreeReader/source/TreeReader/Luminosities/Lumi_data18_lowMu.txt"

FirstTrigger = "HLT_j15"

Rel21 = True # Needed for latest trees

####################################################################################
####################################################################################

counter = 0

# Get list of folders
Folders = []
print 'PATH= ',PATH

if not os.path.isdir(PATH):
 print 'Path does not exist ',PATH
 exit()
 
#print 'walk= ',os.walk(PATH)
#
#if Debug:
# print 'walk= ',os.walk(PATH).next()
print 'walk=1 ',os.walk(PATH).next()[1]
print 'walk=2 ',os.walk(PATH).next()[2]
 
#
#
fPATH=PATH
folderlist=os.walk(PATH).next()[1]
if not folderlist:
 folderlist=os.walk(PATH).next()[2]
print 'folderlist= ',folderlist 
for folder in folderlist:
  print 'folder= ',folder
  if "tree" in folder:
    if not os.walk(PATH).next()[1]:
     fPATH = PATH       
    else:
     fPATH = PATH + "/" + folder
    print 'fPATH= ',fPATH

    if (not folderlist):
     folderlist2=[folder]
    else:
     folderlist2=os.walk(fPATH).next()[2]

    print 'folderlist2= ',folderlist2
     
    for rfile in folderlist2:
#
#    for rfile in os.walk(fPATH).next()[2]:
      if Debug:
       print 'run rfile= ',rfile
      InputFile = rfile	    
      OutFile   = OutputFile + rfile

      if Debug:
       print 'InputFile= ',InputFile,' OutFile= ',OutFile,' MC= ',MC,' PRW= ',PRW,' UnfoldingInputs= ',UnfoldingInputs


      print 'rfile= ',rfile 
      #exit (1)
       
      command = "nice -19 "       
      if MC:
        command += "TreetoHists "
        command += "--path="
        command += fPATH
        command += " --inputFile="
        command += InputFile
        command += " --outputFile="
        command += OutFile
        command += " --isMC=TRUE"
        command += " --mcGenerator="
        command += Generator
        if UnfoldingInputs:
          command += " --unfolding=TRUE"
	  command += " --nReplicas="
	  command += nReplicas
        if Debug:
          command += " --debug=TRUE"
        if Rel21:
          command += " --rel21=TRUE"	  
	if PRW:
          command += " --prw=TRUE"

        if logfile:  
         command += " > "
	 command += OutputFile+"/"
         command += "log_"
	 command += str(counter)
	 command += " 2> "
	 command += OutputFile+"/"
	 command += "err_"
	 command += str(counter)
	 command += " & "
        else:
         command += " & "
         
      if not MC:
        command += "TreetoHists "
        command += "--path="
        command += fPATH
        command += " --inputFile="
        command += InputFile
        command += " --outputFile="
        command += OutFile
        command += " --isMC=FALSE"
        command += " --lumiFile="
        command += LumiFile
        command += " --firstTrigger="
        command += FirstTrigger
        if UnfoldingInputs:
          command += " --unfolding=TRUE"
	  command += " --nReplicas="
	  command += nReplicas
        if Rel21:
          command += " --rel21=TRUE"	  
        if Debug:
          command += " --debug=TRUE"
#        command += " "
       
        if logfile:            
         command += " > "
 	 command += OutputFile+"/"
         command += "log_"
	 command += str(counter)
	 command += " 2> "
	 command += OutputFile+"/"
	 command += "err_"
	 command += str(counter)
         command += " & "
        else:
         command += " & "        
      counter += 1

#  

      print 'run --> ',command
      os.system(command)
 
