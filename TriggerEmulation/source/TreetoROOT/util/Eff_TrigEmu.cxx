// Includes

#include <sstream>
#include <string>
#include <map>
#include <sstream>
#include <string>
#include <vector>
#include <string>
#include <iostream>
#include <fstream>
#include <stdlib.h>
#include <algorithm>
#include <utility>
#include <math.h>
#include <stdio.h>

// ROOT includes
#include <TROOT.h>
#include "TLatex.h"
#include "TLorentzVector.h"
#include "TFile.h"
#include "TTree.h"
#include "TMathText.h"
#include "TCanvas.h"
#include "TChain.h"
#include "TFile.h"
#include "TH2.h"
#include "TStyle.h"
#include "TString.h"
#include "TStopwatch.h"
#include "TMath.h"
#include "TGraphAsymmErrors.h"
#include "TGraphErrors.h"
#include "TLine.h"
#include "TH1.h"
#include "TMathBase.h"
#include "TLegend.h"

// ATLAS styles includes
#include "/afs/cern.ch/user/j/jbossios/work/public/xAOD/Results/AtlasStyle/AtlasUtils.h"
#include "/afs/cern.ch/user/j/jbossios/work/public/xAOD/Results/AtlasStyle/AtlasStyle.C"

#include "TreetoROOT/global.h"

//-----------------------------------------------------------------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------------------------------------------------------------
// Editable

// Path
TString PATH("PATH/");

bool MCScaling = false;

// Data Luminosity
TString strLuminosity = "3.2";

bool PDF = true;
//-----------------------------------------------------------------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------------------------------------------------------------

#include "easyPlot/Plotter1D.h"

//---------------
// Main Function
//---------------

int main(){

  TString InputFile = "TrigEmu_Output.root";

  TString OutputFolder = "TrigEffPlots/";

  SetAtlasStyle();

  //----------
  // Triggers
  //----------

  Triggers.push_back("j15");
  Triggers.push_back("j25");
  Triggers.push_back("j35");
  Triggers.push_back("j45");
  Triggers.push_back("j55");
  Triggers.push_back("j60");
  Triggers.push_back("j85");
  Triggers.push_back("j110");
  Triggers.push_back("j150");
  Triggers.push_back("j175");
  Triggers.push_back("j200");
  Triggers.push_back("j260");
  Triggers.push_back("j360");

  std::map<std::string,int> TriggerThresholds;
  TriggerThresholds["j15"] = 25;
  TriggerThresholds["j25"] = 35;
  TriggerThresholds["j35"] = 45;
  TriggerThresholds["j45"] = 55;
  TriggerThresholds["j55"] = 70;
  TriggerThresholds["j60"] = 85;
  TriggerThresholds["j85"] = 116;
  TriggerThresholds["j110"] = 134;
  TriggerThresholds["j150"] = 194;
  TriggerThresholds["j175"] = 216;
  TriggerThresholds["j200"] = 264;
  TriggerThresholds["j260"] = 318;
  TriggerThresholds["j360"] = 442;

  //--------------------
  // Opening input files
  //--------------------

  // MC
  TString input = PATH; input += InputFile;
  std::cout << "Opening: " << input << std::endl;
  TFile* tin = new TFile(input,"read");

  //----------------------------
  // Inclusive Jet Distributions
  //----------------------------

  Plotter1D *pTBase = new Plotter1D();
  pTBase->SetOutputFolder(OutputFolder);
  pTBase->SetLegendPosition("TopLeft");
  pTBase->SetAxisTitles("p_{T}^ [GeV]","Efficiency");
  if(!PDF) pTBase->SetFormat("EPS");
  pTBase->SetLogx();
  pTBase->SetRange("X",55.,1000.);
  pTBase->SetRange("Y",0,1.1);
  pTBase->SetComparisonType("Ratio Only");
  //pTBase->JetColl("4EM"); // Use AddText() to add extra text
  pTBase->ShowLuminosity(strLuminosity);
  //pTBase->BinomialErrors(); // No implemented in Plotter1D
  pTBase->ShowNoErrorsSecondPanel();

  TString num;
  TString den;
  TString outputName;
  TString Type;
  Plotter1D *pT_Inclusive;
  for(unsigned int j=0;j<Triggers.size();++j){
    num = "pT_offline_num_";
    num += Triggers.at(j);
    std::cout << "Getting: " << num << std::endl;
    den = "pT_offline_den_";
    den += Triggers.at(j);
    std::cout << "Getting: " << den << std::endl;
    outputName = Triggers.at(j);
    pT_Inclusive = new Plotter1D(outputName.Data());
    pT_Inclusive->ApplyFormat(pTBase);
    pT_Inclusive->AddHist("den",tin,den.Data());
    pT_Inclusive->AddHist("num",tin,num.Data());
    pT_Inclusive->SetLegend("num",Triggers.at(j));
    pT_Inclusive->NoTLegend("den");

    // Show Threshold
    pT_Inclusive->PlotLineSecondPanel("X",TriggerThresholds[Triggers.at(j)]);

    if(Triggers.at(j)=="j85") pT_Inclusive->SetRange("X",10,300);
    if(Triggers.at(j)=="j60") pT_Inclusive->SetRange("X",10,300);
    if(Triggers.at(j)=="j55") pT_Inclusive->SetRange("X",10,300);
    if(Triggers.at(j)=="j45") pT_Inclusive->SetRange("X",10,300);
    if(Triggers.at(j)=="j15" || Triggers.at(j)=="j25" || Triggers.at(j)=="j35"){
      pT_Inclusive->SetRange("X",10,300);
    }
    pT_Inclusive->SetMoreLogLabels();
    pT_Inclusive->Write();
    delete pT_Inclusive;
  }

  return 0;

}


