# The name of the package:
atlas_subdir (TreeToROOT)

find_package( ROOT COMPONENTS Tree Matrix Hist RIO MathCore Physics)

message(${ROOT_INCLUDE_DIRS})
message(${ROOT_LIBRARIES})

# add the defined binaries:
atlas_add_executable (TrigEmulation util/TrigEmulation.cxx
    INCLUDE_DIRS ${ROOT_INCLUDE_DIRS}
    LINK_LIBRARIES ${ROOT_LIBRARIES})

atlas_add_executable (Eff_TrigEmu util/Eff_TrigEmu.cxx
    INCLUDE_DIRS ${ROOT_INCLUDE_DIRS}
    LINK_LIBRARIES easyPlot ${ROOT_LIBRARIES})
