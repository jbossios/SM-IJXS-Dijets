#include <vector>
#include <string>
#include <iostream>
#include <stdlib.h>

#include "TStopwatch.h"
#include "TROOT.h"
#include "TTree.h"
#include "TChain.h"
#include "TFile.h"
#include "TH1D.h"
#include "TLorentzVector.h"

using namespace std;

//----------------------------------------------------------------
// Edit
//----------------------------------------------------------------

// Path to tree
TString PATH       = "PATH";
// Name of tree
TString File       = "FILE.root";
// Path and name of output
TString OutputFile = "TrigEmu_Output.root"; 
// Debugging
bool m_debug       = false;

//----------------------------------------------------------------
// Don't touch
//----------------------------------------------------------------

// Triggers
std::vector<std::string > Triggers;

int main() {

    TStopwatch* time = new TStopwatch();

    time->Start();

    gROOT->ProcessLine(".L loader.C+");

    //----------
    // Triggers
    //----------

    Triggers.push_back("HLT_j15");
    Triggers.push_back("HLT_j25");
    Triggers.push_back("HLT_j35");
    Triggers.push_back("HLT_j45");
    Triggers.push_back("HLT_j55");
    Triggers.push_back("HLT_j60");
    Triggers.push_back("HLT_j85");
    Triggers.push_back("HLT_j110");
    Triggers.push_back("HLT_j150");
    Triggers.push_back("HLT_j175");
    Triggers.push_back("HLT_j200");
    Triggers.push_back("HLT_j260");
    Triggers.push_back("HLT_j360");

    std::map<std::string,bool> PassEvent_byTrigger;

    TString InputFile = PATH; InputFile += File;

    //-----------------------
    // Getting Tree
    //-----------------------

    std::cout << "Input file: " << InputFile << std::endl;

    TFile* ft = new TFile(InputFile.Data(),"open");
    ft->cd();

    if (ft == NULL) {
      std::cout<<"Returned null file: "<<ft<<std::endl;
      return 1;
    }

    TTree *tree = new TTree();
    tree = (TTree*)ft->Get("nominal");
    if (tree==NULL) {
      std::cout << "Returned null tree" << std::endl;
      return 1;
    }

    //----------
    // Variables
    //----------

    // Event
    int runNumber=0;
    Long64_t evNumber=0;

    std::vector<float> *jet_pt=0;
    std::vector<float> *jet_eta=0;
    std::vector<float> *jet_phi=0;
    std::vector<float> *jet_E=0;
    std::vector<int> *jet_clean_passLooseBad = 0;
    std::vector<float> *HLTjet_pt=0;
    std::vector<float> *HLTjet_eta=0;
    std::vector<float> *HLTjet_phi=0;
    std::vector<float> *HLTjet_E=0;

    std::vector<float> *L1jet_eta=0;
    std::vector<float> *L1jet_phi=0;
    std::vector<float> *L1jet_et88=0;

    // Passed Triggers
    std::vector<std::string> *passedTriggers = 0;

    //-------------------
    // Necessary branches
    //-------------------

    tree->SetBranchStatus("*",0); //disable all branches

    tree->SetBranchStatus("jet_pt",1);
    tree->SetBranchStatus("jet_eta",1);
    tree->SetBranchStatus("jet_phi",1);
    tree->SetBranchStatus("jet_E",1);

    tree->SetBranchStatus("HLTjet_pt",1);
    tree->SetBranchStatus("HLTjet_eta",1);
    tree->SetBranchStatus("HLTjet_phi",1);
    tree->SetBranchStatus("HLTjet_E",1);

    tree->SetBranchStatus("L1jet_eta",1);
    tree->SetBranchStatus("L1jet_phi",1);
    tree->SetBranchStatus("L1jet_et88",1);

    tree->SetBranchStatus("runNumber",1);
    tree->SetBranchStatus("eventNumber",1);
    tree->SetBranchStatus("jet_clean_passLooseBad",1);
    tree->SetBranchStatus("passedTriggers",1);


    tree->SetBranchAddress("runNumber",&runNumber);
    tree->SetBranchAddress("eventNumber",&evNumber);
    tree->SetBranchAddress("passedTriggers",&passedTriggers);

    tree->SetBranchAddress("jet_clean_passLooseBad",&jet_clean_passLooseBad);

    tree->SetBranchAddress("jet_pt",&jet_pt);
    tree->SetBranchAddress("jet_eta",&jet_eta);
    tree->SetBranchAddress("jet_phi",&jet_phi);
    tree->SetBranchAddress("jet_E",&jet_E);

    tree->SetBranchAddress("HLTjet_pt",&HLTjet_pt);
    tree->SetBranchAddress("HLTjet_eta",&HLTjet_eta);
    tree->SetBranchAddress("HLTjet_phi",&HLTjet_phi);
    tree->SetBranchAddress("HLTjet_E",&HLTjet_E);

    tree->SetBranchAddress("L1jet_eta",&L1jet_eta);
    tree->SetBranchAddress("L1jet_phi",&L1jet_phi);
    tree->SetBranchAddress("L1jet_et88",&L1jet_et88);

    int entries = tree->GetEntries();

    //-----------
    // Histograms
    //-----------

    // Numerator hists
    
    TH1D* pT_offline_num_j360 = new TH1D("pT_offline_num_j360","pT_offline_num_j360", 500,0,500);
    TH1D* pT_offline_num_j260 = new TH1D("pT_offline_num_j260","pT_offline_num_j260", 500,0,500);
    TH1D* pT_offline_num_j200 = new TH1D("pT_offline_num_j200","pT_offline_num_j200", 500,0,500);
    TH1D* pT_offline_num_j175 = new TH1D("pT_offline_num_j175","pT_offline_num_j175", 500,0,500);
    TH1D* pT_offline_num_j150 = new TH1D("pT_offline_num_j150","pT_offline_num_j150", 500,0,500);
    TH1D* pT_offline_num_j110 = new TH1D("pT_offline_num_j110","pT_offline_num_j110", 500,0,500);
    TH1D* pT_offline_num_j85 = new TH1D("pT_offline_num_j85","pT_offline_num_j85", 500,0,500);
    TH1D* pT_offline_num_j60 = new TH1D("pT_offline_num_j60","pT_offline_num_j60", 500,0,500);
    TH1D* pT_offline_num_j55 = new TH1D("pT_offline_num_j55","pT_offline_num_j55", 500,0,500);
    TH1D* pT_offline_num_j45 = new TH1D("pT_offline_num_j45","pT_offline_num_j45", 500,0,500);
    TH1D* pT_offline_num_j35 = new TH1D("pT_offline_num_j35","pT_offline_num_j35", 100,0,60);
    TH1D* pT_offline_num_j25 = new TH1D("pT_offline_num_j25","pT_offline_num_j25", 100,0,60);
    TH1D* pT_offline_num_j15 = new TH1D("pT_offline_num_j15","pT_offline_num_j15", 100,0,60);

    // Denominator hists
    TH1D* pT_offline_den_j360 = new TH1D("pT_offline_den_j360","pT_offline_den_j360", 500,0,500);
    TH1D* pT_offline_den_j260 = new TH1D("pT_offline_den_j260","pT_offline_den_j260", 500,0,500);
    TH1D* pT_offline_den_j200 = new TH1D("pT_offline_den_j200","pT_offline_den_j200", 500,0,500);
    TH1D* pT_offline_den_j175 = new TH1D("pT_offline_den_j175","pT_offline_den_j175", 500,0,500);
    TH1D* pT_offline_den_j150 = new TH1D("pT_offline_den_j150","pT_offline_den_j150", 500,0,500);
    TH1D* pT_offline_den_j110 = new TH1D("pT_offline_den_j110","pT_offline_den_j110", 500,0,500);
    TH1D* pT_offline_den_j85 = new TH1D("pT_offline_den_j85","pT_offline_den_j85", 500,0,500);
    TH1D* pT_offline_den_j60 = new TH1D("pT_offline_den_j60","pT_offline_den_j60", 500,0,500);
    TH1D* pT_offline_den_j55 = new TH1D("pT_offline_den_j55","pT_offline_den_j55", 500,0,500);
    TH1D* pT_offline_den_j45 = new TH1D("pT_offline_den_j45","pT_offline_den_j45", 500,0,60);
    TH1D* pT_offline_den_j35 = new TH1D("pT_offline_den_j35","pT_offline_den_j35", 100,0,60);
    TH1D* pT_offline_den_j25 = new TH1D("pT_offline_den_j25","pT_offline_den_j25", 100,0,60);
    TH1D* pT_offline_den_j15 = new TH1D("pT_offline_den_j15","pT_offline_den_j15", 100,0,60);

    //------------------
    // Loop over entries
    //------------------

    for (int i=0; i<entries; i++) {

        bool should_skip = false;

        tree->GetEntry(i);

        std::cout << "Entry = " << i << " of " << entries << std::endl;

        int njet = jet_pt->size();
        int nHLTjet = HLTjet_pt->size();
        int nL1jet = L1jet_eta->size();

	// Cut 0
	// No jets -> Skip event
        if(njet<1) continue;

	// Cut 1
        // Event Cleaning
        for(int j=0;j<njet;++j){
            if(jet_clean_passLooseBad->at(j)!=1){ // jet not passing LooseBad criteria
                should_skip = true;
                break;
            }
        }
        if(should_skip) continue; // skip event due to a non clean jet

	// Select Reco Jets
        vector<TLorentzVector> recoJets;
        for(int j=0;j<njet;++j){ // Loop over jets
            float  Pt = jet_pt->at(j);
            float  Eta = jet_eta->at(j);
            float  Phi = jet_phi->at(j);
            float  E = jet_E->at(j);
            TLorentzVector Jet;
            Jet.SetPtEtaPhiE(Pt,Eta,Phi,E);
            if (njet>=2) {
                if (jet_pt->at(0)<jet_pt->at(1)) {
                    cout<<"NOT SORTED BY PT!"<<endl;
                }
            }

	    // Jet pT and eta selection
            double pT_min=5; // HLT_j60
            if(Jet.Pt()<pT_min || fabs(Jet.Rapidity())>=3.0) continue;

            recoJets.push_back(Jet);
        }// End: Loop over jets

	// Cut 2
	// At least 1 jet
        if (recoJets.size() <1) continue;

	// Turn on are constructed as a function of pT_offl (leading pT reco jet)
        float pT_offl = recoJets.at(0).Pt();

        PassEvent_byTrigger["HLT_j15"] = false;
        PassEvent_byTrigger["HLT_j25"] = false;
        PassEvent_byTrigger["HLT_j35"] = false;
        PassEvent_byTrigger["HLT_j45"] = false;
        PassEvent_byTrigger["HLT_j55"] = false;
        PassEvent_byTrigger["HLT_j60"] = false;
        PassEvent_byTrigger["HLT_j85"] = false;
        PassEvent_byTrigger["HLT_j110"] = false;
        PassEvent_byTrigger["HLT_j150"] = false;
        PassEvent_byTrigger["HLT_j175"] = false;
        PassEvent_byTrigger["HLT_j200"] = false;
        PassEvent_byTrigger["HLT_j260"] = false;
        PassEvent_byTrigger["HLT_j360"] = false;

        // Decidying to use event for Triggers >= HLT_j45
        for(int j=0;j<nL1jet;++j){
          if (L1jet_et88->at(j) <= 15) continue;
          PassEvent_byTrigger["HLT_j45"] = true;
          PassEvent_byTrigger["HLT_j55"] = true;
          PassEvent_byTrigger["HLT_j60"] = true;
          PassEvent_byTrigger["HLT_j85"] = true;
          PassEvent_byTrigger["HLT_j110"] = true;
          PassEvent_byTrigger["HLT_j150"] = true;
          PassEvent_byTrigger["HLT_j175"] = true;
          PassEvent_byTrigger["HLT_j200"] = true;
          PassEvent_byTrigger["HLT_j260"] = true;
          PassEvent_byTrigger["HLT_j360"] = true;
        }

        // L1_RD0_FILLED (HLT_j15, HLT_j25, and HLT_j35)
        for(unsigned int lll=0;lll<passedTriggers->size();lll++){// Loop over passed triggers
          if(passedTriggers->at(lll)=="L1_RD0_FILLED") {
            PassEvent_byTrigger["HLT_j15"] = true;
            PassEvent_byTrigger["HLT_j25"] = true;
            PassEvent_byTrigger["HLT_j35"] = true;
          }
        }

	// Safe check
        if (nHLTjet == 0 ) {
            cout<<"Problem, Skip event"<<endl;
            cout<<"nJet: "<<njet<<endl;
            cout<<"nHLT: "<<nHLTjet<<endl;
            cout<<"nL1: "<<nL1jet<<endl;
            std::cout << "runNumber: " << runNumber << std::endl;
            std::cout << "eventNumber: " << evNumber << std::endl;
            continue; // skip event
        }

 	// Loop over Triggers
        for (unsigned int t=0; t<Triggers.size();t++) {

            if(m_debug) std::cout << "Trigger: " << Triggers.at(t) << std::endl;
            if(m_debug) std::cout << "PassEvent_byTrigger: " << PassEvent_byTrigger[(Triggers.at(t)).c_str()] << std::endl;

            // L1 passed? 
            if(!PassEvent_byTrigger[(Triggers.at(t)).c_str()]){
                continue; // Skip trigger
            }
            if(m_debug) std::cout << "Trigger passed" << std::endl;

            TString chain = Triggers.at(t);

            //Filling denominator distributions

            if(m_debug) std::cout << "Filling Denominator distributions" << std::endl;

            float et_cut = 1e6;
            float l1_cut = 1e6;

            if (chain == "HLT_j360") {
                et_cut = 360;
                l1_cut = 100;
                pT_offline_den_j360->Fill(pT_offl);
            } else if (chain == "HLT_j260") {
                et_cut = 260;
                l1_cut = 75;
                pT_offline_den_j260->Fill(pT_offl);
            } else if (chain == "HLT_j200") {
                et_cut = 200;
                l1_cut = 50;
                pT_offline_den_j200->Fill(pT_offl);
            } else if (chain == "HLT_j175") {
                et_cut = 175;
                l1_cut = 50;
                pT_offline_den_j175->Fill(pT_offl);
            } else if (chain == "HLT_j150") {
                et_cut = 150;
                l1_cut = 40;
                pT_offline_den_j150->Fill(pT_offl);
            } else if (chain == "HLT_j110") {
                et_cut = 110;
                l1_cut = 30;
                pT_offline_den_j110->Fill(pT_offl);
            } else if (chain == "HLT_j85") {
                et_cut = 85;
                l1_cut = 20;
                pT_offline_den_j85->Fill(pT_offl);
            } else if (chain == "HLT_j60") {
                et_cut = 60;
                l1_cut = 20;
                pT_offline_den_j60->Fill(pT_offl);
            } else if (chain == "HLT_j55") {
                et_cut = 55;
                l1_cut = 15;
                pT_offline_den_j55->Fill(pT_offl);
            } else if (chain == "HLT_j45") {
                et_cut = 45;
                l1_cut = 15;
                pT_offline_den_j45->Fill(pT_offl);
            } else if (chain == "HLT_j35") {
                et_cut = 35;
                pT_offline_den_j35->Fill(pT_offl);
            } else if (chain == "HLT_j25") {
                et_cut = 25;
                pT_offline_den_j25->Fill(pT_offl);
            } else if (chain == "HLT_j15") {
                et_cut = 15;
                pT_offline_den_j15->Fill(pT_offl);
            } else continue;

            //--------------------------
            // Emulation of the trigger
            //--------------------------

            if(m_debug) std::cout << "Emulation of the trigger" << std::endl;

            // Loop over HLT jets
            if(m_debug) std::cout << "Loop over HLT jets" << std::endl;
            vector<TLorentzVector> HLT_Jets;
            for(int j=0;j<nHLTjet;++j) {

		// Save HLT jets over the threshold of the trigger
                if (HLTjet_E->at(j)/cosh(HLTjet_eta->at(j))<=et_cut) continue;
		// Save HLT jets within the eta region of the central trigger
		if (fabs(HLTjet_eta->at(j))>3.2) continue;

                float  hlt_pt = HLTjet_pt->at(j);
                float  hlt_eta = HLTjet_eta->at(j);
                float  hlt_phi = HLTjet_phi->at(j);
                float  hlt_e = HLTjet_E->at(j);

                TLorentzVector v1;
                v1.SetPtEtaPhiE(hlt_pt,hlt_eta,hlt_phi,hlt_e);
                
                bool pass_L1_100 = false;

                if(chain == "HLT_j15" || chain == "HLT_j25" || chain == "HLT_j35") pass_L1_100 = true;
                else{ // Others triggers
                  for(int jj=0;jj<nL1jet;++jj) { // Loop over L1 jets
                    if(L1jet_et88->at(jj) <= l1_cut) continue;
                    pass_L1_100 = true;
                  }
                }    
                if (pass_L1_100) HLT_Jets.push_back(v1);
                
            }



            //Debugging: 
            //Evaluation 1 must pass if the emulation is ok for both prescaled and prescaled chains
            //Evaluation 2 might not pass for prescaled chains

            bool pass=false;
            for(unsigned int lll=0;lll<passedTriggers->size();lll++){// Loop over passed triggers
                if (passedTriggers->at(lll) == chain) {
                    pass = true;
                    if (HLT_Jets.size() ==0) {
                        cout<<"EVALUATION 1 FAILED! "<<"chain: "<<chain<<endl;
                    }
                }
            }

            if (HLT_Jets.size()>0) {
              if (!pass && chain=="HLT_j360") {// Prescaled triggers will probably fail
              cout<<"EVALUATION 2 FAILED! "<<"chain: "<<chain<<endl;
              }
            }

            // Filling numerator distributions

            if(m_debug) std::cout << "Filling Numerator Distributions" << std::endl;

            if (HLT_Jets.size() >0) {
                if (chain == "HLT_j360") {
                    pT_offline_num_j360->Fill(pT_offl);
                } else if (chain == "HLT_j260") {
                    pT_offline_num_j260->Fill(pT_offl);
                } else if (chain == "HLT_j200") {
                    pT_offline_num_j200->Fill(pT_offl);
                } else if (chain == "HLT_j175") {
                    pT_offline_num_j175->Fill(pT_offl);
                } else if (chain == "HLT_j150") {
                    pT_offline_num_j150->Fill(pT_offl);
                } else if (chain == "HLT_j110") {
                    pT_offline_num_j110->Fill(pT_offl);
                } else if (chain == "HLT_j85") {
                    pT_offline_num_j85->Fill(pT_offl);
                } else if (chain == "HLT_j60") {
                    pT_offline_num_j60->Fill(pT_offl);
                } else if (chain == "HLT_j55") {
                    pT_offline_num_j55->Fill(pT_offl);
                } else if (chain == "HLT_j45") {
                    pT_offline_num_j45->Fill(pT_offl);
                } else if (chain == "HLT_j35") {
                    pT_offline_num_j35->Fill(pT_offl);
                } else if (chain == "HLT_j25") {
                    pT_offline_num_j25->Fill(pT_offl);
                } else if (chain == "HLT_j15") {
                    pT_offline_num_j15->Fill(pT_offl);
                } else continue;
            }

        }// END: loop over Triggers

        runNumber = -999;
        jet_pt->clear();
        jet_eta->clear();
        jet_phi->clear();
        jet_E->clear();
        jet_clean_passLooseBad->clear();
        HLTjet_pt->clear();
        HLTjet_eta->clear();
        HLTjet_phi->clear();
        HLTjet_E->clear();
        L1jet_eta->clear();
        L1jet_phi->clear();
        L1jet_et88->clear();

    } // End: Loop over entries

    //------------------------------------
    // Saving Histogramas into a ROOT file
    //------------------------------------

    std::cout << "Saving plots into a ROOT file..." << std::endl;

    // Creating output file
    TFile* tout = new TFile(OutputFile,"recreate");
    std::cout << "Output File = " << OutputFile << std::endl;
    tout->cd();

    pT_offline_num_j360->Write();
    pT_offline_num_j260->Write();
    pT_offline_num_j200->Write();
    pT_offline_num_j175->Write();
    pT_offline_num_j150->Write();
    pT_offline_num_j110->Write();
    pT_offline_num_j85->Write();
    pT_offline_num_j60->Write();
    pT_offline_num_j55->Write();
    pT_offline_num_j45->Write();
    pT_offline_num_j35->Write();
    pT_offline_num_j25->Write();
    pT_offline_num_j15->Write();
    
    pT_offline_den_j360->Write();
    pT_offline_den_j260->Write();
    pT_offline_den_j200->Write();
    pT_offline_den_j175->Write();
    pT_offline_den_j150->Write();
    pT_offline_den_j110->Write();
    pT_offline_den_j85->Write();
    pT_offline_den_j60->Write();
    pT_offline_den_j55->Write();
    pT_offline_den_j45->Write();
    pT_offline_den_j35->Write();
    pT_offline_den_j25->Write();
    pT_offline_den_j15->Write();

    tout->Close();

    ft->Close();

    std::cout << "Done!" << std::endl;

}//END: main()
