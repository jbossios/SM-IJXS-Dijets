#! /bin/bash

cd source
setupATLAS
asetup AnalysisBase,21.2.51,here
cd ../build
cmake ../source
make
cd ..
source build/${CMTCONFIG}/setup.sh
