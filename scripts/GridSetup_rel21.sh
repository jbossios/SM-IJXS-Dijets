#! /bin/bash

cd source
setupATLAS
lsetup rucio "asetup AnalysisBase,21.2.51" panda
localSetupFAX
voms-proxy-init -voms atlas
cd ../build
cmake ../source
make
cd ..
source build/${CMTCONFIG}/setup.sh

        
