#!/usr/bin/python

import os, sys

param=sys.argv

#############################################################################################
# EDIT
#############################################################################################

date = "XX_YY_ZZZZ_AAA_BBB"

user = "user"

LocalPath = "localPATH"

# R20.7 samples
JETM9_2016Data       = False
JETM9_20_7_2015Data  = False
JETM9_Pythia_MC15c   = False

# R21 samples
JETM9_Pythia_MC16a   = False
JETM9_2016Data_rel21 = False
JETM9_2017Data       = False
JETM9_2017Data_Containers  = True

list=[
    "JZ0",  # only for MC16
    "JZ1",
    "JZ2",
    "JZ3",
    "JZ4",
    "JZ5",
    "JZ6",
    "JZ7",  
    "JZ8",  
    "JZ9",  
    "JZ10", 
    "JZ11", 
    "JZ12", 
]

#############################################################################################
# DO NOT MODIFY
#############################################################################################

Data2017_Containers=[ # containers created from 2017 samples below
  "user.ohaldik:data17_13TeV.periodB.physics_Main.deriv.DAOD_JETM9.f8XX_m18XX_p32XX",
  "user.ohaldik:data17_13TeV.periodC.physics_Main.deriv.DAOD_JETM9.f8XX_m18XX_p32XX",
  "user.ohaldik:data17_13TeV.periodD.physics_Main.deriv.DAOD_JETM9.f8XX_m18XX_p32XX",
  "user.ohaldik:data17_13TeV.periodE.physics_Main.deriv.DAOD_JETM9.f8XX_m18XX_p32XX", # 334737 older ftag
  "user.ohaldik:data17_13TeV.periodF.physics_Main.deriv.DAOD_JETM9.f8XX_m18XX_p32XX",
  "user.ohaldik:data17_13TeV.periodH.physics_Main.deriv.DAOD_JETM9.f8XX_m18XX_p32XX",
  "user.ohaldik:data17_13TeV.periodI.physics_Main.deriv.DAOD_JETM9.f8XX_m18XX_p32XX",
  "user.ohaldik:data17_13TeV.periodK.physics_Main.deriv.DAOD_JETM9.f8XX_m18XX_p32XX",
]

Data2017=[ # samples added on 10 Novemberg 2017
####    "data17_13TeV:data17_13TeV.00325789.physics_Main.deriv.DAOD_JETM9.f839_m1824_p3213", # Not in GRL v96
####    "data17_13TeV:data17_13TeV.00328017.physics_Main.deriv.DAOD_JETM9.f836_m1824_p3213", # Not in GRL v96
####    "data17_13TeV:data17_13TeV.00329385.physics_Main.deriv.DAOD_JETM9.f841_m1824_p3213", # Not in GRL v96
####    "data17_13TeV:data17_13TeV.00329484.physics_Main.deriv.DAOD_JETM9.f841_m1824_p3213", # Not in GRL v96
####    "data17_13TeV:data17_13TeV.00329542.physics_Main.deriv.DAOD_JETM9.f841_m1824_p3213", # Not in GRL v96
####    "data17_13TeV:data17_13TeV.00330328.physics_Main.deriv.DAOD_JETM9.f843_m1824_p3213", # Not in GRL v96
####    "data17_13TeV:data17_13TeV.00330857.physics_Main.deriv.DAOD_JETM9.f846_m1839_p3213", # Not in GRL v96
####    "data17_13TeV:data17_13TeV.00330874.physics_Main.deriv.DAOD_JETM9.f846_m1839_p3213", # Not in GRL v96
####    "data17_13TeV:data17_13TeV.00330875.physics_Main.deriv.DAOD_JETM9.f846_m1839_p3213", # Not in GRL v96
####    "data17_13TeV:data17_13TeV.00331019.physics_Main.deriv.DAOD_JETM9.f846_m1839_p3213", # Not in GRL v96
####    "data17_13TeV:data17_13TeV.00331020.physics_Main.deriv.DAOD_JETM9.f846_m1839_p3213", # Not in GRL v96
####    "data17_13TeV:data17_13TeV.00331239.physics_Main.deriv.DAOD_JETM9.f848_m1844_p3267", # Not in GRL v96
####    "data17_13TeV:data17_13TeV.00331462.physics_Main.deriv.DAOD_JETM9.f848_m1844_p3267", # Not in GRL v96
####    "data17_13TeV:data17_13TeV.00331466.physics_Main.deriv.DAOD_JETM9.f848_m1844_p3267", # Not in GRL v96
####    "data17_13TeV:data17_13TeV.00331479.physics_Main.deriv.DAOD_JETM9.f848_m1844_p3267", # Not in GRL v96
####    "data17_13TeV:data17_13TeV.00334710.physics_Main.deriv.DAOD_JETM9.f859_m1860_p3267", # Not in GRL v96
####    "data17_13TeV:data17_13TeV.00335302.physics_Main.deriv.DAOD_JETM9.f869_m1870_p3267", # Not in GRL v96
####    "data17_13TeV:data17_13TeV.00336497.physics_Main.deriv.DAOD_JETM9.f868_m1870_p3267", # Not in GRL v96
####    "data17_13TeV:data17_13TeV.00336497.physics_Main.deriv.DAOD_JETM9.f873_m1885_p3267", # Not in GRL v96
####    "data17_13TeV:data17_13TeV.00336505.physics_Main.deriv.DAOD_JETM9.f868_m1870_p3267", # Not in GRL v96
####    "data17_13TeV:data17_13TeV.00336505.physics_Main.deriv.DAOD_JETM9.f879_m1885_p3267", # Not in GRL v96
####    "data17_13TeV:data17_13TeV.00338480.physics_Main.deriv.DAOD_JETM9.f877_m1892_p3267", # Not in GRL v96
####    "data17_13TeV:data17_13TeV.00338498.physics_Main.deriv.DAOD_JETM9.f877_m1892_p3267", # Not in GRL v96
####    "data17_13TeV:data17_13TeV.00338675.physics_Main.deriv.DAOD_JETM9.f877_m1892_p3267", # Not in GRL v96
####    "data17_13TeV:data17_13TeV.00338767.physics_Main.deriv.DAOD_JETM9.f877_m1892_p3267", # Not in GRL v96
####    "data17_13TeV:data17_13TeV.00338834.physics_Main.deriv.DAOD_JETM9.f877_m1892_p3267", # Not in GRL v96
####    "data17_13TeV:data17_13TeV.00338846.physics_Main.deriv.DAOD_JETM9.f877_m1892_p3267", # Not in GRL v96
####    "data17_13TeV:data17_13TeV.00338933.physics_Main.deriv.DAOD_JETM9.f877_m1897_p3267", # Not in GRL v96
####    "data17_13TeV:data17_13TeV.00338967.physics_Main.deriv.DAOD_JETM9.f877_m1897_p3267", # Not in GRL v96
####    "data17_13TeV:data17_13TeV.00339197.physics_Main.deriv.DAOD_JETM9.f887_m1892_p3267", # Not in GRL v96
####    "data17_13TeV:data17_13TeV.00339197.physics_Main.deriv.DAOD_JETM9.f892_m1897_p3267", # Not in GRL v96
####    "data17_13TeV:data17_13TeV.00339346.physics_Main.deriv.DAOD_JETM9.f887_m1897_p3267", # Not in GRL v96
    "data17_13TeV:data17_13TeV.00325713.physics_Main.deriv.DAOD_JETM9.f839_m1824_p3213",
    "data17_13TeV:data17_13TeV.00325790.physics_Main.deriv.DAOD_JETM9.f839_m1824_p3213",
    "data17_13TeV:data17_13TeV.00326439.physics_Main.deriv.DAOD_JETM9.f838_m1824_p3213",
    "data17_13TeV:data17_13TeV.00326446.physics_Main.deriv.DAOD_JETM9.f838_m1824_p3213",
    "data17_13TeV:data17_13TeV.00326468.physics_Main.deriv.DAOD_JETM9.f838_m1824_p3213",
    "data17_13TeV:data17_13TeV.00326551.physics_Main.deriv.DAOD_JETM9.f838_m1824_p3213",
    "data17_13TeV:data17_13TeV.00326657.physics_Main.deriv.DAOD_JETM9.f838_m1824_p3213",
    "data17_13TeV:data17_13TeV.00326695.physics_Main.deriv.DAOD_JETM9.f838_m1824_p3213",
    "data17_13TeV:data17_13TeV.00326834.physics_Main.deriv.DAOD_JETM9.f837_m1824_p3213",
    "data17_13TeV:data17_13TeV.00326870.physics_Main.deriv.DAOD_JETM9.f837_m1824_p3213",
    "data17_13TeV:data17_13TeV.00326923.physics_Main.deriv.DAOD_JETM9.f837_m1824_p3213",
    "data17_13TeV:data17_13TeV.00326945.physics_Main.deriv.DAOD_JETM9.f837_m1824_p3213",
    "data17_13TeV:data17_13TeV.00327057.physics_Main.deriv.DAOD_JETM9.f837_m1824_p3213",
    "data17_13TeV:data17_13TeV.00327103.physics_Main.deriv.DAOD_JETM9.f838_m1824_p3213",
    "data17_13TeV:data17_13TeV.00327265.physics_Main.deriv.DAOD_JETM9.f838_m1824_p3213",
    "data17_13TeV:data17_13TeV.00327342.physics_Main.deriv.DAOD_JETM9.f838_m1824_p3213",
    "data17_13TeV:data17_13TeV.00327490.physics_Main.deriv.DAOD_JETM9.f838_m1824_p3213",
    "data17_13TeV:data17_13TeV.00327582.physics_Main.deriv.DAOD_JETM9.f838_m1824_p3213",
    "data17_13TeV:data17_13TeV.00327636.physics_Main.deriv.DAOD_JETM9.f838_m1824_p3213",
    "data17_13TeV:data17_13TeV.00327662.physics_Main.deriv.DAOD_JETM9.f838_m1824_p3213",
    "data17_13TeV:data17_13TeV.00327745.physics_Main.deriv.DAOD_JETM9.f838_m1824_p3213",
    "data17_13TeV:data17_13TeV.00327761.physics_Main.deriv.DAOD_JETM9.f838_m1824_p3213",
    "data17_13TeV:data17_13TeV.00327764.physics_Main.deriv.DAOD_JETM9.f836_m1824_p3213",
    "data17_13TeV:data17_13TeV.00327860.physics_Main.deriv.DAOD_JETM9.f836_m1824_p3213",
    "data17_13TeV:data17_13TeV.00327862.physics_Main.deriv.DAOD_JETM9.f836_m1824_p3213",
    "data17_13TeV:data17_13TeV.00328042.physics_Main.deriv.DAOD_JETM9.f836_m1824_p3213",
    "data17_13TeV:data17_13TeV.00328099.physics_Main.deriv.DAOD_JETM9.f836_m1824_p3213",
    "data17_13TeV:data17_13TeV.00328221.physics_Main.deriv.DAOD_JETM9.f836_m1824_p3213",
    "data17_13TeV:data17_13TeV.00328263.physics_Main.deriv.DAOD_JETM9.f836_m1824_p3213",
    "data17_13TeV:data17_13TeV.00328333.physics_Main.deriv.DAOD_JETM9.f836_m1824_p3213",
    "data17_13TeV:data17_13TeV.00328374.physics_Main.deriv.DAOD_JETM9.f836_m1824_p3213",
    "data17_13TeV:data17_13TeV.00328393.physics_Main.deriv.DAOD_JETM9.f836_m1824_p3213",
    "data17_13TeV:data17_13TeV.00329716.physics_Main.deriv.DAOD_JETM9.f847_m1839_p3213",
    "data17_13TeV:data17_13TeV.00329778.physics_Main.deriv.DAOD_JETM9.f843_m1824_p3213",
    "data17_13TeV:data17_13TeV.00329780.physics_Main.deriv.DAOD_JETM9.f843_m1824_p3213",
    "data17_13TeV:data17_13TeV.00329829.physics_Main.deriv.DAOD_JETM9.f843_m1824_p3213",
    "data17_13TeV:data17_13TeV.00329835.physics_Main.deriv.DAOD_JETM9.f843_m1824_p3213",
    "data17_13TeV:data17_13TeV.00329869.physics_Main.deriv.DAOD_JETM9.f843_m1824_p3213",
    "data17_13TeV:data17_13TeV.00329964.physics_Main.deriv.DAOD_JETM9.f843_m1824_p3213",
    "data17_13TeV:data17_13TeV.00330025.physics_Main.deriv.DAOD_JETM9.f843_m1824_p3213",
    "data17_13TeV:data17_13TeV.00330074.physics_Main.deriv.DAOD_JETM9.f843_m1824_p3213",
    "data17_13TeV:data17_13TeV.00330079.physics_Main.deriv.DAOD_JETM9.f843_m1824_p3213",
    "data17_13TeV:data17_13TeV.00330101.physics_Main.deriv.DAOD_JETM9.f843_m1824_p3213",
    "data17_13TeV:data17_13TeV.00330160.physics_Main.deriv.DAOD_JETM9.f843_m1824_p3213",
    "data17_13TeV:data17_13TeV.00330166.physics_Main.deriv.DAOD_JETM9.f843_m1824_p3213",
    "data17_13TeV:data17_13TeV.00330203.physics_Main.deriv.DAOD_JETM9.f843_m1824_p3213",
    "data17_13TeV:data17_13TeV.00330294.physics_Main.deriv.DAOD_JETM9.f843_m1824_p3213",
    "data17_13TeV:data17_13TeV.00330470.physics_Main.deriv.DAOD_JETM9.f843_m1824_p3213",
    "data17_13TeV:data17_13TeV.00331033.physics_Main.deriv.DAOD_JETM9.f846_m1839_p3267",
    "data17_13TeV:data17_13TeV.00331082.physics_Main.deriv.DAOD_JETM9.f846_m1839_p3267",
    "data17_13TeV:data17_13TeV.00331085.physics_Main.deriv.DAOD_JETM9.f859_m1860_p3267",
    "data17_13TeV:data17_13TeV.00331129.physics_Main.deriv.DAOD_JETM9.f848_m1844_p3267",
    "data17_13TeV:data17_13TeV.00331215.physics_Main.deriv.DAOD_JETM9.f848_m1844_p3267",
    "data17_13TeV:data17_13TeV.00331697.physics_Main.deriv.DAOD_JETM9.f848_m1844_p3267",
    "data17_13TeV:data17_13TeV.00331710.physics_Main.deriv.DAOD_JETM9.f848_m1844_p3267",
    "data17_13TeV:data17_13TeV.00331742.physics_Main.deriv.DAOD_JETM9.f848_m1844_p3267",
    "data17_13TeV:data17_13TeV.00331772.physics_Main.deriv.DAOD_JETM9.f848_m1844_p3267",
    "data17_13TeV:data17_13TeV.00331804.physics_Main.deriv.DAOD_JETM9.f848_m1844_p3267",
    "data17_13TeV:data17_13TeV.00331825.physics_Main.deriv.DAOD_JETM9.f848_m1844_p3267",
    "data17_13TeV:data17_13TeV.00331860.physics_Main.deriv.DAOD_JETM9.f848_m1844_p3267",
    "data17_13TeV:data17_13TeV.00331875.physics_Main.deriv.DAOD_JETM9.f848_m1844_p3267",
    "data17_13TeV:data17_13TeV.00331905.physics_Main.deriv.DAOD_JETM9.f848_m1844_p3267",
    "data17_13TeV:data17_13TeV.00331951.physics_Main.deriv.DAOD_JETM9.f848_m1844_p3267",
    "data17_13TeV:data17_13TeV.00331975.physics_Main.deriv.DAOD_JETM9.f851_m1850_p3267",
    "data17_13TeV:data17_13TeV.00332303.physics_Main.deriv.DAOD_JETM9.f851_m1850_p3267",
    "data17_13TeV:data17_13TeV.00332304.physics_Main.deriv.DAOD_JETM9.f851_m1850_p3267",
    "data17_13TeV:data17_13TeV.00332720.physics_Main.deriv.DAOD_JETM9.f851_m1850_p3267",
    "data17_13TeV:data17_13TeV.00332896.physics_Main.deriv.DAOD_JETM9.f854_m1850_p3267",
    "data17_13TeV:data17_13TeV.00332915.physics_Main.deriv.DAOD_JETM9.f854_m1850_p3267",
    "data17_13TeV:data17_13TeV.00332953.physics_Main.deriv.DAOD_JETM9.f854_m1850_p3267",
    "data17_13TeV:data17_13TeV.00332955.physics_Main.deriv.DAOD_JETM9.f854_m1850_p3267",
    "data17_13TeV:data17_13TeV.00333181.physics_Main.deriv.DAOD_JETM9.f854_m1850_p3267",
    "data17_13TeV:data17_13TeV.00333192.physics_Main.deriv.DAOD_JETM9.f857_m1855_p3267",
    "data17_13TeV:data17_13TeV.00333367.physics_Main.deriv.DAOD_JETM9.f857_m1855_p3267",
    "data17_13TeV:data17_13TeV.00333380.physics_Main.deriv.DAOD_JETM9.f857_m1855_p3267",
    "data17_13TeV:data17_13TeV.00333426.physics_Main.deriv.DAOD_JETM9.f857_m1855_p3267",
    "data17_13TeV:data17_13TeV.00333469.physics_Main.deriv.DAOD_JETM9.f857_m1855_p3267",
    "data17_13TeV:data17_13TeV.00333487.physics_Main.deriv.DAOD_JETM9.f857_m1855_p3267",
    "data17_13TeV:data17_13TeV.00333519.physics_Main.deriv.DAOD_JETM9.f857_m1855_p3267",
    "data17_13TeV:data17_13TeV.00333650.physics_Main.deriv.DAOD_JETM9.f857_m1855_p3267",
    "data17_13TeV:data17_13TeV.00333707.physics_Main.deriv.DAOD_JETM9.f857_m1855_p3267",
    "data17_13TeV:data17_13TeV.00333778.physics_Main.deriv.DAOD_JETM9.f857_m1855_p3267",
    "data17_13TeV:data17_13TeV.00333828.physics_Main.deriv.DAOD_JETM9.f857_m1855_p3267",
    "data17_13TeV:data17_13TeV.00333853.physics_Main.deriv.DAOD_JETM9.f857_m1855_p3267",
    "data17_13TeV:data17_13TeV.00333904.physics_Main.deriv.DAOD_JETM9.f857_m1855_p3267",
    "data17_13TeV:data17_13TeV.00333979.physics_Main.deriv.DAOD_JETM9.f859_m1860_p3267",
    "data17_13TeV:data17_13TeV.00333994.physics_Main.deriv.DAOD_JETM9.f859_m1860_p3267",
    "data17_13TeV:data17_13TeV.00334264.physics_Main.deriv.DAOD_JETM9.f859_m1860_p3267",
    "data17_13TeV:data17_13TeV.00334317.physics_Main.deriv.DAOD_JETM9.f859_m1860_p3267",
    "data17_13TeV:data17_13TeV.00334350.physics_Main.deriv.DAOD_JETM9.f859_m1860_p3267",
    "data17_13TeV:data17_13TeV.00334384.physics_Main.deriv.DAOD_JETM9.f859_m1860_p3267",
    "data17_13TeV:data17_13TeV.00334413.physics_Main.deriv.DAOD_JETM9.f859_m1860_p3267",
    "data17_13TeV:data17_13TeV.00334443.physics_Main.deriv.DAOD_JETM9.f859_m1860_p3267",
    "data17_13TeV:data17_13TeV.00334455.physics_Main.deriv.DAOD_JETM9.f859_m1860_p3267",
    "data17_13TeV:data17_13TeV.00334487.physics_Main.deriv.DAOD_JETM9.f859_m1860_p3267",
    "data17_13TeV:data17_13TeV.00334564.physics_Main.deriv.DAOD_JETM9.f859_m1860_p3267",
    "data17_13TeV:data17_13TeV.00334580.physics_Main.deriv.DAOD_JETM9.f859_m1860_p3267",
    "data17_13TeV:data17_13TeV.00334588.physics_Main.deriv.DAOD_JETM9.f859_m1860_p3267",
    "data17_13TeV:data17_13TeV.00334637.physics_Main.deriv.DAOD_JETM9.f859_m1860_p3267",
    "data17_13TeV:data17_13TeV.00334678.physics_Main.deriv.DAOD_JETM9.f859_m1860_p3267",
    "data17_13TeV:data17_13TeV.00334737.physics_Main.deriv.DAOD_JETM9.f861_m1865_p3267",
    "data17_13TeV:data17_13TeV.00334779.physics_Main.deriv.DAOD_JETM9.f867_m1860_p3267",
    "data17_13TeV:data17_13TeV.00334842.physics_Main.deriv.DAOD_JETM9.f867_m1860_p3267",
    "data17_13TeV:data17_13TeV.00334849.physics_Main.deriv.DAOD_JETM9.f867_m1860_p3267",
    "data17_13TeV:data17_13TeV.00334878.physics_Main.deriv.DAOD_JETM9.f867_m1860_p3267",
    "data17_13TeV:data17_13TeV.00334890.physics_Main.deriv.DAOD_JETM9.f867_m1860_p3267",
    "data17_13TeV:data17_13TeV.00334907.physics_Main.deriv.DAOD_JETM9.f867_m1860_p3267",
    "data17_13TeV:data17_13TeV.00334960.physics_Main.deriv.DAOD_JETM9.f868_m1870_p3267",
    "data17_13TeV:data17_13TeV.00334993.physics_Main.deriv.DAOD_JETM9.f868_m1870_p3267",
    "data17_13TeV:data17_13TeV.00335016.physics_Main.deriv.DAOD_JETM9.f868_m1870_p3267",
    "data17_13TeV:data17_13TeV.00335022.physics_Main.deriv.DAOD_JETM9.f868_m1870_p3267",
    "data17_13TeV:data17_13TeV.00335056.physics_Main.deriv.DAOD_JETM9.f868_m1870_p3267",
    "data17_13TeV:data17_13TeV.00335082.physics_Main.deriv.DAOD_JETM9.f868_m1870_p3267",
    "data17_13TeV:data17_13TeV.00335083.physics_Main.deriv.DAOD_JETM9.f868_m1870_p3267",
    "data17_13TeV:data17_13TeV.00335131.physics_Main.deriv.DAOD_JETM9.f868_m1870_p3267",
    "data17_13TeV:data17_13TeV.00335170.physics_Main.deriv.DAOD_JETM9.f868_m1870_p3267",
    "data17_13TeV:data17_13TeV.00335177.physics_Main.deriv.DAOD_JETM9.f868_m1870_p3267",
    "data17_13TeV:data17_13TeV.00335222.physics_Main.deriv.DAOD_JETM9.f868_m1870_p3267",
    "data17_13TeV:data17_13TeV.00335282.physics_Main.deriv.DAOD_JETM9.f868_m1870_p3267",
    "data17_13TeV:data17_13TeV.00335290.physics_Main.deriv.DAOD_JETM9.f868_m1870_p3267",
    "data17_13TeV:data17_13TeV.00336506.physics_Main.deriv.DAOD_JETM9.f873_m1885_p3267",
    "data17_13TeV:data17_13TeV.00336548.physics_Main.deriv.DAOD_JETM9.f873_m1885_p3267",
    "data17_13TeV:data17_13TeV.00336567.physics_Main.deriv.DAOD_JETM9.f873_m1885_p3267",
    "data17_13TeV:data17_13TeV.00336630.physics_Main.deriv.DAOD_JETM9.f868_m1870_p3267",
    "data17_13TeV:data17_13TeV.00336678.physics_Main.deriv.DAOD_JETM9.f868_m1870_p3267",
    "data17_13TeV:data17_13TeV.00336719.physics_Main.deriv.DAOD_JETM9.f868_m1870_p3267",
    "data17_13TeV:data17_13TeV.00336782.physics_Main.deriv.DAOD_JETM9.f868_m1879_p3267",
    "data17_13TeV:data17_13TeV.00336832.physics_Main.deriv.DAOD_JETM9.f868_m1879_p3267",
    "data17_13TeV:data17_13TeV.00336852.physics_Main.deriv.DAOD_JETM9.f871_m1879_p3267",
    "data17_13TeV:data17_13TeV.00336915.physics_Main.deriv.DAOD_JETM9.f871_m1879_p3267",
    "data17_13TeV:data17_13TeV.00336927.physics_Main.deriv.DAOD_JETM9.f871_m1879_p3267",
    "data17_13TeV:data17_13TeV.00336944.physics_Main.deriv.DAOD_JETM9.f871_m1879_p3267",
    "data17_13TeV:data17_13TeV.00336998.physics_Main.deriv.DAOD_JETM9.f871_m1879_p3267",
    "data17_13TeV:data17_13TeV.00337005.physics_Main.deriv.DAOD_JETM9.f871_m1879_p3267",
    "data17_13TeV:data17_13TeV.00337052.physics_Main.deriv.DAOD_JETM9.f871_m1879_p3267",
    "data17_13TeV:data17_13TeV.00337107.physics_Main.deriv.DAOD_JETM9.f871_m1879_p3267",
    "data17_13TeV:data17_13TeV.00337156.physics_Main.deriv.DAOD_JETM9.f871_m1879_p3267",
    "data17_13TeV:data17_13TeV.00337176.physics_Main.deriv.DAOD_JETM9.f871_m1879_p3267",
    "data17_13TeV:data17_13TeV.00337215.physics_Main.deriv.DAOD_JETM9.f871_m1879_p3267",
    "data17_13TeV:data17_13TeV.00337263.physics_Main.deriv.DAOD_JETM9.f871_m1879_p3267",
    "data17_13TeV:data17_13TeV.00337335.physics_Main.deriv.DAOD_JETM9.f871_m1879_p3267",
    "data17_13TeV:data17_13TeV.00337371.physics_Main.deriv.DAOD_JETM9.f871_m1879_p3267",
    "data17_13TeV:data17_13TeV.00337404.physics_Main.deriv.DAOD_JETM9.f871_m1879_p3267",
    "data17_13TeV:data17_13TeV.00337451.physics_Main.deriv.DAOD_JETM9.f873_m1879_p3267",
    "data17_13TeV:data17_13TeV.00337491.physics_Main.deriv.DAOD_JETM9.f873_m1885_p3355",
    "data17_13TeV:data17_13TeV.00337542.physics_Main.deriv.DAOD_JETM9.f873_m1885_p3267",
    "data17_13TeV:data17_13TeV.00337662.physics_Main.deriv.DAOD_JETM9.f877_m1885_p3267",
    "data17_13TeV:data17_13TeV.00337705.physics_Main.deriv.DAOD_JETM9.f877_m1885_p3267",
    "data17_13TeV:data17_13TeV.00337833.physics_Main.deriv.DAOD_JETM9.f886_m1885_p3267",
    "data17_13TeV:data17_13TeV.00338183.physics_Main.deriv.DAOD_JETM9.f877_m1885_p3267",
    "data17_13TeV:data17_13TeV.00338220.physics_Main.deriv.DAOD_JETM9.f877_m1885_p3267",
    "data17_13TeV:data17_13TeV.00338259.physics_Main.deriv.DAOD_JETM9.f877_m1885_p3267",
    "data17_13TeV:data17_13TeV.00338263.physics_Main.deriv.DAOD_JETM9.f877_m1885_p3267",
    "data17_13TeV:data17_13TeV.00338349.physics_Main.deriv.DAOD_JETM9.f877_m1885_p3267",
]

Data2016=[ # samples updated on 7 March 2017 to p2950
    "data16_13TeV:data16_13TeV.periodA.physics_Main.PhysCont.DAOD_JETM9.grp16_v01_p2950", # 296939 - 300287 (30 runs) 
    "data16_13TeV:data16_13TeV.periodB.physics_Main.PhysCont.DAOD_JETM9.grp16_v01_p2950", # 300345 - 300908 (13 runs)
    "data16_13TeV:data16_13TeV.periodC.physics_Main.PhysCont.DAOD_JETM9.grp16_v01_p2950", # 301973 - 302393 (14 runs)
    "data16_13TeV:data16_13TeV.periodD.physics_Main.PhysCont.DAOD_JETM9.grp16_v01_p2950", # 302737 - 303560 (20 runs)
    "data16_13TeV:data16_13TeV.periodE.physics_Main.PhysCont.DAOD_JETM9.grp16_v01_p2950", # 303638 - 303892 ( 8 runs)
    "data16_13TeV:data16_13TeV.periodF.physics_Main.PhysCont.DAOD_JETM9.grp16_v01_p2950", # 303943 - 304494 (13 runs)
    "data16_13TeV:data16_13TeV.periodG.physics_Main.PhysCont.DAOD_JETM9.grp16_v01_p2950", # 305291 - 306714 (27 runs)
    "data16_13TeV:data16_13TeV.periodI.physics_Main.PhysCont.DAOD_JETM9.grp16_v01_p2950", # 307124 - 308084 (22 runs)
    "data16_13TeV:data16_13TeV.periodK.physics_Main.PhysCont.DAOD_JETM9.grp16_v01_p2950", # 309311 - 309759 ( 8 runs)
    "data16_13TeV:data16_13TeV.periodL.physics_Main.PhysCont.DAOD_JETM9.grp16_v01_p2950", # 310015 - 311481 (27 runs)
]

Data2016_rel21=[ # Add more once available, Switched to JETM9
    "data16_13TeV:data16_13TeV.00298773.physics_Main.deriv.DAOD_JETM9.r9264_p3083_p3213",
    "data16_13TeV:data16_13TeV.00298771.physics_Main.deriv.DAOD_JETM9.r9264_p3083_p3213",
    "data16_13TeV:data16_13TeV.00300784.physics_Main.deriv.DAOD_JETM9.r9264_p3083_p3213",
    "data16_13TeV:data16_13TeV.00300418.physics_Main.deriv.DAOD_JETM9.r9264_p3083_p3213",
    "data16_13TeV:data16_13TeV.00302347.physics_Main.deriv.DAOD_JETM9.r9264_p3083_p3213",
    "data16_13TeV:data16_13TeV.00307126.physics_Main.deriv.DAOD_JETM9.r9264_p3083_p3213",
    "data16_13TeV:data16_13TeV.00309440.physics_Main.deriv.DAOD_JETM9.r9264_p3083_p3213",
    "data16_13TeV:data16_13TeV.00303943.physics_Main.deriv.DAOD_JETM9.r9264_p3083_p3213",
    "data16_13TeV:data16_13TeV.00302872.physics_Main.deriv.DAOD_JETM9.r9264_p3083_p3213",
    "data16_13TeV:data16_13TeV.00300571.physics_Main.deriv.DAOD_JETM9.r9264_p3083_p3213",
    "data16_13TeV:data16_13TeV.00298609.physics_Main.deriv.DAOD_JETM9.r9264_p3083_p3213",
    "data16_13TeV:data16_13TeV.00304128.physics_Main.deriv.DAOD_JETM9.r9264_p3083_p3213",
    "data16_13TeV:data16_13TeV.00303421.physics_Main.deriv.DAOD_JETM9.r9264_p3083_p3213",
    "data16_13TeV:data16_13TeV.00305735.physics_Main.deriv.DAOD_JETM9.r9264_p3083_p3213",
    "data16_13TeV:data16_13TeV.00306278.physics_Main.deriv.DAOD_JETM9.r9264_p3083_p3213",
    "data16_13TeV:data16_13TeV.00310249.physics_Main.deriv.DAOD_JETM9.r9264_p3083_p3213",
    "data16_13TeV:data16_13TeV.00307259.physics_Main.deriv.DAOD_JETM9.r9264_p3083_p3213",
    "data16_13TeV:data16_13TeV.00304409.physics_Main.deriv.DAOD_JETM9.r9264_p3083_p3213",
    "data16_13TeV:data16_13TeV.00304178.physics_Main.deriv.DAOD_JETM9.r9264_p3083_p3213",
    "data16_13TeV:data16_13TeV.00303304.physics_Main.deriv.DAOD_JETM9.r9264_p3083_p3213",
    "data16_13TeV:data16_13TeV.00311481.physics_Main.deriv.DAOD_JETM9.r9264_p3083_p3213",
    "data16_13TeV:data16_13TeV.00305380.physics_Main.deriv.DAOD_JETM9.r9264_p3083_p3213",
    "data16_13TeV:data16_13TeV.00305674.physics_Main.deriv.DAOD_JETM9.r9264_p3083_p3213",
    "data16_13TeV:data16_13TeV.00311244.physics_Main.deriv.DAOD_JETM9.r9264_p3083_p3213",
    "data16_13TeV:data16_13TeV.00305671.physics_Main.deriv.DAOD_JETM9.r9264_p3083_p3213",
    "data16_13TeV:data16_13TeV.00297730.physics_Main.deriv.DAOD_JETM9.r9264_p3083_p3213",
    "data16_13TeV:data16_13TeV.00311402.physics_Main.deriv.DAOD_JETM9.r9264_p3083_p3213",
    "data16_13TeV:data16_13TeV.00302919.physics_Main.deriv.DAOD_JETM9.r9264_p3083_p3213",
    "data16_13TeV:data16_13TeV.00303846.physics_Main.deriv.DAOD_JETM9.r9264_p3083_p3213",
    "data16_13TeV:data16_13TeV.00298690.physics_Main.deriv.DAOD_JETM9.r9264_p3083_p3213",
    "data16_13TeV:data16_13TeV.00299144.physics_Main.deriv.DAOD_JETM9.r9264_p3083_p3213",
    "data16_13TeV:data16_13TeV.00307732.physics_Main.deriv.DAOD_JETM9.r9264_p3083_p3213",
    "data16_13TeV:data16_13TeV.00307569.physics_Main.deriv.DAOD_JETM9.r9264_p3083_p3213",
    "data16_13TeV:data16_13TeV.00299055.physics_Main.deriv.DAOD_JETM9.r9264_p3083_p3213",
    "data16_13TeV:data16_13TeV.00305571.physics_Main.deriv.DAOD_JETM9.r9264_p3083_p3213",
    "data16_13TeV:data16_13TeV.00304198.physics_Main.deriv.DAOD_JETM9.r9264_p3083_p3213",
    "data16_13TeV:data16_13TeV.00302925.physics_Main.deriv.DAOD_JETM9.r9264_p3083_p3213",
    "data16_13TeV:data16_13TeV.00300487.physics_Main.deriv.DAOD_JETM9.r9264_p3083_p3213",
    "data16_13TeV:data16_13TeV.00299184.physics_Main.deriv.DAOD_JETM9.r9264_p3083_p3213",
    "data16_13TeV:data16_13TeV.00306419.physics_Main.deriv.DAOD_JETM9.r9264_p3083_p3213",
    "data16_13TeV:data16_13TeV.00305777.physics_Main.deriv.DAOD_JETM9.r9264_p3083_p3213",
    "data16_13TeV:data16_13TeV.00305618.physics_Main.deriv.DAOD_JETM9.r9264_p3083_p3213",
    "data16_13TeV:data16_13TeV.00311365.physics_Main.deriv.DAOD_JETM9.r9264_p3083_p3213",
    "data16_13TeV:data16_13TeV.00311170.physics_Main.deriv.DAOD_JETM9.r9264_p3083_p3213",
    "data16_13TeV:data16_13TeV.00300908.physics_Main.deriv.DAOD_JETM9.r9264_p3083_p3213",
    "data16_13TeV:data16_13TeV.00309390.physics_Main.deriv.DAOD_JETM9.r9264_p3083_p3213",
    "data16_13TeV:data16_13TeV.00307394.physics_Main.deriv.DAOD_JETM9.r9264_p3083_p3213",
    "data16_13TeV:data16_13TeV.00299241.physics_Main.deriv.DAOD_JETM9.r9264_p3083_p3213",
    "data16_13TeV:data16_13TeV.00303338.physics_Main.deriv.DAOD_JETM9.r9264_p3083_p3213",
    "data16_13TeV:data16_13TeV.00299147.physics_Main.deriv.DAOD_JETM9.r9264_p3083_p3213",
    "data16_13TeV:data16_13TeV.00304337.physics_Main.deriv.DAOD_JETM9.r9264_p3083_p3213",
    "data16_13TeV:data16_13TeV.00300800.physics_Main.deriv.DAOD_JETM9.r9264_p3083_p3213",
    "data16_13TeV:data16_13TeV.00308047.physics_Main.deriv.DAOD_JETM9.r9264_p3083_p3213",
    "data16_13TeV:data16_13TeV.00298967.physics_Main.deriv.DAOD_JETM9.r9264_p3083_p3213",
    "data16_13TeV:data16_13TeV.00302380.physics_Main.deriv.DAOD_JETM9.r9264_p3083_p3213",
    "data16_13TeV:data16_13TeV.00311287.physics_Main.deriv.DAOD_JETM9.r9264_p3083_p3213",
    "data16_13TeV:data16_13TeV.00302300.physics_Main.deriv.DAOD_JETM9.r9264_p3083_p3213",
    "data16_13TeV:data16_13TeV.00310370.physics_Main.deriv.DAOD_JETM9.r9264_p3083_p3213",
    "data16_13TeV:data16_13TeV.00307935.physics_Main.deriv.DAOD_JETM9.r9264_p3083_p3213",
    "data16_13TeV:data16_13TeV.00306269.physics_Main.deriv.DAOD_JETM9.r9264_p3083_p3213",
    "data16_13TeV:data16_13TeV.00310738.physics_Main.deriv.DAOD_JETM9.r9264_p3083_p3213",
    "data16_13TeV:data16_13TeV.00304006.physics_Main.deriv.DAOD_JETM9.r9264_p3083_p3213",
    "data16_13TeV:data16_13TeV.00299243.physics_Main.deriv.DAOD_JETM9.r9264_p3083_p3213",
    "data16_13TeV:data16_13TeV.00303079.physics_Main.deriv.DAOD_JETM9.r9264_p3083_p3213",
    "data16_13TeV:data16_13TeV.00308084.physics_Main.deriv.DAOD_JETM9.r9264_p3083_p3213",
    "data16_13TeV:data16_13TeV.00302393.physics_Main.deriv.DAOD_JETM9.r9264_p3083_p3213",
    "data16_13TeV:data16_13TeV.00310015.physics_Main.deriv.DAOD_JETM9.r9264_p3083_p3213",
    "data16_13TeV:data16_13TeV.00307454.physics_Main.deriv.DAOD_JETM9.r9264_p3083_p3213",
    "data16_13TeV:data16_13TeV.00304494.physics_Main.deriv.DAOD_JETM9.r9264_p3083_p3213",
    "data16_13TeV:data16_13TeV.00306451.physics_Main.deriv.DAOD_JETM9.r9264_p3083_p3213",
    "data16_13TeV:data16_13TeV.00307710.physics_Main.deriv.DAOD_JETM9.r9264_p3083_p3213",
    "data16_13TeV:data16_13TeV.00307601.physics_Main.deriv.DAOD_JETM9.r9264_p3083_p3213",
    "data16_13TeV:data16_13TeV.00301915.physics_Main.deriv.DAOD_JETM9.r9264_p3083_p3213",
    "data16_13TeV:data16_13TeV.00304008.physics_Main.deriv.DAOD_JETM9.r9264_p3083_p3213",
    "data16_13TeV:data16_13TeV.00306442.physics_Main.deriv.DAOD_JETM9.r9264_p3083_p3213",
    "data16_13TeV:data16_13TeV.00299343.physics_Main.deriv.DAOD_JETM9.r9264_p3083_p3213",
    "data16_13TeV:data16_13TeV.00302391.physics_Main.deriv.DAOD_JETM9.r9264_p3083_p3213",
    "data16_13TeV:data16_13TeV.00298862.physics_Main.deriv.DAOD_JETM9.r9264_p3083_p3213",
    "data16_13TeV:data16_13TeV.00305811.physics_Main.deriv.DAOD_JETM9.r9264_p3083_p3213",
    "data16_13TeV:data16_13TeV.00298595.physics_Main.deriv.DAOD_JETM9.r9264_p3083_p3213",
    "data16_13TeV:data16_13TeV.00305543.physics_Main.deriv.DAOD_JETM9.r9264_p3083_p3213",
    "data16_13TeV:data16_13TeV.00310634.physics_Main.deriv.DAOD_JETM9.r9264_p3083_p3213",
    "data16_13TeV:data16_13TeV.00306310.physics_Main.deriv.DAOD_JETM9.r9264_p3083_p3213",
    "data16_13TeV:data16_13TeV.00310468.physics_Main.deriv.DAOD_JETM9.r9264_p3083_p3213",
    "data16_13TeV:data16_13TeV.00310872.physics_Main.deriv.DAOD_JETM9.r9264_p3083_p3213",
    "data16_13TeV:data16_13TeV.00311473.physics_Main.deriv.DAOD_JETM9.r9264_p3083_p3213",
    "data16_13TeV:data16_13TeV.00303208.physics_Main.deriv.DAOD_JETM9.r9264_p3083_p3213",
    "data16_13TeV:data16_13TeV.00304308.physics_Main.deriv.DAOD_JETM9.r9264_p3083_p3213",
    "data16_13TeV:data16_13TeV.00301932.physics_Main.deriv.DAOD_JETM9.r9264_p3083_p3213",
    "data16_13TeV:data16_13TeV.00300279.physics_Main.deriv.DAOD_JETM9.r9264_p3083_p3213",
    "data16_13TeV:data16_13TeV.00310405.physics_Main.deriv.DAOD_JETM9.r9264_p3083_p3213",
    "data16_13TeV:data16_13TeV.00309516.physics_Main.deriv.DAOD_JETM9.r9264_p3083_p3213",
    "data16_13TeV:data16_13TeV.00307539.physics_Main.deriv.DAOD_JETM9.r9264_p3083_p3213",
    "data16_13TeV:data16_13TeV.00302137.physics_Main.deriv.DAOD_JETM9.r9264_p3083_p3213",
    "data16_13TeV:data16_13TeV.00310473.physics_Main.deriv.DAOD_JETM9.r9264_p3083_p3213",
    "data16_13TeV:data16_13TeV.00305920.physics_Main.deriv.DAOD_JETM9.r9264_p3083_p3213",
    "data16_13TeV:data16_13TeV.00303059.physics_Main.deriv.DAOD_JETM9.r9264_p3083_p3213",
    "data16_13TeV:data16_13TeV.00303201.physics_Main.deriv.DAOD_JETM9.r9264_p3083_p3213",
    "data16_13TeV:data16_13TeV.00310863.physics_Main.deriv.DAOD_JETM9.r9264_p3083_p3213",
    "data16_13TeV:data16_13TeV.00300687.physics_Main.deriv.DAOD_JETM9.r9264_p3083_p3213",
    "data16_13TeV:data16_13TeV.00304243.physics_Main.deriv.DAOD_JETM9.r9264_p3083_p3213",
    "data16_13TeV:data16_13TeV.00305723.physics_Main.deriv.DAOD_JETM9.r9264_p3083_p3213",
    "data16_13TeV:data16_13TeV.00300540.physics_Main.deriv.DAOD_JETM9.r9264_p3083_p3213",
    "data16_13TeV:data16_13TeV.00303892.physics_Main.deriv.DAOD_JETM9.r9264_p3083_p3213",
    "data16_13TeV:data16_13TeV.00301918.physics_Main.deriv.DAOD_JETM9.r9264_p3083_p3213",
    "data16_13TeV:data16_13TeV.00305727.physics_Main.deriv.DAOD_JETM9.r9264_p3083_p3213",
    "data16_13TeV:data16_13TeV.00302737.physics_Main.deriv.DAOD_JETM9.r9264_p3083_p3213",
    "data16_13TeV:data16_13TeV.00302265.physics_Main.deriv.DAOD_JETM9.r9264_p3083_p3213",
    "data16_13TeV:data16_13TeV.00300415.physics_Main.deriv.DAOD_JETM9.r9264_p3083_p3213",
    "data16_13TeV:data16_13TeV.00307514.physics_Main.deriv.DAOD_JETM9.r9264_p3083_p3213",
    "data16_13TeV:data16_13TeV.00303264.physics_Main.deriv.DAOD_JETM9.r9264_p3083_p3213",
    "data16_13TeV:data16_13TeV.00301912.physics_Main.deriv.DAOD_JETM9.r9264_p3083_p3213",
    "data16_13TeV:data16_13TeV.00298687.physics_Main.deriv.DAOD_JETM9.r9264_p3083_p3213",
    "data16_13TeV:data16_13TeV.00303499.physics_Main.deriv.DAOD_JETM9.r9264_p3083_p3213",
    "data16_13TeV:data16_13TeV.00303560.physics_Main.deriv.DAOD_JETM9.r9264_p3083_p3213",
    "data16_13TeV:data16_13TeV.00310809.physics_Main.deriv.DAOD_JETM9.r9264_p3083_p3213",
    "data16_13TeV:data16_13TeV.00298633.physics_Main.deriv.DAOD_JETM9.r9264_p3083_p3213",
    "data16_13TeV:data16_13TeV.00302269.physics_Main.deriv.DAOD_JETM9.r9264_p3083_p3213",
    "data16_13TeV:data16_13TeV.00303266.physics_Main.deriv.DAOD_JETM9.r9264_p3083_p3213",
    "data16_13TeV:data16_13TeV.00311321.physics_Main.deriv.DAOD_JETM9.r9264_p3083_p3213",
    "data16_13TeV:data16_13TeV.00300655.physics_Main.deriv.DAOD_JETM9.r9264_p3083_p3213",
    "data16_13TeV:data16_13TeV.00300863.physics_Main.deriv.DAOD_JETM9.r9264_p3083_p3213",
    "data16_13TeV:data16_13TeV.00302053.physics_Main.deriv.DAOD_JETM9.r9264_p3083_p3213",
    "data16_13TeV:data16_13TeV.00310247.physics_Main.deriv.DAOD_JETM9.r9264_p3083_p3213",
    "data16_13TeV:data16_13TeV.00303638.physics_Main.deriv.DAOD_JETM9.r9264_p3083_p3213",
    "data16_13TeV:data16_13TeV.00303291.physics_Main.deriv.DAOD_JETM9.r9264_p3083_p3213",
    "data16_13TeV:data16_13TeV.00299584.physics_Main.deriv.DAOD_JETM9.r9264_p3083_p3213",
    "data16_13TeV:data16_13TeV.00309759.physics_Main.deriv.DAOD_JETM9.r9264_p3083_p3213",
    "data16_13TeV:data16_13TeV.00311071.physics_Main.deriv.DAOD_JETM9.r9264_p3083_p3213",
    "data16_13TeV:data16_13TeV.00306448.physics_Main.deriv.DAOD_JETM9.r9264_p3083_p3213",
    "data16_13TeV:data16_13TeV.00307716.physics_Main.deriv.DAOD_JETM9.r9264_p3083_p3213",
    "data16_13TeV:data16_13TeV.00307656.physics_Main.deriv.DAOD_JETM9.r9264_p3083_p3213",
    "data16_13TeV:data16_13TeV.00302829.physics_Main.deriv.DAOD_JETM9.r9264_p3083_p3213",
    "data16_13TeV:data16_13TeV.00304211.physics_Main.deriv.DAOD_JETM9.r9264_p3083_p3213",
    "data16_13TeV:data16_13TeV.00310969.physics_Main.deriv.DAOD_JETM9.r9264_p3083_p3213",
    "data16_13TeV:data16_13TeV.00299288.physics_Main.deriv.DAOD_JETM9.r9264_p3083_p3213",
    "data16_13TeV:data16_13TeV.00302831.physics_Main.deriv.DAOD_JETM9.r9264_p3083_p3213",
    "data16_13TeV:data16_13TeV.00309375.physics_Main.deriv.DAOD_JETM9.r9264_p3083_p3213",
    "data16_13TeV:data16_13TeV.00306384.physics_Main.deriv.DAOD_JETM9.r9264_p3083_p3213",
    "data16_13TeV:data16_13TeV.00307354.physics_Main.deriv.DAOD_JETM9.r9264_p3083_p3213",
    "data16_13TeV:data16_13TeV.00301973.physics_Main.deriv.DAOD_JETM9.r9264_p3083_p3213",
    "data16_13TeV:data16_13TeV.00300345.physics_Main.deriv.DAOD_JETM9.r9264_p3083_p3213",
    "data16_13TeV:data16_13TeV.00310691.physics_Main.deriv.DAOD_JETM9.r9264_p3083_p3213",
    "data16_13TeV:data16_13TeV.00309674.physics_Main.deriv.DAOD_JETM9.r9264_p3083_p3213",
    "data16_13TeV:data16_13TeV.00307306.physics_Main.deriv.DAOD_JETM9.r9264_p3083_p3213",
    "data16_13TeV:data16_13TeV.00300600.physics_Main.deriv.DAOD_JETM9.r9264_p3083_p3213",
    "data16_13TeV:data16_13TeV.00303832.physics_Main.deriv.DAOD_JETM9.r9264_p3083_p3213",
    "data16_13TeV:data16_13TeV.00307195.physics_Main.deriv.DAOD_JETM9.r9264_p3083_p3213",
    "data16_13TeV:data16_13TeV.00307358.physics_Main.deriv.DAOD_JETM9.r9264_p3083_p3213",
    "data16_13TeV:data16_13TeV.00307861.physics_Main.deriv.DAOD_JETM9.r9264_p3083_p3213",
    "data16_13TeV:data16_13TeV.00309640.physics_Main.deriv.DAOD_JETM9.r9264_p3083_p3213",
    "data16_13TeV:data16_13TeV.00302956.physics_Main.deriv.DAOD_JETM9.r9264_p3083_p3213",
    "data16_13TeV:data16_13TeV.00307619.physics_Main.deriv.DAOD_JETM9.r9264_p3083_p3213",
    "data16_13TeV:data16_13TeV.00304431.physics_Main.deriv.DAOD_JETM9.r9264_p3083_p3213",
    "data16_13TeV:data16_13TeV.00310341.physics_Main.deriv.DAOD_JETM9.r9264_p3083_p3213",
    "data16_13TeV:data16_13TeV.00303007.physics_Main.deriv.DAOD_JETM9.r9264_p3083_p3213",
]


Data2015_20_7=[
 "data15_13TeV:data15_13TeV.00276262.physics_Main.merge.DAOD_JETM9.r7562_p2521_p2667",
 "data15_13TeV:data15_13TeV.00276329.physics_Main.merge.DAOD_JETM9.r7562_p2521_p2667",
 "data15_13TeV:data15_13TeV.00276336.physics_Main.merge.DAOD_JETM9.r7562_p2521_p2667",
 "data15_13TeV:data15_13TeV.00276416.physics_Main.merge.DAOD_JETM9.r7562_p2521_p2667",
 "data15_13TeV:data15_13TeV.00276511.physics_Main.merge.DAOD_JETM9.r7562_p2521_p2667",
 "data15_13TeV:data15_13TeV.00276689.physics_Main.merge.DAOD_JETM9.r7562_p2521_p2667",
 "data15_13TeV:data15_13TeV.00276778.physics_Main.merge.DAOD_JETM9.r7562_p2521_p2667",
 "data15_13TeV:data15_13TeV.00276790.physics_Main.merge.DAOD_JETM9.r7562_p2521_p2667",
 "data15_13TeV:data15_13TeV.00276952.physics_Main.merge.DAOD_JETM9.r7562_p2521_p2667",
 "data15_13TeV:data15_13TeV.00276954.physics_Main.merge.DAOD_JETM9.r7562_p2521_p2667",
 "data15_13TeV:data15_13TeV.00278880.physics_Main.merge.DAOD_JETM9.r7562_p2521_p2667",
 "data15_13TeV:data15_13TeV.00278912.physics_Main.merge.DAOD_JETM9.r7562_p2521_p2667",
 "data15_13TeV:data15_13TeV.00278968.physics_Main.merge.DAOD_JETM9.r7562_p2521_p2667",
 "data15_13TeV:data15_13TeV.00279169.physics_Main.merge.DAOD_JETM9.r7562_p2521_p2667",
 "data15_13TeV:data15_13TeV.00279259.physics_Main.merge.DAOD_JETM9.r7562_p2521_p2667",
 "data15_13TeV:data15_13TeV.00279279.physics_Main.merge.DAOD_JETM9.r7562_p2521_p2667",
 "data15_13TeV:data15_13TeV.00279284.physics_Main.merge.DAOD_JETM9.r7562_p2521_p2667",
 "data15_13TeV:data15_13TeV.00279345.physics_Main.merge.DAOD_JETM9.r7562_p2521_p2667",
 "data15_13TeV:data15_13TeV.00279515.physics_Main.merge.DAOD_JETM9.r7562_p2521_p2667",
 "data15_13TeV:data15_13TeV.00279598.physics_Main.merge.DAOD_JETM9.r7562_p2521_p2667",
 "data15_13TeV:data15_13TeV.00279685.physics_Main.merge.DAOD_JETM9.r7562_p2521_p2667",
 "data15_13TeV:data15_13TeV.00279764.physics_Main.merge.DAOD_JETM9.r7562_p2521_p2667",
 "data15_13TeV:data15_13TeV.00279813.physics_Main.merge.DAOD_JETM9.r7562_p2521_p2667",
 "data15_13TeV:data15_13TeV.00279867.physics_Main.merge.DAOD_JETM9.r7562_p2521_p2667",
 "data15_13TeV:data15_13TeV.00279928.physics_Main.merge.DAOD_JETM9.r7562_p2521_p2667",
 "data15_13TeV:data15_13TeV.00279932.physics_Main.merge.DAOD_JETM9.r7562_p2521_p2667",
 "data15_13TeV:data15_13TeV.00279984.physics_Main.merge.DAOD_JETM9.r7562_p2521_p2667",
 "data15_13TeV:data15_13TeV.00280231.physics_Main.merge.DAOD_JETM9.r7562_p2521_p2667",
 "data15_13TeV:data15_13TeV.00280319.physics_Main.merge.DAOD_JETM9.r7562_p2521_p2667",
 "data15_13TeV:data15_13TeV.00280368.physics_Main.merge.DAOD_JETM9.r7562_p2521_p2667",
 "data15_13TeV:data15_13TeV.00280423.physics_Main.merge.DAOD_JETM9.r7562_p2521_p2667",
 "data15_13TeV:data15_13TeV.00280464.physics_Main.merge.DAOD_JETM9.r7562_p2521_p2667",
 "data15_13TeV:data15_13TeV.00280500.physics_Main.merge.DAOD_JETM9.r7562_p2521_p2667",
 "data15_13TeV:data15_13TeV.00280520.physics_Main.merge.DAOD_JETM9.r7562_p2521_p2667",
 "data15_13TeV:data15_13TeV.00280614.physics_Main.merge.DAOD_JETM9.r7562_p2521_p2667",
 "data15_13TeV:data15_13TeV.00280673.physics_Main.merge.DAOD_JETM9.r7562_p2521_p2667",
 "data15_13TeV:data15_13TeV.00280753.physics_Main.merge.DAOD_JETM9.r7562_p2521_p2667",
 "data15_13TeV:data15_13TeV.00280853.physics_Main.merge.DAOD_JETM9.r7562_p2521_p2667",
 "data15_13TeV:data15_13TeV.00280862.physics_Main.merge.DAOD_JETM9.r7562_p2521_p2667",
 "data15_13TeV:data15_13TeV.00280950.physics_Main.merge.DAOD_JETM9.r7562_p2521_p2667",
 "data15_13TeV:data15_13TeV.00280977.physics_Main.merge.DAOD_JETM9.r7562_p2521_p2667",
 "data15_13TeV:data15_13TeV.00281070.physics_Main.merge.DAOD_JETM9.r7562_p2521_p2667",
 "data15_13TeV:data15_13TeV.00281074.physics_Main.merge.DAOD_JETM9.r7562_p2521_p2667",
 "data15_13TeV:data15_13TeV.00281075.physics_Main.merge.DAOD_JETM9.r7562_p2521_p2667",
 "data15_13TeV:data15_13TeV.00281317.physics_Main.merge.DAOD_JETM9.r7562_p2521_p2667",
 "data15_13TeV:data15_13TeV.00281385.physics_Main.merge.DAOD_JETM9.r7562_p2521_p2667",
 "data15_13TeV:data15_13TeV.00281411.physics_Main.merge.DAOD_JETM9.r7562_p2521_p2667",
 "data15_13TeV:data15_13TeV.00282625.physics_Main.merge.DAOD_JETM9.r7562_p2521_p2667",
 "data15_13TeV:data15_13TeV.00282631.physics_Main.merge.DAOD_JETM9.r7562_p2521_p2667",
 "data15_13TeV:data15_13TeV.00282712.physics_Main.merge.DAOD_JETM9.r7562_p2521_p2667",
 "data15_13TeV:data15_13TeV.00282784.physics_Main.merge.DAOD_JETM9.r7562_p2521_p2667",
 "data15_13TeV:data15_13TeV.00282992.physics_Main.merge.DAOD_JETM9.r7562_p2521_p2667",
 "data15_13TeV:data15_13TeV.00283074.physics_Main.merge.DAOD_JETM9.r7562_p2521_p2667",
 "data15_13TeV:data15_13TeV.00283155.physics_Main.merge.DAOD_JETM9.r7562_p2521_p2667",
 "data15_13TeV:data15_13TeV.00283270.physics_Main.merge.DAOD_JETM9.r7562_p2521_p2667",
 "data15_13TeV:data15_13TeV.00283429.physics_Main.merge.DAOD_JETM9.r7562_p2521_p2667",
 "data15_13TeV:data15_13TeV.00283608.physics_Main.merge.DAOD_JETM9.r7562_p2521_p2667",
 "data15_13TeV:data15_13TeV.00283780.physics_Main.merge.DAOD_JETM9.r7562_p2521_p2667",
 "data15_13TeV:data15_13TeV.00284006.physics_Main.merge.DAOD_JETM9.r7562_p2521_p2667",
 "data15_13TeV:data15_13TeV.00284154.physics_Main.merge.DAOD_JETM9.r7562_p2521_p2667",
 "data15_13TeV:data15_13TeV.00284213.physics_Main.merge.DAOD_JETM9.r7562_p2521_p2667",
 "data15_13TeV:data15_13TeV.00284285.physics_Main.merge.DAOD_JETM9.r7562_p2521_p2667",
 "data15_13TeV:data15_13TeV.00284420.physics_Main.merge.DAOD_JETM9.r7562_p2521_p2667",
 "data15_13TeV:data15_13TeV.00284427.physics_Main.merge.DAOD_JETM9.r7562_p2521_p2667",
 "data15_13TeV:data15_13TeV.00284484.physics_Main.merge.DAOD_JETM9.r7562_p2521_p2667",
]

if JETM9_Pythia_MC15c:
  for JZ in list:
    # Setting the correct sample
    if JZ == "JZ1":
      sample = "mc15_13TeV:mc15_13TeV.361021.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ1W.merge.DAOD_JETM9.e3569_s2576_s2132_r7725_r7676_p2666"
    if JZ == "JZ2":
      sample = "mc15_13TeV:mc15_13TeV.361022.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ2W.merge.DAOD_JETM9.e3668_s2576_s2132_r7725_r7676_p2666"
    if JZ == "JZ3":
      sample = "mc15_13TeV:mc15_13TeV.361023.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ3W.merge.DAOD_JETM9.e3668_s2576_s2132_r7725_r7676_p2666"
    if JZ == "JZ4":
      sample = "mc15_13TeV:mc15_13TeV.361024.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ4W.merge.DAOD_JETM9.e3668_s2576_s2132_r7725_r7676_p2666"
    if JZ == "JZ5":
      sample = "mc15_13TeV:mc15_13TeV.361025.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ5W.merge.DAOD_JETM9.e3668_s2576_s2132_r7725_r7676_p2666"
    if JZ == "JZ6":
      sample = "mc15_13TeV:mc15_13TeV.361026.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ6W.merge.DAOD_JETM9.e3569_s2608_s2183_r7725_r7676_p2666"
    if JZ == "JZ7":
      sample = "mc15_13TeV:mc15_13TeV.361027.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ7W.merge.DAOD_JETM9.e3668_s2608_s2183_r7725_r7676_p2666"
    if JZ == "JZ8":
      sample = "mc15_13TeV:mc15_13TeV.361028.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ8W.merge.DAOD_JETM9.e3569_s2576_s2132_r7772_r7676_p2666"
    if JZ == "JZ9":
      sample = "mc15_13TeV:mc15_13TeV.361029.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ9W.merge.DAOD_JETM9.e3569_s2576_s2132_r7772_r7676_p2666"
    if JZ == "JZ10":
      sample = "mc15_13TeV:mc15_13TeV.361030.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ10W.merge.DAOD_JETM9.e3569_s2576_s2132_r7772_r7676_p2666"
    if JZ == "JZ11":
      sample = "mc15_13TeV:mc15_13TeV.361031.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ11W.merge.DAOD_JETM9.e3569_s2608_s2183_r7772_r7676_p2666"
    if JZ == "JZ12":
      sample = "mc15_13TeV:mc15_13TeV.361032.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ12W.merge.DAOD_JETM9.e3668_s2608_s2183_r7772_r7676_p2666"

    command = "xAH_run.py "

    # Input dataset
    command += "--files "
    command += sample

    command += " --submitDir="
    command += "Outputs_Pythia_MC15c_"
    command += JZ

    # Config
    command += " --config "+LocalPath+"/xAODAnaHelpers/data/xah_run_MC.json"

    # Prun options
    command += " --inputRucio prun --optGridMergeOutput=1 --optGridNFilesPerJob=1.0 --optRemoveSubmitDir 1"


    # Output dataset
    command += " --optGridOutputSampleName=user."+user+"."
    command += date
    command += "."
    command += JZ

    # Logfiles
    command += " > logfile_MC15c_Pythia_"
    command += JZ
    command += " 2> errors_MC15c_Pythia_"
    command += JZ

    command += " &"

    print command
    os.system(command)


if JETM9_2016Data:
  for Dataset in Data2016:

    # Setting the correct sample
    container = Dataset[26:33]
    ptag      = Dataset[77:82]

    command = "xAH_run.py "

    # Input dataset
    command += "--files "
    command += Dataset

    command += " --submitDir="
    command += "Outputs_"
    command += container
    command += "_"
    command += ptag

    # Config
    command += " --config "+LocalPath+"/xAODAnaHelpers/data/xah_run_DATA.json"
    
    # Prun options
    command += " --inputRucio prun --optGridMergeOutput=1 --optGridNFilesPerJob=1.0 --optRemoveSubmitDir 1"

    # Output dataset
    command += " --optGridOutputSampleName=user."+user+"."
    command += date
    command += "."
    command += container
    command += "_"
    command += ptag

    # Logfiles
    command += " > logfile_2016data_"
    command += container
    command += "_"
    command += ptag
    command += " 2> errors_2016data_"
    command += container
    command += "_"
    command += ptag

    command += " &"

    print command
    os.system(command)

if JETM9_20_7_2015Data:
  for Dataset in Data2015_20_7:

    # Setting the correct sample
    dataset = Dataset[26:34]

    command = "xAH_run.py "

    # Input dataset
    command += "--files "
    command += Dataset

    command += " --submitDir="
    command += "Outputs_"
    command += dataset

    # Config
    command += " --config "+LocalPath+"/xAODAnaHelpers/data/xah_run_20_7_2015DATA.json"

    # Prun options
    command += " --inputRucio prun --optGridMergeOutput=1 --optGridNFilesPerJob=1.0 --optRemoveSubmitDir 1"

    # Output dataset
    command += " --optGridOutputSampleName=user."+user+"."
    command += date
    command += "."
    command += dataset

    # Logfiles
    command += " > logfile_20_7_2015data_"
    command += dataset
    command += " 2> errors_20_7_2015data_"
    command += dataset

    command += " &"

    print command
    os.system(command)

if JETM9_Pythia_MC16a:
  for JZ in list:
    # Setting the correct sample
    if JZ == "JZ0":
      sample = "mc16_13TeV:mc16_13TeV.361020.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ0W.deriv.DAOD_JETM9.e3569_s3126_r9364_r9315_p3260"
    if JZ == "JZ1":
      sample = "mc16_13TeV:mc16_13TeV.361021.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ1W.deriv.DAOD_JETM9.e3569_s3126_r9364_r9315_p3260"
    if JZ == "JZ2":
      sample = "mc16_13TeV:mc16_13TeV.361022.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ2W.deriv.DAOD_JETM9.e3668_s3126_r9364_r9315_p3260"
    if JZ == "JZ3":
      sample = "mc16_13TeV:mc16_13TeV.361023.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ3W.deriv.DAOD_JETM9.e3668_s3126_r9364_r9315_p3260"
    if JZ == "JZ4":
      sample = "mc16_13TeV:mc16_13TeV.361024.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ4W.deriv.DAOD_JETM9.e3668_s3126_r9364_r9315_p3260"
    if JZ == "JZ5":
      sample = "mc16_13TeV:mc16_13TeV.361025.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ5W.deriv.DAOD_JETM9.e3668_s3126_r9364_r9315_p3260"
    if JZ == "JZ6":
      sample = "mc16_13TeV:mc16_13TeV.361026.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ6W.deriv.DAOD_JETM9.e3569_s3126_r9364_r9315_p3260"
    if JZ == "JZ7":
      sample = "mc16_13TeV:mc16_13TeV.361027.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ7W.deriv.DAOD_JETM9.e3668_s3126_r9364_r9315_p3260"
    if JZ == "JZ8":
      sample = "mc16_13TeV:mc16_13TeV.361028.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ8W.deriv.DAOD_JETM9.e3569_s3126_r9364_r9315_p3260"
    if JZ == "JZ9":
      sample = "mc16_13TeV:mc16_13TeV.361029.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ9W.deriv.DAOD_JETM9.e3569_s3126_r9364_r9315_p3260"
    if JZ == "JZ10":
      sample = "mc16_13TeV:mc16_13TeV.361030.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ10W.deriv.DAOD_JETM9.e3569_s3126_r9364_r9315_p3260"
    if JZ == "JZ11":
      sample = "mc16_13TeV:mc16_13TeV.361031.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ11W.deriv.DAOD_JETM9.e3569_s3126_r9364_r9315_p3260"
    if JZ == "JZ12":
      sample = "mc16_13TeV:mc16_13TeV.361032.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ12W.deriv.DAOD_JETM9.e3668_s3126_s3136_r9364_r9315_p3260"

    command = "xAH_run.py "

    # Input dataset
    command += "--files "
    command += sample

    command += " --submitDir="
    command += "Outputs_Pythia_MC16_"
    command += JZ

    # Config
    command += " --config "+LocalPath+"/source/xAODAnaHelpers/data/xah_run_rel21_MC.json"

    # Prun options
    command += " --inputRucio prun --optGridMergeOutput=1 --optGridNFilesPerJob=1.0 --optRemoveSubmitDir 1"

    # Output dataset
    command += " --optGridOutputSampleName=user."+user+"."
    command += date
    command += "."
    command += JZ

    # Logfiles
    command += " > logfile_MC16_Pythia_"
    command += JZ
    command += " 2> errors_MC16_Pythia_"
    command += JZ

    command += " &"

    print command
    os.system(command)

if JETM9_2016Data_rel21:
  for Dataset in Data2016_rel21:

    # Setting the correct sample
    container = Dataset[26:34] # actually run

    command = "xAH_run.py "

    # Input dataset
    command += "--files "
    command += Dataset

    command += " --submitDir="
    command += "Outputs_"
    command += container

    # Config
    command += " --config "+LocalPath+"/source/xAODAnaHelpers/data/xah_run_rel21_DATA_2016.json"
    
    # Prun options
    command += " --inputRucio prun --optGridMergeOutput=1 --optGridNFilesPerJob=1.0 --optRemoveSubmitDir 1"

    # Output dataset
    command += " --optGridOutputSampleName=user."+user+"."
    command += date
    command += "."
    command += container

    # Logfiles
    command += " > logfile_2016data_rel21_"
    command += container
    command += " 2> errors_2016data_rel21_"
    command += container

    command += " &"

    print command
    os.system(command)

if JETM9_2017Data:
  for Dataset in Data2017:

    # Setting the correct sample
    container = Dataset[26:34] # actually run

    command = "xAH_run.py "

    # Input dataset
    command += "--files "
    command += Dataset

    command += " --submitDir="
    command += "Outputs_"
    command += container

    # Config
    command += " --config "+LocalPath+"/source/xAODAnaHelpers/data/xah_run_rel21_DATA_2017.json"
    
    # Prun options
    command += " --inputRucio prun --optGridMergeOutput=1 --optGridNFilesPerJob=1.0 --optRemoveSubmitDir 1"

    # Output dataset
    command += " --optGridOutputSampleName=user."+user+"."
    command += date
    command += "."
    command += container

    # Logfiles
    command += " > logfile_2017data_"
    command += container
    command += " 2> errors_2017data_"
    command += container

    command += " &"

    print command
    os.system(command)

if JETM9_2017Data_Containers:
  for Dataset in Data2017_Containers:

    # Setting the correct sample
    container = Dataset[26:33] # actually run

    command = "xAH_run.py "

    # Input dataset
    command += "--files "
    command += Dataset

    command += " --submitDir="
    command += "Outputs_"
    command += container

    # Config
    command += " --config "+LocalPath+"/source/xAODAnaHelpers/data/xah_run_rel21_DATA_2017.json"
    
    # Prun options
    command += " --inputRucio prun --optGridMergeOutput=1 --optGridNFilesPerJob=1.0 --optRemoveSubmitDir 1"

    # Output dataset
    command += " --optGridOutputSampleName=user."+user+"."
    command += date
    command += "."
    command += container

    # Logfiles
    command += " > logfile_2017data_"
    command += container
    command += " 2> errors_2017data_"
    command += container

    command += " &"

    print command
    os.system(command)

