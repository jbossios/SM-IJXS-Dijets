Workflow to contribute to this repository

- Clone repository:

git clone ssh://git@gitlab.cern.ch:7999/jbossios/SM-IJXS-Dijets.git

- Create new local branch:

git checkout -b BRANCH_NAME

- Make changes

- Add and commit to stash:
 
git add ...

git commit ..

- Make sure you are still up-to-date:

git pull --rebase origin master 

- Push to remote branch:

git push origin BRANCH_NAME

- Make a Merge Request:

https://gitlab.cern.ch/jbossios/SM-IJXS-Dijets/merge_requests

Recommended Git tutorial:

https://twiki.cern.ch/twiki/bin/view/AtlasComputing/GitTutorial