#!/usr/bin/python

# This python merge into one sample all the JZ samples with the correct weights

import os, sys

param=sys.argv

list = [
    "1",
    "2",
    "3",
    "4",
    "5",
    "6",
    "7",
    "8",
    "9",
]

fileName = "tree"
#fileName = "cutflow"
#fileName = "metadata"

##############################################################################
## DO NOT MODIFY
##############################################################################

print "Merging all JZX outputs"

command = "hadd JZAll_"+fileName+".root "

for i in list:

    command += " JZ" + i + "_"+fileName+"/JZ" + i + "_"+fileName+".root"

print command
os.system(command)


