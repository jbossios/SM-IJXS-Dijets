#!/usr/bin/python

import os, sys

param=sys.argv

list = [
    "JZ1",
    "JZ2",
    "JZ3",
    "JZ4",
    "JZ5",
    "JZ6",
    "JZ7",
    "JZ8",
    "JZ9",
]

user = "USER"

##################################################################################
## DO NOT MODIFY
##################################################################################

print "Moving all JZX outputs"

for i in list:

    precommand1 = "mkdir " + i + "_tree"
    command1 = "mv user."+user+".*" + i + "*tree*/* " + i + "_tree/"
    precommand2 = "mkdir " + i + "_cutflow"
    command2 = "mv user."+user+".*" + i + "*cutflow*/* " + i + "_cutflow/"
    precommand3 = "mkdir " + i + "_metadata"
    command3 = "mv user."+user+".*" + i + "*metadata*/* " + i + "_metadata/"

    print precommand1
    print command1
    print precommand2
    print command2
    print precommand3
    print command3
    os.system(precommand1)
    os.system(command1)
    os.system(precommand2)
    os.system(command2)
    os.system(precommand3)
    os.system(command3)


