#!/usr/bin/python

# This python merge into one sample all the JZ samples with the correct weights

import os, sys

param=sys.argv

list = [
    "1",
    "2",
    "3",
    "4",
    "5",
    "6",
    "7",
    "8",
    "9",
]

##################################################################################
## DO NOT MODIFY
##################################################################################

print "Merging all JZX outputs"

command = ""

for i in list:

    command += "hadd JZ" + i + "_tree/JZ" + i + "_tree.root JZ" + i + "_tree/* && "
    command += "hadd JZ" + i + "_cutflow/JZ" + i + "_cutflow.root JZ" + i + "_cutflow/* && "
    command += "hadd JZ" + i + "_metadata/JZ" + i + "_metadata.root JZ" + i + "_metadata/* && " 
    
command = command[:-3]
command += " &"
print command
os.system(command)


