#include <iostream>
#include <fstream>
#include <cstdlib>
#include <cmath>
#include "TChain.h"
#include "TString.h"
#include "TEnv.h"
#include "TFile.h"
#include <map>
#include <vector>

using namespace std;
typedef vector<TString> StrV;
typedef TString Str;
typedef unsigned int uint;
typedef vector<double> VecD;
typedef vector<int> VecI;

StrV periods;
map<Str,vector<int> > period_runs;


void error(Str msg) { printf("\nERROR:\n  %s\n\n",msg.Data()); abort(); };
void add(StrV &vec, Str a) { vec.push_back(a); };
void add(StrV &vec, Str a, Str b) { add(vec,a); add(vec,b); };
void add(StrV &vec, Str a, Str b, Str c) { add(vec,a,b); add(vec,c); };

void add(VecI &vec, int a) { vec.push_back(a); };
void add(VecD &vec, double a) { vec.push_back(a); };
void add(VecD &vec, double a, double b) { add(vec,a); add(vec,b); };
void add(VecD &vec, double a, double b, double c) { add(vec,a,b); add(vec,c); };
void add(VecD &vec, VecD inVec) { for (uint i=0;i<inVec.size();++i) add(vec,inVec[i]); }
void add(VecI &vec, VecI inVec) { for (uint i=0;i<inVec.size();++i) add(vec,inVec[i]); }


StrV Vectorize(Str str) {
  StrV result;
  TObjArray *strings = str.Tokenize(" ");
  if (strings->GetEntries()==0) return result;
  TIter istr(strings);
  while (TObjString* os=(TObjString*)istr())
    add(result,os->GetString());
  
  return result;
}

VecD VectorizeD(Str str) {
  VecD result;
  StrV vecS = Vectorize(str);
  for (uint i=0;i<vecS.size();++i) 
    result.push_back(atof(vecS[i]));
  return result;
}

int main(int argc, char **argv) {
  if (argc<2) error("Need to give periods/run as argument, e.g.: G_H_I");

  StrV trigs;
  // Edit // Check list of triggers
  trigs.push_back("HLT_j15");
  trigs.push_back("HLT_j25");
  trigs.push_back("HLT_j35");
  trigs.push_back("HLT_j45");
  trigs.push_back("HLT_j55");
  trigs.push_back("HLT_j60");
  trigs.push_back("HLT_j85");
  trigs.push_back("HLT_j110");
  trigs.push_back("HLT_j150");
  trigs.push_back("HLT_j175");
  trigs.push_back("HLT_j200");
  trigs.push_back("HLT_j260");
  trigs.push_back("HLT_j380");
  int N=trigs.size();

  // Edit // Check number of triggers
  double trigLumis[13][13]={0};

  Str runPer=argv[1];
  int run=atol(runPer);

  Str config="RunAndLumiInfo.config";
  TEnv *settings = new TEnv();
  settings->ReadFile(config,kEnvGlobal);
  
  periods = Vectorize(settings->GetValue("Periods",""));
  if (periods.size()==0) error("No Periods specified in "+config);

  for (int pi=0;pi<periods.size();++pi) {
    Str per=periods[pi];
    StrV runStrV = Vectorize(settings->GetValue("Period."+periods[pi],""));
    for (int i=0;i<runStrV.size();++i) 
      period_runs[per].push_back(atol(runStrV[i].Data()));
    printf("Period %s: %d runs\n",periods[pi].Data(),int(period_runs[per].size()));
  }

  VecI runs;
  if (run<=0) {
    StrV pers=Vectorize(Str(runPer).ReplaceAll("_"," "));
    for (int pi=0;pi<pers.size();++pi) 
      add(runs,period_runs[pers[pi]]);
  } else runs.push_back(run);

  if (runs.size()==0) error("Can't make sense of arg for run period: "+runPer);

  for (int ii=0; ii<N; ++ii) {
    for (int jj=ii; jj<N; ++jj) {
      // Edit // Check path and name of lumicalc files
      Str fn1="LumiCalcOutputs/ilumicalc_histograms_"+trigs[ii]+"_276262-284484_OflLumi-13TeV-005.root";
      Str fn2="LumiCalcOutputs/ilumicalc_histograms_"+trigs[jj]+"_276262-284484_OflLumi-13TeV-005.root";
      TFile *f1 = TFile::Open(fn1); if (f1==NULL) error("Can't open file "+fn1);
      TFile *f2 = TFile::Open(fn2); if (f2==NULL) error("Can't open file "+fn2);
      Str treeName="LumiMetaData";
      TTree *t1 = (TTree*)f1->Get(treeName); if (t1==NULL) error("Can't access "+treeName+" in file "+fn1);
      TTree *t2 = (TTree*)f2->Get(treeName); if (t2==NULL) error("Can't access "+treeName+" in file "+fn2);
      
      int N1=t1->GetEntries();
      int N2=t2->GetEntries();
      if (N1!=N2) error("Not the same number of lumiblocks?");
      UInt_t Run1, Run2,LBStart1,LBStart2;
      Float_t L1PS1, L2PS1, L3PS1;
      Float_t L1PS2, L2PS2, L3PS2;
      Float_t time1, time2, inst1, inst2;
      Float_t int1, int2;
      Float_t larfrac1, larfrac2;

      t1->SetBranchAddress("RunNbr", &Run1);
      t1->SetBranchAddress("LBStart", &LBStart1);
      t1->SetBranchAddress("Inst_m_Lumi", &inst1);
      t1->SetBranchAddress("L1Presc", &L1PS1);
      t1->SetBranchAddress("L2Presc", &L2PS1);
      t1->SetBranchAddress("L3Presc", &L3PS1);
      t1->SetBranchAddress("LiveTime", &time1);
      t1->SetBranchAddress("IntLumi",&int1);
      t1->SetBranchAddress("LArfrac",&larfrac1);

      t2->SetBranchAddress("RunNbr", &Run2);
      t2->SetBranchAddress("LBStart", &LBStart2);
      t2->SetBranchAddress("Inst_m_Lumi", &inst2);
      t2->SetBranchAddress("L1Presc", &L1PS2);
      t2->SetBranchAddress("L2Presc", &L2PS2);
      t2->SetBranchAddress("L3Presc", &L3PS2);
      t2->SetBranchAddress("LiveTime", &time2);
      t2->SetBranchAddress("IntLumi",&int2);
      t2->SetBranchAddress("LArfrac",&larfrac2);

      double lumiSum1=0, lumiSum2=0, lumiSumOR=0;
      double lumiSum11=0, lumiSum22=0;
      for (int i=0;i<N1;++i) {
        t1->GetEntry(i); t2->GetEntry(i);
        if (Run1!=Run2) error("Run mismatch");
        if (LBStart1!=LBStart2) error("LBN mismatch");
        bool keep=0;
        for (uint ri=0;ri<runs.size();++ri) 
          if (UInt_t(runs[ri])==Run1) keep=true;
        if (!keep) continue;
        double PS1=L1PS1*L2PS1*L3PS1;
        double PS2=L1PS2*L2PS2*L3PS2;

        //if (PS1<0 && PS2<0) continue;
        bool badPS1 = (L1PS1 < 0 || L2PS1 < 0 || L3PS1 < 0);
        bool badPS2 = (L1PS2 < 0 || L2PS2 < 0 || L3PS2 < 0);
        if (badPS1 && badPS2) continue;

        if (time1!=time2) error("Live time doesn't match");
        if (inst1!=inst2) error("Inst lumi doesn't match");
        if (int1 && fabs(1.0-larfrac1*inst1*time1/PS1/int1)>1e-4 ) {
          printf("Problem Int: %.4f\n",int1);
          printf("Not eqa Int: %.4f\n",larfrac1*inst1*time1/PS1);
          abort();
        }
        
        //if (PS1>0)
        //  lumiSum1 += larfrac1*inst1*time1/PS1;
        //if (PS2>0)
        //  lumiSum2 += larfrac2*inst2*time2/PS2;
        //if (PS1>0 && PS2>0)
        //  lumiSumOR += larfrac1*inst1*time1*(PS1+PS2-1.0)/PS1/PS2;

        if (!badPS1)
          lumiSum1 += larfrac1*inst1*time1/PS1;
        if (!badPS2)
          lumiSum2 += larfrac2*inst2*time2/PS2;
        if (!badPS1 && !badPS2)
          lumiSumOR += larfrac1*inst1*time1*(PS1+PS2-1.0)/PS1/PS2;
        else if (!badPS1)
          lumiSumOR += larfrac1*inst1*time1/PS1;
        else if (!badPS2)
          lumiSumOR += larfrac2*inst2*time2/PS2;


      }
      if (ii==jj) trigLumis[ii][jj]=lumiSum1;
      else trigLumis[ii][jj]=lumiSumOR;
      //std::cout << ii << ":" << jj << " " << trigLumis[ii][jj] << std::endl;
    }
  }

  for (int i=1; i<N; ++i) {
    for (int j=0; j<i; ++j) {
      trigLumis[i][j]=trigLumis[j][i];
    }
  }
  
  std::string Period;
  for(int pi=0;pi<periods.size();++pi){
    Period += periods[pi];
    if(pi!=periods.size()-1) Period += "_";
  }
  // Edit // Check name of outputfolder
  std::string OutputFolder = "Tables/";
  OutputFolder += Period;
  OutputFolder += "/lumi.txt";
  std::ofstream fout(OutputFolder);
  fout << "Periods: " << Period << std::endl;
  for (int i=0; i<N; ++i) {
    for (int j=0; j<N; ++j) {
      fout << trigLumis[i][j]/1000000 << "\t";
      //fout << trigs[i] << "\t" << trigLumis[i][i]/1000000;
    }
    fout << std::endl;
  }
  fout.close();

  return 0;
}
