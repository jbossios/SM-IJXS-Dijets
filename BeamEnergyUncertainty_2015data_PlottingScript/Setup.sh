#! /bin/bash

setupATLAS
mkdir source build
cd source
lsetup "asetup 21.2.10,AnalysisBase"
cd ../build
cmake ../source
make
cd ../
source build/${CMTCONFIG}/setup.sh
