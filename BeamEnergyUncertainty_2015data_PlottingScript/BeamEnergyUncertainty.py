import ROOT
from array import *
ROOT.gROOT.SetBatch(True)  # so does not pop up plots!
ROOT.gROOT.LoadMacro("/afs/cern.ch/user/j/jbossios/work/public/xAOD/Results/AtlasStyle/AtlasStyle.C")
ROOT.SetAtlasStyle()

IJXS_ptmax_up_0=[
  0.0016151,
  0.00167755,
  0.0017368,
  0.00179546,
  0.00185458,
  0.00191532,
  0.0019737,
  0.00203034,
  0.002089,
  0.00214368,
  0.00220324,
  0.0022641,
  0.00231982,
  0.00238208,
  0.00244438,
  0.00248001,
  0.00254867,
  0.00261779,
  0.00269046,
  0.00276878,
  0.0028616,
  0.00294255,
  0.00302688,
  0.00311964,
  0.00321915,
  0.00332242,
  0.00345073,
  0.00358827,
  0.00380688,
  0.00418013,
  0.00486987,
  0.00628655,
  0.00855626,
]

IJXS_ptmax_up_1=[
  0.00163231,
  0.00169997,
  0.00176143,
  0.00182187,
  0.00188514,
  0.00194371,
  0.00200123,
  0.00206367,
  0.00212133,
  0.00218622,
  0.00224666,
  0.00230638,
  0.002368,
  0.00243359,
  0.00250256,
  0.00256815,
  0.00264415,
  0.00272453,
  0.00280652,
  0.00289168,
  0.00296754,
  0.00306724,
  0.00317216,
  0.00328704,
  0.0034132,
  0.00354446,
  0.00369934,
  0.00387421,
  0.00415617,
  0.0046465,
  0.00554686,
  0.00769245,
]

IJXS_ptmax_up_2 = [
  0.00167331,
  0.00174214,
  0.00180804,
  0.00186509,
  0.00194057,
  0.00200877,
  0.00206998,
  0.00213394,
  0.00219716,
  0.00226573,
  0.00233687,
  0.00240439,
  0.00247998,
  0.00255973,
  0.00264991,
  0.00274581,
  0.00284634,
  0.00295814,
  0.00308022,
  0.00321213,
  0.0033526,
  0.00350506,
  0.00366983,
  0.00385085,
  0.00405459,
  0.00425778,
  0.00449461,
  0.00479404,
  0.00527025,
  0.00609715,
  0.00778075,
]

IJXS_ptmax_up_3=[
  0.00174267,
  0.0018144,
  0.00188073,
  0.00194615,
  0.00201136,
  0.00209989,
  0.00217121,
  0.0022564,
  0.00234599,
  0.00244946,
  0.0025546,
  0.00266287,
  0.00278748,
  0.00292261,
  0.00307302,
  0.0032389,
  0.00342547,
  0.00363123,
  0.00385568,
  0.00410021,
  0.00437397,
  0.00466314,
  0.00498852,
  0.00535107,
  0.00575993,
  0.00619867,
  0.00669234,
  0.00731839,
  0.00832368,
  0.0101354,
  0.0136057,
]

IJXS_ptmax_up_4=[
  0.00184911,
  0.00192251,
  0.00202193,
  0.0021141,
  0.00221617,
  0.00232933,
  0.0024662,
  0.00260869,
  0.00277365,
  0.00294422,
  0.00314759,
  0.00336277,
  0.00362126,
  0.00389344,
  0.00420224,
  0.00455597,
  0.00495057,
  0.0053875,
  0.00588292,
  0.00643824,
  0.00705263,
  0.00774944,
  0.00853592,
  0.0094456,
  0.0104766,
  0.0117525,
  0.0133082,
  0.0154745,
]

IJXS_ptmax_up_5=[
  0.00203013,
  0.00215517,
  0.00231707,
  0.00248924,
  0.00269868,
  0.0029457,
  0.00321158,
  0.00351474,
  0.00385893,
  0.00424967,
  0.00469875,
  0.00518762,
  0.00577945,
  0.00640442,
  0.00717679,
  0.00805061,
  0.00910824,
  0.0103006,
  0.0117987,
  0.0137205,
  0.0161493,
  0.0210101,
]

Binning_0 = [
  100,
  116,
  134,
  152,
  172,
  194,
  216,
  240,
  264,
  290,
  318,
  346,
  376,
  408,
  442,
  478,
  516,
  556,
  598,
  642,
  688,
  736,
  786,
  838,
  894,
  952,
  1012,
  1076,
  1162,
  1310,
  1530,
  1992,
  2500,
  3937,
]

Binning_1 = [
  100,
  116,
  134,
  152,
  172,
  194,
  216,
  240,
  264,
  290,
  318,
  346,
  376,
  408,
  442,
  478,
  516,
  556,
  598,
  642,
  688,
  736,
  786,
  838,
  894,
  952,
  1012,
  1076,
  1162,
  1310,
  1530,
  1992,
  3937,
]

Binning_2 = [
  100,
  116,
  134,
  152,
  172,
  194,
  216, 
  240,
  264,
  290,
  318,
  346,
  376,
  408, 
  442,
  478,
  516,
  556,
  598,
  642,
  688,
  736,
  786,
  838,
  894, 
  952,
  1012,
  1076,
  1162,
  1310,
  1530,
  2500,
]

Binning_3 = [
  100,
  116,
  134,
  152,
  172,
  194,
  216,
  240,
  264,
  290,
  318,
  346,
  376,
  408,
  442,
  478, 
  516,
  556,
  598,
  642,
  688,
  736,
  786,
  838,
  894,
  952,
  1012,
  1076,
  1162,
  1310,
  1530,
  1992,
]

Binning_4 = [
  100,
  116,
  134,
  152,
  172,
  194,
  216,
  240,
  264,
  290,
  318,
  346,
  376,
  408,
  442,
  478,
  516,
  556,
  598,
  642,
  688,
  736,
  786,
  838,
  894,
  952,
  1012, 
  1076,
  1162,
]

Binning_5 = [
  100,
  116,
  134,
  152,
  172,
  194,
  216,
  240,
  264,
  290,
  318,
  346,
  376,
  408,
  442,
  478,
  516,
  556,
  598,
  642,
  688,
  736,
  894,
]

Array_0 = array('d',Binning_0)
yBin0 = ROOT.TH1D("yBin0","",len(Array_0)-1,Array_0)
for i in range(1,len(IJXS_ptmax_up_0)+1):
  yBin0.SetBinContent(i,IJXS_ptmax_up_0[i-1]*100)
  yBin0.SetBinError(i,0)
Array_1 = array('d',Binning_1)
yBin1 = ROOT.TH1D("yBin1","",len(Array_1)-1,Array_1)
for i in range(1,len(IJXS_ptmax_up_1)+1):
  yBin1.SetBinContent(i,IJXS_ptmax_up_1[i-1]*100)
  yBin1.SetBinError(i,0)
Array_2 = array('d',Binning_2)
yBin2 = ROOT.TH1D("yBin2","",len(Array_2)-1,Array_2)
for i in range(1,len(IJXS_ptmax_up_2)+1):
  yBin2.SetBinContent(i,IJXS_ptmax_up_2[i-1]*100)
  yBin2.SetBinError(i,0)
Array_3 = array('d',Binning_3)
yBin3 = ROOT.TH1D("yBin3","",len(Array_3)-1,Array_3)
for i in range(1,len(IJXS_ptmax_up_3)+1):
  yBin3.SetBinContent(i,IJXS_ptmax_up_3[i-1]*100)
  yBin3.SetBinError(i,0)
Array_4 = array('d',Binning_4)
yBin4 = ROOT.TH1D("yBin4","",len(Array_4)-1,Array_4)
for i in range(1,len(IJXS_ptmax_up_4)+1):
  yBin4.SetBinContent(i,IJXS_ptmax_up_4[i-1]*100)
  yBin4.SetBinError(i,0)
Array_5 = array('d',Binning_5)
yBin5 = ROOT.TH1D("yBin5","",len(Array_5)-1,Array_5)
for i in range(1,len(IJXS_ptmax_up_5)+1):
  yBin5.SetBinContent(i,IJXS_ptmax_up_5[i-1]*100)
  yBin5.SetBinError(i,0)


Format = ROOT.Plotter1D()   # This is the constructor for format only
Format.SetComparisonType("Normal")   # Will show second panel with ratio
#Format.ShowCME("13")    # Show 13 TeV
Format.SetLogx()
Format.SetATLASlabel("Internal") # Other options: ATLAS, Internal, etc
Format.SetAxisTitles("p_{T} [GeV]","Beam energy uncertainty [%]") # x-axis, y-axis
Format.SetLegendPosition("TopLeft")  # Other options: Top, TopLeft, Bottom
Format.SetMoreLogLabelsX()

pTdistribution = ROOT.Plotter1D("BeamEnergyUncertainty") # Name of the output file
pTdistribution.ApplyFormat(Format)
pTdistribution.SetLegend("0","|y|<0.5")      # Second argument is legend
pTdistribution.SetLegend("1","0.5#leq|y|<1.0")      # Second argument is legend
pTdistribution.SetLegend("2","1.0#leq|y|<1.5")      # Second argument is legend
pTdistribution.SetLegend("3","1.5#leq|y|<2.0")      # Second argument is legend
pTdistribution.SetLegend("4","2.0#leq|y|<2.5")      # Second argument is legend
pTdistribution.SetLegend("5","2.5#leq|y|<3.0")      # Second argument is legend
pTdistribution.AddHist("0",yBin0,"HIST][") # ID,TH1D
pTdistribution.AddHist("1",yBin1,"HIST][") # ID,TH1D
pTdistribution.AddHist("2",yBin2,"HIST][") # ID,TH1D
pTdistribution.AddHist("3",yBin3,"HIST][") # ID,TH1D
pTdistribution.AddHist("4",yBin4,"HIST][") # ID,TH1D
pTdistribution.AddHist("5",yBin5,"HIST][") # ID,TH1D
#pTdistribution.AddText(0.2,0.75,"|y|<3.0") # x, y, text
pTdistribution.Write()  # Saves plot into "pT.pdf"

