#include <string>
#include <string>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <algorithm>
#include <string>
#include <vector>
#include <TROOT.h>
#include <TFile.h>

#include "TH1.h"
#include "TH2F.h"
#include "TProfile.h"
#include "TMath.h"
#include "TDirectory.h"
#include "TGraph.h"
#include "TGraphErrors.h"
#include "TGraphAsymmErrors.h"
#include "TCanvas.h"
#include "TBranch.h"
#include "TTree.h"
#include "TStyle.h"
#include "TF1.h"
#include "TString.h"

using namespace std;

//--------------------------------------------------------------------
// Edit
//--------------------------------------------------------------------

TString Generator = "Pythia";
//TString Generator = "Powheg";
//TString Generator = "Sherpa";

TString PATH = ""; // Path to JZX_tree and JZX_metadata

const int nSlices = 12;
string JZXsamples[nSlices] = {"1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12"};

bool useTDirectory = false; // In latest xAH outpus, the trees are stored inside a TDirectory

// Also take a look to AMI values (thoese are only valid for MC15 samples)

//--------------------------------------------------------------------

TString directoryName = "treeAlgo";

void reweight_function(string sample, TString Treename){

    char inputName[200];

    TString strFile = PATH; strFile += "JZ%s_tree/JZ%s_tree.root";
    sprintf(inputName,strFile.Data(),sample.c_str(),sample.c_str());
    
    // Opening input file with the total number of events
    TString TotalNevents1file = PATH;
    TotalNevents1file += "JZ";
    TotalNevents1file += sample.c_str();
    TotalNevents1file += "_metadata/JZ";
    TotalNevents1file += sample.c_str();
    TotalNevents1file += "_metadata.root";
    TFile* file1events = new TFile(TotalNevents1file,"read");
    // Getting the number of events for this JZX sample
    TH1D* cutflow; // Histogram with the total number of events
    TString cutflowhistoname = "MetaData_EventCount";
    std::cout << "Getting TH1D: " << cutflowhistoname << std::endl;
    cutflow = (TH1D*)file1events->Get(cutflowhistoname);
    double totalevents = cutflow->GetBinContent(1);
    if(Generator.Contains("Powheg")){
      totalevents = cutflow->GetBinContent(3);
      std::cout << "Sum of weights = " << totalevents << std::endl;
    } else{
      std::cout << "Number of events = " << totalevents << std::endl;
    }

    double evn = totalevents;
    if(Generator == "Powheg"){
      evn = totalevents;
      if(sample == "5") evn -= 352220000; // Removing mcEventWeigt of JZ5 which is to high from sumofWeight
    }
    std::cout << "Final number of events to reweight = " << evn << std::endl;

    cout<<"inputName  :  "<<inputName<<endl;

    TFile f1(inputName,"update");
    
    f1.cd();

    double xs;
    double eff;

    TString input = inputName;

    // AMI values
    if(Generator == "Pythia"){// Multiplied by E+6 to be in fb-1
      if (sample == "1"){ xs = 7.8420E+13;  eff = 6.7198E-04; }
      else if (sample == "2"){ xs = 2.4334E+12;  eff = 3.3264E-04; }
      else if (sample == "3"){ xs = 2.6454E+10;  eff = 3.1953E-04; }
      else if (sample == "4"){ xs = 2.5463E+08;  eff = 5.3009E-04; }
      else if (sample == "5"){ xs = 4.5536E+06;  eff = 9.2325E-04; }
      else if (sample == "6"){ xs = 2.5752E+05;  eff = 9.4016E-04; }
      else if (sample == "7"){ xs = 1.6214E+04;  eff = 3.9282E-04; } 
      else if (sample == "8"){ xs = 6.2505E+02;  eff = 1.0162E-02; }
      else if (sample == "9"){ xs = 1.9639E+01;  eff = 1.2054E-02; }
      else if (sample == "10"){ xs = 1.1962;  eff = 5.8935E-03; }
      else if (sample == "11"){ xs = 4.2258E-02;  eff = 2.7015E-03; } 
      else if (sample == "12"){ xs = 1.0367E-03;  eff = 4.2502e-04; } 
    }else if(Generator == "Powheg"){
      if (input.Contains("JZ1")){ xs = 4.2163E+08;  eff = 3.4956E-03; } // Ok
      else if (input.Contains("JZ2")){ xs = 4.8024E+07;  eff = 4.1070E-04; } // Ok
      else if (input.Contains("JZ3")){ xs = 1.9084E+06;  eff = 1.4988E-04; } // Ok
      else if (input.Contains("JZ4")){ xs = 1.7362E+05;  eff = 1.8654E-05; } // Ok
      else if (input.Contains("JZ5")){ xs = 5.2594E+03;  eff = 1.1846E-05; } // Ok
      else if (input.Contains("JZ6")){ xs = 2.7612E+02;  eff = 8.3645E-06; } // Ok
      else if (input.Contains("JZ7")){ xs = 2.6410E+01;  eff = 6.6710E-06; } // Ok 
      else if (input.Contains("JZ8")){ xs = 7.7298E-02;  eff = 8.3211E-05; } // Ok 
      else if (input.Contains("JZ9")){ xs = 2.7470E-03;  eff = 9.0501E-05; } // Ok
    } else if(Generator == "Sherpa"){
      if (sample == "1"){ xs = 20595000;  eff = 0.08573; } // Ok
      else if (sample == "2"){ xs = 107290;  eff = 0.1417; } // Ok
      else if (sample == "3"){ xs = 13075;  eff = 0.018538; } // Ok
      else if (sample == "4"){ xs = 96.076;  eff = 0.027758; } // Ok
      else if (sample == "5"){ xs = 2.725;  eff = 0.018391; } // Ok
      else if (sample == "6"){ xs = 0.20862;  eff = 0.0086905; } // Ok
      else if (sample == "7"){ xs = 0.043733;  eff = 0.0030859; } // Ok
      else if (sample == "8"){ xs = 0.00033372;  eff = 0.014505; } // Ok
      else if (sample == "9"){ xs = 0.000058952;  eff = 0.0030666; } // Ok 
      else if (sample == "10"){ xs = 0.0000055732;  eff = 9.4788E-04; } // Ok 
      else if (sample == "11"){ xs = 0.00000012593;  eff = 7.0498E-04; } // Ok 
      else if (sample == "12"){ xs = 0.0000000011029;  eff = 4.6255E-04; } // Ok 
    }


    cout<<"xs: "<<xs<<endl;
    cout<<"eff: "<<eff<<endl;

    float reweight = xs*eff/evn;

    float EvweightEM;   float new_weightEM;

    TTree *EM = NULL;
    if(useTDirectory){
      TDirectory* TDirF_tmp = f1.GetDirectory(directoryName.Data());
      TDirF_tmp->GetObject(Treename.Data(),EM);
    } else{
      EM = (TTree*)f1.Get(Treename.Data());
    }

    TBranch* newBranchEM = EM->Branch("weight",&new_weightEM, "weight/F");

    EM->SetBranchAddress("mcEventWeight",&EvweightEM);

    int entriesEM = EM->GetEntries();
    
    for (int i=0; i<entriesEM; i++) {
	EM->GetEntry(i);
	new_weightEM = EvweightEM*reweight;
	newBranchEM->Fill();
    }
    f1.cd(directoryName.Data());
    EM->Write("",TObject::kOverwrite);
}

void computeWeight() {
    const int NTrees = 1;
    TString Trees[NTrees] = {"nominal"};
    for(int i=0;i<NTrees;++i){
      cout << "Reweighting Tree: " << Trees[i] << std::endl;
      for(int j=0;j<nSlices;++j){
        reweight_function(JZXsamples[j],Trees[i]);
        cout << "JZ"+JZXsamples[j]+" reweighted " << endl;
      }	    
    }
}


