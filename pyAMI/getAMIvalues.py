import pyAMI.client
import pyAMI.atlas.api as AtlasAPI
import os, sys

client = pyAMI.client.Client('atlas')
AtlasAPI.init()

######################################################################################################
# EDIT
######################################################################################################

useEVNT = True

samples = [
  "SAMPLE_1" ,
  "SAMPLE_2" ,
]

###########################################################################################################
# DO NOT MODIFY
###########################################################################################################

# Loop over samples
for dataset in samples:
  run = dataset.split(".",2)
  info = str(run[1])
  prov = AtlasAPI.get_dataset_prov(client,dataset)
  arr = prov['node']
  for dict in arr:
    d = dict['logicalDatasetName']
    if run[1] in d and "minbias" not in d:
      if not useEVNT:
        if ".AOD." in d:
         dd = AtlasAPI.get_dataset_info(client,d)
         xsec = dd[0]['crossSection']
         eff = dd[0]['genFiltEff']
         break
      elif "evgen" in d:
        dd = AtlasAPI.get_dataset_info(client,d)
        xsec = dd[0]['crossSection']
        eff = dd[0]['genFiltEff']
        break
  info += " " + str(xsec) + " " + str(eff) 
  print info			  
