import os,sys
import numpy as np
import pandas as pd

Date = "25062017"

PDFs = [ # IJXS & DIJETS
  "CT14",
  "MMHT2014",
  "NNPDF30nlo",
  "HERAPDF20NLO",
  "ABM16",
]

Scales = [ # IJXS
  "ptmax",
  "ptjet",
]


##############################################
# DO NOT MODIFY
##############################################

# Open input file and get all lines
ifile = open("Inputs/"+Date+"/Summary.txt", "r")
Lines = ifile.readlines()

# Separate lines between IJXS and Dijets
IJXS_Lines   = []
DIJETS_Lines = []
# Loop over lines
for line in Lines:
  if "dijet" in line:
    DIJETS_Lines.append(line)
  elif "incljet" in line:
    IJXS_Lines.append(line)

######################
# Save what's needed
######################

# DIJETS
DIJETS_Global_pvalue = {}
DIJETS_Global_chi    = {}
DIJETS_Global_dof    = {}
DIJETS_Individual_pvalue = {}
DIJETS_Individual_chi    = {}
DIJETS_Individual_dof    = {}
for pdf in PDFs:
  DIJETS_Global_pvalue[pdf]     = []
  DIJETS_Global_chi[pdf]        = []
  DIJETS_Global_dof[pdf]        = []
  DIJETS_Individual_pvalue[pdf] = []
  DIJETS_Individual_chi[pdf]    = []
  DIJETS_Individual_dof[pdf]    = []
# IJXS
IJXS_Global_pvalue = {}
IJXS_Global_chi    = {}
IJXS_Global_dof    = {}
IJXS_Individual_pvalue = {}
IJXS_Individual_chi    = {}
IJXS_Individual_dof    = {}
for pdf in PDFs:
  for scale in ["ptjet","ptmax"]:
    IJXS_Global_pvalue[pdf+'_'+scale]     = []
    IJXS_Global_chi[pdf+'_'+scale]        = []
    IJXS_Global_dof[pdf+'_'+scale]        = []
    IJXS_Individual_pvalue[pdf+'_'+scale] = []
    IJXS_Individual_chi[pdf+'_'+scale]    = []
    IJXS_Individual_dof[pdf+'_'+scale]    = []

# Loop over DIJETS lines
for i in range(len(DIJETS_Lines)):
  pdf = (DIJETS_Lines[i].split()[0]).split("_")[1]            # GET PDF
  chi    = str(int(round(float(DIJETS_Lines[i].split()[2])))) # GET chi2
  dof    = str(int(float(DIJETS_Lines[i].split()[6])))        # GET dof
  # GET p-value
  pvalue = ""
  if round(float(DIJETS_Lines[i].split()[4])*100,1)>=10:
    pvalue = str(int(round(float(DIJETS_Lines[i].split()[4])*100)))
  else:
    pvalue = "%.1f" % round(float(DIJETS_Lines[i].split()[4])*100,1)
  # Fill
  if "yStar1yStar2yStar3yStar4yStar5yStar6" in DIJETS_Lines[i].split()[0]: # GLOBAL
    DIJETS_Global_pvalue[pdf].append(pvalue)
    DIJETS_Global_chi[pdf].append(chi)
    DIJETS_Global_dof[pdf].append(dof)
  else: # INVIDIVUAL YSTAR BINS
    DIJETS_Individual_pvalue[pdf].append(pvalue)
    DIJETS_Individual_chi[pdf].append(chi)
    DIJETS_Individual_dof[pdf].append(dof)

# Loop over IJXS lines
for i in range(len(IJXS_Lines)):
  pdf = (IJXS_Lines[i].split()[0]).split("_")[1]            # GET PDF
  chi    = str(int(round(float(IJXS_Lines[i].split()[2])))) # GET chi2
  dof    = str(int(float(IJXS_Lines[i].split()[6])))        # GET dof
  # GET p-value
  pvalue = ""
  if round(float(IJXS_Lines[i].split()[4])*100,1)>=10:
    pvalue = str(int(round(float(IJXS_Lines[i].split()[4])*100)))
  else:
    pvalue = "%.1f" % round(float(IJXS_Lines[i].split()[4])*100,1)
  # GET scale
  scale = []
  if "perjetscale" in IJXS_Lines[i]:
    scale = "ptjet"
  elif "pereventscale" in IJXS_Lines[i]:
    scale = "ptmax"
  # Fill
  if "eta1eta2eta3eta4eta5eta6" in IJXS_Lines[i].split()[0] and "SplitSyst" not in IJXS_Lines[i]: # GLOBAL
    IJXS_Global_pvalue[pdf+'_'+scale].append(pvalue)
    IJXS_Global_chi[pdf+'_'+scale].append(chi)   
    IJXS_Global_dof[pdf+'_'+scale].append(dof)
  elif "SplitSyst" not in IJXS_Lines[i]: # INVIDIVUAL Y BINS
    IJXS_Individual_pvalue[pdf+'_'+scale].append(pvalue)
    IJXS_Individual_chi[pdf+'_'+scale].append(chi)
    IJXS_Individual_dof[pdf+'_'+scale].append(dof)
ifile.close()

# WRITE TABLES
ofile = open("Outputs/"+Date+"/Chi2Tables.tex","w") 

# Definitions
begin = "\\begin{table}[htp!]\n"
end   = "\\end{table}\n"
centering  = "\\centering\n"
tabcolsep  = "\\tabcolsep=0.2cm\n"
topline    = "\\hline\\hline\n" 
bottomline = "\\hline\\hline\n"
endtabular = "\\end{tabular}\n"
line       = "\\hline\n"

# IJXS Table # 1
caption = "\\caption{Summary of observed $P_\\text{obs}$ values from the comparison of the inclusive jet cross-section and the NLO pQCD prediction corrected for non-perturbative and electroweak effects for various PDF sets, for the two scale choices and for each rapidity bin of the measurement.}\n"
label = "\\label{tab:chi2_IJXS}\n"
ofile.write(begin) 
ofile.write(centering)
ofile.write(tabcolsep)
ofile.write("\\begin{tabular}{c|rrrrr}\n")
ofile.write(topline)
ofile.write(" & \\multicolumn{5}{c}{$P_\\text{obs}$} \\\\\n")
ofile.write("Rapidity ranges & CT14 & MMHT 2014 & NNPDF 3.0 & HERAPDF 2.0 & ABMP16 \\\\\n")
ofile.write(line)
for scale in Scales:
  if scale == "ptmax":
    ofile.write("$p_{\\rm T}^{\\mathrm{max}}$ \\\\\n")
  if scale == "ptjet":
    ofile.write(line)
    ofile.write("$p_{\\rm T}^{\\mathrm{jet}}$ \\\\\n")
  ofile.write(line)
  string = ""
  for i in range(0,6):
    if i==0:
      bin = "$|y|<0.5$"
    if i==1:
      bin = "$0.5\\le|y|<1.0$"
    if i==2:
      bin = "$1.0\\le|y|<1.5$"
    if i==3:
      bin = "$1.5\\le|y|<2.0$"
    if i==4:
      bin = "$2.0\\le|y|<2.5$"
    if i==5:
      bin = "$2.5\\le|y|<3.0$"
    string += bin
    for pdf in PDFs:
      string += " & "+IJXS_Individual_pvalue[pdf+"_"+scale][i]+"\\%"
    string += " \\\\\n"
  ofile.write(string)
ofile.write(bottomline)
ofile.write(endtabular)
ofile.write(caption)
ofile.write(label)
ofile.write(end)
ofile.write("\n\n")


# IJXS Table # 2
caption = "\\caption{Summary of $\chi^2/\\text{dof}$ values obtained from a global fit using all \\pt{} and rapidity bins, comparing the inclusive jet cross-section and the NLO pQCD prediction corrected for non-perturbative and electroweak effects for several PDF sets and for the two scale choices. All the corresponding $p$-values are $\\ll10^{-3}$.}\n"
label = "\\label{tab:chi2_IJXS_global}\n"
ofile.write(begin) 
ofile.write(centering)
ofile.write("\\tabcolsep=0.15cm\n")
ofile.write("\\begin{tabular}{c|c|c|c|c|c}\n")
ofile.write(topline)
ofile.write("$\\chi^2/\\text{dof}$ & \\multirow{2}{*}{CT14} & \\multirow{2}{*}{MMHT 2014} & \\multirow{2}{*}{NNPDF 3.0} & \\multirow{2}{*}{HERAPDF 2.0} & \\multirow{2}{*}{ABMP16} \\\\\n")
ofile.write("all $|y|$ bins &  & & & & \\\\\n")
ofile.write(line)
ptmax = "$p_{\\rm T}^{\\mathrm{max}}$"
for pdf in PDFs:
 choice = pdf+"_ptmax"
 ptmax += " & "+IJXS_Global_chi[choice][0]+"/"+IJXS_Global_dof[choice][0]
ptmax += "\\\\\n"
ofile.write(ptmax)
ofile.write(line)
ptjet = "$p_{\\rm T}^{\\mathrm{jet}}$"
for pdf in PDFs:
 choice = pdf+"_ptjet"
 ptjet += " & "+IJXS_Global_chi[choice][0]+"/"+IJXS_Global_dof[choice][0]
ptjet += "\\\\\n"
ofile.write(ptjet)
ofile.write(bottomline)
ofile.write(endtabular)
ofile.write(caption)
ofile.write(label)
ofile.write(end)
ofile.write("\n\n")

# DIJETS
caption = "\\caption{Summary of observed $P_\\text{obs}$ values obtained from the comparison of the dijet cross-section and the NLO pQCD prediction corrected for non-perturbative and electroweak effects for various PDF sets and for each individual \\ystar{} range. The last row of the table corresponds to a global fit using all $\\twomass{j}{j}$ and \\ystar{} bins of the dijet measurement.}\n"
label = "\\label{tab:chi2_DIJETS}\n"
ofile.write(begin) 
ofile.write(centering)
ofile.write(tabcolsep)
ofile.write("\\begin{tabular}{c|rrrrr}\n")
ofile.write(topline)
ofile.write(" & \\multicolumn{5}{c}{$P_\\text{obs}$} \\\\\n")
ofile.write("\\ystar{} ranges & CT14 & MMHT 2014 & NNPDF 3.0 & HERAPDF 2.0 & ABMP16\\\\\n")
ofile.write(line)
string = ""
for i in range(0,6):
  if i==0:
    bin = "$y^{*}<0.5$"
  if i==1:
    bin = "$0.5\\le y^{*}<1.0$"
  if i==2:
    bin = "$1.0\\le y^{*}<1.5$"
  if i==3:
    bin = "$1.5\\le y^{*}<2.0$"
  if i==4:
    bin = "$2.0\\le y^{*}<2.5$"
  if i==5:
    bin = "$2.5\\le y^{*}<3.0$"
  string += bin
  for pdf in PDFs:
    string += " & "+DIJETS_Individual_pvalue[pdf][i]+"\\%"
  string += " \\\\\n"
ofile.write(string)
ofile.write(line)
string = "all \\ystar{} bins"
for pdf in PDFs:
  string += " & "+DIJETS_Global_pvalue[pdf][0]+"\\%"
string += "\\\\\n"
ofile.write(string)
ofile.write(bottomline)
ofile.write(endtabular)
ofile.write(caption)
ofile.write(label)
ofile.write(end)
ofile.close() 




